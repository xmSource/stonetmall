//
//  ZbQyLocateViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/13.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbQyLocateViewController : BaseViewController

@property (nonatomic, copy) void (^markLocationBlock)(CLLocationCoordinate2D pos);

@end
