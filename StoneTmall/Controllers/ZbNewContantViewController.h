//
//  ZbNewContantViewController.h
//  StoneTmall
//
//  Created by Apple on 15/9/16.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, Zb_Controller_Type) {
    Zb_Controller_MAINEDIT = 0,    //主联系修改
    Zb_Controller_ADDCONACT,       //新增联系人
    Zb_Controller_OTHEREDIT,       //其他联系人修改
} ;

@interface ZbNewContantViewController : BaseViewController

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSDictionary *dict;

- (void)setControllerType:(Zb_Controller_Type)contactType;


@end
