//
//  ZbCompanyTypeViewController.m
//  StoneTmall
//
//  Created by chyo on 15/10/9.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbCompanyTypeViewController.h"

#define HTTP_GET_COMPANYTYPE 1           // 获取所有经营范围
#define HTTP_ADD_COMPANYTYPE 2           // 添加经营类型
#define HTTP_DEL_COMPANYTYPE 3           // 删除经营类型
#define HTTP_DEL_COMPANYTYPE_LIMIT 4     // 关联数量限制

@interface ZbCompanyTypeViewController ()<UITableViewDelegate, UITableViewDataSource>

// 所有经营类型数组
@property (nonatomic, strong) NSMutableArray *array;

// tableview
@property (nonatomic, strong) IBOutlet UITableView *tv;

// 经营类型数量限制
@property (nonatomic, assign) NSInteger addTypesLimit;

@end

@implementation ZbCompanyTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)isSelectClick:(ZbIndexButton *)btn {
    // 准备添加类型，要判断数量限制
    if (!btn.selected) {
        ZbQyInfo *qyinfo = [ZbWebService sharedQy];
        if (qyinfo.company_types.count >= self.addTypesLimit) {
            self.hud.labelText = @"已超过最大关联数量上限";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
    }
    
    btn.selected = !btn.selected;
    if (btn.selected) {
        [self addCompany:btn.index];
    } else {
        [self delCompany:btn.index];
    }
}

#pragma mark - loaddata
// 获取所有经营类型
- (void)loadData {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyTypeList] delegate:self];
    conn.tag = HTTP_GET_COMPANYTYPE;
    [conn start];
}

// 获取经营类型限制
- (void)loadAddCompanyTypesLimit {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getAddCompanyTypeLimit] delegate:self];
    conn.tag = HTTP_DEL_COMPANYTYPE_LIMIT;
    [conn start];
}

- (void)addCompany:(NSInteger)index {
    NSDictionary *dict = [self.array objectAtIndex:index];
    NSString *name = dict[@"class_name"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService addCompanyType:name] delegate:self];
    conn.tag = HTTP_ADD_COMPANYTYPE;
    conn.userInfo = [NSNumber numberWithInteger:index];
    [conn start];
}

- (void)delCompany:(NSInteger)index {
    NSDictionary *dict = [self.array objectAtIndex:index];
    NSString *name = dict[@"class_name"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService delCompanyType:name] delegate:self];
    conn.tag = HTTP_DEL_COMPANYTYPE;
    conn.userInfo = [NSNumber numberWithInteger:index];
    [conn start];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.array.count;
}

/**
 *  设置cell的内容
 *
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"itemCell" forIndexPath:indexPath];
    
    UILabel *titleLab = (UILabel *)[cell viewWithTag:2];
    ZbIndexButton *btn = (ZbIndexButton *)[cell viewWithTag:3];
    
    btn.index = indexPath.row;
    [btn addTarget:self action:@selector(isSelectClick:) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableDictionary *dict = [self.array objectAtIndex:indexPath.row];
    NSString *curClassName = dict[@"class_name"];
    titleLab.text = curClassName;
    
    // 关联状态
    BOOL isSelect = NO;
    ZbQyInfo *qyinfo = [ZbWebService sharedQy];
    for (NSString *str in qyinfo.company_types) {
        isSelect = [str isEqualToString:curClassName];
        if (isSelect) {
            break;
        }
    }
    btn.selected = isSelect;
    
    return cell;
}

/**
 *  设置cell的高度
 *
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0f;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    // 按钮
    ZbIndexButton *btn = (ZbIndexButton *)[cell viewWithTag:3];
    // 等同按钮点击
    [self isSelectClick:btn];
    
    return;
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = dict[@"errno"];
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (connection.tag == HTTP_GET_COMPANYTYPE) {
        if (result.integerValue == 0) {
            NSArray *array = dict[@"data"];
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
            
            [self.tv reloadData];
            [self loadAddCompanyTypesLimit];
        }
    } else if (connection.tag == HTTP_ADD_COMPANYTYPE) {
        NSNumber *index = connection.userInfo;
        if (result.integerValue == 0) {
            self.hud.labelText = @"添加成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
            
            NSDictionary *dict = [self.array objectAtIndex:index.integerValue];
            NSString *name = dict[@"class_name"];
            ZbQyInfo *qyinfo = [ZbWebService sharedQy];
            [qyinfo.company_types addObject:name];
            [self.tv reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index.integerValue inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            return;
        }
        // 添加失败
        self.hud.labelText = @"添加失败";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        [self.tv reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index.integerValue inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (connection.tag == HTTP_DEL_COMPANYTYPE) {
        NSNumber *index = connection.userInfo;
        if (result.integerValue == 0) {
            self.hud.labelText = @"取消成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
            
            NSDictionary *dict = [self.array objectAtIndex:index.integerValue];
            NSString *name = dict[@"class_name"];
            ZbQyInfo *qyinfo = [ZbWebService sharedQy];
            [qyinfo.company_types removeObject:name];
            [self.tv reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index.integerValue inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            return;
        }
        // 取消失败
        self.hud.labelText = @"取消失败";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        [self.tv reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index.integerValue inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (connection.tag == HTTP_DEL_COMPANYTYPE_LIMIT) {
        // 经营类型限制
        [self.hud hide:NO];
        if (result.integerValue != 0) {
            return;
        }
        NSNumber *limit = dict[@"data"][@"limit"];
        self.addTypesLimit = limit.integerValue;
    }
}

@end
