//
//  ZbPubSupViewController.h
//  StoneTmall
//
//  Created by chyo on 15/9/1.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbPubSupViewController : BaseViewController

@property (assign, nonatomic) Zb_Home_Message_Type curMessageIndex;

@end
