//
//  ZbAboutViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/11/4.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbAboutViewController.h"
#import "ConsWebController.h"

@interface ZbAboutViewController () <UITableViewDelegate, UITableViewDataSource>

// tableview控件
@property (nonatomic, strong) IBOutlet UITableView *tv;
// 版本号label
@property (nonatomic, strong) IBOutlet UILabel *labVersion;

@end

@implementation ZbAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    self.labVersion.text = [NSString stringWithFormat:@"当前版本 v%@", [infoDict objectForKey:@"CFBundleShortVersionString"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource
// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    // 获取控件
    cell = [tableView dequeueReusableCellWithIdentifier:@"aboutCell" forIndexPath:indexPath];
    UILabel *labTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *labDetail = (UILabel *)[cell viewWithTag:2];
    
    if (indexPath.row == 0) {
        labTitle.text = @"关于石猫";
        labDetail.text = @"";
    } else if (indexPath.row == 1) {
        labTitle.text = @"关注我们";
        labDetail.text = @"微信公众号 stonetmall";
    } else if (indexPath.row == 2) {
        labTitle.text = @"帮助";
        labDetail.text = @"";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        // 关于石猫
        ConsWebController *webController = [[ConsWebController alloc] init];
        webController.mainUrl = ABOUT_US_URL;
        webController.showTitle = @"关于石猫";
        [self.navigationController pushViewController:webController animated:YES];
    } else if (indexPath.row == 1) {
        // 关注我们
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        [pasteboard setString:@"stonetmall"];
        [MBProgressHUD showTextHudInView:self.view withText:@"已复制公众号到剪切版，请到微信搜索公众号" lastTime:2.0 completeBlock:nil];
    } else if (indexPath.row == 2) {
        // 帮助
        ConsWebController *webController = [[ConsWebController alloc] init];
        webController.mainUrl = HOW_TO_USE_URL;
        webController.showTitle = @"帮助";
        [self.navigationController pushViewController:webController animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
