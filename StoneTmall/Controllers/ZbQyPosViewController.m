//
//  ZbQyPosViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/10.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQyPosViewController.h"
#import "BNCoreServices.h"
#import "LocationServiceManager.h"

@interface ZbQyPosViewController () <BMKMapViewDelegate, BNNaviRoutePlanDelegate, BNNaviUIManagerDelegate>

// 地图
@property (nonatomic, strong) IBOutlet BMKMapView *mapView;
// 导航按钮
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btnGuide;

@end

@implementation ZbQyPosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationItem.rightBarButtonItem = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.mapView viewWillAppear];
    self.mapView.delegate = self;
    
    NSString *companyName = self.curCompanyDict[@"company_name"];
    self.title = companyName;
    [self drawMapView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.mapView viewWillDisappear];
    self.mapView.delegate = nil;
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
}

#pragma mark - 点击事件
// 导航按钮点击
- (IBAction)btnGuideOnClick:(id)sender {
    BMKUserLocation *curPos = [[LocationServiceManager shareManager] getCurLocation];
    if (!curPos) {
        self.hud.labelText = @"请开启定位功能";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    [self startNavi];
    return;
}

#pragma mark - MapView
// 设置当前位置
- (void)drawMapView {
    
    // 清空mapView标注
    [_mapView removeAnnotations:_mapView.annotations];
    
    NSNumber *lng = self.curCompanyDict[@"company_longitude"];
    NSNumber *lat = self.curCompanyDict[@"company_latitude"];
    CLLocationCoordinate2D pos = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue);
    [_mapView setCenterCoordinate:pos];
    
    NSString *companyName = self.curCompanyDict[@"company_name"];
    // 公司位置图标
    BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc]init];
    annotation.coordinate = pos;
    annotation.title = companyName;
    [_mapView addAnnotation:annotation];
    [_mapView selectAnnotation:annotation animated:YES];
    
    //地图比例尺级别，在手机上当前可使用的级别为3-19级
    _mapView.zoomLevel = 16;
}

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation {
    BMKPinAnnotationView *myAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"iconAnnotation"];
    myAnnotationView.image = [UIImage imageNamed:@"public_icn_location"];
    return myAnnotationView;
}

#pragma mark - 导航
- (void)startNavi
{
    //节点数组
    NSMutableArray *nodesArray = [[NSMutableArray alloc]    initWithCapacity:2];
    
    NSNumber *lng = self.curCompanyDict[@"company_longitude"];
    NSNumber *lat = self.curCompanyDict[@"company_latitude"];
    //起点
    BMKUserLocation *curPos = [[LocationServiceManager shareManager] getCurLocation];
    BNRoutePlanNode *startNode = [[BNRoutePlanNode alloc] init];
    startNode.pos = [[BNPosition alloc] init];
    startNode.pos.x = curPos.location.coordinate.longitude;
    startNode.pos.y = curPos.location.coordinate.latitude;
    startNode.pos.eType = BNCoordinate_BaiduMapSDK;
    [nodesArray addObject:startNode];
    
    //终点
    BNRoutePlanNode *endNode = [[BNRoutePlanNode alloc] init];
    endNode.pos = [[BNPosition alloc] init];
    endNode.pos.x = lng.doubleValue;
    endNode.pos.y = lat.doubleValue;
    endNode.pos.eType = BNCoordinate_BaiduMapSDK;
    [nodesArray addObject:endNode];
    //发起路径规划
    [BNCoreServices_RoutePlan startNaviRoutePlan:BNRoutePlanMode_Recommend naviNodes:nodesArray time:nil delegete:self userInfo:nil];
}

#pragma mark - BNNaviRoutePlanDelegate
//算路成功回调
-(void)routePlanDidFinished:(NSDictionary *)userInfo
{
    NSLog(@"算路成功");
    //路径规划成功，开始导航
    [BNCoreServices_UI showNaviUI:BN_NaviTypeReal delegete:self isNeedLandscape:NO];
}

//算路失败回调
- (void)routePlanDidFailedWithError:(NSError *)error andUserInfo:(NSDictionary *)userInfo
{
    NSLog(@"算路失败");
    if ([error code] == BNRoutePlanError_LocationFailed) {
        NSLog(@"获取地理位置失败");
    }
    else if ([error code] == BNRoutePlanError_LocationServiceClosed)
    {
        NSLog(@"定位服务未开启");
    }
}

//算路取消回调
-(void)routePlanDidUserCanceled:(NSDictionary*)userInfo {
    NSLog(@"算路取消");
}

#pragma mark - BNNaviUIManagerDelegate

//退出导航回调
-(void)onExitNaviUI:(NSDictionary*)extraInfo
{
    NSLog(@"退出导航");
}

//退出导航声明页面回调
- (void)onExitDeclarationUI:(NSDictionary*)extraInfo
{
    NSLog(@"退出导航声明页面");
}

-(void)onExitDigitDogUI:(NSDictionary*)extraInfo
{
    NSLog(@"退出电子狗页面");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
