//
//  ZbQyPosViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/10.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbQyPosViewController : BaseViewController

// 当前企业字典
@property (nonatomic, weak) NSDictionary *curCompanyDict;

@end
