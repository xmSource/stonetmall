//
//  ZbDropTreeDownViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/8.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

// 选中block
typedef void (^selectClickBlock)(NSString *typeParent, NSString *typeSub, NSInteger parentIndex, NSInteger subIndex, BOOL isDismiss);

@interface ZbDropTreeDownViewController : BaseViewController

// 关闭界面
- (void)dismiss:(BOOL)animated;

// 显示选择器
+ (ZbDropTreeDownViewController *)showInViewController:(UIViewController *)controller startY:(CGFloat)startY typeArray:(NSArray *)typeArray subTypeDict:(NSDictionary *)subTypeDict lastMainIndex:(NSInteger)lastMainIndex lastSubIndex:(NSInteger)lastSubIndex selectBlock:(selectClickBlock)block;

@end
