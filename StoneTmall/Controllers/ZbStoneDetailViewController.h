//
//  ZbStoneDetailViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbStoneDetailViewController : BaseViewController

// 当前石材名
@property (nonatomic, strong) NSString *stoneName;

@end
