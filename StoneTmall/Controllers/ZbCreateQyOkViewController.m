//
//  ZbCreateQyOkViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/19.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbCreateQyOkViewController.h"
#import "ZbQiYeInfoEtViewController.h"

@interface ZbCreateQyOkViewController ()

// 开始使用按钮
@property (nonatomic, strong) IBOutlet UIButton *btnUseNow;
// 继续完善按钮
@property (nonatomic, strong) IBOutlet UIButton *btnEdit;

@end

@implementation ZbCreateQyOkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 按钮圆角
    self.btnUseNow.layer.cornerRadius = 3;
    self.btnEdit.layer.cornerRadius = 3;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 点击事件
// 开始使用石猫点击
- (IBAction)btnUseNowOnClick:(id)sender {
    UIViewController *present = self.navigationController.presentingViewController;
    if (present != nil) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
    return;
}

// 继续完善企业资料点击
- (IBAction)btnEditCompanyInfoOnClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbQiYeInfoEtViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbQiYeInfoEtViewController"];
    controller.curEditType = Zb_Qy_Edit_Type_OnCreate;
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
