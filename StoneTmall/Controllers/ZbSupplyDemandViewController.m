//
//  ZbSupplyDemandViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSupplyDemandViewController.h"
#import "ZbSupplyViewController.h"
#import "ZbDemandViewController.h"
#import "ZbPubSupViewController.h"
#import "ZbSearchSingleViewController.h"

@interface ZbSupplyDemandViewController () <UIScrollViewDelegate>

// scrollview容器
@property (strong, nonatomic) IBOutlet UIScrollView *svMain;

// 求购按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageDemand;
// 供货按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageSupply;
// 求购controller
@property (strong, nonatomic) ZbDemandViewController *leftPageController;
// 供货controller
@property (strong, nonatomic) ZbSupplyViewController *rightPageController;
// 选中线leading
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcSelectLineLeading;

// 当前选择页面
@property (assign, nonatomic) NSInteger curPageIndex;

@end

@implementation ZbSupplyDemandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.svMain.scrollsToTop = NO;
    self.curPageIndex = 0;
    [self pageChange];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 切页逻辑
- (void)pageChange
{
    // 按钮激活状态
    self.btnPageDemand.enabled = !(self.curPageIndex == 0);
    self.btnPageSupply.enabled = (self.curPageIndex == 0);
//    self.title = self.curPageIndex == 0 ? @"求购信息" : @"供货信息";
    
    [self.leftPageController onPageShowChange:(self.curPageIndex == 0)];
    [self.rightPageController onPageShowChange:(self.curPageIndex != 0)];
    return;
}

#pragma mark - 点击事件
// 搜索按钮点击
- (IBAction)btnSearchOnClick:(id)sender {
    ZbSearchSingleViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSearchSingleViewController"];
    controller.curSearchType = Zb_SearchSingle_Type_SupllyDemand;
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

// 求购页按钮点击
- (IBAction)btnPageDemandOnClick:(id)sender {
    self.curPageIndex = 0;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 供货页按钮点击
- (IBAction)btnPageSupplyOnClick:(id)sender {
    self.curPageIndex = 1;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 发布按钮点击
- (IBAction)btnPublicOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        self.hud.labelText = @"尚未登录，无法操作";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
        }];
        return;
    }
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbPubSupViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbPubSupViewController"];
    controller.curMessageIndex = self.curPageIndex == 0 ? Zb_Home_Message_Type_Buy : Zb_Home_Message_Type_Supply;
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 选中线条跟随滚动
    CGFloat lineScrollRange = DeviceWidth / 2;
    CGFloat percent = offsetX / DeviceWidth;
    self.alcSelectLineLeading.constant = lineScrollRange * percent;
    
    // 判断是否切页
    if (fmod(offsetX, scrollView.frame.size.width)  == 0) {
        int index = offsetX / scrollView.frame.size.width;
        self.curPageIndex = index;
        [self pageChange];
    }
}

#pragma mark - 导航
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Demand"]) {
        self.leftPageController = segue.destinationViewController;
    } else {
        self.rightPageController = segue.destinationViewController;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
