//
//  ZbSelectPayWayViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

typedef void (^payWayClickBlock)(Zb_Pay_Way_Type);

@interface ZbSelectPayWayViewController : BaseViewController

// 类方法，创建支付方式选择框
+ (void)showInViewController:(UIViewController *)controller block:(payWayClickBlock)block;

@end
