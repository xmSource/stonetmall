//
//  ZbDdOrderViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/11.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

// 下拉购物车点击类型
typedef enum {
    Zb_Dd_Order_Click_Buy,            // 购买
    Zb_Dd_Order_Click_Cart,           // 加入购物车
    Zb_Dd_Order_Click_Chat,           // 微聊
    Zb_Dd_Order_Click_Cancel,         // 取消
} Zb_Dd_Order_Click_Type;

// 点击block
typedef void (^btnClickBlock)(Zb_Dd_Order_Click_Type, NSMutableArray *);

@interface ZbDdOrderViewController : BaseViewController

// 显示选择器
+ (ZbDdOrderViewController *)showInViewController:(UIViewController *)controller stoneName:(NSString *)stoneName dataArray:(NSMutableArray *)array btnClickBlock:(btnClickBlock)block;

@end
