//
//  ConsScannerController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-8.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTScannerController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ZXingObjC.h"

#define HOR_SPACE_RATE 6       // 水平边框距离比例

@interface CTScannerController () <ZXCaptureDelegate>

@property (nonatomic, assign) CGRect scanRect;
@property (nonatomic, strong) UIImageView * scanLine;
@property (nonatomic, strong) NSTimer * animTimer;
@property (nonatomic,strong) ZXCapture *capture;
@property (nonatomic, assign) BOOL isCapInit;

- (void)scanLineAnimation;
- (void)addCover:(CGRect )rect;
- (void)initCapture;

@end

@implementation CTScannerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"扫描二维码";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isCapInit = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.hidesBottomBarWhenPushed = YES;
    // 返回
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_btn_arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarItemOnClick:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 查看相机权限
    BOOL available = YES;
    NSString * mediaType = AVMediaTypeVideo; // Or AVMediaTypeAudio
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if (authStatus == AVAuthorizationStatusDenied) {
        available = NO;
    }
    if (!available) {
        // 无相机权限提示
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"扫描提示" message:@"您需要在 设置 - 隐私 - 相机 中打开应用的相机访问权限" delegate:nil cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    if (!self.isCapInit) {
        self.isCapInit = YES;
        // 初始化界面
        [self initCapture];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.capture stop];
    self.capture = nil;
    [self.animTimer invalidate];
    self.animTimer = nil;
}

// 初始化界面、扫码视频
- (void)initCapture
{
    CGFloat horSpace = DeviceWidth / 6;
    CGFloat scanWidth = horSpace * 4;
    self.scanRect = CGRectMake(horSpace, (CGRectGetHeight(self.view.bounds) - scanWidth) / 2, scanWidth, scanWidth);
    
    // 扫码区域
    self.capture = [[ZXCapture alloc] init];
    self.capture.camera = self.capture.back;
    self.capture.focusMode = AVCaptureFocusModeContinuousAutoFocus;
    self.capture.rotation = 90.0f;
    self.capture.layer.frame = self.view.bounds;
    [self.view.layer addSublayer:self.capture.layer];
    self.capture.delegate = self;
    self.capture.layer.frame = self.view.bounds;
    self.capture.scanRect = self.scanRect;
    
    // 扫码外围半透明覆盖层
    [self addCover:CGRectMake(0, 0, self.view.bounds.size.width, self.scanRect.origin.y)];
    [self addCover:CGRectMake(0, self.scanRect.origin.y, self.scanRect.origin.x, self.scanRect.size.height)];
    [self addCover:CGRectMake(self.scanRect.origin.x + self.scanRect.size.width, self.scanRect.origin.y, self.scanRect.origin.x, self.scanRect.size.height)];
    [self addCover:CGRectMake(0, self.scanRect.origin.y + self.scanRect.size.height, self.view.bounds.size.width, self.view.bounds.size.height - (self.scanRect.origin.y + self.scanRect.size.height))];
    
    // 文字提示
    UILabel * tip = [[UILabel alloc] initWithFrame:CGRectZero];
    CGPoint tipPos = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMaxY(self.scanRect) + 25);
    tip.textColor = UIColorFromHexString(@"#AAAAAA");
    tip.font = [UIFont boldSystemFontOfSize:12.0f];
    tip.text = @"将二维码放入框内，即可自动扫描";
    tip.textAlignment = NSTextAlignmentCenter;
    tip.backgroundColor = [UIColor clearColor];
    [tip sizeToFit];
    tip.center = tipPos;
    [self.view addSubview:tip];
    
    // 扫码范围边框
    CGRect leftRect = CGRectMake(self.scanRect.origin.x, self.scanRect.origin.y, 16, 16);
    [self addScanAround:leftRect imageName:@"public_icn_scanning_left"];
    CGRect left1Rect = CGRectMake(self.scanRect.origin.x, CGRectGetMaxY(self.scanRect) - 16, 16, 16);
    [self addScanAround:left1Rect imageName:@"public_icn_scanning_left2"];
    CGRect rightRect = CGRectMake(CGRectGetMaxX(self.scanRect) - 16, self.scanRect.origin.y, 16, 16);
    [self addScanAround:rightRect imageName:@"public_icn_scanning_right"];
    CGRect right1Rect = CGRectMake(CGRectGetMaxX(self.scanRect) - 16, CGRectGetMaxY(self.scanRect) - 16, 16, 16);
    [self addScanAround:right1Rect imageName:@"public_icn_scanning_right2"];
    
    // 移动扫码线条
    self.scanLine = [[UIImageView alloc] initWithFrame:CGRectMake(self.scanRect.origin.x, self.scanRect.origin.y + 10, self.scanRect.size.width, 2)];
    self.scanLine.contentMode = UIViewContentModeScaleAspectFit;
    self.scanLine.image = [UIImage imageNamed:@"public_Scan_line"];
    self.scanLine.tag = 1;
    [self.view addSubview:self.scanLine];
    
    // 定时器移动扫码线条
    self.animTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(scanLineAnimation) userInfo:nil repeats:YES];
}

// 添加半透明覆盖层
- (void)addCover:(CGRect)rect
{
    UIView * view = [[UIView alloc] initWithFrame:rect];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.4;
    [self.view addSubview:view];
}

// 添加边框
- (void)addScanAround:(CGRect)rect imageName:(NSString *)name
{
    UIImageView *scanLeft = [[UIImageView alloc] initWithFrame:rect];
    scanLeft.image = [UIImage imageNamed:name];
    [self.view addSubview:scanLeft];
}

// 扫码线条动画
- (void)scanLineAnimation
{
    if (self.scanLine.tag == 1) {
        self.scanLine.center = CGPointMake(self.scanLine.center.x, self.scanLine.center.y + 2);
        if (self.scanLine.frame.origin.y + self.scanLine.frame.size.height >= self.scanRect.origin.y + self.scanRect.size.height - 10) {
            self.scanLine.tag = 2;
        }
    } else {
        self.scanLine.center = CGPointMake(self.scanLine.center.x, self.scanLine.center.y - 2);
        if (self.scanLine.frame.origin.y <= self.scanRect.origin.y + 10) {
            self.scanLine.tag = 1;
        }
    }
}

#pragma mark - ZXCaptureDelegate
- (void)captureResult:(ZXCapture *)capture result:(ZXResult *)result {
    if (!result){
        return;
    }
    if (!self.capture.running) {
        return;
    }
    
    [self.capture stop];
    DLog(@"%@",[NSString stringWithFormat:@"条码内容:%@", result.text]);
    if (self.delegate && [self.delegate respondsToSelector:@selector(ctScannerController:didReceivedScanResult:)]) {
        [self.delegate ctScannerController:self didReceivedScanResult:result.text];
    }
}


@end
