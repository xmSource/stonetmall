//
//  ZbMyFuncViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/6.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbMyFuncViewController.h"
#import "ZbStonePageController.h"
#import "ZbGoodsPageController.h"
#import "ZbQiyePageController.h"
#import "ZbSupPageViewController.h"

@interface ZbMyFuncViewController ()<UIScrollViewDelegate>

// scrollview容器
@property (strong, nonatomic) IBOutlet UIScrollView *svMain;

// 石材页按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageStone;
// 产品页按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageGoods;
// 企业页按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageQiYe;
// 供需信息页按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageSupDed;
// 石材controller
@property (strong, nonatomic) ZbStonePageController *stonePageController;
// 产品controller
@property (strong, nonatomic) ZbGoodsPageController *goodsPageController;
// 企业controller
@property (strong, nonatomic) ZbQiyePageController *qiYePageController;
// 信息controller
@property (strong, nonatomic) ZbSupPageViewController *supPageController;

// 选中线leading
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcSelectLineLeading;

// 当前选择页面
@property (assign, nonatomic) NSInteger curPageIndex;

@property (nonatomic, assign) ZbCxl_Type curZbCxlType;


@end

@implementation ZbMyFuncViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.svMain.scrollsToTop = NO;
    self.curPageIndex = 0;
    
    // 友盟统计用
    self.myName = @"查询容器";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setControllerType:(ZbCxl_Type)type {
    _curZbCxlType = type;
    if (type == ZbCxl_Type_comme) {
        self.title = @"推荐给我";
    } else if(type == ZbCXl_Type_colle) {
        self.title = @"我的收藏";
    } else {
        self.title = @"我参与的";
    }
}


#pragma mark - 点击事件

// 石材页按钮点击
- (IBAction)btnPageStoneOnClick:(id)sender {
    self.curPageIndex = 0;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 产品页按钮点击
- (IBAction)btnPageGoodsOnClick:(id)sender {
    self.curPageIndex = 1;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 企业页按钮点击
- (IBAction)btnPageQiYeOnClick:(id)sender {
    self.curPageIndex = 2;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 信息页按钮点击
- (IBAction)btnPagerewardOnClick:(id)sender {
    self.curPageIndex = 3;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}


# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 选中线条跟随滚动
    CGFloat lineScrollRange = DeviceWidth / 4;
    CGFloat percent = offsetX / DeviceWidth;
    self.alcSelectLineLeading.constant = lineScrollRange * percent;
    
    // 判断是否切页
    if (fmod(offsetX, scrollView.frame.size.width)  == 0) {
        int index = offsetX / scrollView.frame.size.width;
        self.curPageIndex = index;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"stone"]) {
        self.stonePageController = segue.destinationViewController;
        [self.stonePageController setControllerType:_curZbCxlType];
    } else if ([segue.identifier isEqualToString:@"goods"]){
        self.goodsPageController = segue.destinationViewController;
        [self.goodsPageController setControllerType:_curZbCxlType];
    } else if ([segue.identifier isEqualToString:@"qiye"]){
        self.qiYePageController = segue.destinationViewController;
        [self.qiYePageController setControllerType:_curZbCxlType];
    } else {
        self.supPageController = segue.destinationViewController;
        [self.supPageController setControllerType:_curZbCxlType];
    }
}

@end
