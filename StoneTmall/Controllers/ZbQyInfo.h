//
//  ZbQyInfo.h
//  StoneTmall
//
//  Created by chyo on 15/9/6.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZbQyInfo : NSObject

@property (nonatomic, strong) NSString *error;
@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) NSString *company_id;
@property (nonatomic, strong) NSString *company_name;
@property (nonatomic, strong) NSString *company_description;
@property (nonatomic, strong) NSString *company_clicks;
@property (nonatomic, strong) NSString *company_phone;
@property (nonatomic, strong) NSString *company_tel;
@property (nonatomic, strong) NSString *company_contact;
@property (nonatomic, strong) NSString *company_site;
@property (nonatomic, strong) NSString *company_area;
@property (nonatomic, strong) NSString *company_country;
@property (nonatomic, strong) NSString *company_prov;
@property (nonatomic, strong) NSString *company_city;
@property (nonatomic, strong) NSString *company_level;
@property (nonatomic, strong) NSString *company_mark;
@property (nonatomic, strong) NSMutableArray *company_types;
@property (nonatomic, strong) NSString *company_marks;
@property (nonatomic, strong) NSString *company_likes;
@property (nonatomic, strong) NSString *company_latitude;
@property (nonatomic, strong) NSString *company_longitude;
@property (nonatomic, strong) NSString *company_image;
@property (nonatomic, strong) NSArray *company_images;
@property (nonatomic, strong) NSString *company_licence;
@property (nonatomic, strong) NSString *company_licence_ok;
@property (nonatomic, strong) NSString *company_grow;

- (id)initWithDictionary:(NSMutableDictionary*)jsonObject;

@end
