//
//  ZbRewardListViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/29.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardListViewController.h"
#import "YKuImageInLoopScrollView.h"
#import "ZbSupplyDemandTableViewCell.h"
#import "ZbRewardDetailViewController.h"

#define HTTP_REQUEST_TAG_DATA 1           // 数据
#define HTTP_REQUEST_TAG_MORE 2           // 更多数据 

@interface ZbRewardListViewController () <UITableViewDataSource, UITableViewDelegate>

// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;

// 所有数据
@property (nonatomic, strong) NSMutableArray *dataArray;

// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

@end

@implementation ZbRewardListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSupplyDemandTableViewCell" bundle:nil] forCellReuseIdentifier:SupplyDemandCellIdentifier];
    
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    [self requesttData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)onPageShowChange:(BOOL)isShow {
    self.tv.scrollsToTop = isShow;
}

#pragma mark - 接口调用
// 重载数据
- (void)requesttData {
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn;
    if (self.curRewardListType == Zb_RewardList_Type_Ing) {
        conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getPaidDemandListByQuery:@"" pageIndex:page] delegate:self];
    } else {
        conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getFinishPaidDemandListByQuery:@"" pageIndex:page] delegate:self];
    }
    conn.tag = HTTP_REQUEST_TAG_DATA;
    [conn start];
}

// 加载更多数据
- (void)requestMoreData {
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn;
    if (self.curRewardListType == Zb_RewardList_Type_Ing) {
        conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getPaidDemandListByQuery:@"" pageIndex:page] delegate:self];
    } else {
        conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getFinishPaidDemandListByQuery:@"" pageIndex:page] delegate:self];
    }
    conn.tag = HTTP_REQUEST_TAG_MORE;
    [conn start];
    return;
}

#pragma mark - 点击事件
// xx按钮点击
- (IBAction)btnXxOnClick:(UIButton *)sender {
    return;
}

#pragma mark - UITableViewDataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbSupplyDemandTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
    [cell setPostDict:[self.dataArray objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
    ZbRewardDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardDetailViewController"];
    controller.curPostId = curDict[@"post_id"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 重置选项
    [self requesttData];
}

- (void)footerRefresh {
    [self requestMoreData];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    NSMutableArray *dataArray = resultDic[@"data"];
    if (connection.tag == HTTP_REQUEST_TAG_DATA) {
        NSMutableArray *tagArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:tagArray];
        if (tagArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        
        [self.hud hide:NO];
        [self.tv reloadData];
        if (self.dataArray.count > 0) {
            CGRect rectTop = CGRectMake(0, 0, 1, 1);
            [self.tv scrollRectToVisible:rectTop animated:YES];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_MORE) {
        // 上拉加载更多
        [self.tv.footer endRefreshing];
        NSMutableArray *tagArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.dataArray addObjectsFromArray:tagArray];
        if (tagArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
