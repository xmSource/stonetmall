//
//  ZbDropDownViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/30.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbDropDownViewController.h"
#import "ZbDropTextCell.h"
#import "ZbDropPicCell.h"
#import "ZbDropSortCell.h"

#define MAX_SHOW_HEIGHT_RATE (3/4.0)

@interface ZbDropDownViewController () <UITableViewDataSource, UITableViewDelegate>

// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// tv的top约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvTop;
// tv的height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvHeight;
// 遮罩层
@property (nonatomic, strong) IBOutlet UIControl *controlCover;

// 颜色字典
@property (nonatomic, strong) NSDictionary *colorDict;
// 数据列表
@property (nonatomic, strong) NSMutableArray *array;

// 当前选中索引
@property (nonatomic, assign) NSInteger curSelectIndex;
// 选中回调
@property (nonatomic, copy) selectBlock curBlock;

@end

@implementation ZbDropDownViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:NSStringFromClass([ZbDropTextCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ZbDropTextCell class])];
    [self.tv registerNib:[UINib nibWithNibName:NSStringFromClass([ZbDropPicCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ZbDropPicCell class])];
    [self.tv registerNib:[UINib nibWithNibName:NSStringFromClass([ZbDropSortCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ZbDropSortCell class])];
    
    // 颜色字典
    self.colorDict = @{@"黄色":UIColorFromHexString(@"FFFF00"), @"蓝色":UIColorFromHexString(@"3888E1"), @"绿色":UIColorFromHexString(@"00CFA0"), @"红色":UIColorFromHexString(@"F78C48"), @"紫色":UIColorFromHexString(@"800080"), @"白色":UIColorFromHexString(@"FFFFFF"), @"灰色":UIColorFromHexString(@"AAAAAA"), @"棕色":UIColorFromHexString(@"CDBA96"), @"黑色":UIColorFromHexString(@"333333"), };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//  类方法
+ (ZbDropDownViewController *)showInViewController:(UIViewController *)controller startY:(CGFloat)startY dataArray:(NSArray *)array type:(Zb_DropDown_Type)type selectIndex:(NSInteger)index selectBlock:(selectBlock)block isNeedAll:(BOOL)isNeed {
    UIStoryboard *pickSb = [UIStoryboard storyboardWithName:@"DropDown" bundle:nil];
    ZbDropDownViewController *dropDown = [pickSb instantiateViewControllerWithIdentifier:@"ZbDropDownViewController"];
    
    [controller addChildViewController:dropDown];
    [controller.view addSubview:dropDown.view];
    
    [dropDown viewWillAppear:YES];
    
    dropDown.curSelectIndex = index;
    NSMutableArray *dataArray = [NSMutableArray new];
    if (isNeed) {
        dataArray = [NSMutableArray arrayWithObject:@{@"class_name":@"全部"}];
        [dataArray addObjectsFromArray:array];
        dropDown.array = dataArray;
    } else {
        [dataArray addObjectsFromArray:array];
        dropDown.array = dataArray;
    }
    dropDown.curBlock = block;
    // 下拉框界面大小
    CGRect viewFrame = CGRectMake(0, startY, CGRectGetWidth(controller.view.bounds), CGRectGetHeight(controller.view.bounds) - startY);
    dropDown.view.frame = viewFrame;
    // 选择类型
    dropDown.curType = type;
    // 单高度
    CGFloat cellHeight;
    // cell总高度
    CGFloat height;
    switch (type) {
        case Zb_DropDown_Type_Text:
        {
            // 文字
            cellHeight = [ZbDropTextCell getRowHeight];
            height = cellHeight * [ZbDropTextCell getRowCount:array.count];
            break;
        }
        case Zb_DropDown_Type_Color:
        {
            // 颜色
            cellHeight = [ZbDropPicCell getRowHeight];
            height = cellHeight * [ZbDropPicCell getRowCount:array.count];
            break;
        }
        case Zb_DropDown_Type_Pic:
        {
            // 图文
            cellHeight = [ZbDropPicCell getRowHeight];
            height = cellHeight * [ZbDropPicCell getRowCount:array.count];
            break;
        }
        case Zb_DropDown_Type_Sort:
        {
            // 排序
            cellHeight = [ZbDropSortCell getRowHeight];
            height = cellHeight * 1;
            break;
        }
        default:
            break;
    }
    // 可显示高度
    CGFloat showHeight = MIN(height, CGRectGetHeight(viewFrame) * MAX_SHOW_HEIGHT_RATE);
    dropDown.alcTvHeight.constant = showHeight;
//    dropDown.alcTvTop.constant = -showHeight;
//    dropDown.controlCover.alpha = 0;
    dropDown.alcTvTop.constant = 0;
    dropDown.controlCover.alpha = 1;
//    [dropDown.view layoutIfNeeded];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
//    [UIView animateWithDuration:0.3 animations:^(void) {
//        dropDown.alcTvTop.constant = 0;
//        dropDown.controlCover.alpha = 1;
//        [dropDown.view layoutIfNeeded];
//    } completion:^(BOOL finished) {
//        
//    }];
    return dropDown;
}

// 关闭界面
- (void)dismiss:(BOOL)animated {
    animated = NO;
    if (!animated) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        return;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.tv.frame = CGRectMake(0, -CGRectGetHeight(self.tv.frame), CGRectGetWidth(self.tv.frame), CGRectGetHeight(self.tv.frame));
        self.controlCover.alpha = 0;
    } completion:^(BOOL finished){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    return;
}

#pragma mark - 点击事件
- (IBAction)btnCoverOnClick:(id)sender {
    BOOL isDismiss = YES;
    self.curBlock(0, nil, isDismiss);
    [self dismiss:YES];
    return;
}

#pragma mark - UITableViewDataSource
// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = self.array.count;
    switch (self.curType) {
        case Zb_DropDown_Type_Text:
            // 文字
            return [ZbDropTextCell getRowCount:count];
        case Zb_DropDown_Type_Color:
            // 颜色
            return [ZbDropPicCell getRowCount:count];
        case Zb_DropDown_Type_Pic:
            // 图文
            return [ZbDropPicCell getRowCount:count];
        case Zb_DropDown_Type_Sort:
            // 排序
            return 1;
            
        default:
            break;
    }
    return count;
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.curType) {
        case Zb_DropDown_Type_Text:
            // 文字
            return [ZbDropTextCell getRowHeight];
        case Zb_DropDown_Type_Color:
            // 颜色
            return [ZbDropPicCell getRowHeight];
        case Zb_DropDown_Type_Pic:
            // 图文
            return [ZbDropPicCell getRowHeight];
        case Zb_DropDown_Type_Sort:
            // 排序
            return [ZbDropSortCell getRowHeight];
            
        default:
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.curType) {
        case Zb_DropDown_Type_Text:
        {
            // 文字
            ZbDropTextCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZbDropTextCell class])];
            [cell setRow:indexPath.row dataArray:self.array];
            cell.dropTextClickBlock = ^(NSInteger index, NSDictionary *curDict) {
                if (self.curBlock) {
                    // 选项
                    NSString *name = curDict[@"class_name"];
                    BOOL isDismiss = NO;
                    self.curBlock(index, name, isDismiss);
                }
                [self dismiss:YES];
            };
            return cell;
        }
        case Zb_DropDown_Type_Color:
        case Zb_DropDown_Type_Pic:
        {
            // 图文
            ZbDropPicCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZbDropPicCell class])];
            [cell setRow:indexPath.row dataArray:self.array];
            cell.dropPicClickBlock = ^(NSInteger index, NSDictionary *curDict) {
                if (self.curBlock) {
                    // 选项
                    NSString *name = curDict[@"class_name"];
                    BOOL isDismiss = NO;
                    self.curBlock(index, name, isDismiss);
                }
                [self dismiss:YES];
            };
            return cell;
        }
        case Zb_DropDown_Type_Sort:
        {
            // 排序
            ZbDropSortCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZbDropSortCell class])];
            [cell drawDataArray:self.array];
            cell.dropPicClickBlock = ^(NSInteger index, NSDictionary *curDict) {
                if (self.curBlock) {
                    // 选项
                    NSString *name = curDict[@"class_name"];
                    BOOL isDismiss = NO;
                    self.curBlock(index, name, isDismiss);
                }
                [self dismiss:YES];
            };
            return cell;
        }
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
