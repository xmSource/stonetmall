//
//  ZbRewardJoinViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/20.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbRewardJoinViewController : BaseViewController

// 当前悬赏id
@property (nonatomic, weak) NSString *curPostId;

// 发布成功block
@property (nonatomic, copy) void (^pubOkBlock)(void);

@end
