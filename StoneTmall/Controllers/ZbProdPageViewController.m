//
//  ZbProdPageViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/17.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbProdPageViewController.h"
#import "ZbFindProductTableViewCell.h"
#import "ZbProductDetailViewController.h"

#define HTTP_REQUEST_TAG_DATA 1           // 数据
#define HTTP_REQUEST_TAG_MORE 2           // 更多数据

@interface ZbProdPageViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) IBOutlet UITableView *tv;
@property (nonatomic, assign) NSInteger curPageIndex;

@end

@implementation ZbProdPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [[NSMutableArray alloc] init];
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbFindProductTableViewCell" bundle:nil] forCellReuseIdentifier:FindProductCellIdentifier];
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

#pragma mark - 读取数据
// 读取发布产品数据
- (void)loadData {
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMyProductList:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DATA;
    [conn start];
}
// 读取发布产品更多数据
- (void)requestMoreData {
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMyProductList:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MORE;
    [conn start];
}


#pragma mark - TableViewDelegate &Datasource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbFindProductTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        ZbFindProductTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:FindProductCellIdentifier];
        // 绘制
        NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
        [cell setInfoDict:dict];
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ZbProductDetailViewController *controller = [main instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
    NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
    NSString *productId = curDict[@"product_id"];
    controller.productId = productId;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 重置选项
    [self loadData];
}

- (void)footerRefresh {
    [self requestMoreData];
}


#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    if (result.intValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (connection.tag == HTTP_REQUEST_TAG_DATA) {
        NSMutableDictionary *dataDict = resultDic[@"data"];
        NSMutableArray *tagArray = ((NSMutableDictionary *)dataDict)[@"rows"];
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:tagArray];
        [self.tv reloadData];
        
        if (tagArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        [self.hud hide:NO];
        [self.tv reloadData];
        if (self.dataArray.count > 0) {
            CGRect rectTop = CGRectMake(0, 0, 1, 1);
            [self.tv scrollRectToVisible:rectTop animated:YES];
        }

    } else if (connection.tag == HTTP_REQUEST_TAG_MORE) {
        NSMutableDictionary *dataDict = resultDic[@"data"];
        NSMutableArray *array = dataDict[@"rows"];
        [self.dataArray addObjectsFromArray:array];
        if (array.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.hud hide:NO];
        [self.tv reloadData];
    }
}

@end
