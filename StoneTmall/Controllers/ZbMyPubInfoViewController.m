//
//  MyPubInfoViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/17.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbMyPubInfoViewController.h"
#import "ZbProdPageViewController.h"
#import "ZbSupADedPageViewController.h"

@interface ZbMyPubInfoViewController ()<UIScrollViewDelegate>

// scrollview容器
@property (strong, nonatomic) IBOutlet UIScrollView *svMain;

// 登录页按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageProd;
// 注册页按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageSup;
// 查保养controller
@property (strong, nonatomic) ZbProdPageViewController *leftPageController;
// 查保险controller
@property (strong, nonatomic) ZbSupADedPageViewController *rightPageController;
// 选中线leading
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcSelectLineLeading;

// 当前选择页面
@property (assign, nonatomic) NSInteger curPageIndex;


@end

@implementation ZbMyPubInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.svMain.scrollsToTop = NO;
    self.curPageIndex = 0;
    
    // 友盟统计用
    self.myName = @"查询容器";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - 点击事件
// 产品页按钮点击
- (IBAction)btnPageProdOnClick:(id)sender {
    self.curPageIndex = 0;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 供需页按钮点击
- (IBAction)btnPageSupOnClick:(id)sender {
    self.curPageIndex = 1;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 选中线条跟随滚动
    CGFloat lineScrollRange = DeviceWidth / 2;
    CGFloat percent = offsetX / DeviceWidth;
    self.alcSelectLineLeading.constant = lineScrollRange * percent;
    
    // 判断是否切页
    if (fmod(offsetX, scrollView.frame.size.width)  == 0) {
        int index = offsetX / scrollView.frame.size.width;
        self.curPageIndex = index;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"prod"]) {
        self.leftPageController = segue.destinationViewController;
    } else {
        self.rightPageController = segue.destinationViewController;
    }
}

@end
