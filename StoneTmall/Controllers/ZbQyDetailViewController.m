//
//  ZbQyDetailViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQyDetailViewController.h"
#import "YKuImageInLoopScrollView.h"
#import "ZbFindProductTableViewCell.h"
#import "ZbStoneSimilarTableViewCell.h"
#import "ZbSeparateTableViewCell.h"
#import "ZbTypeTableViewCell.h"
#import "ZbQyIntroductionTableViewCell.h"
#import "ZbQySingleInfoTableViewCell.h"
#import "ZbStoneDetailViewController.h"
#import <ShareSDK/ShareSDK.h>
#import "ZbLoginMainViewController.h"
#import "ZbProductDetailViewController.h"
#import "ZbQyPosViewController.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "ConsWebController.h"
#import "ZLPhoto.h"
#import "ChatViewController.h"
#import "ZbLinkStoneViewController.h"
#import "UITableView+FDTemplateLayoutCell.h"

#define HTTP_REQUEST_TAG_STONE 1              // 主营石材
#define HTTP_REQUEST_TAG_PRODUCT 2            // 主营产品
#define HTTP_REQUEST_TAG_MORE_ADDR 3          // 更多地址
#define HTTP_REQUEST_TAG_MORE_CONTACT 4       // 更多联系人
#define HTTP_REQUEST_TAG_INFO 5               // 企业详情
#define HTTP_REQUEST_TAG_MARK 6               // 收藏
#define HTTP_REQUEST_TAG_UNMARK 7             // 取消收藏
#define HTTP_REQUEST_TAG_LIKE 8               // 点赞
#define HTTP_REQUEST_TAG_UNLIKE 9             // 取消赞

@interface ZbQyDetailViewController () <UITableViewDataSource, UITableViewDelegate, YKuImageInLoopScrollViewDelegate, ZLPhotoPickerBrowserViewControllerDataSource, ZLPhotoPickerBrowserViewControllerDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 滚动图片容器
@property (nonatomic, strong) IBOutlet UIView *imageLoopContainer;
// 滚动图片容器height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcImageLoopContainerHeight;
// 名称
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 等级
@property (nonatomic, strong) IBOutlet UILabel *labLevel;
// 等级描述文字
@property (nonatomic, strong) IBOutlet UILabel *labLevelDes;
// 等级背景
@property (nonatomic, strong) IBOutlet UIView *levelBg;
// 认证企业图片
@property (nonatomic, strong) IBOutlet UIImageView *ivLicenceOk;
// 认证企业描述文字
@property (nonatomic, strong) IBOutlet UILabel *labLicenceOkDes;
// 成长值
@property (nonatomic, strong) IBOutlet UILabel *labGrow;
// 成长值描述文字
@property (nonatomic, strong) IBOutlet UILabel *labGrowDes;
// 成长值背景
@property (nonatomic, strong) IBOutlet UIView *bgGrow;
// 微聊咨询按钮
@property (nonatomic, strong) IBOutlet UIButton *btnChat;
// 赞按钮
@property (nonatomic, strong) IBOutlet UIButton *btnPraise;
// 交互
@property (nonatomic, strong) IBOutlet UILabel *labCommunicate;
// 收藏按钮
@property (nonatomic, strong) IBOutlet UIButton *btnMark;

// 滚动图片
@property (nonatomic, strong) YKuImageInLoopScrollView *imageLoopView;

// 主营产品数组
@property (nonatomic, strong) NSMutableArray *productArray;
// 主营石材数组
@property (nonatomic, strong) NSMutableArray *stoneArray;
// 其他联系人数组
@property (nonatomic, strong) NSMutableArray *otherContactArray;
// 其他地址数组
@property (nonatomic, strong) NSMutableArray *otherAddrArray;

// 当前企业详情字典
@property (nonatomic, strong) NSMutableDictionary *curQyDetailDict;

// 是否展开简介
@property (nonatomic, assign) BOOL isDesExpand;
// 是否展开经营范围
@property (nonatomic, assign) BOOL isTypesExpand;
// 是否展开联系人
@property (nonatomic, assign) BOOL isContactExpand;
// 是否展开地址
@property (nonatomic, assign) BOOL isAddrExpand;

@end

@implementation ZbQyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.productArray = [NSMutableArray array];
    self.stoneArray = [NSMutableArray array];
    self.otherContactArray = [NSMutableArray array];
    self.otherAddrArray = [NSMutableArray array];
    
    // 圆角
    self.btnChat.layer.cornerRadius = 3;
    self.btnChat.layer.masksToBounds = YES;
    self.btnChat.layer.cornerRadius = 3;
    self.btnPraise.layer.cornerRadius = 3;
    self.btnPraise.layer.borderWidth = 1;
    self.btnPraise.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    self.levelBg.layer.cornerRadius = 3;
    self.bgGrow.layer.cornerRadius = 3;
    
    // 默认微聊不激活
    [self setBtnChatEnable:NO];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbFindProductTableViewCell" bundle:nil] forCellReuseIdentifier:FindProductCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneSimilarTableViewCell" bundle:nil] forCellReuseIdentifier:StoneSimilarCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbTypeTableViewCell" bundle:nil] forCellReuseIdentifier:TypeHeaderIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSeparateTableViewCell" bundle:nil] forCellReuseIdentifier:SeparateCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbQyIntroductionTableViewCell" bundle:nil] forCellReuseIdentifier:QyIntroductionCellIdentifier];
    
    // 上拉重新加载
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    // 获取详情数据
    [self requestStoneListData];
    [self requestProductListData];
    [self requestAddrData];
    [self requestContactData];
    [self requestQyDetailData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 登录判断
- (void)requestLogin {
    self.hud.labelText = @"尚未登录，无法操作";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
        UIStoryboard *loginSb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        UINavigationController *nav = [loginSb instantiateViewControllerWithIdentifier:@"LoginNav"];
        ZbLoginMainViewController *loginController = (ZbLoginMainViewController *)nav.viewControllers[0];
        loginController.loginSucessBlock = ^(void) {
            [self.tv.header performSelectorOnMainThread:@selector(beginRefreshing) withObject:nil waitUntilDone:NO];
        };
        [self.view.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }];
    return;
}

#pragma mark - 界面绘制
// 信息绘制
- (void)drawInfo {
    // 企业名
    NSString *companyName = self.curQyDetailDict[@"company_name"];
    self.title = companyName;
    self.labName.text = companyName;
    
    // 图片高度
    CGFloat tagImageHeight = DeviceWidth / 1.6;
    self.alcImageLoopContainerHeight.constant = tagImageHeight;
    
    // header高度
    [self calcTvHeader];
    
    // 添加图片滚动
    self.imageLoopView = [[YKuImageInLoopScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, tagImageHeight)];
    self.imageLoopView.delegate = self;
    // 设置yKuImageInLoopScrollView显示类型
    self.imageLoopView.scrollViewType = ScrollViewDefault;
    // 设置styledPageControl位置
    [self.imageLoopView.styledPageControl setPageControlSite:PageControlSiteMiddle];
    [self.imageLoopView.styledPageControl setBottomDistance:12];
    // 设置styledPageControl已选中下的内心圆颜色
    [self.imageLoopView.styledPageControl setCoreSelectedColor:MAIN_COLOR];
    [self.imageLoopView.styledPageControl setCoreNormalColor:[UIColor whiteColor]];
    [self.imageLoopContainer addSubview:self.imageLoopView];
    return;
}

// 详情绘制
- (void)drawDetailInfo {
    // 绘制信息
    [self drawInfo];
    // 等级
    NSString *companyLevel = self.curQyDetailDict[@"company_level"];
    self.labLevel.text = [NSString stringWithFormat:@"V%@", companyLevel];
    self.labLevelDes.text = [NSString stringWithFormat:@"诚信企业%@级", companyLevel];
    self.levelBg.backgroundColor = companyLevel.integerValue > 0 ? MAIN_ORANGE_COLOR : UIColorFromHexString(@"CCCCCC");
    // 营业执照验证状态
    NSString *licenceOk = self.curQyDetailDict[@"company_licence_ok"];
    if (licenceOk.integerValue == 1) {
        self.ivLicenceOk.highlighted = YES;
        self.labLicenceOkDes.text = [NSString stringWithFormat:@"营业执照已验证"];
    } else {
        self.ivLicenceOk.highlighted = NO;
        self.labLicenceOkDes.text = [NSString stringWithFormat:@"营业执照未验证"];
    }
    // 成长值
    NSString *grow = self.curQyDetailDict[@"company_grow"];
    self.labGrow.text = [NSString stringWithFormat:@"%@", grow];
    self.labGrowDes.text = [NSString stringWithFormat:@"成长值%@", grow];
    self.bgGrow.backgroundColor = grow.integerValue > 0 ? UIColorFromHexString(@"04A875") : UIColorFromHexString(@"CCCCCC");
    // 收藏状态
    [self drawMarkStatus];
    // 赞状态
    [self drawLikeStatus];
    
    // 微聊按钮状态
    NSString *userName = self.curQyDetailDict[@"user_name"];
    if (userName && [userName isKindOfClass:[NSString class]] && userName.length > 0) {
        [self setBtnChatEnable:YES];
    } else {
        [self setBtnChatEnable:NO];
    }
//    // zb test 暂时隐藏微聊
//    [self setBtnChatEnable:NO];
    return;
}

// 重算header高度
- (void)calcTvHeader {
    // header高度
    CGFloat tagHeaderHeight = [self.tvHeader systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect headerFrame = CGRectMake(0, 0, DeviceWidth, tagHeaderHeight);
    self.tvHeader.frame = headerFrame;
    [self.tv setTableHeaderView:self.tvHeader];
}

// 绘制收藏状态
- (void)drawMarkStatus {
    NSNumber *mark = self.curQyDetailDict[@"company_mark"];
    self.btnMark.selected = mark.integerValue > 0;
    // 交互
    NSNumber *clicks = self.curQyDetailDict[@"company_clicks"];
    NSNumber *marks = self.curQyDetailDict[@"company_marks"];
    NSString *communicate = [NSString stringWithFormat:@"%@ 浏览  %@ 收藏", clicks, marks];
    self.labCommunicate.text = communicate;
    return;
}

// 绘制点赞状态
- (void)drawLikeStatus {
    // 赞状态
    NSNumber *like = self.curQyDetailDict[@"company_like"];
    self.btnPraise.selected = like.integerValue > 0;
    // 赞数量
    NSNumber *likes = self.curQyDetailDict[@"company_likes"];
    [self.btnPraise setTitle:[NSString stringWithFormat:@"%@", likes] forState:UIControlStateNormal];
    return;
}

// 设置微聊按钮激活状态
- (void)setBtnChatEnable:(BOOL)isEnable {
    self.btnChat.enabled = isEnable;
    self.btnChat.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

#pragma mark - 接口调用
// 获取主营石材接口
- (void)requestStoneListData {
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyStoneList:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_STONE;
    [conn start];
    return;
}

// 获取主营产品接口
- (void)requestProductListData {
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyProductList:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_PRODUCT;
    [conn start];
    return;
}

// 获取更多地址接口
- (void)requestAddrData {
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyAddressListByComId:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MORE_ADDR;
    [conn start];
    return;
}

// 获取更多联系人接口
- (void)requestContactData {
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyContactListByComId:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MORE_CONTACT;
    [conn start];
    return;
}

// 获取详情接口
- (void)requestQyDetailData {
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyInfo:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_INFO;
    [conn start];
    return;
}

// 收藏接口
- (void)requestMark {
    self.hud.labelText = @"收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService markCompany:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MARK;
    [conn start];
    return;
}

// 取消收藏接口
- (void)requestUnMark {
    self.hud.labelText = @"取消收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService unmarkCompany:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNMARK;
    [conn start];
    return;
}

// 点赞接口
- (void)requestLike {
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService likeCompany:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_LIKE;
    [conn start];
    return;
}

// 点赞接口
- (void)requestUnlike {
    NSString *companyId = self.companyId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService unlikeCompany:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNLIKE;
    [conn start];
    return;
}

#pragma mark - 点击事件
// 分享按钮点击
- (IBAction)btnShareOnClick:(id)sender {
    // 公司信息
    NSString *companyName = self.curQyDetailDict[@"company_name"];
    NSString *des = self.curQyDetailDict[@"company_description"];
    if (des.length > SHARE_CONTENT_MAX_LEN) {
        des = [des substringToIndex:SHARE_CONTENT_MAX_LEN];
        des = [NSString stringWithFormat:@"%@...", des];
    }
    NSString *imgUrl = [ZbWebService getImageFullUrl:self.curQyDetailDict[@"company_image_small"]];
    // 分享信息
    NSString *title = [NSString stringWithFormat:@"石猫石材搜索-%@", companyName];
    NSString *shareUrl = [ZbWebService shareCompanyUrl:self.companyId];
//    NSDictionary *appDataDict = @{@"tag":[NSNumber numberWithInt:Zb_Share_Ext_Type_Company], @"company_id":self.companyId};
//    NSString *appDataJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:appDataDict options:0 error:nil] encoding:NSUTF8StringEncoding];
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:des
                                       defaultContent:@""
                                                image:[ShareSDK imageWithUrl:imgUrl]
                                                title:title
                                                  url:shareUrl
                                          description:@"企业详情"
                                            mediaType:SSPublishContentMediaTypeNews];
    // 微信好友数据
    [publishContent addWeixinSessionUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeApp] content:des title:title url:shareUrl image:[ShareSDK imageWithUrl:imgUrl] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
    // 微信朋友圈数据
    [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeNews] content:des title:title url:shareUrl image:[ShareSDK imageWithUrl:imgUrl] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
    // 短信数据
    [publishContent addSMSUnitWithContent:[NSString stringWithFormat:@"[石猫]向你推荐： %@ ，链接为：%@", companyName, shareUrl]];
    
    // 分享按钮列表
    NSArray *shareList;
    if ([WXApi isWXAppInstalled] && [QQApiInterface isQQInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeQQ, ShareTypeSMS, nil];
    } else if ([WXApi isWXAppInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeSMS, nil];
    } else if ([QQApiInterface isQQInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeQQ, ShareTypeSMS, nil];
    } else {
        shareList = [ShareSDK getShareListWithType:ShareTypeSMS, nil];
    }
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:shareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];
    return;
}

// 收藏按钮点击
- (IBAction)btnMarkOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    NSNumber *mark = self.curQyDetailDict[@"company_mark"];
    NSNumber *marks = self.curQyDetailDict[@"company_marks"];
    if (mark.integerValue == 0) {
        // 未收藏，则收藏
        [self requestMark];
        [self.curQyDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"company_mark"];
        [self.curQyDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue + 1)] forKey:@"company_marks"];
    } else {
        // 已收藏，则取消
        [self requestUnMark];
        [self.curQyDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"company_mark"];
        [self.curQyDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue - 1)] forKey:@"company_marks"];
    }
    [self drawMarkStatus];
    return;
}

// 赞按钮点击
- (IBAction)btnPraiseOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    NSNumber *like = self.curQyDetailDict[@"company_like"];
    NSNumber *likes = self.curQyDetailDict[@"company_likes"];
    if (like.integerValue == 0) {
        // 未赞，则收藏
        [self requestLike];
        [self.curQyDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"company_like"];
        [self.curQyDetailDict setValue:[NSNumber numberWithInteger:(likes.integerValue + 1)] forKey:@"company_likes"];
    } else {
        // 已赞，则取消
        [self requestUnlike];
        [self.curQyDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"company_like"];
        [self.curQyDetailDict setValue:[NSNumber numberWithInteger:(likes.integerValue - 1)] forKey:@"company_likes"];
    }
    [self drawLikeStatus];
    return;
}

// 微聊按钮点击
- (IBAction)btnChatOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    // 聊天系统登录判断
    if (![[[EaseMob sharedInstance] chatManager] isLoggedIn]) {
        self.hud.labelText = @"聊天系统数据正在加载中..请稍候";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    };
    
    NSString *userName = self.curQyDetailDict[@"user_name"];
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
    if (loginUsername && loginUsername.length > 0) {
        if ([loginUsername isEqualToString:userName]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notChatSelf", @"can't talk to yourself") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
            [alertView show];
            
            return;
        }
    }
//    // 有可能不是好友，因此添加此用户昵称信息
//    [ZbSaveManager addUserInfo:@{@"user_name":userName, @"user_nickname":self.curQyDetailDict[@"user_nickname"], @"user_image":self.curQyDetailDict[@"user_image"]}];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:userName isGroup:NO];
    [self.navigationController pushViewController:chatVC animated:YES];
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 8;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
        {
            // 企业介绍
            return 1;
        }
        case 1:
        {
            // 经营范围
            return 1;
        }
        case 2:
        {
            // 联系人
            if (self.otherContactArray.count > 0 && !self.isContactExpand) {
                return 2;
            } else {
                return self.otherContactArray.count + 1;
            }
        }
        case 3:
            // 电话
            return 1;
        case 4:
        {
            // 地址
            if (self.otherAddrArray.count > 0 && !self.isAddrExpand) {
                return 2;
            } else {
                return self.otherAddrArray.count + 1;
            }
        }
        case 5:
            // 网址
            return 1;
        case 6:
            // 主营石材
            return self.stoneArray.count > 0 ? 1 : 0;
        case 7:
            // 主营产品
            return self.productArray.count;
        default:
            return 0;
    }
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 企业介绍
            static ZbQyIntroductionTableViewCell *sizingCell = nil;
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                sizingCell = [tableView dequeueReusableCellWithIdentifier:QyIntroductionCellIdentifier];
                sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tv.frame), CGRectGetHeight(sizingCell.bounds));
            });
            NSString *des = self.curQyDetailDict[@"company_description"];
            return [sizingCell calcCellHeight:des expand:self.isDesExpand];
        }
        case 1:
        {
            // 经营范围
            static ZbQyIntroductionTableViewCell *sizingCell = nil;
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                sizingCell = [tableView dequeueReusableCellWithIdentifier:QyIntroductionCellIdentifier];
                sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tv.frame), CGRectGetHeight(sizingCell.bounds));
            });
            NSArray *types = self.curQyDetailDict[@"company_types"];
            NSString *typesStr = [types componentsJoinedByString:@"、"];
            return [sizingCell calcCellHeight:typesStr expand:self.isTypesExpand];
        }
        case 2:
        {
            // 联系人
            if (self.otherContactArray.count > 0 && !self.isContactExpand) {
                if (indexPath.row == 0) {
                    return 45;
                } else {
                    // 展开项高度
                    return 35;
                }
            } else {
                // 联系人高度
                return 45;
            }
        }
        case 3:
            // 电话
            return 45;
        case 4:
        {
            // 地址
            static ZbQySingleInfoTableViewCell *sizingCell = nil;
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                sizingCell = [tableView dequeueReusableCellWithIdentifier:@"QySingleInfoCell"];
                sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tv.frame), CGRectGetHeight(sizingCell.bounds));
                sizingCell.labText.numberOfLines = 0;
            });
            if (self.otherAddrArray.count > 0 && !self.isAddrExpand) {
                if (indexPath.row == 0) {
                    // 主地址
                    NSString *prov = self.curQyDetailDict[@"company_prov"];
                    NSString *city = self.curQyDetailDict[@"company_city"];
                    NSString *area = self.curQyDetailDict[@"company_area"];
                    NSString *addrShow = [NSString stringWithFormat:@"%@%@%@", prov, city, area];
                    addrShow = addrShow.length <= 0 ? @"未添加" : addrShow;
                    return [sizingCell calcCellHeight:addrShow];
                } else {
                    // 展开项高度
                    return 35;
                }
            } else {
                // 地址高度
                NSString *addrShow;
                if (indexPath.row > 0) {
                    // 其他地址
                    NSDictionary *curAddrDict = [self.otherAddrArray objectAtIndex:(indexPath.row - 1)];
                    NSString *prov = curAddrDict[@"prov"];
                    NSString *city = curAddrDict[@"city"];
                    NSString *area = curAddrDict[@"area"];
                    addrShow = [NSString stringWithFormat:@"%@%@%@", prov, city, area];
                } else {
                    // 主地址
                    NSString *prov = self.curQyDetailDict[@"company_prov"];
                    NSString *city = self.curQyDetailDict[@"company_city"];
                    NSString *area = self.curQyDetailDict[@"company_area"];
                    addrShow = [NSString stringWithFormat:@"%@%@%@", prov, city, area];
                }
                addrShow = addrShow.length <= 0 ? @"未添加" : addrShow;
                return [sizingCell calcCellHeight:addrShow];
            }
        }
        case 5:
            // 网址
            return 45;
        case 6:
            // 主营石材
            return [ZbStoneSimilarTableViewCell getRowHeight];
        case 7:
            // 主营产品
            return [ZbFindProductTableViewCell getRowHeight];
            
        default:
            return 0;
    }
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 企业介绍
            ZbQyIntroductionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:QyIntroductionCellIdentifier forIndexPath:indexPath];
            
            NSString *des = self.curQyDetailDict[@"company_description"];
            [cell setIntroductionName:@"企业简介" content:des expand:self.isDesExpand];
            cell.expandClickBlock = ^(void) {
                self.isDesExpand = YES;
                [tableView reloadData];
//                [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            return cell;
        }
        case 1:
        {
            // 经营范围
            ZbQyIntroductionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:QyIntroductionCellIdentifier forIndexPath:indexPath];
            
            NSArray *types = self.curQyDetailDict[@"company_types"];
            NSString *typesStr = [types componentsJoinedByString:@"、"];
            [cell setIntroductionName:@"经营范围" content:typesStr expand:self.isTypesExpand];
            cell.expandClickBlock = ^(void) {
                self.isTypesExpand = YES;
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            return cell;
        }
        case 2:
        {
            // 联系人
            if (self.otherContactArray.count > 0 && !self.isContactExpand) {
                if (indexPath.row == 0) {
                    ZbQySingleInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QySingleInfoCell"];
                    // 主联系人
                    NSMutableArray *contactArray = [NSMutableArray array];
                    NSString *companyContact = self.curQyDetailDict[@"company_contact"];
                    if (companyContact != nil && companyContact.length > 0) {
                        [contactArray addObject:companyContact];
                    }
                    NSString *companyPhone = self.curQyDetailDict[@"company_phone"];
                    if (companyPhone != nil && companyPhone.length > 0) {
                        NSString *firstPhone = [[companyPhone componentsSeparatedByString:@","] firstObject];
                        [contactArray addObject:firstPhone];
                    }
                    NSString *contact = [contactArray componentsJoinedByString:@" "];
                    cell.labText.numberOfLines = 1;
                    cell.labText.text = contact.length <= 0 ? @"未添加" : contact;
                    cell.alcBottomLineLeading.constant = 50;
                    cell.ivArrow.hidden = NO;
                    cell.ivIcon.hidden = NO;
                    cell.ivIcon.image = [UIImage imageNamed:@"public_btn_contacts"];
                    return cell;
                } else {
                    // 展开项
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExpandInfoCell"];
                    UILabel *labText = (UILabel *)[cell viewWithTag:1];
                    labText.text = [NSString stringWithFormat:@"查看另外%lu个联系人", (unsigned long)self.otherContactArray.count];
                    return cell;
                }
            } else {
                // 联系人
                ZbQySingleInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QySingleInfoCell"];
                NSString *contact;
                if (indexPath.row > 0) {
                    // 其他联系人
                    NSDictionary *curContactDict = [self.otherContactArray objectAtIndex:(indexPath.row - 1)];
                    contact = [NSString stringWithFormat:@"%@ %@", curContactDict[@"name"], curContactDict[@"phone"]];
                } else {
                    // 主联系人
                    NSMutableArray *contactArray = [NSMutableArray array];
                    NSString *companyContact = self.curQyDetailDict[@"company_contact"];
                    if (companyContact != nil && companyContact.length > 0) {
                        [contactArray addObject:companyContact];
                    }
                    NSString *companyPhone = self.curQyDetailDict[@"company_phone"];
                    if (companyPhone != nil && companyPhone.length > 0) {
                        NSString *firstPhone = [[companyPhone componentsSeparatedByString:@","] firstObject];
                        [contactArray addObject:firstPhone];
                    }
                    contact = [contactArray componentsJoinedByString:@" "];
                }
                cell.labText.numberOfLines = 1;
                cell.labText.text = contact.length <= 0 ? @"未添加" : contact;
                cell.alcBottomLineLeading.constant = 50;
                cell.ivArrow.hidden = NO;
                cell.ivIcon.hidden = indexPath.row != 0;
                cell.ivIcon.image = [UIImage imageNamed:@"public_btn_contacts"];
                return cell;
            }
        }
        case 3:
        {
            // 电话
            ZbQySingleInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QySingleInfoCell"];
            NSString *tel = self.curQyDetailDict[@"company_tel"];
            cell.labText.numberOfLines = 1;
            cell.labText.text = tel;
            cell.alcBottomLineLeading.constant = 15;
            cell.ivIcon.hidden = NO;
            cell.ivArrow.hidden = NO;
            cell.ivIcon.image = [UIImage imageNamed:@"public_btn_phone_gray"];
            return cell;
        }
        case 4:
        {
            // 地址
            if (self.otherAddrArray.count > 0 && !self.isAddrExpand) {
                if (indexPath.row == 0) {
                    ZbQySingleInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QySingleInfoCell"];
                    // 主地址
                    NSString *prov = self.curQyDetailDict[@"company_prov"];
                    NSString *city = self.curQyDetailDict[@"company_city"];
                    NSString *area = self.curQyDetailDict[@"company_area"];
                    NSString *addrShow = [NSString stringWithFormat:@"%@%@%@", prov, city, area];
                    cell.labText.numberOfLines = 0;
                    cell.labText.text = addrShow.length <= 0 ? @"未添加" : addrShow;
                    cell.alcBottomLineLeading.constant = 50;
                    cell.ivIcon.hidden = NO;
                    cell.ivArrow.hidden = NO;
                    cell.ivIcon.image = [UIImage imageNamed:@"public_btn_location_bla"];
                    NSNumber *lng = self.curQyDetailDict[@"company_longitude"];
                    NSNumber *lat = self.curQyDetailDict[@"company_latitude"];
                    if (lng.integerValue > 0 && lat.integerValue > 0) {
                        cell.ivArrow.hidden = NO;
                    } else {
                        cell.ivArrow.hidden = YES;
                    }
                    return cell;
                } else {
                    // 展开项
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExpandInfoCell"];
                    UILabel *labText = (UILabel *)[cell viewWithTag:1];
                    labText.text = [NSString stringWithFormat:@"查看另外%lu个地址", (unsigned long)self.otherAddrArray.count];
                    return cell;
                }
            } else {
                // 地址
                ZbQySingleInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QySingleInfoCell"];
                NSString *addrShow;
                if (indexPath.row > 0) {
                    // 其他地址
                    NSDictionary *curAddrDict = [self.otherAddrArray objectAtIndex:(indexPath.row - 1)];
                    NSString *prov = curAddrDict[@"prov"];
                    NSString *city = curAddrDict[@"city"];
                    NSString *area = curAddrDict[@"area"];
                    addrShow = [NSString stringWithFormat:@"%@%@%@", prov, city, area];
                    cell.ivArrow.hidden = YES;
                } else {
                    // 主地址
                    NSString *prov = self.curQyDetailDict[@"company_prov"];
                    NSString *city = self.curQyDetailDict[@"company_city"];
                    NSString *area = self.curQyDetailDict[@"company_area"];
                    addrShow = [NSString stringWithFormat:@"%@%@%@", prov, city, area];
                    cell.ivArrow.hidden = NO;
                    NSNumber *lng = self.curQyDetailDict[@"company_longitude"];
                    NSNumber *lat = self.curQyDetailDict[@"company_latitude"];
                    if (lng.integerValue > 0 && lat.integerValue > 0) {
                        cell.ivArrow.hidden = NO;
                    } else {
                        cell.ivArrow.hidden = YES;
                    }
                }
                cell.labText.numberOfLines = 0;
                cell.labText.text = addrShow.length <= 0 ? @"未添加" : addrShow;
                cell.alcBottomLineLeading.constant = 50;
                cell.ivIcon.hidden = indexPath.row != 0;
                cell.ivIcon.image = [UIImage imageNamed:@"public_btn_location_bla"];
                return cell;
            }
        }
        case 5:
        {
            // 网址
            ZbQySingleInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QySingleInfoCell"];
            NSString *site = self.curQyDetailDict[@"company_site"];
            cell.labText.numberOfLines = 1;
            cell.labText.text = site;
            cell.alcBottomLineLeading.constant = 0;
            cell.ivIcon.hidden = NO;
            cell.ivArrow.hidden = NO;
            cell.ivIcon.image = [UIImage imageNamed:@"public_btn_link"];
            return cell;
        }
        case 6:
        {
            // 主营石材
            ZbStoneSimilarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:StoneSimilarCellIdentifier forIndexPath:indexPath];
            [cell setStoneArray:self.stoneArray];
            cell.stoneClickBlock = ^(NSInteger stoneIndex, NSDictionary *stoneDict) {
                // 石材点击
                ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
                NSString *stoneName = stoneDict[@"stone_name"];
                controller.stoneName = stoneName;
                [self.navigationController pushViewController:controller animated:YES];
            };
            return cell;
        }
        case 7:
        {
            // 主营产品
            ZbFindProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FindProductCellIdentifier forIndexPath:indexPath];
            NSDictionary *curDict = [self.productArray objectAtIndex:indexPath.row];
            [curDict setValue:self.curQyDetailDict[@"company_level"] forKey:@"company_level"];
            [curDict setValue:self.curQyDetailDict[@"company_name"] forKey:@"company_name"];
            [cell setInfoDict:curDict];
            return cell;
        }
        default:
            return nil;
    }
}

// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            return 0;
        case 6:
        {
            if (self.stoneArray.count <= 0) {
                return 0;
            }
            return [ZbTypeTableViewCell getRowHeight];
        }
        case 7:
        {
            if (self.productArray.count <= 0) {
                return 0;
            }
            return [ZbTypeTableViewCell getRowHeight];
        }
            
        default:
            return 0;
    }
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
    switch (section) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            return nil;
        case 6:
        {
            if (self.stoneArray.count <= 0) {
                return [[UIView alloc] init];
            }
            headerCell.labTypeName.text = @"主营石材";
            // 主营石材支持点击
            headerCell.btnClick.hidden = NO;
            headerCell.ivArrow.hidden = NO;
            headerCell.clickBlock = ^(void) {
                if (self.stoneArray.count <= 0) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"该公司未添加主营石材" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
                UIStoryboard *mine = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
                ZbLinkStoneViewController *controller = [mine instantiateViewControllerWithIdentifier:@"ZbLinkStoneViewController"];
                controller.StoneArray = self.stoneArray;
                [self.navigationController pushViewController:controller animated:YES];
            };
            break;
        }
        case 7:
        {
            if (self.productArray.count <= 0) {
                return [[UIView alloc] init];
            }
            headerCell.labTypeName.text = @"主营产品";
            headerCell.btnClick.hidden = YES;
            headerCell.ivArrow.hidden = YES;
            break;
        }
            
        default:
            break;
    }
    return headerCell;
}

// section尾部高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [ZbSeparateTableViewCell getRowHeight];
        case 1:
            return [ZbSeparateTableViewCell getRowHeight];
        case 2:
            return 0;
        case 3:
            return 0;
        case 4:
            return 0;
        case 5:
            return [ZbSeparateTableViewCell getRowHeight];
        case 6:
        {
            if (self.stoneArray.count <= 0 || self.productArray.count <= 0) {
                return 0;
            }
            return [ZbSeparateTableViewCell getRowHeight];
        }
        case 7:
            return 0;
            
        default:
            return 0;
    }
}

// section尾部view
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UITableViewCell *footerCell;
    switch (section) {
        case 0:
        {
            footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
            break;
        }
        case 1:
        {
            footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
            break;
        }
        case 2:
        {
            return [[UIView alloc] init];
        }
        case 3:
        {
            return [[UIView alloc] init];
        }
        case 4:
        {
            return [[UIView alloc] init];
        }
        case 5:
        {
            footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
            break;
        }
        case 6:
        {
            if (self.stoneArray.count <= 0 || self.productArray.count <= 0) {
                return [[UIView alloc] init];
            }
            footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
            break;
        }
        case 7:
        {
            return [[UIView alloc] init];
        }
        default:
            break;
    }
    return footerCell;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
            //  企业简介
            break;
        case 1:
            // 经营范围
            break;
        case 2:
        {
            // 联系人
            if (self.otherContactArray.count > 0 && !self.isContactExpand) {
                if (indexPath.row == 0) {
                    // 主联系人
                    NSString *companyPhone = self.curQyDetailDict[@"company_phone"];
                    NSString *firstPhone = [[companyPhone componentsSeparatedByString:@","] firstObject];
                    if (firstPhone != nil && firstPhone.length > 0) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", firstPhone]]];
                    }
                } else {
                    // 展开项点击
                    self.isContactExpand = YES;
                    [tableView reloadData];
                }
            } else {
                if (indexPath.row > 0) {
                    // 其他联系人
                    NSDictionary *curContactDict = [self.otherContactArray objectAtIndex:(indexPath.row - 1)];
                    NSString *phone = curContactDict[@"phone"];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", phone]]];
                } else {
                    // 主联系人
                    NSString *companyPhone = self.curQyDetailDict[@"company_phone"];
                    NSString *firstPhone = [[companyPhone componentsSeparatedByString:@","] firstObject];
                    if (firstPhone != nil && firstPhone.length > 0) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", firstPhone]]];
                    }
                }
            }
            break;
        }
        case 3:
        {
            // 电话
            NSString *tel = self.curQyDetailDict[@"company_tel"];
            if (tel.length > 0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", tel]]];
            }
            break;
        }
        case 4:
        {
            // 地址
            if (self.otherAddrArray.count > 0 && !self.isAddrExpand) {
                if (indexPath.row == 0) {
                    // 地址
                    NSNumber *lng = self.curQyDetailDict[@"company_longitude"];
                    NSNumber *lat = self.curQyDetailDict[@"company_latitude"];
                    if (lng.integerValue > 0 && lat.integerValue > 0) {
                        ZbQyPosViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyPosViewController"];
                        controller.curCompanyDict = self.curQyDetailDict;
                        [self.navigationController pushViewController:controller animated:YES];
                    }
                } else {
                    // 展开项点击
                    self.isAddrExpand = YES;
                    [tableView reloadData];
                }
            } else {
                // 地址
                if (indexPath.row == 0) {
                    NSNumber *lng = self.curQyDetailDict[@"company_longitude"];
                    NSNumber *lat = self.curQyDetailDict[@"company_latitude"];
                    if (lng.integerValue > 0 && lat.integerValue > 0) {
                        ZbQyPosViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyPosViewController"];
                        controller.curCompanyDict = self.curQyDetailDict;
                        [self.navigationController pushViewController:controller animated:YES];
                    }
                } else {
                    
                }
            }
            break;
        }
        case 5:
        {
            // 网址
            NSString *site = self.curQyDetailDict[@"company_site"];
            if (site.length > 0) {
                ConsWebController *webController = [[ConsWebController alloc] init];
                webController.mainUrl = site;
                [self.navigationController pushViewController:webController animated:YES];
            }
            break;
        }
        case 6:
        {
            // 主营石材
            break;
        }
        case 7:
        {
            // 主营产品
            NSDictionary *curDict = [self.productArray objectAtIndex:indexPath.row];
            ZbProductDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
            NSString *productId = curDict[@"product_id"];
            controller.productId = productId;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
            
        default:
            break;
    }
    return;
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 清公司小图缓存
    NSString *smallImgUrl = [ZbWebService getImageFullUrl:self.curQyDetailDict[@"company_image_small"]];
    [[SDWebImageManager sharedManager].imageCache removeImageForKey:smallImgUrl withCompletion:nil];
    NSString *imgUrl = [ZbWebService getImageFullUrl:self.curQyDetailDict[@"company_image"]];
    [[SDWebImageManager sharedManager].imageCache removeImageForKey:imgUrl withCompletion:nil];
    
    [self requestStoneListData];
    [self requestProductListData];
    [self requestAddrData];
    [self requestContactData];
    [self requestQyDetailData];
}

#pragma mark - YKuImageInLoopScrollViewDelegate
- (int)numOfPageForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    NSArray *companyImages = self.curQyDetailDict[@"company_images"];
    int count = (int)(companyImages.count);
    return MAX(1, count);
}

- (int)widthForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    int width = DeviceWidth;
    return width;
}

- (UIView *)scrollView:(YKuImageInLoopScrollView *)ascrollView viewAtPageIndex:(int)apageIndex
{
    // 图片数据
    NSArray *companyImages = self.curQyDetailDict[@"company_images"];
    NSString *imgUrl = companyImages.count <= 0 ? @"no.exists.image.url" : [companyImages objectAtIndex:apageIndex];
    // 图片控件
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    UIImageView *curImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CGRectGetHeight(ascrollView.frame))];
    [curImageView sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]]];
    curImageView.contentMode = UIViewContentModeScaleAspectFill;
    [bgView addSubview:curImageView];
    return bgView;
}

- (void)scrollView:(YKuImageInLoopScrollView*) ascrollView didTapIndex:(int)apageIndex{
    NSArray *companyImages = self.curQyDetailDict[@"company_images"];
    if (companyImages.count <= 0) {
        return;
    }
    // 图片游览器
    ZLPhotoPickerBrowserViewController *pickerBrowser = [[ZLPhotoPickerBrowserViewController alloc] init];
    // 淡入淡出效果
    // pickerBrowser.status = UIViewAnimationAnimationStatusFade;
    //    pickerBrowser.toView = btn;
    // 数据源/delegate
    pickerBrowser.delegate = self;
    pickerBrowser.dataSource = self;
    // 当前选中的值
    pickerBrowser.currentIndexPath = [NSIndexPath indexPathForItem:apageIndex inSection:0];
    // 展示控制器
    [pickerBrowser showPickerVc:self.navigationController];
}

/*
 选中第几页
 @param didSelectedPageIndex 选中的第几项，[0-numOfPageForScrollView];
 */
-(void) scrollView:(YKuImageInLoopScrollView*) ascrollView didSelectedPageIndex:(int) apageIndex
{
    //    NSLog(@"didSelectedPageIndex:%d",apageIndex);
}

#pragma mark - <ZLPhotoPickerBrowserViewControllerDataSource>
- (long)numberOfSectionInPhotosInPickerBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser{
    return 1;
}

- (long)photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser numberOfItemsInSection:(NSUInteger)section{
    NSArray *companyImages = self.curQyDetailDict[@"company_images"];
    return (int)(companyImages.count);
}

#pragma mark - 每个组展示什么图片,需要包装下ZLPhotoPickerBrowserPhoto
- (ZLPhotoPickerBrowserPhoto *) photoBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser photoAtIndexPath:(NSIndexPath *)indexPath{
    // 图片数据
    NSArray *companyImages = self.curQyDetailDict[@"company_images"];
    NSString *imgUrl = [companyImages objectAtIndex:indexPath.row];
    
    ZLPhotoPickerBrowserPhoto *photo = [[ZLPhotoPickerBrowserPhoto alloc] init];
    photo.photoURL = [NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]];
    
    //    photo.toView = self.imageLoopContainer;
    return photo;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    [self.tv.header endRefreshing];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    [self.hud hide:NO];
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        [self.tv.header endRefreshing];
        return;
    }
    
    if (connection.tag == HTTP_REQUEST_TAG_STONE) {
        NSMutableArray *array = resultDic[@"data"];
        [self.stoneArray removeAllObjects];
        [self.stoneArray addObjectsFromArray:array];
        
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_PRODUCT) {
        NSMutableArray *array = resultDic[@"data"];
        [self.productArray removeAllObjects];
        [self.productArray addObjectsFromArray:array];
        
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_MORE_ADDR) {
        NSMutableArray *array = resultDic[@"data"];
        [self.otherAddrArray removeAllObjects];
        [self.otherAddrArray addObjectsFromArray:array];
        
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_MORE_CONTACT) {
        NSMutableArray *array = resultDic[@"data"];
        [self.otherContactArray removeAllObjects];
        [self.otherContactArray addObjectsFromArray:array];
        
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_INFO) {
        self.curQyDetailDict = resultDic[@"data"];
        [ZbWebService filterNullData:self.curQyDetailDict];
        
        [self.tv.header endRefreshing];
        [self drawDetailInfo];
        [self.tv reloadData];
        [self.imageLoopView reloadData];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
