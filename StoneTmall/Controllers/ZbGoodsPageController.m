//
//  ZbGoodsPageController.m
//  StoneTmall
//
//  Created by chyo on 15/9/7.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbGoodsPageController.h"
#import "ZbProductTableViewCell.h"
#import "ZbProductDetailViewController.h"

@interface ZbGoodsPageController ()

@property (nonatomic, strong) IBOutlet UITableView *tv;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic) ZbCxl_Type zbcxlType;


@end

@implementation ZbGoodsPageController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [[NSMutableArray alloc] init];
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbProductTableViewCell" bundle:nil] forCellReuseIdentifier:ProductCellIdentifier];
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)setControllerType:(ZbCxl_Type)ZbCxlType {
    _zbcxlType = ZbCxlType;
    if (ZbCxlType == ZbCxl_Type_comme) {
        //推荐
    } else if (ZbCxlType == ZbCXl_Type_colle) {
        //收藏
        [self loadData];
    } else if (ZbCxlType == ZbCxl_Type_invol) {
        //参与
    }
}

#pragma mark - 读取数据
// 读取收藏数据
- (void)loadData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMyMarkProductList] delegate:self];
    [conn start];
}

#pragma mark - TableViewDelegate &Datasource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbProductTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZbProductTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:ProductCellIdentifier];
    // 绘制
    NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
    [cell setInfoDict:dict];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ZbProductDetailViewController *controller = [main instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
    NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
    NSString *productId = curDict[@"mark_eid"];
    controller.productId = productId;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 重置选项
    if (_zbcxlType == ZbCxl_Type_comme) {
        //推荐
    } else if (_zbcxlType == ZbCXl_Type_colle) {
        //收藏
        [self loadData];
    } else if (_zbcxlType == ZbCxl_Type_invol) {
        //参与
    }
}


#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (result.integerValue == 0) {
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:(NSArray *)resultDic[@"data"]];
        [self.tv reloadData];
    }
}

@end
