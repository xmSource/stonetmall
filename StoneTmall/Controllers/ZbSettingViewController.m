//
//  ZbSettingViewController.m
//  StoneTmall
//
//  Created by Apple on 15/8/31.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSettingViewController.h"
#import "ZbTelValidateViewController.h"
#import "ZbChangePwdViewController.h"
#import "ZbShipAdrViewController.h"

@interface ZbSettingViewController ()<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tv;
@property (nonatomic) int sum;

@end

@implementation ZbSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.sum = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

#pragma mark - 读取数据
- (void)loadData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getPostAddressList] delegate:self];
    [conn start];
}

- (void)uploadImage:(UIImage *)image {
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    NSDictionary *params = [ZbWebService bindMyCard];
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        // 图片压缩
        NSData* imageData = UIImageJPEGRepresentation(image, 0.4);
        NSString *fileName = [NSString stringWithFormat:@"sg_image"];
        
        // 图片文件流
        [formData appendPartWithFileData:imageData name:fileName fileName:fileName mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"%@", responseObject);
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = @"修改失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        // 审核地址变更
        NSMutableDictionary *dict = [ZbWebService sharedUser];
        dict[@"user_card"] = resultDic[@"data"][@"fullname"];
        dict[@"user_card_ok"] = [NSString stringWithFormat:@"%@", [NSNumber numberWithInt:Zb_User_Verify_Status_Status_Watting]];
        
        [self.tv reloadData];
        self.hud.labelText = @"修改成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"修改失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
    return;
}

#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    if (buttonIndex == 0) {
        // 拍照
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        // 相册选择
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
    });
    return;
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^() {
        self.hud.labelText = @"努力提交中..";
        [self.hud show:NO];
        
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        CGSize imgSize = portraitImg.size;
        CGSize tagSize = imgSize;
        CGFloat rate = imgSize.width / imgSize.height;
        if (imgSize.width >= imgSize.height) {
            if (imgSize.width > 1000) {
                tagSize = CGSizeMake(1000, 1000 / rate);
            }
        } else {
            if (imgSize.height > 1000) {
                tagSize = CGSizeMake(1000 * rate, 1000);
            }
        }
        // 去除图片旋转属性
        UIGraphicsBeginImageContext(tagSize);
        [portraitImg drawInRect:CGRectMake(0, 0, tagSize.width, tagSize.height)];
        portraitImg = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        UITableViewCell *cell = [self.tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
        UIImageView *imgLicense = (UIImageView *)[cell viewWithTag:2];
        imgLicense.image = portraitImg;
        [self uploadImage:portraitImg];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    return;
}

#pragma mark - UITableViewDataSource
// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0f;
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"sectionHeaderCell" ];
    // 父控件名称
    UILabel *labSectionName = (UILabel *)[headerCell viewWithTag:1];
    switch (section) {
            case 0:
            {
                labSectionName.text = @"账户信息";
                break;
            }
            case 1:
            {
                labSectionName.text = @"身份认证";
                break;
            }
            case 2:
            {
                labSectionName.text = @"收货地址";
                break;
            }
        }
    return headerCell;
}

// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowofSec = 0;
    switch (section) {
        case 0:
            rowofSec = 2;
            break;
            
        case 1:
            rowofSec = 3;
            break;
            
        case 2:
            rowofSec = 1;
            break;
            
        default:
            break;
    }
    return rowofSec;
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return  45;
            break;
            
        case 1:
            if (indexPath.row == 2) {
                return 70;
            } else {
                return  45;
            }
            break;
            
        case 2:
            return 45;
            break;
                        
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    NSDictionary *dict = [ZbWebService sharedUser];
    
    switch (indexPath.section) {
        case 0: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"userInfoCell" forIndexPath:indexPath];
            UILabel *labName = (UILabel *)[cell viewWithTag:1];
            UILabel *labdetail = (UILabel *)[cell viewWithTag:2];
            UILabel *labuserId = (UILabel *)[cell viewWithTag:3];
            UIImageView *backFlag = (UIImageView *)[cell viewWithTag:4];
            switch (indexPath.row) {
                case 0:
                    labName.text = @"用户名";
                    labuserId.text = dict[@"user_name"];
                    labdetail.hidden = YES;
                    labuserId.hidden = NO;
                    backFlag.hidden = YES;
                    break;
                    
                case 1:
                    labName.text = @"登录密码";
                    labdetail.text = @"● ● ● ● ● ●";
                    labdetail.hidden = NO;
                    labuserId.hidden = YES;
                    break;
                    
                default:
                    break;
                }
            }
            break;

            
        case 1: {
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"userInfoCell" forIndexPath:indexPath];
                UILabel *labName = (UILabel *)[cell viewWithTag:1];
                UILabel *labdetail = (UILabel *)[cell viewWithTag:2];
                UILabel *labuserId = (UILabel *)[cell viewWithTag:3];
                UIImageView *backFlag = (UIImageView *)[cell viewWithTag:4];
                backFlag.hidden = NO;
                labName.text = @"手机号";
                labdetail.text = dict[@"user_phone"];
                labuserId.hidden = YES;
                labdetail.hidden = NO;


            } else if (indexPath.row == 1) {
                NSNumber *user_weixin_openid = dict[@"user_weixin_openid"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"userInfoCell" forIndexPath:indexPath];
                UILabel *labName = (UILabel *)[cell viewWithTag:1];
                UILabel *labdetail = (UILabel *)[cell viewWithTag:2];
                UILabel *labuserId = (UILabel *)[cell viewWithTag:3];
                UIImageView *backFlag = (UIImageView *)[cell viewWithTag:4];
                backFlag.hidden = YES;
                labName.text = @"微信绑定";
                labuserId.text = user_weixin_openid.integerValue == 1 ? @"已绑定" : @"未绑定";
                labuserId.hidden = NO;
                labdetail.hidden = YES;

            } else if (indexPath.row == 2) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"userIdCell" forIndexPath:indexPath];
                UILabel *labName = (UILabel *)[cell viewWithTag:1];
                UIImageView *userImge = (UIImageView *)[cell viewWithTag:2];
                UILabel *flagLab = (UILabel *)[cell viewWithTag:10];
                userImge.layer.borderWidth = 1;
                userImge.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
                labName.text = @"身份证";
                [userImge sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:dict[@"user_card"]]]];
                NSString *user_card_ok = dict[@"user_card_ok"];
                if (user_card_ok.intValue == Zb_User_Verify_Status_Status_Default) {
                    userImge.hidden = YES;
                    flagLab.hidden = YES;
                } else if (user_card_ok.intValue == Zb_User_Verify_Status_Status_Fail) {
                    flagLab.hidden = NO;
                    userImge.hidden = NO;
                    flagLab.text = @"审核失败";
                } else if (user_card_ok.intValue == Zb_User_Verify_Status_Status_Success) {
                    flagLab.hidden = NO;
                    userImge.hidden = NO;
                    flagLab.text = @"审核通过";
                } else if (user_card_ok.intValue == Zb_User_Verify_Status_Status_Watting) {
                    flagLab.hidden = NO;
                    userImge.hidden = NO;
                    flagLab.text = @"正在审核";
                } else {
                    flagLab.hidden = YES;
                    userImge.hidden = YES;
                    }
                }
            }
            break;
            
        case 2:{
            cell = [tableView dequeueReusableCellWithIdentifier:@"userInfoCell" forIndexPath:indexPath];
            UILabel *labName = (UILabel *)[cell viewWithTag:1];
            UILabel *labNum = (UILabel *)[cell viewWithTag:2];
            UILabel *labuserId = (UILabel *)[cell viewWithTag:3];
            UIImageView *backFlag = (UIImageView *)[cell viewWithTag:4];
            backFlag.hidden = NO;
            
            labName.text = @"收货地址";
            labNum.text = [NSString stringWithFormat:@"%d个", self.sum];
            labuserId.hidden = YES;
            labNum.hidden = NO;
            break;
            
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 1) {
        UIStoryboard *minesb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
        ZbChangePwdViewController *controller = [minesb instantiateViewControllerWithIdentifier:@"ZbChangePwdViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        UIStoryboard *loginsb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        ZbTelValidateViewController *controller = [loginsb instantiateViewControllerWithIdentifier:@"ZbTelValidateViewController"];
        controller.curValidateType = Zb_TelValidate_Type_BindTel;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 1 && indexPath.row == 2) {
        NSDictionary *dict = [ZbWebService sharedUser];
        NSString *user_card_ok = dict[@"user_card_ok"];
        if (user_card_ok.intValue == Zb_User_Verify_Status_Status_Success || user_card_ok.intValue == Zb_User_Verify_Status_Status_Watting) {
            return;
        }
        
        UIActionSheet *actionSheet= [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册中选择", nil];
        [actionSheet showInView:self.view];
    
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        UIStoryboard *minesb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
        ZbShipAdrViewController *controller = [minesb instantiateViewControllerWithIdentifier:@"ZbShipAdrViewController"];
        [controller setControllerType:Zb_Adr_PerSon];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSString *result = dict[@"errno"];
    if (result.integerValue == 0) {
        NSArray *array = dict[@"data"];
        self.sum = (int)array.count;
        [self.tv reloadData];
    }
}


@end
