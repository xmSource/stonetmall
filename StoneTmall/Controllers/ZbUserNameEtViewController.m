//
//  ZbUserNameEtViewController.m
//  StoneTmall
//
//  Created by chyo on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbUserNameEtViewController.h"

@interface ZbUserNameEtViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *userName;


@end

@implementation ZbUserNameEtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.userName.text = self.name;
}

- (void)loadData {
    NSDictionary *dic = [ZbWebService sharedUser];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getUserInfoParams:dic[@"user_name"]] delegate:self];
    [conn start];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - UITextFiele Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - 修改用户昵称
- (IBAction)saveInfo:(id)sender {
    if ([self.userName.text isEqualToString:self.name] || self.userName.text.length == 0) {
        self.hud.labelText = @"没有修改,无需保存";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    [self.view endEditing:YES];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editMyNickname:self.userName.text] delegate:self];
    [conn start];
    
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"修改失败,请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (result.integerValue != 0) {
        return;
    }

    if (result.integerValue == 0) {
        self.hud.labelText = @"修改成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
}

@end
