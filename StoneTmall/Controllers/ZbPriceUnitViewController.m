//
//  ZbPriceUnitViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/11/1.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbPriceUnitViewController.h"
#import "ZbDatePickViewController.h"

#define HTTP_REQUEST_TAG_UNITLIST 1                    // 价格单位列表

@interface ZbPriceUnitViewController () <UITextFieldDelegate, PopoverViewDelegate>

// 价格输入框
@property (nonatomic, strong) IBOutlet UITextField *tfPrice;
// 单位输入框
@property (nonatomic, strong) IBOutlet UITextField *tfPriceUnit;
// 保存按钮
@property (nonatomic, strong) IBOutlet UIButton *btnSave;
// 添加弹出框
@property (nonatomic, strong) PopoverView *pv;

// 当前计量单位
@property (nonatomic, strong) NSString *curPriceUnit;

@end

@implementation ZbPriceUnitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 按钮圆角
    self.btnSave.layer.cornerRadius = 2;
    self.btnSave.layer.masksToBounds = YES;
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 加载价格单位列表
    [self getPriceUnitList];
    
    // 保存按钮默认不激活
    [self setBtnSaveEnable:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 设置保存按钮激活状态
- (void)setBtnSaveEnable:(BOOL)isEnable {
    self.btnSave.enabled = isEnable;
    self.btnSave.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

#pragma mark - 接口调用
// 价格列表
- (void)getPriceUnitList {
    NSArray *priceUnitArray = [ZbSaveManager getPriceUnitArray];
    if (priceUnitArray != nil) {
        // 已加载过计量单位数据
        return;
    }
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getPriceUnitList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNITLIST;
    [conn start];
    return;
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

// 单位选择按钮点击
- (IBAction)btnUnitSelectOnClick:(id)sender {
    [self.view endEditing:YES];
    NSArray *priceUnitArray = [ZbSaveManager getPriceUnitArray];
    if (priceUnitArray == nil) {
        return;
    }
    
    ZbDatePickViewController *picker = [ZbDatePickViewController showInViewController:self dataArray:priceUnitArray btnClickBlock:^(Zb_Dp_Click_Type ZbClickType, NSString *str) {
        switch (ZbClickType) {
            case Zb_Dp_Click_Cancel:
                return ;
                break;
                
            case Zb_Dp_Click_Sure:
            {
                self.tfPriceUnit.text = [NSString stringWithFormat:@"/%@", str];
                [self textFieldChanged:self.tfPriceUnit];
                self.curPriceUnit = str;
                break;
            }
            default:
                break;
        }
    }];
    picker.labHint.text = @"选择计量单位";
//    CGPoint point = [self.tfPriceUnit convertPoint:CGPointMake(CGRectGetMidX(self.tfPriceUnit.bounds), CGRectGetMaxY(self.tfPriceUnit.bounds) - 8) toView:self.view];
//    self.pv = [PopoverView showPopoverAtPoint:point
//                                       inView:self.view
//                              withStringArray:priceUnitArray
//                                     delegate:self];
    return;
}

// 保存按钮点击
- (IBAction)btnSaveOnClick:(id)sender {
    if (self.selectOkBlock) {
        self.selectOkBlock(self.tfPrice.text, self.curPriceUnit);
    }
    [self.navigationController popViewControllerAnimated:YES];
    return;
}

#pragma mark - UITextField delegate
// 文本变化
- (IBAction)textFieldChanged:(id)sender {
    if (self.tfPrice.text.length > 0 && self.tfPriceUnit.text.length > 0) {
        [self setBtnSaveEnable:YES];
    } else {
        [self setBtnSaveEnable:NO];
    }
    return;
}

#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    NSArray *priceUnitArray = [ZbSaveManager getPriceUnitArray];
    NSString *unit = [priceUnitArray objectAtIndex:index];
    self.tfPriceUnit.text = [NSString stringWithFormat:@"/%@", unit];
    [self textFieldChanged:self.tfPriceUnit];
    self.curPriceUnit = unit;
    
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.1f];
}

- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    self.pv = nil;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_TAG_UNITLIST) {
        // 加载失败
        if (result.integerValue != 0) {
            return;
        }
        NSMutableArray *dataArray = resultDic[@"data"];
        [ZbSaveManager setPriceUnitArray:dataArray];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
