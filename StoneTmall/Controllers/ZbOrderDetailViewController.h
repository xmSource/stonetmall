//
//  ZbOrderDetailViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/11/24.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbOrderDetailViewController : BaseViewController

@property (nonatomic, weak) NSDictionary *curOrderDict;

@end
