//
//  ZbNoticeViewController.m
//  StoneTmall
//
//  Created by chyo on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbNoticeViewController.h"
#import "ZbNoticeTableViewCell.h"
#import "UITableView+FDTemplateLayoutCell.h"

#define HTTP_REQUEST_TAG_DATA 0         // 重载数据
#define HTTP_REQUEST_TAG_MORE 1         // 加载更多数据

@interface ZbNoticeViewController ()<UITableViewDelegate, UITableViewDataSource>

//通知数组
@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic, strong) IBOutlet UITableView *tv;

// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

@end

@implementation ZbNoticeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array = [NSMutableArray array];
    
    __weak typeof(self) weakSelf = self;
    //添加上下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 接口调用
// 重载数据
- (void)requestData {
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMyMessageList:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DATA;
    [conn start];
    return;
}

// 加载更多数据
- (void)requestMoreData {
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMyMessageList:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MORE;
    [conn start];
    return;
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.array.count;
}

/**
 *  设置cell的内容
 *
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZbNoticeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noticeCell" forIndexPath:indexPath];

    NSDictionary *curNotiDict = [self.array objectAtIndex:indexPath.row];
    
    [cell setNoticeDict:curNotiDict];
    
    return cell;
}

/**
 *  设置cell的高度
 *
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *curDict = [self.array objectAtIndex:indexPath.row];
    NSString *content = curDict[@"message_body"];
    
    if (content.length > 30) {
        CGFloat height = [tableView fd_heightForCellWithIdentifier:NoticeCellIdentifier configuration:^(ZbNoticeTableViewCell *cell) {
            [cell setNoticeDictForCalc:curDict];
        }];
        return height;
    }
    return 70;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    return;
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    [self requestData];
}

- (void)footerRefresh {
    [self requestMoreData];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        [self.tv.header endRefreshing];
        [self.tv.footer endRefreshing];
        return;
    }
    
    NSMutableArray *dataArray = resultDic[@"data"];
    if (connection.tag == HTTP_REQUEST_TAG_DATA) {
        NSMutableArray *notiArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:notiArray];
        
        if (notiArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        
        [self.hud hide:NO];
        [self.tv reloadData];
        if (self.array.count > 0) {
            CGRect rectTop = CGRectMake(0, 0, 1, 1);
            [self.tv scrollRectToVisible:rectTop animated:YES];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_MORE) {
        // 上拉加载更多
        [self.tv.footer endRefreshing];
        NSMutableArray *notiArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.array addObjectsFromArray:notiArray];
        if (notiArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    }
}

@end
