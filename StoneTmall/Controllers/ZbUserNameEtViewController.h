//
//  ZbUserNameEtViewController.h
//  StoneTmall
//
//  Created by chyo on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbUserNameEtViewController : BaseViewController

@property (nonatomic, strong) NSString *name;

@end
