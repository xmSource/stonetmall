//
//  ZbRewardJoinViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/20.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardJoinViewController.h"
#import "AFNetworking.h"
#import "ConsWebController.h"

#define HTTP_REQUEST_TAG_SEND 5               // 投标回答

@interface ZbRewardJoinViewController () <UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

// 描述输入框
@property (nonatomic, strong) IBOutlet UITextView *tvDes;
// 描述输入框高度约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvDesHeight;
// 描述提示文字
@property (nonatomic, strong) IBOutlet UILabel *labDesPlaceHolder;
// 已读条款按钮
@property (nonatomic, strong) IBOutlet UIButton *btnRead;
// 提交按钮
@property (nonatomic, strong) IBOutlet UIButton *btnCommit;

// 选择图片按钮数组
@property (nonatomic, strong) IBOutletCollection(UIControl) NSArray *imgPickBtnArray;

// 已选择图片列表
@property (nonatomic, strong) NSMutableArray *imageArray;

@end

@implementation ZbRewardJoinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageArray = [NSMutableArray array];
    
    // 边框和圆角
    self.tvDes.layer.borderWidth = 1;
    self.tvDes.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.tvDes.layer.cornerRadius = 3;
    self.tvDes.layer.masksToBounds = YES;
    self.btnCommit.layer.cornerRadius = 3;
    self.btnCommit.layer.masksToBounds = YES;

    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 适配iphone4s
    if (!Greater_Than_480_Height) {
        self.alcTvDesHeight.constant = 100;
    }
    
    // 默认不激活发布按钮
    [self setBtnCommitEnable:NO];
    
    // 只支持一张图片
    for (NSInteger i = 0; i < self.imgPickBtnArray.count; i++) {
        // 显隐
        UIButton *imgPickBtn = [self.imgPickBtnArray objectAtIndex:i];
        imgPickBtn.hidden = YES;
    }
    UIButton *first = [self.imgPickBtnArray firstObject];
    self.imgPickBtnArray = @[first];
    [self drawPickImage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 设置发布按钮激活状态
- (void)setBtnCommitEnable:(BOOL)isEnable {
    self.btnCommit.enabled = isEnable;
    self.btnCommit.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

#pragma mark - 界面绘制
// 选择图片信息绘制
- (void)drawPickImage {
    
//    UIButton *imgPickBtn = [self.imgPickBtnArray firstObject];
//    if (self.imageArray.count > 0) {
//        // 图片
//        UIImage *image = [self.imageArray firstObject];
//        [imgPickBtn setBackgroundImage:image forState:UIControlStateNormal];
//    } else {
//        UIImage *image = [UIImage imageNamed:@"public_pic_add"];
//        [imgPickBtn setBackgroundImage:image forState:UIControlStateNormal];
//    }
    for (NSInteger i = 0; i < self.imgPickBtnArray.count; i++) {
        // 显隐
        UIButton *imgPickBtn = [self.imgPickBtnArray objectAtIndex:i];
        imgPickBtn.hidden = i > self.imageArray.count;
        
        // 图片
        UIImage *image = i < self.imageArray.count ? [self.imageArray objectAtIndex:i] : [UIImage imageNamed:@"public_pic_add"];
        [imgPickBtn setBackgroundImage:image forState:UIControlStateNormal];
    }
    return;
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

// 阅读按钮点击
- (IBAction)btnReadOnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    sender.selected = !sender.selected;
    
    // 发布按钮激活
    if (self.tvDes.text.length > 0 && self.btnRead.selected) {
        [self setBtnCommitEnable:YES];
    } else {
        [self setBtnCommitEnable:NO];
    }
    return;
}

// 免责协议点击
- (IBAction)btnItemsOnClick:(id)sender {
    ConsWebController *webController = [[ConsWebController alloc] init];
    webController.showTitle = @"免责协议";
    webController.mainUrl = JOIN_REWARD_ITEMS_URL;
    [self.navigationController pushViewController:webController animated:YES];
    return;
}

// 选择图片点击
- (IBAction)btnPickImageOnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    
    NSInteger index = sender.tag;
    if (index < self.imageArray.count) {
        return;
    }
    
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册选择", nil];
    action.tag = index;
    [action showInView:self.navigationController.view];
    return;
}

// 发布按钮点击
- (IBAction)btnPubOnClick:(id)sender {
    if (self.tvDes.text.length < 10) {
        self.hud.labelText = @"字数不足";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (self.tvDes.text.length > 700) {
        self.hud.labelText = @"字数过长";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    NSDictionary *params = [ZbWebService addPaidDemandComment:self.curPostId content:self.tvDes.text];
    
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        // 图片
        for (NSInteger i = 0; i < self.imageArray.count; i++) {
            UIImage *image = [self.imageArray objectAtIndex:i];
            // 图片压缩
            NSData* imageData = UIImageJPEGRepresentation(image, 0.4);
            NSString *fileName = i == 0 ? @"sg_image" : [NSString stringWithFormat:@"image_%ld", (long)i];
            // 图片文件流
            [formData appendPartWithFileData:imageData name:fileName fileName:fileName mimeType:@"image/jpeg"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        DLog(@"%@", resultDic);
        
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        // 发布成功block
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            self.pubOkBlock();
        });
        
        self.hud.labelText = @"参与成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"发布失败，请稍候重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
    return;
}

#pragma mark - UITextView delegate
- (void)textViewDidChange:(UITextView *)textView {
    self.labDesPlaceHolder.hidden = textView.text.length > 0;
    
    // 发布按钮激活
    if (textView.text.length > 0 && self.btnRead.selected) {
        [self setBtnCommitEnable:YES];
    } else {
        [self setBtnCommitEnable:NO];
    }
    return;
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    if (buttonIndex == 0) {
        // 拍照
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        // 相册选择
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
    });
    return;
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        CGSize imgSize = portraitImg.size;
        CGSize tagSize = imgSize;
        CGFloat rate = imgSize.width / imgSize.height;
        if (imgSize.width >= imgSize.height) {
            if (imgSize.width > 1000) {
                tagSize = CGSizeMake(1000, 1000 / rate);
            }
        } else {
            if (imgSize.height > 1000) {
                tagSize = CGSizeMake(1000 * rate, 1000);
            }
        }
        // 去除图片旋转属性
        UIGraphicsBeginImageContext(tagSize);
        [portraitImg drawInRect:CGRectMake(0, 0, tagSize.width, tagSize.height)];
        UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [self.imageArray addObject:reSizeImage];
        [self drawPickImage];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
