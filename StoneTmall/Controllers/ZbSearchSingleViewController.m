//
//  ZbSearchSingleViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSearchSingleViewController.h"
#import "ZbTypeTableViewCell.h"
#import "ZbStoneTableViewCell.h"
#import "ZbSupplyDemandTableViewCell.h"
#import "ZbQiyeTableViewCell.h"
#import "ZbProductTableViewCell.h"
#import "ZbSearchRecordTableViewCell.h"
#import "ZbIndexCharTableViewCell.h"
#import "ZbStoneDetailViewController.h"
#import "ZbQyDetailViewController.h"
#import "ZbProductDetailViewController.h"
#import "ZbSupplyDemandDetailViewController.h"
#import "ZbRewardDetailViewController.h"

#define HTTP_REQUEST_LOADDATA 0        // 下拉刷新数据
#define HTTP_REQUEST_LOADMORE 1        // 上拉加载更多数据

@interface ZbSearchSingleViewController () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

// 导航栏titleView
@property (nonatomic, strong) IBOutlet UIView *navTitleView;
// 导航栏搜索栏
@property (nonatomic, strong) IBOutlet UITextField *tfSearch;
// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// 搜索无记录view
@property (nonatomic, strong) IBOutlet UIView *noResultView;
// 搜索无记录描述文字label
@property (nonatomic, strong) IBOutlet UILabel *labNoResult;

// 历史记录数组
@property (nonatomic, strong) NSMutableArray *hisRecordArray;
// 搜索结果
@property (nonatomic, strong) NSMutableArray *resultArray;

// 是否搜索状态
@property (nonatomic, assign) BOOL isSearching;
// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

@end

@implementation ZbSearchSingleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hisRecordArray = Mutable_ArrayInit;
    self.resultArray = Mutable_ArrayInit;
    
    // 圆角
    self.navTitleView.layer.cornerRadius = CGRectGetHeight(self.navTitleView.frame) / 2;
    self.navTitleView.layer.masksToBounds = YES;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneTableViewCell" bundle:nil] forCellReuseIdentifier:StoneCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbTypeTableViewCell" bundle:nil] forCellReuseIdentifier:TypeHeaderIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbQiyeTableViewCell" bundle:nil] forCellReuseIdentifier:QiyeCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbProductTableViewCell" bundle:nil] forCellReuseIdentifier:ProductCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSupplyDemandTableViewCell" bundle:nil] forCellReuseIdentifier:SupplyDemandCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSearchRecordTableViewCell" bundle:nil] forCellReuseIdentifier:SearchRecordCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbIndexCharTableViewCell" bundle:nil] forCellReuseIdentifier:IndexCharCellIdentifier];
    
    //添加上拉加载更多
    __weak typeof(self) weakSelf = self;
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
    
    // 搜索无记录描述文字
    switch (self.curSearchType) {
        case Zb_SearchSingle_Type_Stone:
        {
            self.labNoResult.text = @"对不起，没有找到您要搜索的石材";
            break;
        }
        case Zb_SearchSingle_Type_Product:
        {
            self.labNoResult.text = @"对不起，没有找到您要搜索的产品";
            break;
        }
        case Zb_SearchSingle_Type_Qiye:
        {
            self.labNoResult.text = @"对不起，没有找到您要搜索的企业";
            break;
        }
        case Zb_SearchSingle_Type_SupllyDemand:
        {
            self.labNoResult.text = @"对不起，没有找到您要搜索的供需信息";
            break;
        }
            
        default:
            break;
    }
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
//    [self loadSearchHisRecordData];
//    [self.tfSearch becomeFirstResponder];
    self.tfSearch.text = self.searchKey;
    [self.tfSearch becomeFirstResponder];
    [self textFieldChanged:self.tfSearch];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 点击事件
// 返回点击
- (IBAction)leftBarItemOnClick:(id)sender {
    self.isSearching = NO;
    [self.tfSearch resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

// 取消点击
- (IBAction)btnCancelOnClick:(id)sender {
    self.isSearching = NO;
    [self.tfSearch resignFirstResponder];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - 接口调用
// 获取搜索记录
- (void)loadSearchHisRecordData {
    self.tv.footer.hidden = YES;
    switch (self.curSearchType) {
        case Zb_SearchSingle_Type_Stone:
            self.hisRecordArray = [NSMutableArray arrayWithArray:[[ZbSaveManager shareManager] getGlobalStoneSearchHistory]];
            break;
        case Zb_SearchSingle_Type_Product:
            self.hisRecordArray = [NSMutableArray arrayWithArray:[[ZbSaveManager shareManager] getGlobalProductSearchHistory]];
            break;
        case Zb_SearchSingle_Type_Qiye:
            self.hisRecordArray = [NSMutableArray arrayWithArray:[[ZbSaveManager shareManager] getGlobalQySearchHistory]];
            break;
        case Zb_SearchSingle_Type_SupllyDemand:
            self.hisRecordArray = [NSMutableArray arrayWithArray:[[ZbSaveManager shareManager] getGlobalSAndDSearchHistory]];
            break;
            
        default:
            break;
    }
    [self.tv reloadData];
}

// 获取数据列表接口
- (void)requestSearchData:(NSString *)key {
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    NSString *params = @"";
    switch (self.curSearchType) {
        case Zb_SearchSingle_Type_Stone:
            params = [ZbWebService globalStoneSearchParams:key pageIndex:page pageSize:SINGLE_SEARCH_PAGE_SIZE];
            break;
        case Zb_SearchSingle_Type_Product:
            params = [ZbWebService globalProductSearchParams:key pageIndex:page pageSize:SINGLE_SEARCH_PAGE_SIZE];
            break;
        case Zb_SearchSingle_Type_Qiye:
            params = [ZbWebService globalQySearchParams:key pageIndex:page pageSize:SINGLE_SEARCH_PAGE_SIZE];
            break;
        case Zb_SearchSingle_Type_SupllyDemand:
            params = [ZbWebService globalSAndDSearchParams:key pageIndex:page pageSize:SINGLE_SEARCH_PAGE_SIZE];
            break;
            
        default:
            break;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:params delegate:self];
    conn.tag = HTTP_REQUEST_LOADDATA;
    [conn start];
    return;
}

// 上拉加载更多数据列表接口
- (void)requestMoreSearchData:(NSString *)key {
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    NSString *params = @"";
    switch (self.curSearchType) {
        case Zb_SearchSingle_Type_Stone:
            params = [ZbWebService globalStoneSearchParams:key pageIndex:page pageSize:SINGLE_SEARCH_PAGE_SIZE];
            break;
        case Zb_SearchSingle_Type_Product:
            params = [ZbWebService globalProductSearchParams:key pageIndex:page pageSize:SINGLE_SEARCH_PAGE_SIZE];
            break;
        case Zb_SearchSingle_Type_Qiye:
            params = [ZbWebService globalQySearchParams:key pageIndex:page pageSize:SINGLE_SEARCH_PAGE_SIZE];
            break;
        case Zb_SearchSingle_Type_SupllyDemand:
            params = [ZbWebService globalSAndDSearchParams:key pageIndex:page pageSize:SINGLE_SEARCH_PAGE_SIZE];
            break;
            
        default:
            break;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:params delegate:self];
    conn.tag = HTTP_REQUEST_LOADMORE;
    [conn start];
    return;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 添加全局搜索历史记录
    NSString *key = textField.text;
    if (key.length > 0) {
        switch (self.curSearchType) {
            case Zb_SearchSingle_Type_Stone:
                [[ZbSaveManager shareManager] addGlobalStoneSearchHistory:key];
                break;
            case Zb_SearchSingle_Type_Product:
                [[ZbSaveManager shareManager] addGlobalProductSearchHistory:key];
                break;
            case Zb_SearchSingle_Type_Qiye:
                [[ZbSaveManager shareManager] addGlobalQySearchHistory:key];
                break;
            case Zb_SearchSingle_Type_SupllyDemand:
                [[ZbSaveManager shareManager] addGlobalSAndDSearchHistory:key];
                break;
                
            default:
                break;
        }
        [[ZbSaveManager shareManager] addGlobalSearchHistory:key];
    }
    return;
}

- (IBAction)textFieldChanged:(UITextField *)sender {
    if (sender.text.length <= 0) {
        // 历史记录
        self.isSearching = NO;
        [self loadSearchHisRecordData];
    } else {
        // 搜索
        if (self.isSearching == NO) {
            self.isSearching = YES;
            // 刚从历史记录状态切到搜索状态，要重新reload，否则点击了历史记录cell会闪退
            [self.tv reloadData];
            [self requestSearchData:sender.text];
        } else {
            if (sender.markedTextRange != nil) {
                return;
            }
            [self requestSearchData:sender.text];
        }
    }
    return;
}

#pragma mark - UITableViewDataSource
// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.isSearching) {
        return self.hisRecordArray.count;
    }
    // 石材一行两列，独立计算行数
    if (self.curSearchType == Zb_SearchSingle_Type_Stone) {
        return [ZbStoneTableViewCell getRowCount:self.resultArray];
    }
    return self.resultArray.count;
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearching) {
        return [ZbSearchRecordTableViewCell getRowHeight];
    }
    switch (self.curSearchType) {
        case Zb_SearchSingle_Type_Stone:
        {
            return [ZbStoneTableViewCell getRowHeight];
        }
        case Zb_SearchSingle_Type_Product:
        {
            return [ZbProductTableViewCell getRowHeight];
        }
        case Zb_SearchSingle_Type_Qiye:
        {
            return [ZbQiyeTableViewCell getRowHeight];
        }
        case Zb_SearchSingle_Type_SupllyDemand:
        {
            return [ZbSupplyDemandTableViewCell getRowHeight];
        }
            
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearching) {
        ZbSearchRecordTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SearchRecordCellIdentifier];
        cell.labRecordName.text = [self.hisRecordArray objectAtIndex:indexPath.row];
        return cell;
    }
    switch (self.curSearchType) {
        case Zb_SearchSingle_Type_Stone:
        {
            ZbStoneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:StoneCellIdentifier];
            [cell setStoneRow:indexPath.row dataArray:self.resultArray];
            // 石材点击
            cell.stoneClickBlock = ^(NSInteger stoneIndex, NSDictionary *stoneDict) {
                [self.tfSearch resignFirstResponder];
                ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
                NSString *stoneName = stoneDict[@"stone_name"];
                controller.stoneName = stoneName;
                [self.navigationController pushViewController:controller animated:YES];
            };
            return cell;
        }
        case Zb_SearchSingle_Type_Product:
        {
            ZbProductTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:ProductCellIdentifier];
            [cell setInfoDict:[self.resultArray objectAtIndex:indexPath.row]];
            return cell;
        }
        case Zb_SearchSingle_Type_Qiye:
        {
            ZbQiyeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:QiyeCellIdentifier];
            [cell setInfoDict:[self.resultArray objectAtIndex:indexPath.row]];
            return cell;
        }
        case Zb_SearchSingle_Type_SupllyDemand:
        {
            ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
            [cell setPostDict:[self.resultArray objectAtIndex:indexPath.row]];
            return cell;
        }
            
        default:
            return nil;
    }
}

// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (!self.isSearching) {
        return [ZbIndexCharTableViewCell getRowHeight];
    }
    return [ZbTypeTableViewCell getRowHeight];
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (!self.isSearching) {
        ZbIndexCharTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:IndexCharCellIdentifier];
        cell.labChar.text = @"历史记录";
        return cell;
    }
    ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
    switch (self.curSearchType) {
        case Zb_SearchSingle_Type_Stone:
        {
            headerCell.labTypeName.text = @"石材";
            break;
        }
        case Zb_SearchSingle_Type_Product:
        {
            headerCell.labTypeName.text = @"产品";
            break;
        }
        case Zb_SearchSingle_Type_Qiye:
        {
            headerCell.labTypeName.text = @"企业";
            break;
        }
        case Zb_SearchSingle_Type_SupllyDemand:
        {
            headerCell.labTypeName.text = @"供需信息";
            break;
        }
            
        default:
            break;
    }
    return headerCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (!self.isSearching) {
        NSString *key = [self.hisRecordArray objectAtIndex:indexPath.row];
        self.tfSearch.text = key;
        [self.tfSearch becomeFirstResponder];
        [self textFieldChanged:self.tfSearch];
        [self.tv reloadData];
        return;
    }
    
    [self.tfSearch resignFirstResponder];
    switch (self.curSearchType) {
        case Zb_SearchSingle_Type_Stone:
        {
            NSDictionary *curDict = [self.resultArray objectAtIndex:indexPath.row];
            ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
            controller.stoneName = curDict[@"stone_name"];
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case Zb_SearchSingle_Type_Product:
        {
            NSDictionary *curDict = [self.resultArray objectAtIndex:indexPath.row];
            ZbProductDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
            controller.productId = curDict[@"product_id"];
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case Zb_SearchSingle_Type_Qiye:
        {
            NSDictionary *curDict = [self.resultArray objectAtIndex:indexPath.row];
            ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
            controller.companyId = curDict[@"company_id"];
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case Zb_SearchSingle_Type_SupllyDemand:
        {
            NSDictionary *curDict = [self.resultArray objectAtIndex:indexPath.row];
            NSString *postType = curDict[@"post_type"];
            if (postType.intValue == Zb_Home_Message_Type_Reward) {
                // 供需
                ZbRewardDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardDetailViewController"];
                controller.curPostId = curDict[@"post_id"];
                [self.navigationController pushViewController:controller animated:YES];
            } else {
                // 悬赏
                ZbSupplyDemandDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
                controller.curPostId = curDict[@"post_id"];
                [self.navigationController pushViewController:controller animated:YES];
            }
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - 上拉或下拉刷新
/**
 *  上拉刷新
 */
- (void)footerRefresh {
    [self requestMoreSearchData:self.tfSearch.text];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    // 用户已结束搜索，有些请求还在路上，不处理
    if (!self.isSearching) {
        return;
    }
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 加载失败
    if (result.integerValue != 0) {
        return;
    }
    
    NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
    if (connection.tag == HTTP_REQUEST_LOADDATA) {
        // 下拉重刷
        [self.resultArray removeAllObjects];
        [self.resultArray addObjectsFromArray:dataArray];
        if (dataArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
            self.noResultView.hidden = YES;
        } else {
            self.tv.footer.hidden = YES;
            self.noResultView.hidden = NO;
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_LOADMORE) {
        // 上拉加载更多
        [self.resultArray addObjectsFromArray:dataArray];
        if (dataArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
