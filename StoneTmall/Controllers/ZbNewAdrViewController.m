//
//  ZbNewAdrViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/11.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbNewAdrViewController.h"
#import "ZbTypeSelectViewController.h"

#define HTTP_LOAD_SSDATA 1
#define HTTP_ADD_ADDRESS 2

@interface ZbNewAdrViewController ()

@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *phoneField;
@property (strong, nonatomic) IBOutlet UITextField *provField;
@property (strong, nonatomic) IBOutlet UITextField *cityField;
@property (strong, nonatomic) IBOutlet UITextView *contView;
@property (strong, nonatomic) IBOutlet UILabel *labPlaceholder;
@property (strong, nonatomic) IBOutlet UIButton *commitBtn;
@property (nonatomic) POST_TYPE postType;

@property (nonatomic, strong) NSArray *provArray;
@property (nonatomic, strong) NSDictionary *dict;

// 选中省份一级分类
@property (nonatomic, assign) NSInteger curTypeMain;
// 一级分类文字
@property (nonatomic, strong) NSString *curTypeMainText;
// 选中城市二级分类
@property (nonatomic, assign) NSInteger curTypeSub;
// 二级分类文字
@property (nonatomic, strong) NSString *curTypeSubText;


@end

@implementation ZbNewAdrViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.provArray = [[NSArray alloc] init];
    self.dict = [[NSDictionary alloc] init];
    if (_postType == POST_EDIT) {
        self.nameField.text = self.detailDict[@"name"];
        self.phoneField.text = self.detailDict[@"phone"];
        self.curTypeMainText = self.detailDict[@"prov"];
        self.curTypeSubText = self.detailDict[@"city"];
        self.contView.text = self.detailDict[@"area"];
    }
    if(self.contView.text.length > 0) {
        self.labPlaceholder.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.commitBtn.layer.cornerRadius = 2.0f;
    self.commitBtn.layer.masksToBounds = YES;
    self.provField.text = self.curTypeMainText;
    self.cityField.text = self.curTypeSubText;
}
- (void)setContollerType:(POST_TYPE)postType {
    _postType = postType;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - 点击事件
- (IBAction)provOnClick:(id)sender {
    [self.view endEditing:YES];
    NSArray *typeArray = [[ZbSaveManager shareManager] getProvTypeArray];
    
    if (typeArray != nil) {
        // 类别数据已加载过
        NSDictionary *subTypeDict = [[ZbSaveManager shareManager] getCitySubTypeDict];
        ZbTypeSelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTypeSelectViewController"];
        controller.title = @"省市选择";
        controller.typeArray = typeArray;
        controller.subTypeDict = subTypeDict;
        controller.lastParentIndex = self.curTypeMain;
        controller.lastSubIndex = self.curTypeSub;
        controller.selectClickBlock = ^(NSString *typeMain, NSString *typeSub, NSInteger mainIndex, NSInteger subIndex) {
            self.curTypeMain = mainIndex;
            self.curTypeSub = subIndex;
            self.curTypeMainText = typeMain;
            self.curTypeSubText = typeSub;
        };
        [self.navigationController  pushViewController:controller animated:YES];
        return;
    }
    
    // 加载分类数据
    [self requestTypeData];
    return;
}

- (IBAction)cityOnClick:(id)sender {
    [self.view endEditing:YES];
    NSArray *typeArray = [[ZbSaveManager shareManager] getProvTypeArray];
    
    if (typeArray != nil) {
        // 类别数据已加载过
        NSDictionary *subTypeDict = [[ZbSaveManager shareManager] getCitySubTypeDict];
        ZbTypeSelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTypeSelectViewController"];
        controller.title = @"省市选择";
        controller.typeArray = typeArray;
        controller.subTypeDict = subTypeDict;
        controller.lastParentIndex = self.curTypeMain;
        controller.lastSubIndex = self.curTypeSub;
        controller.selectClickBlock = ^(NSString *typeMain, NSString *typeSub, NSInteger mainIndex, NSInteger subIndex) {
            self.curTypeMain = mainIndex;
            self.curTypeSub = subIndex;
            self.curTypeMainText = typeMain;
            self.curTypeSubText = typeSub;
        };
        [self.navigationController  pushViewController:controller animated:YES];
        return;
    }
    
    // 加载分类数据
    [self requestTypeData];
    return;

}

#pragma mark - 读取数据
- (void)requestTypeData {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];

    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getRegionList] delegate:self];
    conn.tag = HTTP_LOAD_SSDATA;
    [conn start];
}

- (IBAction)finshOnClick:(id)sender {
    
    if (self.nameField.text.length <= 0) {
        self.hud.labelText = @"请输入收货人姓名";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    } else if (self.phoneField.text.length <= 0) {
        self.hud.labelText = @"请输入手机号";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    } if (self.provField.text.length <= 0) {
        self.hud.labelText = @"请选择省市";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    } if (self.cityField.text.length <= 0) {
        self.hud.labelText = @"请选择省市";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }if (self.contView.text.length <= 0) {
        self.hud.labelText = @"请输入详细地址";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    
    [self.view endEditing:YES];
    
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *params;
    switch (_postType) {
        case POST_ADD:
            params = [ZbWebService addPostAddress:self.nameField.text phone:self.phoneField.text country:@"中国" prov:self.provField.text city:self.cityField.text area:self.contView.text];
            break;
          
        case POST_EDIT:
            params = [ZbWebService editMyAddress:self.detailDict[@"id"] name:self.nameField.text phone:self.phoneField.text country:@"中国" prov:self.provField.text city:self.cityField.text area:self.contView.text];
            break;

        default:
            break;
    }
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:params delegate:self];
    conn.tag = HTTP_ADD_ADDRESS;
    [conn start];
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.nameField]) {
        [self.phoneField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - UITextView delegatect
- (void)textViewDidChange:(UITextView *)textView {
    self.labPlaceholder.hidden = textView.text.length > 0;
    return;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - CITYSELECT Delegate
- (void)getNameAdr:(NSString *)adr {
    self.provField.text = adr;
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"获取分类失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSString *result = dict[@"errno"];
    if (result.integerValue != 0) {
        return;
    }
    if (connection.tag == HTTP_LOAD_SSDATA) {
        if (result.integerValue == 0) {
            [self.hud hide:NO];
            NSArray *array = dict[@"data"];
            NSArray *tagArray = [ZbWebService treeSTypeParser:array];
            NSArray *typeArray = [tagArray firstObject];
            NSDictionary *subTypeDict = [tagArray lastObject];
            [[ZbSaveManager shareManager] setProvTypeArray:typeArray];
            [[ZbSaveManager shareManager] setCitySubTypeDict:subTypeDict];
            
            ZbTypeSelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTypeSelectViewController"];
            controller.title = @"省市选择";
            controller.typeArray = typeArray;
            controller.subTypeDict = subTypeDict;
            controller.lastParentIndex = self.curTypeMain;
            controller.lastSubIndex = self.curTypeSub;
            controller.selectClickBlock = ^(NSString *typeMain, NSString *typeSub, NSInteger mainIndex, NSInteger subIndex) {
                self.curTypeMain = mainIndex;
                self.curTypeSub = subIndex;
                self.curTypeMainText = typeMain;
                self.curTypeSubText = typeSub;
            };
            [self.navigationController  pushViewController:controller animated:YES];
        }
    } else if (connection.tag == HTTP_ADD_ADDRESS) {
        if (result.integerValue == 0) {
            self.hud.labelText = @"提交成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            self.hud.labelText = dict[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        }
    }
}

@end
