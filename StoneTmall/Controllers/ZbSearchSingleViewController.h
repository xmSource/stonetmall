//
//  ZbSearchSingleViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

// 手机验证类型
typedef enum {
    Zb_SearchSingle_Type_Stone,                // 搜索石材
    Zb_SearchSingle_Type_Product,              // 搜索产品
    Zb_SearchSingle_Type_Qiye,                 // 搜索企业
    Zb_SearchSingle_Type_SupllyDemand,         // 搜索供需信息
} Zb_SearchSingle_Type;

@interface ZbSearchSingleViewController : BaseViewController

// 当前单搜类型
@property (nonatomic, assign) Zb_SearchSingle_Type curSearchType;
// 搜索key
@property (nonatomic, copy) NSString *searchKey;

@end
