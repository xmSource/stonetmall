//
//  ZbPriceUnitViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/11/1.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbPriceUnitViewController : BaseViewController

// 价格确定block
@property (nonatomic, copy) void (^ selectOkBlock)(NSString *price, NSString *priceUnit);

@end
