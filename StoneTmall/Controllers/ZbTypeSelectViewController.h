//
//  ZbTypeSelectViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/8.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbTypeSelectViewController : BaseViewController

// 分类数组
@property (nonatomic, weak) NSArray *typeArray;
// 子分类数组
@property (nonatomic, weak) NSDictionary *subTypeDict;
// 上次选中父类
@property (nonatomic, assign) NSInteger lastParentIndex;
// 上次选中子类
@property (nonatomic, assign) NSInteger lastSubIndex;

// 选中block
@property (nonatomic, copy) void (^selectClickBlock)(NSString *typeParent, NSString *typeSub, NSInteger parentIndex, NSInteger subIndex);

@end
