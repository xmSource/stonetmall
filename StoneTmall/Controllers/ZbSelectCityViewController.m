//
//  ZbSelectCityViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/20.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSelectCityViewController.h"
#import "BATableView.h"
#import "ZbIndexCharTableViewCell.h"

@interface ZbSelectCityViewController () <UITextFieldDelegate, BATableViewDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// headerView
@property (nonatomic, strong) IBOutlet UIView *headerView;
// 输入框背景
@property (nonatomic, strong) IBOutlet UIView *searchInputBg;
// 输入框
@property (nonatomic, strong) IBOutlet UITextField *tfSearch;
// 当前城市
@property (nonatomic, strong) IBOutlet UILabel *labCurCity;
// BATableView
@property (nonatomic, strong) BATableView *baTv;

// 所有数据
@property (nonatomic, strong) NSArray *dataArray;
// 筛选后显示的数据
@property (nonatomic, strong) NSMutableArray *showDataArray;

@end

@implementation ZbSelectCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = Mutable_ArrayInit;
    self.showDataArray = [NSMutableArray array];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbIndexCharTableViewCell" bundle:nil] forCellReuseIdentifier:IndexCharCellIdentifier];
    
    // 输入框背景圆角
    self.searchInputBg.layer.cornerRadius = 3;
    self.searchInputBg.layer.masksToBounds = YES;
    
    // zb test 测试城市数据
    self.dataArray = [NSMutableArray arrayWithArray:@[@{@"indexTitle":@"A", @"data":@[@{@"name":@"奥迪"}, @{@"name":@"阿三"}, @{@"name":@"安徽"}, @{@"name":@"阿富汗"}, @{@"name":@"安海"}, ]},
                                                      @{@"indexTitle":@"B", @"data":@[@{@"name":@"北京"}, @{@"name":@"巴西"}, @{@"name":@"比利时"}, @{@"name":@"毕尔巴"}, @{@"name":@"本泽马"}, ]},
                                                      @{@"indexTitle":@"C", @"data":@[@{@"name":@"成都"}, @{@"name":@"陈"}, @{@"name":@"赤道"}, @{@"name":@"长安"}, @{@"name":@"常春藤"}, ]}, ]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self createTableView];
}

// 创建BATableView
- (void) createTableView {
    self.baTv = [[BATableView alloc] initWithTableView:self.tv frame:self.view.bounds];
    self.baTv.delegate = self;
//    [self.view addSubview:self.baTv];
    
//    // 头部
//    CGRect frame = self.headerView.frame;
//    [self.headerView removeFromSuperview];
//    frame.size.width = DeviceWidth;
//    self.headerView.frame = frame;
//    [self.tv.tableView setTableHeaderView:self.headerView];
    
    // 初始无搜索
    [self searchWithKeyWord:nil];
}

#pragma mark - 点击事件
// 查询
- (void)searchWithKeyWord:(NSString *)keyWord {
    if (keyWord == nil || keyWord.length <= 0) {
        // 重置
        self.showDataArray = [NSMutableArray arrayWithArray:self.dataArray];
    } else {
        // 搜索
        [self.showDataArray removeAllObjects];
        for (NSDictionary *infoDict in self.dataArray) {
            NSString *key = infoDict[@"indexTitle"];
            NSArray *itemArray = infoDict[@"data"];
            // 遍历首字母对应的数组
            NSMutableArray *tempArray = [NSMutableArray array];
            for (NSDictionary *infoDict in itemArray) {
                NSString *name = infoDict[@"name"];
                if ([name rangeOfString:keyWord].length > 0) {
                    [tempArray addObject:infoDict];
                }
            }
            
            // 如果这个首字母有匹配的数据，保留此首字母
            if (tempArray.count > 0) {
                [self.showDataArray addObject:@{@"indexTitle":key, @"data":tempArray}];
            }
        }
    }
    [self.baTv reloadData];
    return;
}

#pragma mark - UITextField 变化
- (IBAction)textFieldChanged:(UITextField *)sender {
    return;
}

#pragma mark - UITableViewDataSource
- (NSArray *) sectionIndexTitlesForABELTableView:(BATableView *)tableView {
    NSMutableArray * indexTitles = [NSMutableArray array];
    for (NSDictionary * sectionDictionary in self.showDataArray) {
        [indexTitles addObject:sectionDictionary[@"indexTitle"]];
    }
    return indexTitles;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.showDataArray.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.showDataArray[section][@"data"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * cellName = @"UITableViewCell";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.text = self.showDataArray[indexPath.section][@"data"][indexPath.row][@"name"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [ZbIndexCharTableViewCell getRowHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZbIndexCharTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:IndexCharCellIdentifier];
    cell.labChar.text = self.showDataArray[section][@"indexTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *name = self.showDataArray[indexPath.section][@"data"][indexPath.row][@"name"];
    // 回传数据
    if (self.selectBlock) {
        self.selectBlock(name);
    }
    [self leftBarItemOnClick:nil];
}

#pragma mark - 搜索栏delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    NSString *keyWord = textField.text;
    [self searchWithKeyWord:keyWord];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
