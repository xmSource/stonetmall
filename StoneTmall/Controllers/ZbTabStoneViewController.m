//
//  ZbTabStomeViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTabStoneViewController.h"
#import "BATableView.h"
#import "ZbStoneTableViewCell.h"
#import "ZbIndexCharTableViewCell.h"
#import "ZbStoneSearchViewController.h"
#import "ZbDropDownViewController.h"
#import "ZbStoneDetailViewController.h"

#define HTTP_REQUEST_TAG_TYPE 1         // 石材类别列表
#define HTTP_REQUEST_TAG_COLOR 2        // 石材颜色列表
#define HTTP_REQUEST_TAG_TEXTURE 3      // 石材纹理列表
#define HTTP_REQUEST_TAG_DATA 4         // 重载石材数据
#define HTTP_REQUEST_TAG_MORE 5         // 加载更多石材数据

@interface ZbTabStoneViewController () <BATableViewDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// headerView
@property (nonatomic, strong) IBOutlet UIView *headerView;
// BATableView
@property (nonatomic, strong) BATableView *baTv;
// 下拉框标题数组
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *labFilterArray;
// 导航栏titleView
@property (nonatomic, strong) IBOutlet UIView *navTitleView;
// 导航栏titleView圆角容器
@property (nonatomic, strong) IBOutlet UIView *navRoundContainer;
// 热门分类容器
@property (nonatomic, strong) IBOutlet UIView *hotTypeView;
// 热门分类control列表
@property (nonatomic, strong) IBOutletCollection(UIControl) NSArray *controlHotArray;

// 所有数据
@property (nonatomic, strong) NSMutableArray *dataArray;

// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

// 当前类别选中项
@property (nonatomic, assign) NSInteger curTypeIndex;
// 石材分类文字
@property (nonatomic, strong) NSString *curTypeText;
// 当前颜色选中项
@property (nonatomic, assign) NSInteger curColorIndex;
// 当前纹理选中项
@property (nonatomic, assign) NSInteger curTextureIndex;

// 当前下拉框
@property (nonatomic, strong) ZbDropDownViewController *curDropDownController;

@end

@implementation ZbTabStoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    self.curTypeIndex = -1;
    self.curColorIndex = -1;
    self.curTextureIndex = -1;
    
    // 圆角
    self.navRoundContainer.layer.cornerRadius = CGRectGetHeight(self.navTitleView.frame) / 2;
    self.navRoundContainer.layer.masksToBounds = YES;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneTableViewCell" bundle:nil] forCellReuseIdentifier:StoneCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbIndexCharTableViewCell" bundle:nil] forCellReuseIdentifier:IndexCharCellIdentifier];
    
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
    
    // 初始化热门分类
    NSArray *localStoneTypeArray = [[ZbSaveManager shareManager] getLocalStoneTypeArray];
    if (localStoneTypeArray != nil) {
        // 有本地用本地存储的，否则就是默认的
        [self drawHotType:localStoneTypeArray];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    // 创建索引右边栏
    [self createTableView];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    [self requestTypeData];
    [self requestColorData];
    [self requestTextureData];
    [self requestData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    self.navTitleView.frame = CGRectMake(0, 7, 600, 30);
    self.tabBarController.navigationItem.titleView = self.navTitleView;
}

// 创建BATableView
- (void) createTableView {
    CGRect bounds = self.view.bounds;
    bounds.origin.y = FILTER_CONTAINER_HEIGHT;
    bounds.size.height -= FILTER_CONTAINER_HEIGHT;
    self.baTv = [[BATableView alloc] initWithTableView:self.tv frame:bounds];
    self.baTv.delegate = self;
}

#pragma mark - 界面绘制
// 绘制热门分类
- (void)drawHotType:(NSArray *)typeArray {
    for (NSInteger i= 0; i < self.controlHotArray.count; i++) {
        UIControl *curControl = self.controlHotArray[i];
        UIImageView *ivIcon = [curControl viewWithTag:2];
        UILabel *labName = [curControl viewWithTag:1];
        
        if (i >= typeArray.count) {
            curControl.hidden = YES;
            continue;
        }
        curControl.hidden = NO;
        NSDictionary *curTypeDict = typeArray[i];
        labName.text = curTypeDict[@"class_name"];
        [ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:curTypeDict[@"class_logo"]]]];
    }
}

#pragma mark - 接口调用
// 获取石材类别列表
- (void)requestTypeData {
    NSArray *typeArray = [[ZbSaveManager shareManager] getStoneTypeArray];
    if (typeArray != nil) {
        // 类别数据已加载过
        return;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneTypeList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_TYPE;
    [conn start];
}

// 获取石材颜色列表
- (void)requestColorData {
    NSArray *colorArray = [[ZbSaveManager shareManager] getStoneColorArray];
    if (colorArray != nil) {
        // 颜色数据已加载过
        return;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneColorList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_COLOR;
    [conn start];
}

// 获取石材纹理列表
- (void)requestTextureData {
    NSArray *textureArray = [[ZbSaveManager shareManager] getStoneTextureArray];
    if (textureArray != nil) {
        // 纹理数据已加载过
        return;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneTextureList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_TEXTURE;
    [conn start];
}

// 重载石材数据
- (void)requestData {
    // 类别
    NSString *type = @"";
    if (self.curTypeText.length > 0) {
        type = self.curTypeText;
        if ([type isEqualToString:@"全部"]) {
            type = @"";
        }
    }
    // 颜色
    NSString *color = @"";
    if (self.curColorIndex != -1) {
        NSArray *colorArray = [[ZbSaveManager shareManager] getStoneColorArray];
        if (self.curColorIndex > 0) {
            if (self.curColorIndex - 1 < colorArray.count) {
                color = colorArray[self.curColorIndex - 1][@"class_name"];
            }
        }
    }
    // 纹理
    NSString *texture = @"";
    if (self.curTextureIndex != -1) {
        NSArray *textureArray = [[ZbSaveManager shareManager] getStoneTextureArray];
        if (self.curTextureIndex > 0) {
            if (self.curTextureIndex - 1 < textureArray.count) {
                texture = textureArray[self.curTextureIndex - 1][@"class_name"];
            }
        }
    }
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneList:@"" type:type color:color texture:texture pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DATA;
    [conn start];
    return;
}

// 加载更多石材数据
- (void)requestMoreData {
    // 类别
    NSString *type = @"";
    if (self.curTypeText.length > 0) {
        type = self.curTypeText;
        if ([type isEqualToString:@"全部"]) {
            type = @"";
        }
    }
    // 颜色
    NSString *color = @"";
    if (self.curColorIndex != -1) {
        NSArray *colorArray = [[ZbSaveManager shareManager] getStoneColorArray];
        if (self.curColorIndex > 0) {
            if (self.curColorIndex - 1 < colorArray.count) {
                color = colorArray[self.curColorIndex - 1][@"class_name"];
            }
        }
    }
    // 纹理
    NSString *texture = @"";
    if (self.curTextureIndex != -1) {
        NSArray *textureArray = [[ZbSaveManager shareManager] getStoneTextureArray];
        if (self.curTextureIndex > 0) {
            if (self.curTextureIndex - 1 < textureArray.count) {
                texture = textureArray[self.curTextureIndex - 1][@"class_name"];
            }
        }
    }
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneList:@"" type:type color:color texture:texture pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MORE;
    [conn start];
    return;
}

#pragma mark - 点击事件
// 搜索按钮点击
- (IBAction)btnSearchOnClick:(id)sender {
    ZbStoneSearchViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneSearchViewController"];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

// 热门分类点击
- (IBAction)btnHotOnClick:(UIControl *)sender {
    UILabel *labName = [sender viewWithTag:1];
    if ([labName.text isEqualToString:@"展开"]) {
        // 最后一个默认为分类展开
        UIControl *firstControl = [[UIControl alloc] init];
        [self btnFilterOnClick:firstControl];
        return;
    }
    self.curTypeText = labName.text;
    UILabel *labFilterName = [self.labFilterArray objectAtIndex:sender.tag];
    labFilterName.text = self.curTypeText;
    [self requestData];
    return;
}

// 筛选按钮点击
- (IBAction)btnFilterOnClick:(UIControl *)sender {
    switch (sender.tag) {
        case 0:
        {
            // 分类
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Pic) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
        case 1:
        {
            // 颜色
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Color) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
        case 2:
        {
            // 纹理
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Text) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
    }
    
    // 关闭旧菜单
    if (self.curDropDownController != nil) {
        [self.curDropDownController dismiss:NO];
        self.curDropDownController = nil;
    }
    switch (sender.tag) {
        case 0:
        {
            NSArray *typeArray = [[ZbSaveManager shareManager] getStoneTypeArray];
            if (typeArray == nil) {
                self.hud.labelText = @"获取分类数据失败，请下拉重刷";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:CGRectGetMaxY(self.headerView.frame) dataArray:typeArray type:Zb_DropDown_Type_Pic selectIndex:self.curTypeIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self.labFilterArray objectAtIndex:sender.tag];
                labFilterName.text = name;
                weakSelf.curTypeIndex = index;
                weakSelf.curTypeText = name;
                [weakSelf requestData];
                
                self.curDropDownController = nil;
            } isNeedAll:YES];
            [self.view bringSubviewToFront:self.headerView];
            break;
        }
        case 1:
        {
            NSArray *colorArray = [[ZbSaveManager shareManager] getStoneColorArray];
            if (colorArray == nil) {
                self.hud.labelText = @"获取颜色数据失败，请下拉重刷";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:CGRectGetMaxY(self.headerView.frame) dataArray:colorArray type:Zb_DropDown_Type_Color selectIndex:self.curColorIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self.labFilterArray objectAtIndex:sender.tag];
                labFilterName.text = name;
                weakSelf.curColorIndex = index;
                [weakSelf requestData];
                
                self.curDropDownController = nil;
            } isNeedAll:YES];
            [self.view bringSubviewToFront:self.headerView];
            break;
        }
        case 2:
        {
            NSArray *textureArray = [[ZbSaveManager shareManager] getStoneTextureArray];
            if (textureArray == nil) {
                self.hud.labelText = @"获取纹理数据失败，请下拉重刷";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:CGRectGetMaxY(self.headerView.frame) dataArray:textureArray type:Zb_DropDown_Type_Text selectIndex:self.curTextureIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self.labFilterArray objectAtIndex:sender.tag];
                labFilterName.text = name;
                weakSelf.curTextureIndex = index;
                [weakSelf requestData];
                
                self.curDropDownController = nil;
            } isNeedAll:YES];
            [self.view bringSubviewToFront:self.headerView];
            break;
        }
        default:
            break;
    }
    return;
}

#pragma mark - UITableViewDataSource
- (NSArray *) sectionIndexTitlesForABELTableView:(BATableView *)tableView {
    NSMutableArray * indexTitles = [NSMutableArray array];
    for (NSDictionary * sectionDictionary in self.dataArray) {
        [indexTitles addObject:sectionDictionary[@"indexTitle"]];
    }
    return indexTitles;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ZbStoneTableViewCell getRowCount:self.dataArray[section][@"data"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbStoneTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZbStoneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:StoneCellIdentifier];
    
    // 绘制
    [cell setStoneRow:indexPath.row dataArray:self.dataArray[indexPath.section][@"data"] ];
    // 石材点击
    cell.stoneClickBlock = ^(NSInteger stoneIndex, NSDictionary *stoneDict) {
        ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
        NSString *stoneName = stoneDict[@"stone_name"];
        controller.stoneName = stoneName;
        [self.navigationController pushViewController:controller animated:YES];
    };
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [ZbIndexCharTableViewCell getRowHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZbIndexCharTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:IndexCharCellIdentifier];
    cell.labChar.text = self.dataArray[section][@"indexTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 重置选项
    self.curTypeIndex = -1;
    self.curColorIndex = -1;
    self.curTextureIndex = -1;
    self.curTypeText = nil;
    
    // 重置筛选标签文字
    for (NSInteger i = 0; i < self.labFilterArray.count; i++) {
        UILabel *labFilterName = [self.labFilterArray objectAtIndex:i];
        labFilterName.text = [STONE_FILETER_DEFAULT_NAME_ARRAY objectAtIndex:i];
    }
    [self requestTypeData];
    [self requestColorData];
    [self requestTextureData];
    [self requestData];
}

- (void)footerRefresh {
    [self requestMoreData];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    if (connection.tag != HTTP_REQUEST_TAG_MORE) {
        [self.tv.header endRefreshing];
        [self.tv.footer endRefreshing];
    }
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    NSMutableArray *dataArray = resultDic[@"data"];
    if (connection.tag == HTTP_REQUEST_TAG_TYPE) {
        [[ZbSaveManager shareManager] setStoneTypeArray:dataArray];
        // 绘制热门分类
        [self drawHotType:dataArray];
    } else if (connection.tag == HTTP_REQUEST_TAG_COLOR) {
        [[ZbSaveManager shareManager] setStoneColorArray:dataArray];
    } else if (connection.tag == HTTP_REQUEST_TAG_TEXTURE) {
        [[ZbSaveManager shareManager] setStoneTextureArray:dataArray];
    } else if (connection.tag == HTTP_REQUEST_TAG_DATA) {
        NSMutableArray *stoneArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [stoneArray enumerateObjectsUsingBlock:^(NSMutableDictionary *stoneDict, NSUInteger index, BOOL *stop) {
            NSString *name = stoneDict[@"stone_name"];
            NSString * firstPinyinLetter = [[NSString stringWithFormat:@"%c", pinyinFirstLetter([name characterAtIndex:0])] uppercaseString];
            [stoneDict setValue:firstPinyinLetter forKey:@"description"];
        }];
        NSMutableArray *tagArray = [ZbWebService sortByFirstCharact:stoneArray];
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:tagArray];
        if (stoneArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        
        [self.hud hide:NO];
        [self.baTv reloadData];
        if (self.dataArray.count > 0) {
            if (self.tv.contentOffset.y > 0) {
                [self.tv setContentOffset:CGPointMake(0, 0) animated:NO];
            }
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_MORE) {
        [self.tv.footer endRefreshing];
        // 上拉加载更多
        NSMutableArray *stoneArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        // 生成首字母数组
        [stoneArray enumerateObjectsUsingBlock:^(NSMutableDictionary *stoneDict, NSUInteger index, BOOL *stop) {
            NSString *name = stoneDict[@"stone_name"];
            NSString * firstPinyinLetter = [[NSString stringWithFormat:@"%c", pinyinFirstLetter([name characterAtIndex:0])] uppercaseString];
            [stoneDict setValue:firstPinyinLetter forKey:@"description"];
        }];
        NSMutableArray *tagArray = [ZbWebService sortByFirstCharact:stoneArray];
        // 拼接首字母数组
        [ZbWebService firstCharactArrayJoin:self.dataArray addArray:tagArray];
        if (stoneArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.baTv reloadData];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
