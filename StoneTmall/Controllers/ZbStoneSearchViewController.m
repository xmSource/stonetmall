//
//  ZbStoneSearchViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbStoneSearchViewController.h"
#import "ZbSearchRecordTableViewCell.h"
#import "ZbIndexCharTableViewCell.h"
#import "ZbStoneTableViewCell.h"
#import "ZbDropDownViewController.h"
#import "ZbStoneDetailViewController.h"

#define HTTP_REQUEST_LOADDATA 0        // 下拉刷新数据
#define HTTP_REQUEST_LOADMORE 1        // 上拉加载更多数据

@interface ZbStoneSearchViewController () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

// 导航栏titleView
@property (nonatomic, strong) IBOutlet UIView *navTitleView;
// 导航栏搜索栏
@property (nonatomic, strong) IBOutlet UITextField *tfSearch;
// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// 搜索无记录view
@property (nonatomic, strong) IBOutlet UIView *noResultView;
// headerView
@property (nonatomic, strong) IBOutlet UIView *headerView;
// headerView高度约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcHeaderViewHeight;
// 下拉框标题数组
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *labFilterArray;

// 历史记录数组
@property (nonatomic, strong) NSMutableArray *hisRecordArray;
// 所有数据
@property (nonatomic, strong) NSMutableArray *dataArray;

// 是否搜索状态
@property (nonatomic, assign) BOOL isSearching;
// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;
// 当前类别选中项
@property (nonatomic, assign) NSInteger curTypeIndex;
// 当前颜色选中项
@property (nonatomic, assign) NSInteger curColorIndex;
// 当前纹理选中项
@property (nonatomic, assign) NSInteger curTextureIndex;

// 当前下拉框
@property (nonatomic, strong) ZbDropDownViewController *curDropDownController;

@end

@implementation ZbStoneSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hisRecordArray = Mutable_ArrayInit;
    self.dataArray = [NSMutableArray array];
    self.curTypeIndex = -1;
    self.curColorIndex = -1;
    self.curTextureIndex = -1;
    
    // 圆角
    self.navTitleView.layer.cornerRadius = CGRectGetHeight(self.navTitleView.frame) / 2;
    self.navTitleView.layer.masksToBounds = YES;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneTableViewCell" bundle:nil] forCellReuseIdentifier:StoneCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSearchRecordTableViewCell" bundle:nil] forCellReuseIdentifier:SearchRecordCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbIndexCharTableViewCell" bundle:nil] forCellReuseIdentifier:IndexCharCellIdentifier];
    
    //添加上拉加载更多
    __weak typeof(self) weakSelf = self;
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self loadSearchHisRecordData];
    [self.tfSearch becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 点击事件
- (IBAction)leftBarItemOnClick:(id)sender {
    self.isSearching = NO;
    [self.tfSearch resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

// 筛选按钮点击
- (IBAction)btnFilterOnClick:(UIControl *)sender {
    [self.tfSearch resignFirstResponder];
    switch (sender.tag) {
        case 0:
        {
            // 分类
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Pic) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
        case 1:
        {
            // 颜色
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Color) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
        case 2:
        {
            // 纹理
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Text) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
    }
    
    // 关闭旧菜单
    if (self.curDropDownController != nil) {
        [self.curDropDownController dismiss:NO];
        self.curDropDownController = nil;
    }
    switch (sender.tag) {
        case 0:
        {
            NSArray *typeArray = [[ZbSaveManager shareManager] getStoneTypeArray];
            if (typeArray == nil) {
                self.hud.labelText = @"获取分类数据失败";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:CGRectGetMaxY(self.headerView.frame) dataArray:typeArray type:Zb_DropDown_Type_Pic selectIndex:self.curTypeIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self.labFilterArray objectAtIndex:sender.tag];
                labFilterName.text = name;
                weakSelf.curTypeIndex = index;
                [weakSelf requestSearchData:self.tfSearch.text];
                
                self.curDropDownController = nil;
            } isNeedAll:YES];
            [self.view bringSubviewToFront:self.headerView];
            break;
        }
        case 1:
        {
            NSArray *colorArray = [[ZbSaveManager shareManager] getStoneColorArray];
            if (colorArray == nil) {
                self.hud.labelText = @"获取颜色数据失败";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:CGRectGetMaxY(self.headerView.frame) dataArray:colorArray type:Zb_DropDown_Type_Color selectIndex:self.curColorIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self.labFilterArray objectAtIndex:sender.tag];
                labFilterName.text = name;
                weakSelf.curColorIndex = index;
                [weakSelf requestSearchData:self.tfSearch.text];
                
                self.curDropDownController = nil;
            } isNeedAll:YES];
            [self.view bringSubviewToFront:self.headerView];
            break;
        }
        case 2:
        {
            NSArray *textureArray = [[ZbSaveManager shareManager] getStoneTextureArray];
            if (textureArray == nil) {
                self.hud.labelText = @"获取纹理数据失败";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:CGRectGetMaxY(self.headerView.frame) dataArray:textureArray type:Zb_DropDown_Type_Text selectIndex:self.curTextureIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self.labFilterArray objectAtIndex:sender.tag];
                labFilterName.text = name;
                weakSelf.curTextureIndex = index;
                [weakSelf requestSearchData:self.tfSearch.text];
                
                self.curDropDownController = nil;
            } isNeedAll:YES];
            [self.view bringSubviewToFront:self.headerView];
            break;
        }
        default:
            break;
    }
    return;
}

#pragma mark - 接口调用
// 获取搜索记录
- (void)loadSearchHisRecordData {
    // 隐藏筛选栏
    self.alcHeaderViewHeight.constant = 0;
    self.headerView.hidden = YES;
    self.tv.footer.hidden = YES;
    // 获取搜索记录
    self.hisRecordArray = [NSMutableArray arrayWithArray:[[ZbSaveManager shareManager] getGlobalStoneSearchHistory]];
    [self.tv reloadData];
}

// 查询数据接口
- (void)requestSearchData:(NSString *)key {
    // 显示筛选栏
    self.alcHeaderViewHeight.constant = FILTER_CONTAINER_HEIGHT;
    self.headerView.hidden = NO;
    [self.tv reloadData];
    
    // 类别
    NSString *type = @"";
    if (self.curTypeIndex != -1) {
        NSArray *typeArray = [[ZbSaveManager shareManager] getStoneTypeArray];
        if (self.curTypeIndex > 0) {
            if (self.curTypeIndex - 1 < typeArray.count) {
                type = typeArray[self.curTypeIndex - 1][@"class_name"];
            }
        }
    }
    // 颜色
    NSString *color = @"";
    if (self.curColorIndex != -1) {
        NSArray *colorArray = [[ZbSaveManager shareManager] getStoneColorArray];
        if (self.curColorIndex > 0) {
            if (self.curColorIndex - 1 < colorArray.count) {
                color = colorArray[self.curColorIndex - 1][@"class_name"];
            }
        }
    }
    // 纹理
    NSString *texture = @"";
    if (self.curTextureIndex != -1) {
        NSArray *textureArray = [[ZbSaveManager shareManager] getStoneTextureArray];
        if (self.curTextureIndex > 0) {
            if (self.curTextureIndex - 1 < textureArray.count) {
                texture = textureArray[self.curTextureIndex - 1][@"class_name"];
            }
        }
    }
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneList:key type:type color:color texture:texture pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_LOADDATA;
    [conn start];
    return;
}

// 上拉加载更多数据列表接口
- (void)requestMoreSearchData:(NSString *)key {
    // 类别
    NSString *type = @"";
    if (self.curTypeIndex != -1) {
        NSArray *typeArray = [[ZbSaveManager shareManager] getStoneTypeArray];
        if (self.curTypeIndex > 0) {
            if (self.curTypeIndex - 1 < typeArray.count) {
                type = typeArray[self.curTypeIndex - 1][@"class_name"];
            }
        }
    }
    // 颜色
    NSString *color = @"";
    if (self.curColorIndex != -1) {
        NSArray *colorArray = [[ZbSaveManager shareManager] getStoneColorArray];
        if (self.curColorIndex > 0) {
            if (self.curColorIndex - 1 < colorArray.count) {
                color = colorArray[self.curColorIndex - 1][@"class_name"];
            }
        }
    }
    // 纹理
    NSString *texture = @"";
    if (self.curTextureIndex != -1) {
        NSArray *textureArray = [[ZbSaveManager shareManager] getStoneTextureArray];
        if (self.curTextureIndex > 0) {
            if (self.curTextureIndex - 1 < textureArray.count) {
                texture = textureArray[self.curTextureIndex - 1][@"class_name"];
            }
        }
    }
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneList:key type:type color:color texture:texture pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_LOADMORE;
    [conn start];
    return;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 添加全局搜索历史记录
    NSString *key = textField.text;
    if (key.length > 0) {
        [[ZbSaveManager shareManager] addGlobalStoneSearchHistory:key];
        [[ZbSaveManager shareManager] addGlobalSearchHistory:key];
    }
    return;
}

- (IBAction)textFieldChanged:(UITextField *)sender {
    if (sender.text.length <= 0) {
        // 历史记录
        self.isSearching = NO;
        [self loadSearchHisRecordData];
    } else {
        // 搜索
        if (self.isSearching == NO) {
            self.isSearching = YES;
            // 刚从历史记录状态切到搜索状态，要重新reload，否则点击了历史记录cell会闪退
            [self.tv reloadData];
            [self requestSearchData:sender.text];
        } else {
            if (sender.markedTextRange != nil) {
                return;
            }
            [self requestSearchData:sender.text];
        }
    }
    return;
}

#pragma mark - UITableViewDataSource
// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.isSearching) {
        return self.hisRecordArray.count;
    }
    return [ZbStoneTableViewCell getRowCount:self.dataArray];
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearching) {
        return [ZbSearchRecordTableViewCell getRowHeight];
    }
    return [ZbStoneTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearching) {
        ZbSearchRecordTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SearchRecordCellIdentifier];
        cell.labRecordName.text = [self.hisRecordArray objectAtIndex:indexPath.row];
        return cell;
    }
    ZbStoneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:StoneCellIdentifier];
    [cell setStoneRow:indexPath.row dataArray:self.dataArray];
    // 石材点击
    cell.stoneClickBlock = ^(NSInteger stoneIndex, NSDictionary *stoneDict) {
        [self.tfSearch resignFirstResponder];
        ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
        NSString *stoneName = stoneDict[@"stone_name"];
        controller.stoneName = stoneName;
        [self.navigationController pushViewController:controller animated:YES];
    };
    return cell;
}

// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (!self.isSearching) {
        return [ZbIndexCharTableViewCell getRowHeight];
    }
    return 0;
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (!self.isSearching) {
        ZbIndexCharTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:IndexCharCellIdentifier];
        cell.labChar.text = @"历史记录";
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (!self.isSearching) {
        NSString *key = [self.hisRecordArray objectAtIndex:indexPath.row];
        self.tfSearch.text = key;
        [self.tfSearch becomeFirstResponder];
        [self textFieldChanged:self.tfSearch];
        [self.tv reloadData];
        return;
    }
}

#pragma mark - 上拉或下拉刷新
/**
 *  上拉刷新
 */
- (void)footerRefresh {
    [self requestMoreSearchData:self.tfSearch.text];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    // 用户已结束搜索，有些请求还在路上，不处理
    if (!self.isSearching) {
        return;
    }
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 加载失败
    if (result.integerValue != 0) {
        return;
    }
    
    NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
    if (connection.tag == HTTP_REQUEST_LOADDATA) {
        // 下拉重刷
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:dataArray];
        if (dataArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
            self.noResultView.hidden = YES;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
            self.noResultView.hidden = NO;
        }
        [self.tv reloadData];
        if (self.dataArray.count > 0) {
            [self.tv scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    } else if (connection.tag == HTTP_REQUEST_LOADMORE) {
        // 上拉加载更多
        [self.dataArray addObjectsFromArray:dataArray];
        if (dataArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
