//
//  ZbSupADedPageViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/17.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSupADedPageViewController.h"
#import "ZbSupplyDemandTableViewCell.h"
#import "ZbRewardDetailViewController.h"
#import "ZbSupplyDemandDetailViewController.h"

#define HTTP_REQUEST_TAG_DATA 1           // 数据
#define HTTP_REQUEST_TAG_MORE 2           // 更多数据

@interface ZbSupADedPageViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) IBOutlet UITableView *tv;
@property (nonatomic, assign) NSInteger curPageIndex;

@end

@implementation ZbSupADedPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [[NSMutableArray alloc] init];
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSupplyDemandTableViewCell" bundle:nil] forCellReuseIdentifier:SupplyDemandCellIdentifier];
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}
#pragma mark - 读取数据
// 读取供需数据
- (void)loadData {
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMySupplyDemandList:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DATA;
    [conn start];
}
// 读取供需更多数据
- (void)requestMoreData {
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex + 1]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMySupplyDemandList:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MORE;
    [conn start];
}

#pragma mark - TableViewDelegate &Datasource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbSupplyDemandTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // 需求
    ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
    [cell setPostDict:[self.dataArray objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *curPost = [self.dataArray objectAtIndex:indexPath.row];
    NSString *postType = curPost[@"post_type"];
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if (postType.intValue == Zb_Home_Message_Type_Reward) {
        // 悬赏
        ZbRewardDetailViewController *controller = [main instantiateViewControllerWithIdentifier:@"ZbRewardDetailViewController"];
        controller.curPostId = curPost[@"post_id"];
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        // 供需
        ZbSupplyDemandDetailViewController *detail = [main instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
        detail.curPostId = curPost[@"post_id"];
        [self.navigationController pushViewController:detail animated:YES];
    }
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 重置选项
    [self loadData];
}

- (void)footerRefresh {
    [self requestMoreData];
}


#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (connection.tag == HTTP_REQUEST_TAG_DATA) {
        NSMutableArray *dataArray = resultDic[@"data"];
        NSMutableArray *tagArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:tagArray];
        if (tagArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        [self.hud hide:NO];
        [self.tv reloadData];
        if (self.dataArray.count > 0) {
            CGRect rectTop = CGRectMake(0, 0, 1, 1);
            [self.tv scrollRectToVisible:rectTop animated:YES];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_MORE) {
        NSMutableArray *dataArray = resultDic[@"data"];
        NSMutableArray *tagArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.dataArray addObjectsFromArray:tagArray];
        if (tagArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.hud hide:NO];
        [self.tv reloadData];
    }
}

@end
