//
//  ZbShopCartViewController.m
//  StoneTmall
//
//  Created by chyo on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbShopCartViewController.h"
#import "ZbSampleOrderViewController.h"

@interface ZbShopCartViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tv;

// 全选图片
@property (strong, nonatomic) IBOutlet UIImageView *allSelectImg;
// 确定按钮
@property (nonatomic, strong) IBOutlet UIControl *controlAllSelect;

// 确定按钮
@property (nonatomic, strong) IBOutlet UIButton *confirmBtn;

// 全部金额
@property (nonatomic, strong) IBOutlet UILabel *totolPriceLab;


// 购物车数据
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation ZbShopCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array = Mutable_ArrayInit;
    self.confirmBtn.layer.cornerRadius = 3.0f;
    self.confirmBtn.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.array removeAllObjects];
    [self.array addObjectsFromArray:[ZbSaveManager getShoppingCart]];
    if (self.array.count > 0) {
        [self.tv reloadData];
    }
    [self drawBottomView];
}

- (void)drawBottomView {
    
    if (self.array.count == 0) {
        self.confirmBtn.enabled = NO;
    } else {
        self.confirmBtn.enabled = YES;
    }
    
    self.confirmBtn.backgroundColor = self.confirmBtn.enabled ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    
    float sumPrice = 0.00;
    for (int i = 0; i < self.array.count; i ++) {
        NSDictionary *curDict = [self.array objectAtIndex:i];
        if ([curDict[@"HS_isSelect"] isEqual:@1]) {
            float price = [curDict[@"spec_price"] floatValue] / 100;
            float count = [curDict[@"count"] floatValue];
            float signalPrice = price * count;
            sumPrice += signalPrice;
        }
    }
    
    // 设置全选按钮勾选状态
    BOOL isAllSelectStatus = YES;
    for (int i = 0; i < self.array.count; i ++) {
        NSDictionary *curDict = [self.array objectAtIndex:i];
        if (![curDict[@"HS_isSelect"] isEqual:@1]) {
            isAllSelectStatus = NO;
            break;
        }
    }
    
    if (self.array.count == 0) {
        isAllSelectStatus = NO;
    }
    
    if (isAllSelectStatus) {
        self.controlAllSelect.selected = YES;
        self.allSelectImg.image = [UIImage imageNamed:@"public_icn_Checked"];
    } else {
        self.controlAllSelect.selected = NO;
        self.allSelectImg.image = [UIImage imageNamed:@"public_icn_unChecked"];
    }
    
    self.totolPriceLab.text = [NSString stringWithFormat:@"%.2f", sumPrice];
}

#pragma mark - 点击事件
// 全选按钮点击
- (IBAction)allSelectOnClick:(UIControl *)control {
    if (self.array.count <= 0) {
        return;
    }
    // 全选勾选图片变化
    control.selected = !control.selected;
    if (control.selected) {
        self.allSelectImg.image = [UIImage imageNamed:@"public_icn_Checked"];
    } else {
        self.allSelectImg.image = [UIImage imageNamed:@"public_icn_unChecked"];
    }
    
    // 数据变化
    NSNumber *selectStatus = control.selected ? @1 : @0;
    for (NSMutableDictionary *curDict in self.array) {
        [curDict setValue:selectStatus forKey:@"HS_isSelect"];
    }
    // 重绘cell
    [self.tv reloadData];
    [self drawBottomView];
}

// 单个物品选择点击
- (void)selectGoosFlsgClick:(ZbIndexButton *)curBtn {
    // 本样品勾选图片改变
    curBtn.selected = !curBtn.selected;
    // 勾选数据改变
    NSMutableDictionary *curDict = [self.array objectAtIndex:curBtn.index];
    NSNumber *selectStatus = curBtn.selected ? @1 : @0;
    [curDict setValue:selectStatus forKey:@"HS_isSelect"];

    // 总价、全选按钮变化
    [self drawBottomView];
}

// 增加物品数量点击
- (void)plusOnClick:(ZbIndexButton *)btn {
    UITableViewCell *cell = [self.tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:btn.index inSection:0]];
    UILabel *textField = (UILabel *)[cell viewWithTag:9];
    NSInteger numsOfGoods = [textField.text integerValue];
    numsOfGoods --;
    if (numsOfGoods <= 1) {
        numsOfGoods = 1;
    }
    
    NSDictionary *dict = [self.array objectAtIndex:btn.index];
    [dict setValue:[NSNumber numberWithInteger:numsOfGoods] forKey:@"count"];
    textField.text = [NSString stringWithFormat:@"%@", dict[@"count"]];
    if ([dict[@"HS_isSelect"] isEqual:@1]) {
        [self drawBottomView];
    }
}
// 删减物品点击
- (void)addOnClick:(ZbIndexButton *)btn {
    UITableViewCell *cell = [self.tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:btn.index inSection:0]];
    UILabel *textField = (UILabel *)[cell viewWithTag:9];
    NSInteger numsOfGoods = [textField.text integerValue];
    numsOfGoods ++;
    if (numsOfGoods >= 999) {
        numsOfGoods = 999;
    }
    
    NSDictionary *dict = [self.array objectAtIndex:btn.index];
    [dict setValue:[NSNumber numberWithInteger:numsOfGoods] forKey:@"count"];
    textField.text = [NSString stringWithFormat:@"%@", dict[@"count"]];
    if ([dict[@"HS_isSelect"] isEqual:@1]) {
        [self drawBottomView];
    }
}
// 确认按钮点击
- (IBAction)confirmOrder:(id)sender {
    if ([self.totolPriceLab.text isEqualToString:@"0.00"]) {
        self.hud.labelText = @"请选择样品下单";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *curDict in self.array) {
        if ([[curDict objectForKey:@"HS_isSelect"] isEqual:@1]) {
            // 要添加的购物车数据
            [array addObject:curDict];
        }
    }
    
    UIStoryboard *mainsb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ZbSampleOrderViewController *controller = [mainsb instantiateViewControllerWithIdentifier:@"ZbSampleOrderViewController"];
    controller.cartSamplyArray = array;
    // 支付成功，删除选中购物车物品
    controller.payOkBlock = ^(void) {
        [self.array removeObjectsInArray:array];
        [[ZbSaveManager getShoppingCart] removeObjectsInArray:array];
        [self.tv reloadData];
        
        // 删除后，全选按钮、总价变化
        [self drawBottomView];
    };
    [self.navigationController pushViewController:controller animated:YES];
}



#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.array.count;
}

/**
 *  设置cell的内容
 *
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopCartCell" forIndexPath:indexPath];
    
    ZbIndexButton *isSelectBtn = (ZbIndexButton *)[cell viewWithTag:1];
    isSelectBtn.index = indexPath.row;
    [isSelectBtn addTarget:self action:@selector(selectGoosFlsgClick:) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *sampleImage = (UIImageView *)[cell viewWithTag:2];
    UILabel *titleLab = (UILabel *)[cell viewWithTag:3];
    UILabel *specLab = (UILabel *)[cell viewWithTag:4];
    UILabel *priceLab = (UILabel *)[cell viewWithTag:5];
    ZbIndexButton *plus = (ZbIndexButton *)[cell viewWithTag:8];
    UILabel *countLab = (UILabel *)[cell viewWithTag:9];
    ZbIndexButton *add = (ZbIndexButton *)[cell viewWithTag:10];
    UIView *operView = (UIView *)[cell viewWithTag:11];
    
    // 数量容器圆角边框
    operView.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    operView.layer.borderWidth = 1;
    operView.layer.cornerRadius = 3;
    operView.layer.masksToBounds = YES;
    
    // 当前数据
    NSMutableDictionary *curDict = [self.array objectAtIndex:indexPath.row];
    // 勾选状态
    NSNumber *selectStatus = curDict[@"HS_isSelect"];
    BOOL isSelect = (selectStatus == nil || selectStatus.integerValue == 0) ? NO : YES;
    isSelectBtn.selected = isSelect;
    // 数量加减
    [plus addTarget:self action:@selector(plusOnClick:) forControlEvents:UIControlEventTouchUpInside];
    plus.index = indexPath.row;
    [add addTarget:self action:@selector(addOnClick:) forControlEvents:UIControlEventTouchUpInside];
    add.index = indexPath.row;
    
    [sampleImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:curDict[@"stone_image"]]]];
    titleLab.text = curDict[@"stone_name"];
    specLab.text = curDict[@"spec_name"];
    float price = [curDict[@"spec_price"] floatValue] / 100;
    priceLab.text = [NSString stringWithFormat:@"%.2f", price];
    countLab.text = [NSString stringWithFormat:@"%@", curDict[@"count"]];
    
    return cell;
}

/**
 *  设置cell的高度
 *
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85.0f;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // 点cell等于点勾选框
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    ZbIndexButton *isSelectBtn = (ZbIndexButton *)[cell viewWithTag:1];
    [self selectGoosFlsgClick:isSelectBtn];
    return;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // 删除购物车物品
        id sample = [self.array objectAtIndex:indexPath.row];
        [self.array removeObject:sample];
        [[ZbSaveManager getShoppingCart] removeObject:sample];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // 删除后，全选按钮、总价变化
        [self drawBottomView];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


@end
