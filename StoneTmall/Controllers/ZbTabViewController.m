//
//  ZbTabViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTabViewController.h"
#import "ZbQyDetailViewController.h"
#import "ZbProductDetailViewController.h"
#import "ZbStoneDetailViewController.h"
#import "ZbSupplyDemandDetailViewController.h"
#import "ZbRewardDetailViewController.h"
#import "MBProgressHUD.h"
#import "EaseMob.h"
#import "ChatViewController.h"
#import "EMCDDeviceManager.h"
#import "UserProfileManager.h"
#import "RobotManager.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "ZbGuideViewController.h"
#import "ConsWebController.h"
#import "ZbTabHomeViewController.h"
#import "UIButton+Badge.h"
#import "ZbTabMineViewController.h"
#import "ZbTabHomeViewController.h"

static NSString *kMessageType = @"MessageType";
static NSString *kConversationChatter = @"ConversationChatter";
static NSString *kGroupName = @"GroupName";

//两次提示的默认间隔
static const CGFloat kDefaultPlaySoundInterval = 3.0;

#define HTTP_REQUEST_COMPANY 0          // 企业详情
#define HTTP_REQUEST_VERSION 1          // 版本号
#define HTTP_REQUEST_AUTOLOGIN 2        // 自动登录
#define HTTP_REQUEST_KEFU 3             // 客服信息
#define HTTP_REQUEST_CHATUSERINFO 4     // 聊天用户昵称、头像
#define HTTP_REQUEST_AD 5               // 闪屏广告列表
#define HTTP_REQUEST_CTRL 8             // vip开关控制数据

@interface ZbTabViewController () <CTURLConnectionDelegate, UIAlertViewDelegate, IChatManagerDelegate>

@property (nonatomic, strong) MBProgressHUD *hud;            // 提示器
@property (nonatomic, assign) BOOL isInit;                   // 是否已初始化UI

@property (strong, nonatomic) NSDate *lastPlaySoundDate;

@end

@implementation ZbTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 初始化统一的文字提示器
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.removeFromSuperViewOnHide = NO;
    [self.view addSubview:self.hud];
    [self.hud hide:NO];
    
    // 顶部边缘线
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tabBar.frame), 0.6)];
    lineView.backgroundColor = UIColorFromHexString(@"BBBBBB");
    [self.tabBar addSubview:lineView];
    
    // 石材图库背景icon
    UIImageView *ivShicaiBg = [[UIImageView alloc] init];
    ivShicaiBg.image = [UIImage imageNamed:@"tab_shicai_bg"];
    [self.tabBar addSubview:ivShicaiBg];
    [ivShicaiBg sizeToFit];
    ivShicaiBg.center = CGPointMake(self.tabBar.center.x, -9.4);
    // 石材图库icon
    UIImageView *ivShicai = [[UIImageView alloc] init];
    ivShicai.image = [UIImage imageNamed:@"tab_shicai"];
    [self.tabBar addSubview:ivShicai];
    [ivShicai sizeToFit];
    ivShicai.center = CGPointMake(self.tabBar.center.x, 7);
    
    // 检查版本号
    [self loadVersionData];
    // 请求客服信息
    [self loadKefuData];
    // 请求ip信息
    [self loadIPAddr];
    // 请求广告信息
    [self getSplashScreenAdList];
    // 请求VIP开关信息
    [self getCtrlInfo];
    
    // 自动登录
    if ([ZbWebService recoverCookie]) {
        [self autoLogin];
    }
    
    // 注册环信delegate
    [self registerNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUntreatedApplyCount) name:@"setupUntreatedApplyCount" object:nil];
    // 注册微信请求显示信息通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationWxShowMessage:) name:NOTIFICATION_WX_SHOW_MESSAGE object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.isInit) {
        self.isInit = YES;
        [self viewWillAppearOnce];
    }
    
    BOOL isLogin = [ZbWebService isLogin];
    NSDictionary *user = [ZbWebService sharedUser];
    
    if (!isLogin) {
        // 未登录
        return;
    }
    
    if (![user[@"company_id"] isEqualToString:@"0"]) {
        // 企业用户
        [self loadQyData];
    }
}

// 初始化UI时机，只执行一次
- (void)viewWillAppearOnce {
    return;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // 是否播放过引导页
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSNumber *guideSeen = [userDefault valueForKey:SAVE_KEY_GUIDE_SEEN];
    if (guideSeen == nil) {
        // 播放引导页
        ZbGuideViewController *guideController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbGuideViewController"];
        [self.navigationController presentViewController:guideController animated:YES completion:nil];
    }
    
    // 要显示的点击分享
    NSDictionary *shareDict = [ZbSaveManager getClickShareDict];
    if (shareDict != nil) {
        [self handleShareInfo:shareDict];
        [ZbSaveManager setClickAdDict:nil];
    } else {
        // 显示点击的闪屏广告内容
        NSDictionary *adDict = [ZbSaveManager getClickAdDict];
        if (adDict != nil) {
            // 有分享则不显示
            NSString *companyId = adDict[@"company_id"];
            ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
            controller.companyId = companyId;
            [self.navigationController pushViewController:controller animated:YES];
            [ZbSaveManager setClickAdDict:nil];
        }
    }
    
    // 显示点击的推送信息
    NSDictionary *notify = [ZbSaveManager getClickNotification];
    if (notify != nil) {
        [self handleAppNotification:notify];
        [ZbSaveManager setClickNotification:nil];
    }
}

#pragma mark - 维护企业信息
// 读取个人企业信息
- (void)loadQyData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getBindCompanyInfo] delegate:self];
    conn.tag = HTTP_REQUEST_COMPANY;
    [conn start];
}

// 自动登录
- (void)autoLogin {
//    NSLog(@"自动登录开始!!!");
//    NSLog(@"cookies=%@", [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies);
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService autoLogin] delegate:self];
    conn.tag = HTTP_REQUEST_AUTOLOGIN;
    [conn start];
    return;
}

// 检查版本号
- (void)loadVersionData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:APP_CHECKVERSION_URL delegate:self];
    conn.tag = HTTP_REQUEST_VERSION;
    [conn start];
}

// 请求客服信息
- (void)loadKefuData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getKefuInfo] delegate:self];
    conn.tag = HTTP_REQUEST_KEFU;
    [conn start];
}

// 获取ip地址
- (void)loadIPAddr {
    NSError *error;
    NSURL *ipURL = [NSURL URLWithString:@"http://automation.whatismyip.com/n09230945.asp"];
    NSString *ip = [NSString stringWithContentsOfURL:ipURL encoding:NSUTF8StringEncoding error:&error];
    
//    DLog(@"手机的IP是：%@", ip);
}

// 获取闪屏广告列表
- (void)getSplashScreenAdList {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getSplashScreenAdList] delegate:self];
    conn.tag = HTTP_REQUEST_AD;
    [conn start];
}

// 控制信息
- (void)getCtrlInfo {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCtrlInfo] delegate:self];
    conn.tag = HTTP_REQUEST_CTRL;
    [conn start];
}

#pragma mark - private

-(void)registerNotifications
{
    [self unregisterNotifications];
    
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
//    [[EaseMob sharedInstance].callManager addDelegate:self delegateQueue:nil];
}

-(void)unregisterNotifications
{
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
//    [[EaseMob sharedInstance].callManager removeDelegate:self];
}

// 统计未读消息数
-(void)setupUnreadMessageCount
{
    // 未读处理
    [self unReadMessageAndApplyLogic];
}

- (void)setupUntreatedApplyCount
{
    // 未读处理
    [self unReadMessageAndApplyLogic];
    
//    // 聊天主界面未读处理
//    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//    MainViewController *chatMainViewContrller = nil;
//    for (id viewController in viewControllers)
//    {
//        if ([viewController isKindOfClass:[MainViewController class]])
//        {
//            chatMainViewContrller = viewController;
//            break;
//        }
//    }
//    if (chatMainViewContrller)
//    {
//        [chatMainViewContrller setupUntreatedApplyCount];
//    }
}

- (void)unReadMessageAndApplyLogic {
    // 未读请求
    NSInteger unreadCount = [[[ApplyViewController shareController] dataSource] count];
    // 加未读消息
    NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    
    if (![ZbWebService isLogin]) {
        unreadCount = 0;
    }
    // app红点
    UIApplication *application = [UIApplication sharedApplication];
    [application setApplicationIconBadgeNumber:unreadCount];
    // 红点值
    NSString *badge;
    if (unreadCount <= 0) {
        badge = nil;
    } else if (unreadCount > 99) {
        badge = @"99+";
    } else {
        badge = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:unreadCount]];
    }
    // 主页面红点
    ZbTabHomeViewController *home = (ZbTabHomeViewController *)[self.viewControllers firstObject];
    if (home) {
        home.btnChat.badgeValue = badge;
        home.btnChat.badgeOffsetX = -15;
    }
    // 我的tab红点
    ZbTabMineViewController *mine = self.viewControllers.lastObject;
    if (mine) {
        if (![ZbWebService isLogin]) {
            [mine drawUserInfo];
            badge = nil;
        }
        mine.tabBarItem.badgeValue = badge;
        [mine.tv reloadData];
    }
    return;
}

#pragma mark - public
// 处理信鸽来的推送
- (void)handleAppNotification:(NSDictionary *)userInfo {
    NSString *pushType = userInfo[@"push_type"];
    NSString *idInfo = userInfo[@"id"];
    if ([pushType isEqualToString:@"web"]) {
        // 网页
        NSString *url = userInfo[@"url"];
        NSString *title = userInfo[@"title"];
        ConsWebController *webController = [[ConsWebController alloc] init];
        webController.showTitle = title;
        webController.mainUrl = url;
        [self.navigationController pushViewController:webController animated:YES];
    } else if ([pushType isEqualToString:@"stone"]) {
        // 石材
//        ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
//        controller.stoneName = stoneName;
//        [self.navigationController pushViewController:controller animated:YES];
    } else if ([pushType isEqualToString:@"company"]) {
        // 企业
        ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
        controller.companyId = idInfo;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([pushType isEqualToString:@"product"]) {
        // 产品
        ZbProductDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
        controller.productId = idInfo;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([pushType isEqualToString:@"supplydemand"]) {
        // 供需
        ZbSupplyDemandDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
        controller.curPostId = idInfo;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([pushType isEqualToString:@"paiddemand"]) {
        // 悬赏
        ZbRewardDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardDetailViewController"];
        controller.curPostId = idInfo;
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        // 不支持类型
    }
}

#pragma mark - 通知
// 微信请求显示信息通知
- (void)notificationWxShowMessage:(NSNotification *)notif {
    // 通知信息
    NSDictionary *info = notif.object;
    [self handleShareInfo:info];
    return;
}

// 处理分享
- (void)handleShareInfo:(NSDictionary *)shareDict {
    NSDictionary *info = shareDict;
    NSNumber *tag = info[@"tag"];
    // 跳转
    switch (tag.intValue) {
        case Zb_Share_Ext_Type_Stone:
        {
            // 石材
            NSString *stoneName = info[@"stone_name"];
            ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
            controller.stoneName = stoneName;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case Zb_Share_Ext_Type_Product:
        {
            // 产品
            NSString *productId = info[@"product_id"];
            ZbProductDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
            controller.productId = productId;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case Zb_Share_Ext_Type_Company:
        {
            // 企业
            NSString *companyId = info[@"company_id"];
            ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
            controller.companyId = companyId;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case Zb_Share_Ext_Type_SAndD:
        {
            // 供需
            NSString *postId = info[@"post_id"];
            ZbSupplyDemandDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
            controller.curPostId = postId;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case Zb_Share_Ext_Type_Reward:
        {
            // 悬赏
            NSString *postId = info[@"post_id"];
            ZbRewardDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardDetailViewController"];
            controller.curPostId = postId;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case Zb_Share_Ext_Type_News:
        {
            // 新闻
            NSString *postId = info[@"post_id"];
            ZbSupplyDemandDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
            controller.curPostId = postId;
            controller.isNews = YES;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        default:
            break;
    }
    // 清空已显示分享
    [ZbSaveManager setClickShareDict:nil];
    return;
}

#pragma mark - UIAlertView delegate
// 确认框点击
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    // 跳转appStore
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:APP_APPSTORE_URL]];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    if (connection.tag == HTTP_REQUEST_AUTOLOGIN) {
        NSLog(@"自动登录 didFailWithError=%@", error);
    }
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_COMPANY) {
        // 企业详情
        if (result.integerValue != 0) {
            // 加载失败
            return;
        }
        ZbQyInfo *info = [[ZbQyInfo alloc] initWithDictionary:resultDic[@"data"]];
        [ZbWebService setQy:info];
    } else if (connection.tag == HTTP_REQUEST_VERSION) {
        // 版本号
        NSInteger resultCount = [[resultDic objectForKey:@"resultCount"] integerValue];
        if (resultCount != 1) {
            return;
        }
        // 最新版本号
        NSArray *resultArray = [resultDic objectForKey:@"results"];
        NSDictionary *infoDict = [resultArray objectAtIndex:0];
        NSString *lastestVersion = [infoDict objectForKey:@"version"];
        // 本地版本号
        NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
        NSString *currentVersion = [appInfo objectForKey:@"CFBundleShortVersionString"];
        // 更新内容
        NSString *releaseNotes = infoDict[@"releaseNotes"];
        
        if ([lastestVersion doubleValue] > [currentVersion doubleValue]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"新版本已发布！" message:releaseNotes delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"前往", nil];
            [alert show];
        }
    } else if (connection.tag == HTTP_REQUEST_AUTOLOGIN) {
        // 自动登录
        if (result.integerValue != 0) {
            // 失败
//            [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:YES completion:^(NSDictionary *info, EMError *error) {
//                if (!error) {
//                    DLog(@"自动登录失败，环信退出成功");
//                }
//            } onQueue:nil];
            AppDelegate *applegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [applegate clearLoginInfo];
            return;
        }
        
        // 设置用户信息
        NSMutableDictionary *user = resultDic[@"data"];
        NSString *account = user[@"user_name"];
        NSString *psw = user[@"easemob_passwd"];
//        // 继续同步登录环信
//        EMError *error = nil;
//        [[EaseMob sharedInstance].chatManager loginWithUsername:account password:psw error:&error];
//        if (error) {
//            AppDelegate *applegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//            [applegate clearLoginInfo];
//            return;
//        }
//        DLog(@"环信登录成功成功!");
//        
//        //获取数据库中数据
//        [[EaseMob sharedInstance].chatManager loadDataFromDatabase];
//        
//        //获取群组列表
//        [[EaseMob sharedInstance].chatManager asyncFetchMyGroupsList];
//
//        
//        // 环信已设置自动登录，这里不需要再登录环信
        // 设置用户数据
        [ZbWebService setUser:user];
        // 保存cookie到本地
        [ZbWebService saveCookie];
        // 提示
        self.hud.labelText = @"自动登录成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
        
        if (![user[@"company_id"] isEqualToString:@"0"]) {
            // 企业用户
            [self loadQyData];
        }
        
        // 异步登录环信
        [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:account password:psw completion:^(NSDictionary *loginInfo, EMError *error) {
            if (!error && loginInfo) {
                DLog(@"环信登录成功成功!");
                
                //获取数据库中数据
                [[EaseMob sharedInstance].chatManager loadDataFromDatabase];
                
                //获取群组列表
                [[EaseMob sharedInstance].chatManager asyncFetchMyGroupsList];
            }
        } onQueue:nil];
        
        //    //设置是否自动登录
        //    [[EaseMob sharedInstance].chatManager setIsAutoLoginEnabled:YES];
        //
        //    // 旧数据转换 (如果您的sdk是由2.1.2版本升级过来的，需要家这句话)
        //    [[EaseMob sharedInstance].chatManager importDataToNewDatabase];
    } else if (connection.tag == HTTP_REQUEST_KEFU) {
        // 客服信息
        if (result.integerValue != 0) {
            // 失败
            return;
        }
        // 设置客服信息
        NSDictionary *kefuDict = resultDic[@"data"];
        [ZbSaveManager setKefuDict:kefuDict];
    } else if (connection.tag == HTTP_REQUEST_CHATUSERINFO) {
        // 加载失败
        if (result.integerValue != 0) {
            return;
        }
        NSArray *array = resultDic[@"data"];
        if (array.count > 0) {
            [ZbSaveManager addUserInfoArray:array];
            // 刷新界面
            NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            MainViewController *chatMainViewContrller = nil;
            for (id viewController in viewControllers)
            {
                if ([viewController isKindOfClass:[MainViewController class]])
                {
                    chatMainViewContrller = viewController;
                    break;
                }
            }
            if (chatMainViewContrller)
            {
                [chatMainViewContrller onUserExtraInfoChange];
            }
        }
    } else if (connection.tag == HTTP_REQUEST_AD) {
        // 加载失败
        if (result.integerValue != 0) {
            return;
        }
        NSArray *array = resultDic[@"data"];
        
        // 存储广告列表
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:[NSArray arrayWithArray:array] forKey:SAVE_KEY_AD];
        [userDefault synchronize];
        
        if (array.count > 0) {
            // 随机一条下次播放
            int randIndex = arc4random() % array.count;
            // 存储下一次播放索引
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:[NSNumber numberWithInt:randIndex] forKey:SAVE_KEY_AD_INDEX];
            [userDefault synchronize];
            
            // 优先下载好下一次要播放的图片
            NSDictionary *adDict = [array objectAtIndex:randIndex];
            NSString *ad_image = adDict[@"ad_image"];
            NSString *imageUrl = [ZbWebService getImageFullUrl:ad_image];
            BOOL isExists = [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:imageUrl]];
            if (!isExists) {
                [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imageUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//                    NSLog(@"下一张===下载图片完毕,url=%@", imageURL);
                }];
            }
            
            // 下载其他图片
            for (NSDictionary *curDict in array) {
                if ([curDict isEqual:adDict]) {
                    continue;
                }
                NSString *ad_image = curDict[@"ad_image"];
                NSString *imageUrl = [ZbWebService getImageFullUrl:ad_image];
                BOOL isExists = [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:imageUrl]];
                if (isExists) {
                    continue;
                }
                [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imageUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                    NSLog(@"其他图片===下载图片完毕,url=%@", imageURL);
                }];
            }
        } else {
            // 清空下一次播放索引
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:nil forKey:SAVE_KEY_AD_INDEX];
            [userDefault synchronize];
        }
    } else if (connection.tag == HTTP_REQUEST_CTRL) {
        // 开关信息
        if (result.integerValue != 0) {
            // 加载失败
            return;
        }
        
        // 设置开关信息
        NSMutableDictionary *infoDict = resultDic[@"data"];
        [ZbSaveManager setCtrlInfo:infoDict];
        // 绘制悬赏开启状态
        ZbTabHomeViewController *home = self.viewControllers.firstObject;
        [home setRewardOpenStatus];
    }
    return;
}

#pragma mark - call

- (BOOL)canRecord
{
    __block BOOL bCanRecord = YES;
//    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending)
//    {
//        if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
//            [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
//                bCanRecord = granted;
//            }];
//        }
//    }
//    
//    if (!bCanRecord) {
//        UIAlertView * alt = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"setting.microphoneNoAuthority", @"No microphone permissions") message:NSLocalizedString(@"setting.microphoneAuthority", @"Please open in \"Setting\"-\"Privacy\"-\"Microphone\".") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", @"OK"), nil];
//        [alt show];
//    }
//    
    return bCanRecord;
}

- (void)callOutWithChatter:(NSNotification *)notification
{
//    id object = notification.object;
//    if ([object isKindOfClass:[NSDictionary class]]) {
//        if (![self canRecord]) {
//            return;
//        }
//        
//        EMError *error = nil;
//        NSString *chatter = [object objectForKey:@"chatter"];
//        EMCallSessionType type = [[object objectForKey:@"type"] intValue];
//        EMCallSession *callSession = nil;
//        if (type == eCallSessionTypeAudio) {
//            callSession = [[EaseMob sharedInstance].callManager asyncMakeVoiceCall:chatter timeout:50 error:&error];
//        }
//        else if (type == eCallSessionTypeVideo){
//            if (![CallViewController canVideo]) {
//                return;
//            }
//            callSession = [[EaseMob sharedInstance].callManager asyncMakeVideoCall:chatter timeout:50 error:&error];
//        }
//        
//        if (callSession && !error) {
//            [[EaseMob sharedInstance].callManager removeDelegate:self];
//            
//            CallViewController *callController = [[CallViewController alloc] initWithSession:callSession isIncoming:NO];
//            callController.modalPresentationStyle = UIModalPresentationOverFullScreen;
//            [self presentViewController:callController animated:NO completion:nil];
//        }
//        
//        if (error) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", @"error") message:error.description delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
//            [alertView show];
//        }
//    }
}

- (void)callControllerClose:(NSNotification *)notification
{
    //    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    //    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    //    [audioSession setActive:YES error:nil];
    
//    [[EaseMob sharedInstance].callManager addDelegate:self delegateQueue:nil];
}

#pragma mark - IChatManagerDelegate 消息变化

- (void)didUpdateConversationList:(NSArray *)conversationList
{
    [self setupUnreadMessageCount];
    // 对话变化重取用户信息，补充陌生人数据
    NSMutableArray *namesArray = [NSMutableArray array];
    // 会话列表
    NSArray *conversations = [[EaseMob sharedInstance].chatManager conversations];
    for (EMConversation *convs in conversations) {
        if (convs.conversationType == eConversationTypeChat) {
            UserProfileEntity *my = [[UserProfileManager sharedInstance] getUserProfileByUsername:convs.chatter];
            if (my == nil) {
                // 未取过的用户数据才去获取
                [namesArray addObject:convs.chatter];
            }
        }
    }
    // 去除重复
    NSSet *set = [NSSet setWithArray:namesArray];
    namesArray = [NSMutableArray arrayWithArray:[set allObjects]];
    if (namesArray.count <= 0) {
        return;
    }
    
    NSString *names = [namesArray componentsJoinedByString:@"|"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getUsersByUserNames:names] delegate:self];
    conn.tag = HTTP_REQUEST_CHATUSERINFO;
    [conn start];
}

// 未读消息数量变化回调
-(void)didUnreadMessagesCountChanged
{
    [self setupUnreadMessageCount];
}

- (void)didFinishedReceiveOfflineMessages
{
    [self setupUnreadMessageCount];
}

- (void)didFinishedReceiveOfflineCmdMessages
{
    
}

- (BOOL)needShowNotification:(NSString *)fromChatter
{
    BOOL ret = YES;
    NSArray *igGroupIds = [[EaseMob sharedInstance].chatManager ignoredGroupIds];
    for (NSString *str in igGroupIds) {
        if ([str isEqualToString:fromChatter]) {
            ret = NO;
            break;
        }
    }
    
    return ret;
}

// 收到消息回调
-(void)didReceiveMessage:(EMMessage *)message
{
    BOOL needShowNotification = (message.messageType != eMessageTypeChat) ? [self needShowNotification:message.conversationChatter] : YES;
    if (needShowNotification) {
#if !TARGET_IPHONE_SIMULATOR
        
        //        BOOL isAppActivity = [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive;
        //        if (!isAppActivity) {
        //            [self showNotificationWithMessage:message];
        //        }else {
        //            [self playSoundAndVibration];
        //        }
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        switch (state) {
            case UIApplicationStateActive:
                [self playSoundAndVibration];
                break;
            case UIApplicationStateInactive:
                [self playSoundAndVibration];
                break;
            case UIApplicationStateBackground:
                [self showNotificationWithMessage:message];
                break;
            default:
                break;
        }
#endif
    }
}

-(void)didReceiveCmdMessage:(EMMessage *)message
{
//    [self showHint:NSLocalizedString(@"receiveCmd", @"receive cmd message")];
}

- (void)playSoundAndVibration{
    NSTimeInterval timeInterval = [[NSDate date]
                                   timeIntervalSinceDate:self.lastPlaySoundDate];
    if (timeInterval < kDefaultPlaySoundInterval) {
        //如果距离上次响铃和震动时间太短, 则跳过响铃
        DLog(@"skip ringing & vibration %@, %@", [NSDate date], self.lastPlaySoundDate);
        return;
    }
    
    //保存最后一次响铃时间
    self.lastPlaySoundDate = [NSDate date];
    
    // 声音
    BOOL isPlayVoice = [[[NSUserDefaults standardUserDefaults] objectForKey:CHAT_SETTING_VOICE] boolValue];
    if (isPlayVoice) {
        // 收到消息时，播放音频
        [[EMCDDeviceManager sharedInstance] playNewMessageSound];
    }
    BOOL isPlayShake = [[[NSUserDefaults standardUserDefaults] objectForKey:CHAT_SETTING_SHAKE] boolValue];
    if (isPlayShake) {
        // 收到消息时，震动
        [[EMCDDeviceManager sharedInstance] playVibration];
    }
}

- (void)showNotificationWithMessage:(EMMessage *)message
{
    EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
    //发送本地推送
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate date]; //触发通知的时间
    
    if (options.displayStyle == ePushNotificationDisplayStyle_messageSummary) {
        id<IEMMessageBody> messageBody = [message.messageBodies firstObject];
        NSString *messageStr = nil;
        switch (messageBody.messageBodyType) {
            case eMessageBodyType_Text:
            {
                messageStr = ((EMTextMessageBody *)messageBody).text;
            }
                break;
            case eMessageBodyType_Image:
            {
                messageStr = NSLocalizedString(@"message.image", @"Image");
            }
                break;
            case eMessageBodyType_Location:
            {
                messageStr = NSLocalizedString(@"message.location", @"Location");
            }
                break;
            case eMessageBodyType_Voice:
            {
                messageStr = NSLocalizedString(@"message.voice", @"Voice");
            }
                break;
            case eMessageBodyType_Video:{
                messageStr = NSLocalizedString(@"message.video", @"Video");
            }
                break;
            default:
                break;
        }
        
        NSString *title = [[UserProfileManager sharedInstance] getNickNameWithUsername:message.from];
        if (message.messageType == eMessageTypeGroupChat) {
            NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
            for (EMGroup *group in groupArray) {
                if ([group.groupId isEqualToString:message.conversationChatter]) {
                    title = [NSString stringWithFormat:@"%@(%@)", message.groupSenderName, group.groupSubject];
                    break;
                }
            }
        }
        else if (message.messageType == eMessageTypeChatRoom)
        {
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            NSString *key = [NSString stringWithFormat:@"OnceJoinedChatrooms_%@", [[[EaseMob sharedInstance].chatManager loginInfo] objectForKey:@"username" ]];
            NSMutableDictionary *chatrooms = [NSMutableDictionary dictionaryWithDictionary:[ud objectForKey:key]];
            NSString *chatroomName = [chatrooms objectForKey:message.conversationChatter];
            if (chatroomName)
            {
                title = [NSString stringWithFormat:@"%@(%@)", message.groupSenderName, chatroomName];
            }
        }
        
        notification.alertBody = [NSString stringWithFormat:@"%@:%@", title, messageStr];
    }
    else{
        notification.alertBody = NSLocalizedString(@"receiveMessage", @"you have a new message");
    }
    
#warning 去掉注释会显示[本地]开头, 方便在开发中区分是否为本地推送
    //notification.alertBody = [[NSString alloc] initWithFormat:@"[本地]%@", notification.alertBody];
    
    notification.alertAction = NSLocalizedString(@"open", @"Open");
    notification.timeZone = [NSTimeZone defaultTimeZone];
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:self.lastPlaySoundDate];
    if (timeInterval < kDefaultPlaySoundInterval) {
        DLog(@"skip ringing & vibration %@, %@", [NSDate date], self.lastPlaySoundDate);
    } else {
        notification.soundName = UILocalNotificationDefaultSoundName;
        self.lastPlaySoundDate = [NSDate date];
    }
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    [userInfo setObject:[NSNumber numberWithInt:message.messageType] forKey:kMessageType];
    [userInfo setObject:message.conversationChatter forKey:kConversationChatter];
    notification.userInfo = userInfo;
    
    //发送通知
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    //    UIApplication *application = [UIApplication sharedApplication];
    //    application.applicationIconBadgeNumber += 1;
}

#pragma mark - IChatManagerDelegate 登陆回调（主要用于监听自动登录是否成功）

- (void)didLoginWithInfo:(NSDictionary *)loginInfo error:(EMError *)error
{
    //    if (error) {
    //        NSString *hintText = NSLocalizedString(@"reconnection.retry", @"Fail to log in your account, is try again... \nclick 'logout' button to jump to the login page \nclick 'continue to wait for' button for reconnection successful");
    //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt")
    //                                                            message:hintText
    //                                                           delegate:self
    //                                                  cancelButtonTitle:NSLocalizedString(@"reconnection.wait", @"continue to wait")
    //                                                  otherButtonTitles:NSLocalizedString(@"logout", @"Logout"),
    //                                  nil];
    //        alertView.tag = 99;
    //        [alertView show];
    //        [_chatListVC isConnect:NO];
    //    }
}

#pragma mark - IChatManagerDelegate 好友变化

- (void)didReceiveBuddyRequest:(NSString *)username
                       message:(NSString *)message
{
#if !TARGET_IPHONE_SIMULATOR
    [self playSoundAndVibration];
    
    BOOL isAppActivity = [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive;
    if (!isAppActivity) {
        //发送本地推送
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = [NSDate date]; //触发通知的时间
        notification.alertBody = [NSString stringWithFormat:NSLocalizedString(@"friend.somebodyAddWithName", @"%@ add you as a friend"), username];
        notification.alertAction = NSLocalizedString(@"open", @"Open");
        notification.timeZone = [NSTimeZone defaultTimeZone];
    }
#endif
}

- (void)_removeBuddies:(NSArray *)userNames
{
    [[EaseMob sharedInstance].chatManager removeConversationsByChatters:userNames deleteMessages:YES append2Chat:YES];
    
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    ChatViewController *chatViewContrller = nil;
    for (id viewController in viewControllers)
    {
        if ([viewController isKindOfClass:[ChatViewController class]] && [userNames containsObject:[(ChatViewController *)viewController chatter]])
        {
            chatViewContrller = viewController;
            break;
        }
    }
    if (chatViewContrller)
    {
        [viewControllers removeObject:chatViewContrller];
        [self.navigationController setViewControllers:viewControllers animated:YES];
    }
}

- (void)didUpdateBuddyList:(NSArray *)buddyList
            changedBuddies:(NSArray *)changedBuddies
                     isAdd:(BOOL)isAdd
{
    if (!isAdd)
    {
        NSMutableArray *deletedBuddies = [NSMutableArray array];
        for (EMBuddy *buddy in changedBuddies)
        {
            if ([buddy.username length])
            {
                [deletedBuddies addObject:buddy.username];
            }
        }
        if (![deletedBuddies count])
        {
            return;
        }
        
        [self _removeBuddies:deletedBuddies];
    } else {
        // clear conversation
        NSArray *conversations = [[EaseMob sharedInstance].chatManager conversations];
        NSMutableArray *deleteConversations = [NSMutableArray arrayWithArray:conversations];
        NSMutableDictionary *buddyDic = [NSMutableDictionary dictionary];
        for (EMBuddy *buddy in buddyList) {
            if ([buddy.username length]) {
                [buddyDic setObject:buddy forKey:buddy.username];
            }
        }
        for (EMConversation *conversation in conversations) {
            if (conversation.conversationType == eConversationTypeChat) {
                if ([buddyDic objectForKey:conversation.chatter]) {
                    [deleteConversations removeObject:conversation];
                }
            } else {
                [deleteConversations removeObject:conversation];
            }
        }
        if ([deleteConversations count] > 0) {
            NSMutableArray *deletedBuddies = [NSMutableArray array];
            for (EMConversation *conversation in deleteConversations) {
                if (![[RobotManager sharedInstance] isRobotWithUsername:conversation.chatter]) {
                    [deletedBuddies addObject:conversation.chatter];
                }
            }
            if ([deletedBuddies count] > 0) {
                [self _removeBuddies:deletedBuddies];
            }
        }
        
        // 好友话变化重取用户信息，补充新好友数据
        NSMutableArray *namesArray = [NSMutableArray array];
        for (EMBuddy *buddy in changedBuddies)
        {
            if ([buddy.username length])
            {
                UserProfileEntity *my = [[UserProfileManager sharedInstance] getUserProfileByUsername:buddy.username];
                if (my == nil) {
                    // 未取过的用户数据才去获取
                    [namesArray addObject:buddy.username];
                }
            }
        }
        // 去除重复
        NSSet *set = [NSSet setWithArray:namesArray];
        namesArray = [NSMutableArray arrayWithArray:[set allObjects]];
        if (namesArray.count <= 0) {
            return;
        }
        
        NSString *names = [namesArray componentsJoinedByString:@"|"];
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getUsersByUserNames:names] delegate:self];
        conn.tag = HTTP_REQUEST_CHATUSERINFO;
        [conn start];
    }
}

- (void)didRemovedByBuddy:(NSString *)username
{
    [self _removeBuddies:@[username]];
}

- (void)didAcceptedByBuddy:(NSString *)username
{
    // 别人接受我的好友请求，走不到didUpdateBuddyList，必须在这里请求好友昵称头像
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getUsersByUserNames:username] delegate:self];
    conn.tag = HTTP_REQUEST_CHATUSERINFO;
    [conn start];
}

- (void)didRejectedByBuddy:(NSString *)username
{
//    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"friend.beRefusedToAdd", @"you are shameless refused by '%@'"), username];
//    TTAlertNoTitle(message);
}

- (void)didAcceptBuddySucceed:(NSString *)username
{
}

#pragma mark - IChatManagerDelegate 群组变化

- (void)didReceiveGroupInvitationFrom:(NSString *)groupId
                              inviter:(NSString *)username
                              message:(NSString *)message
{
#if !TARGET_IPHONE_SIMULATOR
    [self playSoundAndVibration];
#endif
}

//接收到入群申请
- (void)didReceiveApplyToJoinGroup:(NSString *)groupId
                         groupname:(NSString *)groupname
                     applyUsername:(NSString *)username
                            reason:(NSString *)reason
                             error:(EMError *)error
{
    if (!error) {
#if !TARGET_IPHONE_SIMULATOR
        [self playSoundAndVibration];
#endif
    }
}

- (void)didReceiveGroupRejectFrom:(NSString *)groupId
                          invitee:(NSString *)username
                           reason:(NSString *)reason
{
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"friend.beRefusedToAdd", @"you are shameless refused by '%@'"), username];
    TTAlertNoTitle(message);
}


- (void)didReceiveAcceptApplyToJoinGroup:(NSString *)groupId
                               groupname:(NSString *)groupname
{
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"group.agreedAndJoined", @"agreed to join the group of \'%@\'"), groupname];
    [self showHint:message];
}

#pragma mark - IChatManagerDelegate 收到聊天室邀请

- (void)didReceiveChatroomInvitationFrom:(NSString *)chatroomId
                                 inviter:(NSString *)username
                                 message:(NSString *)message
{
    message = [NSString stringWithFormat:NSLocalizedString(@"chatroom.somebodyInvite", @"%@ invite you to join chatroom \'%@\'"), username, chatroomId];
    [self showHint:message];
}

#pragma mark - IChatManagerDelegate 登录状态变化

- (void)didLoginFromOtherDevice
{
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:NO completion:^(NSDictionary *info, EMError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"loginAtOtherDevice", @"your login account has been in other places") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
        [alertView show];
        
        // 返回根界面
        [self.navigationController popToRootViewControllerAnimated:NO];
        // 清空登录状态
        AppDelegate *applegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [applegate clearLoginInfo];
        // 未读处理
        [self setupUntreatedApplyCount];
        
    } onQueue:nil];
}

- (void)didRemovedFromServer
{
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:NO completion:^(NSDictionary *info, EMError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"loginUserRemoveFromServer", @"your account has been removed from the server side") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
        [alertView show];
        
        // 返回根界面
        [self.navigationController popToRootViewControllerAnimated:NO];
        // 清空登录状态
        AppDelegate *applegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [applegate clearLoginInfo];
        // 未读处理
        [self setupUntreatedApplyCount];
    } onQueue:nil];
}

- (void)didServersChanged
{
}

- (void)didAppkeyChanged
{
}

#pragma mark - 自动登录回调

- (void)willAutoReconnect{
    //    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    //    NSNumber *showreconnect = [ud objectForKey:@"identifier_showreconnect_enable"];
    //    if (showreconnect && [showreconnect boolValue]) {
    //        [self hideHud];
    //        [self showHint:NSLocalizedString(@"reconnection.ongoing", @"reconnecting...")];
    //    }
}

- (void)didAutoReconnectFinishedWithError:(NSError *)error{
    //    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    //    NSNumber *showreconnect = [ud objectForKey:@"identifier_showreconnect_enable"];
    //    if (showreconnect && [showreconnect boolValue]) {
    //        [self hideHud];
    //        if (error) {
    //            [self showHint:NSLocalizedString(@"reconnection.fail", @"reconnection failure, later will continue to reconnection")];
    //        }else{
    //            [self showHint:NSLocalizedString(@"reconnection.success", @"reconnection successful！")];
    //        }
    //    }
}

#pragma mark - ICallManagerDelegate

//- (void)callSessionStatusChanged:(EMCallSession *)callSession changeReason:(EMCallStatusChangedReason)reason error:(EMError *)error
//{
    //    if (callSession.status == eCallSessionStatusConnected)
    //    {
    //        EMError *error = nil;
    //        do {
    //            BOOL isShowPicker = [[[NSUserDefaults standardUserDefaults] objectForKey:@"isShowPicker"] boolValue];
    //            if (isShowPicker) {
    //                error = [EMError errorWithCode:EMErrorInitFailure andDescription:NSLocalizedString(@"call.initFailed", @"Establish call failure")];
    //                break;
    //            }
    //
    //            if (![self canRecord]) {
    //                error = [EMError errorWithCode:EMErrorInitFailure andDescription:NSLocalizedString(@"call.initFailed", @"Establish call failure")];
    //                break;
    //            }
    //
    //#warning 在后台不能进行视频通话
    //            if(callSession.type == eCallSessionTypeVideo && ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive || ![CallViewController canVideo])){
    //                error = [EMError errorWithCode:EMErrorInitFailure andDescription:NSLocalizedString(@"call.initFailed", @"Establish call failure")];
    //                break;
    //            }
    //
    //            if (!isShowPicker){
    //                [[EaseMob sharedInstance].callManager removeDelegate:self];
    //                CallViewController *callController = [[CallViewController alloc] initWithSession:callSession isIncoming:YES];
    //                callController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    //                [self presentViewController:callController animated:NO completion:nil];
    //                if ([self.navigationController.topViewController isKindOfClass:[ChatViewController class]])
    //                {
    //                    ChatViewController *chatVc = (ChatViewController *)self.navigationController.topViewController;
    //                    chatVc.isInvisible = YES;
    //                }
    //            }
    //        } while (0);
    //        
    //        if (error) {
    //            [[EaseMob sharedInstance].callManager asyncEndCall:callSession.sessionId reason:eCallReasonHangup];
    //            return;
    //        }
    //    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
