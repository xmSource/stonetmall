//
//  ZbTabViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbTabViewController : UITabBarController

- (void)setupUntreatedApplyCount;

#pragma mark - public
// 处理信鸽来的推送
- (void)handleAppNotification:(NSDictionary *)userInfo;

@end
