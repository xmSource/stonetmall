//
//  ZbQyDetailViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbQyDetailViewController : BaseViewController

// 当前企业字典
@property (nonatomic, strong) NSString *companyId;

@end
