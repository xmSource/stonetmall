//
//  ZbSearchViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/24.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSearchViewController.h"
#import "ZbTypeTableViewCell.h"
#import "ZbSeeMoreTableViewCell.h"
#import "ZbSearchStoneTableViewCell.h"
#import "ZbSupplyDemandTableViewCell.h"
#import "ZbSmallQyTableViewCell.h"
#import "ZbProductTableViewCell.h"
#import "ZbSearchRecordTableViewCell.h"
#import "ZbIndexCharTableViewCell.h"
#import "ZbSearchSingleViewController.h"
#import "ZbStoneDetailViewController.h"
#import "ZbQyDetailViewController.h"
#import "ZbProductDetailViewController.h"
#import "ZbSupplyDemandDetailViewController.h"
#import "ZbRewardDetailViewController.h"

#define HTTP_REQUEST_STONE 0          // 搜石材
#define HTTP_REQUEST_PRODUCT 1        // 搜产品
#define HTTP_REQUEST_COMPANY 2        // 搜企业
#define HTTP_REQUEST_S_AND_D 3        // 搜供需

@interface ZbSearchViewController () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

// 导航栏titleView
@property (nonatomic, strong) IBOutlet UIView *navTitleView;
// 导航栏搜索栏
@property (nonatomic, strong) IBOutlet UITextField *tfSearch;
// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;

// 历史记录数组
@property (nonatomic, strong) NSMutableArray *hisRecordArray;
// 企业数组
@property (nonatomic, strong) NSMutableArray *companyArray;
// 产品数组
@property (nonatomic, strong) NSMutableArray *productArray;
// 供需信息数组
@property (nonatomic, strong) NSMutableArray *supplyArray;
// 石材数组
@property (nonatomic, strong) NSMutableArray *stoneArray;

// 是否搜索状态
@property (nonatomic, assign) BOOL isSearching;

@end

@implementation ZbSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hisRecordArray = Mutable_ArrayInit;
    self.companyArray = Mutable_ArrayInit;
    self.productArray = Mutable_ArrayInit;
    self.supplyArray = Mutable_ArrayInit;
    self.stoneArray = Mutable_ArrayInit;
    
    // 圆角
    self.navTitleView.layer.cornerRadius = CGRectGetHeight(self.navTitleView.frame) / 2;
    self.navTitleView.layer.masksToBounds = YES;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSearchStoneTableViewCell" bundle:nil] forCellReuseIdentifier:SearchStoneCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbTypeTableViewCell" bundle:nil] forCellReuseIdentifier:TypeHeaderIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSeeMoreTableViewCell" bundle:nil] forCellReuseIdentifier:SeeMoreFooterIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSmallQyTableViewCell" bundle:nil] forCellReuseIdentifier:SmallQiyeCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbProductTableViewCell" bundle:nil] forCellReuseIdentifier:ProductCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSupplyDemandTableViewCell" bundle:nil] forCellReuseIdentifier:SupplyDemandCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSearchRecordTableViewCell" bundle:nil] forCellReuseIdentifier:SearchRecordCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbIndexCharTableViewCell" bundle:nil] forCellReuseIdentifier:IndexCharCellIdentifier];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self loadSearchHisRecordData];
    [self.tfSearch becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 点击事件
- (IBAction)leftBarItemOnClick:(id)sender {
    self.isSearching = NO;
    [self.tfSearch resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 接口调用
// 获取搜索记录
- (void)loadSearchHisRecordData {
    self.hisRecordArray = [NSMutableArray arrayWithArray:[[ZbSaveManager shareManager] getGlobalSearchHistory]];
    [self.tv reloadData];
}

// 获取石材列表接口
- (void)requestStoneData:(NSString *)key {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService globalStoneSearchParams:key pageIndex:@"1" pageSize:GLOBAL_SEARCH_PAGE_SIZE] delegate:self];
    conn.tag = HTTP_REQUEST_STONE;
    conn.userInfo = key;
    [conn start];
    return;
}

// 获取产品列表接口
- (void)requestProductData:(NSString *)key {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService globalProductSearchParams:key pageIndex:@"1" pageSize:GLOBAL_SEARCH_PAGE_SIZE] delegate:self];
    conn.tag = HTTP_REQUEST_PRODUCT;
    conn.userInfo = key;
    [conn start];
    return;
}

// 获取企业列表接口
- (void)requestQyData:(NSString *)key {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService globalQySearchParams:key pageIndex:@"1" pageSize:GLOBAL_SEARCH_PAGE_SIZE] delegate:self];
    conn.tag = HTTP_REQUEST_COMPANY;
    conn.userInfo = key;
    [conn start];
    return;
}

// 获取供需列表接口
- (void)requestSupplyDemandData:(NSString *)key {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService globalSAndDSearchParams:key pageIndex:@"1" pageSize:GLOBAL_SEARCH_PAGE_SIZE] delegate:self];
    conn.tag = HTTP_REQUEST_S_AND_D;
    conn.userInfo = key;
    [conn start];
    return;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // 添加全局搜索历史记录
    NSString *key = textField.text;
    if (key.length > 0) {
        [[ZbSaveManager shareManager] addGlobalSearchHistory:key];
    }
    return;
}

- (IBAction)textFieldChanged:(UITextField *)sender {
    if (sender.text.length <= 0) {
        // 历史记录
        self.isSearching = NO;
        [self loadSearchHisRecordData];
    } else {
        // 搜索
        if (self.isSearching == NO) {
            self.isSearching = YES;
            // 刚从历史记录状态切到搜索状态，要重新reload，否则点击了历史记录cell会闪退
            [self.tv reloadData];
            [self requestStoneData:sender.text];
            [self requestProductData:sender.text];
            [self requestQyData:sender.text];
            [self requestSupplyDemandData:sender.text];
        } else {
            if (sender.markedTextRange != nil) {
                return;
            }
            [self requestStoneData:sender.text];
            [self requestProductData:sender.text];
            [self requestQyData:sender.text];
            [self requestSupplyDemandData:sender.text];
        }
    }
    return;
}

#pragma mark - UITableViewDataSource
// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.isSearching ? 4 : 1;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.isSearching) {
        return self.hisRecordArray.count;
    }
    switch (section) {
        case 0:
        {
            return self.stoneArray.count;
        }
        case 1:
        {
            return self.productArray.count;
        }
        case 2:
        {
            return self.companyArray.count;
        }
        case 3:
        {
            return self.supplyArray.count;
        }
            
        default:
            return 0;
    }
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearching) {
        return [ZbSearchRecordTableViewCell getRowHeight];
    }
    switch (indexPath.section) {
        case 0:
        {
            return [ZbSearchStoneTableViewCell getRowHeight];
        }
        case 1:
        {
            return [ZbProductTableViewCell getRowHeight];
        }
        case 2:
        {
            return [ZbSmallQyTableViewCell getRowHeight];
        }
        case 3:
        {
            return [ZbSupplyDemandTableViewCell getRowHeight];
        }
            
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearching) {
        ZbSearchRecordTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SearchRecordCellIdentifier];
        cell.labRecordName.text = [self.hisRecordArray objectAtIndex:indexPath.row];
        return cell;
    }
    switch (indexPath.section) {
        case 0:
        {
            ZbSearchStoneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SearchStoneCellIdentifier];
            [cell setInfoDict:[self.stoneArray objectAtIndex:indexPath.row]];
            return cell;
        }
        case 1:
        {
            ZbProductTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:ProductCellIdentifier];
            [cell setInfoDict:[self.productArray objectAtIndex:indexPath.row]];
            return cell;
        }
        case 2:
        {
            ZbSmallQyTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SmallQiyeCellIdentifier];
            [cell setInfoDict:[self.companyArray objectAtIndex:indexPath.row]];
            return cell;
        }
        case 3:
        {
            ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
            [cell setPostDict:[self.supplyArray objectAtIndex:indexPath.row]];
            return cell;
        }
            
        default:
            return 0;
    }
}

// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (!self.isSearching) {
        return [ZbIndexCharTableViewCell getRowHeight];
    }
    return [ZbTypeTableViewCell getRowHeight];
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (!self.isSearching) {
        ZbIndexCharTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:IndexCharCellIdentifier];
        cell.labChar.text = @"历史记录";
        return cell;
    }
    ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
    switch (section) {
        case 0:
        {
            headerCell.labTypeName.text = @"石材";
            break;
        }
        case 1:
        {
            headerCell.labTypeName.text = @"产品";
            break;
        }
        case 2:
        {
            headerCell.labTypeName.text = @"企业";
            break;
        }
        case 3:
        {
            headerCell.labTypeName.text = @"供需信息";
            break;
        }
            
        default:
            break;
    }
    return headerCell;
}

// section尾部高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (!self.isSearching) {
        return 0;
    }
    return section == 3 ? [ZbSeeMoreTableViewCell getRowHeight:NO] : [ZbSeeMoreTableViewCell getRowHeight:YES];
}

// section尾部view
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (!self.isSearching) {
        return nil;
    }
    ZbSeeMoreTableViewCell *footerCell = [tableView dequeueReusableCellWithIdentifier:SeeMoreFooterIdentifier];
    switch (section) {
        case 0:
        {
            footerCell.labDes.text = @"查看更多相关石材";
            footerCell.clickBlock = ^(void) {
                ZbSearchSingleViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSearchSingleViewController"];
                controller.curSearchType = Zb_SearchSingle_Type_Stone;
                controller.searchKey = self.tfSearch.text;
                [self.navigationController pushViewController:controller animated:YES];
            };
            break;
        }
        case 1:
        {
            footerCell.labDes.text = @"查看更多相关产品";
            footerCell.clickBlock = ^(void) {
                ZbSearchSingleViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSearchSingleViewController"];
                controller.curSearchType = Zb_SearchSingle_Type_Product;
                controller.searchKey = self.tfSearch.text;
                [self.navigationController pushViewController:controller animated:YES];
            };
            break;
        }
        case 2:
        {
            footerCell.labDes.text = @"查看更多相关企业";
            footerCell.clickBlock = ^(void) {
                ZbSearchSingleViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSearchSingleViewController"];
                controller.curSearchType = Zb_SearchSingle_Type_Qiye;
                controller.searchKey = self.tfSearch.text;
                [self.navigationController pushViewController:controller animated:YES];
            };
            break;
        }
        case 3:
        {
            footerCell.labDes.text = @"查看更多供需信息";
            footerCell.clickBlock = ^(void) {
                ZbSearchSingleViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSearchSingleViewController"];
                controller.curSearchType = Zb_SearchSingle_Type_SupllyDemand;
                controller.searchKey = self.tfSearch.text;
                [self.navigationController pushViewController:controller animated:YES];
            };
            break;
        }
            
        default:
            break;
    }
    return footerCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (!self.isSearching) {
        NSString *key = [self.hisRecordArray objectAtIndex:indexPath.row];
        self.tfSearch.text = key;
        [self.tfSearch becomeFirstResponder];
        [self textFieldChanged:self.tfSearch];
        [self.tv reloadData];
        return;
    }
    [self.tfSearch resignFirstResponder];
    switch (indexPath.section) {
        case 0:
        {
            ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
            NSDictionary *stoneDict = [self.stoneArray objectAtIndex:indexPath.row];
            NSString *stoneName = stoneDict[@"stone_name"];
            controller.stoneName = stoneName;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case 1:
        {
            NSDictionary *curDict = [self.productArray objectAtIndex:indexPath.row];
            ZbProductDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
            controller.productId = curDict[@"product_id"];
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case 2:
        {
            NSDictionary *curDict = [self.companyArray objectAtIndex:indexPath.row];
            ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
            controller.companyId = curDict[@"company_id"];
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case 3:
        {
            NSDictionary *curDict = [self.supplyArray objectAtIndex:indexPath.row];
            NSString *postType = curDict[@"post_type"];
            if (postType.intValue == Zb_Home_Message_Type_Reward) {
                // 供需
                ZbRewardDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardDetailViewController"];
                controller.curPostId = curDict[@"post_id"];
                [self.navigationController pushViewController:controller animated:YES];
            } else {
                // 悬赏
                ZbSupplyDemandDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
                controller.curPostId = curDict[@"post_id"];
                [self.navigationController pushViewController:controller animated:YES];
            }
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    // 用户已结束搜索，有些请求还在路上，不处理
    if (!self.isSearching) {
        return;
    }
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 任一加载失败，中断请求
    if (result.integerValue != 0) {
        return;
    }
    
    if (connection.tag == HTTP_REQUEST_STONE) {
        // 石材
        NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
        // 添加石材数据
        [self.stoneArray removeAllObjects];
        [self.stoneArray addObjectsFromArray:dataArray];
        [self.tv reloadData];
        // 请求下一个数据
//        [self requestProductData:connection.userInfo];
    } else if (connection.tag == HTTP_REQUEST_PRODUCT) {
        // 产品
        NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
        // 添加产品数据
        [self.productArray removeAllObjects];
        [self.productArray addObjectsFromArray:dataArray];
        [self.tv reloadData];
        // 请求下一个数据
//        [self requestQyData:connection.userInfo];
    } else if (connection.tag == HTTP_REQUEST_COMPANY) {
        // 企业
        NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
        // 添加企业数据
        [self.companyArray removeAllObjects];
        [self.companyArray addObjectsFromArray:dataArray];
        [self.tv reloadData];
        
        // 请求下一个数据
//        [self requestSupplyDemandData:connection.userInfo];
    } else if (connection.tag == HTTP_REQUEST_S_AND_D) {
        // 供需
        NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
        // 添加供需数据
        [self.supplyArray removeAllObjects];
        [self.supplyArray addObjectsFromArray:dataArray];
        
        [self.tv reloadData];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
