//
//  ZbRewardContainerViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardContainerViewController.h"
#import "ZbRewardListViewController.h"
#import "ZbPubSupViewController.h"

@interface ZbRewardContainerViewController () <UIScrollViewDelegate>

// scrollview容器
@property (strong, nonatomic) IBOutlet UIScrollView *svMain;

// 进行中按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageIng;
// 已完成按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageDone;
// 进行中悬赏列表controller
@property (strong, nonatomic) ZbRewardListViewController *leftPageController;
// 已完成悬赏列表controller
@property (strong, nonatomic) ZbRewardListViewController *rightPageController;
// 选中线leading
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcSelectLineLeading;

// 当前选择页面
@property (assign, nonatomic) NSInteger curPageIndex;

@end

@implementation ZbRewardContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.svMain.scrollsToTop = NO;
    self.curPageIndex = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 切页逻辑
- (void)pageChange
{
    // 按钮激活状态
    self.btnPageIng.enabled = !(self.curPageIndex == 0);
    self.btnPageDone.enabled = (self.curPageIndex == 0);
    //    self.title = self.curPageIndex == 0 ? @"求购信息" : @"供货信息";
    
    [self.leftPageController onPageShowChange:(self.curPageIndex == 0)];
    [self.rightPageController onPageShowChange:(self.curPageIndex != 0)];
    return;
}

#pragma mark - 点击事件
// 进行中页按钮点击
- (IBAction)btnPageIngOnClick:(id)sender {
    self.curPageIndex = 0;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 已完成页按钮点击
- (IBAction)btnPageDoneOnClick:(id)sender {
    self.curPageIndex = 1;
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 发布按钮点击
- (IBAction)btnPublicOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        self.hud.labelText = @"尚未登录，无法操作";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
        }];
        return;
    }
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbPubSupViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbPubSupViewController"];
    controller.curMessageIndex = Zb_Home_Message_Type_Reward;
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 选中线条跟随滚动
    CGFloat lineScrollRange = DeviceWidth / 2;
    CGFloat percent = offsetX / DeviceWidth;
    self.alcSelectLineLeading.constant = lineScrollRange * percent;
    
    // 判断是否切页
    if (fmod(offsetX, scrollView.frame.size.width)  == 0) {
        int index = offsetX / scrollView.frame.size.width;
        self.curPageIndex = index;
        [self pageChange];
    }
}

#pragma mark - 导航
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Rewarding"]) {
        self.leftPageController = segue.destinationViewController;
        self.leftPageController.curRewardListType = Zb_RewardList_Type_Ing;
    } else {
        self.rightPageController = segue.destinationViewController;
        self.rightPageController.curRewardListType = Zb_RewardList_Type_Done;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
