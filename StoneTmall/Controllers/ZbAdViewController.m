//
//  ZbAdViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/11/2.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbAdViewController.h"
#import "AppDelegate.h"

@interface ZbAdViewController () <MZTimerLabelDelegate>

// 图片控件
@property (nonatomic, strong) IBOutlet UIImageView *ivAd;
// 倒计时view
@property (nonatomic, strong) IBOutlet UIView *viewCountDown;
// 倒计时label
@property (nonatomic, strong) IBOutlet UILabel *labCountDown;
// 倒计时处理类
@property (nonatomic, strong) MZTimerLabel *timerLabel;
// 当前闪屏广告
@property (nonatomic, strong) NSDictionary *curAdDict;

@end

@implementation ZbAdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *notify = [ZbSaveManager getClickNotification];
    if (notify != nil) {
        // 点推送进来的，就不闪屏了
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate onAdShowOk];
        return;
    }
    
    // 要播放的图片
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSNumber *randIndex = [userDefault valueForKey:SAVE_KEY_AD_INDEX];
    NSArray *adArray = [userDefault valueForKey:SAVE_KEY_AD];
    if (randIndex == nil || adArray == nil || randIndex.integerValue >= adArray.count) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate onAdShowOk];
    } else {
        NSDictionary *adDict = [adArray objectAtIndex:randIndex.integerValue];
        NSString *ad_image = adDict[@"ad_image"];
        NSString *imageUrl = [ZbWebService getImageFullUrl:ad_image];
        BOOL isExists = [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:imageUrl]];
        if (isExists) {
            self.curAdDict = adDict;
            [self.ivAd sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            
            self.viewCountDown.hidden = NO;
            self.viewCountDown.layer.cornerRadius = 2;
            self.viewCountDown.layer.masksToBounds = YES;
            
            // 图片点击手势
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adImageTap)];
            [self.ivAd addGestureRecognizer:tap];
            
            // 倒计时点击手势
            UITapGestureRecognizer *countDownTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countDownTap)];
            [self.viewCountDown addGestureRecognizer:countDownTap];
            
            // 倒计时label
            self.timerLabel = [[MZTimerLabel alloc] initWithLabel:self.labCountDown andTimerType:MZTimerLabelTypeTimer];
            self.timerLabel.delegate = self;
            self.timerLabel.timerType = MZTimerLabelTypeTimer;
            self.timerLabel.timeFormat = @"s";
            [self.timerLabel setCountDownTime:3];
            [self.timerLabel start];
        } else {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate onAdShowOk];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

#pragma mark - 计时器
-(void)timerLabel:(MZTimerLabel*)timerLabel finshedCountDownTimerWithTime:(NSTimeInterval)countTime {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate onAdShowOk];
    return;
}

#pragma mark - 点击事件
// 图片点击
- (void)adImageTap {
    [self.timerLabel pause];
    [ZbSaveManager setClickAdDict:self.curAdDict];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate onAdShowOk];
    return;
}

// 倒计时点击跳过
- (void)countDownTap {
    [self.timerLabel pause];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate onAdShowOk];
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
