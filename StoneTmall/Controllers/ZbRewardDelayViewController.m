//
//  ZbRewardDelayViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/21.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardDelayViewController.h"
#import <CoreText/CoreText.h>
#import "ZbRewardManager.h"
#import "ZbDatePickViewController.h"
#import "ZbSelectPayWayViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"

#define HTTP_REQUEST_TAG_DELAY 1              // 延长期限
#define HTTP_REQUEST_TAG_PayOrder 2           // 支付

@interface ZbRewardDelayViewController () <UITextFieldDelegate>

// 帖子类型
@property (nonatomic, strong) IBOutlet UILabel *labPostType;
// 帖子类型背景
@property (nonatomic, strong) IBOutlet UIView *bgPostType;
// 标题
@property (nonatomic, strong) IBOutlet UILabel *labTitle;
// 时间
@property (nonatomic, strong) IBOutlet UILabel *labDate;
// 截止
@property (nonatomic, strong) IBOutlet UILabel *labTimeRemain;
// 参与
@property (nonatomic, strong) IBOutlet UILabel *labJoins;
// 悬赏金额容器
@property (nonatomic, strong) IBOutlet UIView *bgAddMoney;
// 悬赏期限容器
@property (nonatomic, strong) IBOutlet UIView *bgAddTime;
// 金额输入框
@property (nonatomic, strong) IBOutlet UITextField *tfAddMoney;
// 期限输入框
@property (nonatomic, strong) IBOutlet UITextField *tfAddTime;
// 支付按钮
@property (nonatomic, strong) IBOutlet UIButton *btnPay;

@end

@implementation ZbRewardDelayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 圆角、边框
    self.bgPostType.layer.cornerRadius = 3;
    self.bgPostType.layer.masksToBounds = YES;
    self.bgAddMoney.layer.cornerRadius = 3;
    self.bgAddMoney.layer.masksToBounds = YES;
    self.bgAddTime.layer.cornerRadius = 3;
    self.bgAddTime.layer.masksToBounds = YES;
    self.btnPay.layer.cornerRadius = 3;
    
    // 注册收到微信登录请求回应的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotificationWxPayResult:) name:NOTIFICATION_WX_PAY_RESULT object:nil];
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 支付按钮初始不激活
    [self setBtnPayEnable:NO];
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    // 绘制悬赏信息
    [self drawInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 设置支付按钮激活状态
- (void)setBtnPayEnable:(BOOL)isEnable {
    self.btnPay.enabled = isEnable;
    self.btnPay.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

#pragma mark - 界面绘制
// 绘制悬赏信息
- (void)drawInfo {
    // 类型
    [ZbRewardManager setTypeText:self.labPostType typeBg:self.bgPostType postDict:self.curPostDict];
    // 标题
    NSString *title = self.curPostDict[@"post_title"];
    NSNumber *cent = self.curPostDict[@"post_price"];
    NSNumber *yuan = [NSNumber numberWithInteger:(cent.integerValue / 100)];
    NSString *price = [NSString stringWithFormat:@"¥%@ ", yuan];
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", price, title]];
    // 价格字属性
    [attriString addAttribute:NSForegroundColorAttributeName
                        value:UIColorFromHexString(@"EF684D")
                        range:NSMakeRange(0, price.length)];
    [attriString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:16] range:NSMakeRange(0, price.length)];
    // 标题字属性
    [attriString addAttribute:NSForegroundColorAttributeName
                        value:UIColorFromHexString(@"333333")
                        range:NSMakeRange(price.length, title.length)];
    [attriString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(price.length, title.length)];
    self.labTitle.attributedText = attriString;
    
    // 时间
    NSString *date = [CommonCode getTimeShowText:self.curPostDict[@"post_modified"]];
    self.labDate.text = date;
    // 悬赏状态
    [ZbRewardManager setStatusText:self.labTimeRemain postDict:self.curPostDict];
    // 参与人数
    self.labJoins.text = [NSString stringWithFormat:@"%@人参与", self.curPostDict[@"post_joins"]];
    return;
}

#pragma mark - 接口调用
// 延长接口调用
- (void)requestAddDays {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *addDays = [self.tfAddTime.text stringByReplacingOccurrencesOfString:@"天" withString:@""];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService applyAddDaysForPaidDemand:self.curPostDict[@"post_id"] addDays:addDays addMoney:self.tfAddMoney.text] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DELAY;
    [conn start];
    return;
}

// 支付接口
- (void)requestPayOrder:(NSString *)orderNumber payType:(NSString *)payType {
    self.hud.labelText = @"支付中..";
    [self.hud show:NO];
    
    // 支付类型
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService payOrder:orderNumber payType:payType] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_PayOrder;
    conn.userInfo = payType;
    [conn start];
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.tfAddMoney resignFirstResponder];
}

// 支付按钮点击
- (IBAction)btnPayOnClick:(id)sender {
    [self viewTap];
    
    [self requestAddDays];
    return;
}

// 延长时间选择
- (IBAction)btnSelectDaysOnClick:(id)sender {
    [self.view endEditing:YES];
    NSArray *dArray = [NSArray arrayWithObjects:@"1天", @"2天", @"3天", @"4天", @"5天", @"6天", @"7天", @"8天", @"9天", @"10天", @"11天", @"12天", @"13天", @"14天", @"15天", @"16天", @"17天", @"18天", @"19天", @"20天", @"21天", @"22天", @"23天", @"24天", @"25天", @"26天", @"27天", @"28天", @"29天", @"30天", nil];
    
    [ZbDatePickViewController showInViewController:self dataArray:dArray btnClickBlock:^(Zb_Dp_Click_Type ZbClickType, NSString *str) {
        switch (ZbClickType) {
            case Zb_Dp_Click_Cancel:
                return ;
                break;
                
            case Zb_Dp_Click_Sure:
                self.tfAddTime.text = str;
                [self textFieldChanged:nil];
                break;
                
            default:
                break;
        }
    }];
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)textFieldChanged:(id)sender {
    if (self.tfAddMoney.text.length > 0 && self.tfAddTime.text.length > 0) {
        [self setBtnPayEnable:YES];
    } else {
        [self setBtnPayEnable:NO];
    }
    return;
}

#pragma mark - 微信
// 微信授权结果通知
- (void)onNotificationWxPayResult:(NSNotification *)notify {
    [self.hud hide:NO];
    PayResp *resp = notify.object;
    switch(resp.errCode){
        case WXSuccess:
        {
            //服务器端查询支付通知或查询API返回的结果再提示成功
            NSString *info = [NSString stringWithFormat:@"恭喜您支付成功"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:info delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            break;
        }
        default:
        {
            DLog(@"支付失败，retcode=%d",resp.errCode);
            // 支付失败
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            break;
        }
    }
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"提交失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_TAG_DELAY) {
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        NSString *order_number = resultDic[@"data"];
        [ZbSelectPayWayViewController showInViewController:self.navigationController block:^(Zb_Pay_Way_Type type) {
            NSString *payType = [NSString stringWithFormat:@"%u", type];
            [self requestPayOrder:order_number payType:payType];
        }];
    } else if (connection.tag == HTTP_REQUEST_TAG_PayOrder) {
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self.hud hide:NO];
        Zb_Pay_Way_Type payType = ((NSString *)connection.userInfo).intValue;
        NSDictionary *pay_charge = resultDic[@"data"][@"pay_charge"];
        if (payType == Zb_Pay_Way_Zfb) {
            NSString *orderString = pay_charge[@"order_string"];
            // 支付
            [[AlipaySDK defaultService] payOrder:orderString fromScheme:WX_URL_SCHEME callback:^(NSDictionary *resultDic) {
                DLog(@"##order detail alipay reslut = %@",resultDic);
                NSString *resultStatus = resultDic[@"resultStatus"];
                if (resultStatus.integerValue != 9000) {
                    // 支付失败
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
                
                NSString *info = [NSString stringWithFormat:@"恭喜您支付成功"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:info delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
            }];
        } else {
            PayReq *request = [[PayReq alloc] init];
            request.openID = pay_charge[@"appid"];
            request.partnerId = pay_charge[@"partnerid"];
            request.prepayId = pay_charge[@"prepayid"];
            request.package = pay_charge[@"package"];
            request.nonceStr = pay_charge[@"noncestr"];
            NSString *time = pay_charge[@"timestamp"];
            request.timeStamp = time.integerValue;
            request.sign = pay_charge[@"sign"];
            [WXApi sendReq:request];
        }
    }
    return;
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // 申请成功block
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        self.applyOkBlock();
    });
    
    //跳转到主界面
    self.hud.labelText = @"申请成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
