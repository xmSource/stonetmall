//
//  ZbMyorderViewController.m
//  StoneTmall
//
//  Created by chyo on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbMyorderViewController.h"
#import "ZbMyOrder.h"
#import "ZbSelectPayWayViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"
#import "ZbOrderDetailViewController.h"

#define HTTP_REQUEST_TAG_DATA 1         // 重载订单数据
#define HTTP_REQUEST_TAG_MORE 2         // 加载更多订单数据
#define HTTP_REQUEST_TAG_PayOrder 3     // 支付订单
#define HTTP_REQUEST_TAG_CONFIRM 4      // 确认订单

@interface ZbMyorderViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, UIAlertViewDelegate>

// 订单数组
@property (nonatomic, strong) NSMutableArray *orderArray;
// 物品数组
@property (nonatomic, strong) NSMutableArray *goodsArray;

@property (nonatomic, strong) IBOutlet UITableView *tv;
// 当前页数
@property (nonatomic) NSInteger curPageIndex;
// 当前操作订单索引
@property (nonatomic) NSInteger curOrderIndex;

@end

@implementation ZbMyorderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.orderArray = [[NSMutableArray alloc] init];
    self.goodsArray = [[NSMutableArray alloc] init];
    
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
    
    // 注册收到微信登录请求回应的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotificationWxPayResult:) name:NOTIFICATION_WX_PAY_RESULT object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 获取订单数据
    [self loadData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 请求数据
- (void)loadData {
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMyOrderList:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DATA;
    [conn start];
}

- (void)requestMoreData {
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex + 1]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMyOrderList:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MORE;
    [conn start];
}

- (void)confirmOrder:(NSString *)orderNum index:(NSString *)index {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService confirmMyOrder:orderNum] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_CONFIRM;
    conn.userInfo = index;
    [conn start];
}

#pragma mark - 微信
// 微信授权结果通知
- (void)onNotificationWxPayResult:(NSNotification *)notify {
    [self.hud hide:NO];
    PayResp *resp = notify.object;
    switch(resp.errCode){
        case WXSuccess:
        {
            //服务器端查询支付通知或查询API返回的结果再提示成功
            NSString *info = [NSString stringWithFormat:@"恭喜您支付成功"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:info delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            break;
        }
        default:
        {
            DLog(@"支付失败，retcode=%d",resp.errCode);
            // 支付失败
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            break;
        }
    }
    return;
}

#pragma mark - 点击事件
- (void)confirmClick:(ZbIndexButton *)btn {
        NSDictionary *dict = [self.orderArray objectAtIndex:btn.index];
        int statusOrder = [dict[@"order_status"] intValue];
        switch (statusOrder) {
                // 订单未支付
            case Zb_User_Order_Status_Status_Wzf: {
                // 支付类型
                NSString *payType = [NSString stringWithFormat:@"%@", dict[@"order_pay"]];
                NSString *orderNumber = dict[@"order_number"];
                CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService payOrder:orderNumber payType:payType] delegate:self];
                conn.tag = HTTP_REQUEST_TAG_PayOrder;
                conn.userInfo = payType;
                [conn start];
                self.curOrderIndex = btn.index;
                break;
            }
                // 订单已支付
            case Zb_User_Order_Status_Status_Yzf:
                break;
                // 订单已发货
            case Zb_User_Order_Status_Status_Yfh:{
                NSString *orderNum = dict[@"order_number"];
                [self confirmOrder:orderNum index:[NSString stringWithFormat:@"%ld", (long)btn.index]];
                break;
            }
                // 订单已完成
            case Zb_User_Order_Status_Status_Ywc:
                break;
                // 订单已过期
            case Zb_User_Order_Status_Status_Ygq:
                break;
                // 订单有退款
            case Zb_User_Order_Status_Status_Ytk:
                break;
                
            default:
                break;
        }
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return self.orderArray.count;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    self.goodsArray = [self.orderArray objectAtIndex:section][@"order_items"];
    return self.goodsArray.count;
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"itemsCell" forIndexPath:indexPath];
    UIImageView *img = (UIImageView *)[cell viewWithTag:3];
    UILabel *labGoodsName = (UILabel *)[cell viewWithTag:4];
    UILabel *labSpecs = (UILabel *)[cell viewWithTag:5];
    UILabel *labprice = (UILabel *)[cell viewWithTag:6];
    UILabel *labnum = (UILabel *)[cell viewWithTag:7];
    
    self.goodsArray = [self.orderArray objectAtIndex:indexPath.section][@"order_items"];
    NSDictionary *dict = [self.goodsArray objectAtIndex:indexPath.row];
    [img sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:dict[@"item_stone_image"]]]];
    labGoodsName.text = dict[@"item_product_tag"];
    labSpecs.text = dict[@"item_spec_name"];
    float price = ([dict[@"item_price"] floatValue] / 100);
    labprice.text = [NSString stringWithFormat:@"%.2f", price];
    labnum.text = [NSString stringWithFormat:@"元 * %@", dict[@"item_amount"]];
    
    return cell;
}

// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0f;
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"sectionHeaderCell" ];
    // section名控件
    UILabel *labDate = (UILabel *)[headerCell viewWithTag:1];
    NSDictionary *dict = [self.orderArray objectAtIndex:section];
    labDate.text = dict[@"order_inserted"];
    UILabel *labFlag = (UILabel *)[headerCell viewWithTag:2];
    
    int statusOrder = [dict[@"order_status"] intValue];
    switch (statusOrder) {
        case Zb_User_Order_Status_Status_Wzf:
            labFlag.text = @"未支付";
            break;
            
        case Zb_User_Order_Status_Status_Yzf:
            labFlag.text = @"未发货";
            break;
            
        case Zb_User_Order_Status_Status_Yfh:
            labFlag.text = @"已发货";
            break;
            
        case Zb_User_Order_Status_Status_Ywc:
            labFlag.text = @"已完成";
            break;
            
        case Zb_User_Order_Status_Status_Ygq:
            labFlag.text = @"已过期";
            break;
            
        case Zb_User_Order_Status_Status_Ytk:
            labFlag.text = @"已退款";
            break;

            
        default:
            break;
    }
    
    return headerCell.contentView;
}

// section尾部高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 50.0f;
}

// section尾部view
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UITableViewCell *footerCell = [tableView dequeueReusableCellWithIdentifier:@"sectionFooterCell"];
    UILabel *labTotnum = (UILabel *)[footerCell viewWithTag:8];
    UILabel *labTotPrice = (UILabel *)[footerCell viewWithTag:9];

    ZbIndexButton *btnConfirm =(ZbIndexButton *)[footerCell viewWithTag:10];
    btnConfirm.layer.cornerRadius = 2.0f;
    btnConfirm.index = section;
    [btnConfirm addTarget:self action:@selector(confirmClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.goodsArray = [self.orderArray objectAtIndex:section][@"order_items"];
    labTotnum.text = [NSString stringWithFormat:@"共%ld件商品合计:          元", self.goodsArray.count];
    
    float sumPrice = 0.00;
    for (NSDictionary *dict in self.goodsArray) {
        float price = [dict[@"item_price"] floatValue] / 100;
        float amount = [dict[@"item_amount"] floatValue];
        float tPrice = price * amount;
        sumPrice += tPrice;
    }
    
    labTotPrice.text = [NSString stringWithFormat:@"%.2f", sumPrice];
    
    NSDictionary *dict = [self.orderArray objectAtIndex:section];
    int statusOrder = [dict[@"order_status"] intValue];
    NSString *btnTitle = @"";
    switch (statusOrder) {
        case Zb_User_Order_Status_Status_Wzf:
            btnTitle = @"去支付";
            btnConfirm.enabled = YES;
            btnConfirm.hidden = NO;
            break;
            
        case Zb_User_Order_Status_Status_Yzf:
            btnTitle = @"待发货";
            btnConfirm.hidden = YES;
            break;
            
        case Zb_User_Order_Status_Status_Yfh:
            btnTitle = @"确认收货";
            btnConfirm.hidden = NO;
            break;
            
        case Zb_User_Order_Status_Status_Ywc:
            btnTitle = @"已完成";
            btnConfirm.hidden = YES;
            break;
            
        case Zb_User_Order_Status_Status_Ygq:
            btnTitle = @"已过期";
            btnConfirm.hidden = YES;
            break;
            
        case Zb_User_Order_Status_Status_Ytk:
            btnTitle = @"有退款";
            btnConfirm.hidden = YES;
            break;
            
        default:
            break;
    }
    [btnConfirm setTitle:btnTitle forState:UIControlStateNormal];
    [btnConfirm setTitle:btnTitle forState:UIControlStateHighlighted];
    btnConfirm.backgroundColor = MAIN_ORANGE_COLOR;

    return footerCell.contentView;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *orderDict = [self.orderArray objectAtIndex:indexPath.section];
    ZbOrderDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbOrderDetailViewController"];
    controller.curOrderDict = orderDict;
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSMutableDictionary *orderDict = [self.orderArray objectAtIndex:self.curOrderIndex];
    [orderDict setValue:[NSNumber numberWithInt:Zb_User_Order_Status_Status_Yzf]forKey:@"order_status"];
    [self.tv reloadSections:[NSIndexSet indexSetWithIndex:self.curOrderIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    NSMutableDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSString *result = resultDic[@"errno"];
    if (result.integerValue != 0) {
        return;
    }

    if (connection.tag == HTTP_REQUEST_TAG_DATA) {
        NSDictionary *psDict = resultDic[@"data"];
        NSArray *array = psDict[@"rows"];
        [self.orderArray removeAllObjects];
        [self.orderArray addObjectsFromArray:array];
        if (self.orderArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        
        [self.hud hide:NO];
        [self.tv reloadData];
        if (self.orderArray.count > 0) {
            CGRect rectTop = CGRectMake(0, 0, 1, 1);
            [self.tv scrollRectToVisible:rectTop animated:YES];
        }
        
    } else if (connection.tag == HTTP_REQUEST_TAG_MORE) {
        // 上拉加载更多
        [self.tv.footer endRefreshing];
        NSDictionary *psDict = resultDic[@"data"];
        NSArray *array = psDict[@"rows"];
        [self.orderArray addObjectsFromArray:array];
        if (array.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
         [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_PayOrder) {
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        [self.hud hide:NO];
        
        NSDictionary *pay_charge = resultDic[@"data"][@"pay_charge"];
        NSString *orderString = pay_charge[@"order_string"];
        Zb_Pay_Way_Type payType = ((NSString *)connection.userInfo).intValue;
        if (payType == Zb_Pay_Way_Zfb) {
        // 支付
            [[AlipaySDK defaultService] payOrder:orderString fromScheme:WX_URL_SCHEME callback:^(NSDictionary *resultDic) {
                DLog(@"##order detail alipay reslut = %@",resultDic);
                NSString *resultStatus = resultDic[@"resultStatus"];
                if (resultStatus.integerValue != 9000) {
                    // 支付失败
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
                
                NSString *info = [NSString stringWithFormat:@"恭喜您支付成功"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:info delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
            }];
        } else {
            PayReq *request = [[PayReq alloc] init];
            request.openID = pay_charge[@"appid"];
            request.partnerId = pay_charge[@"partnerid"];
            request.prepayId = pay_charge[@"prepayid"];
            request.package = pay_charge[@"package"];
            request.nonceStr = pay_charge[@"noncestr"];
            NSString *time = pay_charge[@"timestamp"];
            request.timeStamp = time.integerValue;
            request.sign = pay_charge[@"sign"];
            [WXApi sendReq:request];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_CONFIRM) {
        if (result.integerValue == 0) {
            self.hud.labelText = @"确认成功";
            NSInteger index = [connection.userInfo integerValue];
            NSMutableDictionary *dict = [self.orderArray objectAtIndex:index];
            [dict setValue:[NSNumber numberWithInt:Zb_User_Order_Status_Status_Ywc]forKey:@"order_status"];
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
            [self.tv reloadSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:UITableViewRowAnimationAutomatic];
            return;
        } else {
            self.hud.labelText = @"确认失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
    }
    return;
}

#pragma mark - 上拉或下拉刷新
/**
 *  下拉刷新
 */
- (void)headerRefresh {
    [self loadData];
}

- (void)footerRefresh {
    [self requestMoreData];
}

@end
