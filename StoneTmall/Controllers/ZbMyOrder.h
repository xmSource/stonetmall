//
//  ZbMyOrder.h
//  StoneTmall
//
//  Created by chyo on 15/9/6.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZbMyOrder : NSObject

@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) NSString *error;
@property (nonatomic, strong) NSString *order_id;
@property (nonatomic, strong) NSString *order_number;
@property (nonatomic, strong) NSString *order_inserted;
@property (nonatomic, strong) NSString *order_status;
@property (nonatomic, strong) NSString *order_product_price;
@property (nonatomic, strong) NSString *order_logistic_price;
@property (nonatomic, strong) NSString *item_product_id;
@property (nonatomic, strong) NSString *item_spec_name;
@property (nonatomic, strong) NSString *item_amount;

- (id)initWithDictionary:(NSMutableDictionary*)jsonObject;

@end
