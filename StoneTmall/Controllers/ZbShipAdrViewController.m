//
//  ZbShipAdrViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/4.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbShipAdrViewController.h"
#import "ZbNewAdrViewController.h"
#import "ZbNewQAdrViewController.h"

#define HTTP_LOAD_PDATA 1
#define HTTP_LOAD_QDATA 2
#define HTTP_DEL_PDATA 3
#define HTTP_DEL_QDATA 4
#define HTTP_QIYE_LIMIT 5

@interface ZbShipAdrViewController ()<UITableViewDelegate, UITableViewDataSource>

// 收货地址数组
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (strong, nonatomic) IBOutlet UITableView *tv;
@property (nonatomic) Zb_Adr_Type adrType;
@property (nonatomic, assign) int qiYeLimit;

@property (nonatomic) NSInteger curDelIndex;

@end

@implementation ZbShipAdrViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.curDelIndex = -1;
    self.qiYeLimit = 0;
    self.dataArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    switch (_adrType) {
        case Zb_Adr_PerSon:
            self.title = @"收货地址";
            [self loadData];
            break;
            
        case Zb_Adr_QiYe:
            self.title = @"企业地址";
            [self loadQyData];
            break;
            
        default:
            break;
    }
}

- (void)setControllerType:(Zb_Adr_Type)zbAdrType {
    _adrType = zbAdrType;
}

#pragma mark - 读取数据
- (void)loadData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getPostAddressList] delegate:self];
    conn.tag = HTTP_LOAD_PDATA;
    [conn start];
}

- (void)loadQiYeLimit {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getAddCompanyAddressLimit] delegate:self];
    conn.tag = HTTP_QIYE_LIMIT;
    [conn start];
}

- (void)loadQyData {
    ZbQyInfo *info = [ZbWebService sharedQy];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyAddressList:info.company_id] delegate:self];
    conn.tag = HTTP_LOAD_QDATA;
    [conn start];
}

- (void)delData:(NSString *)id {
    if (_adrType == Zb_Adr_QiYe) {
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService delCompanyAddress:id] delegate:self];
        conn.tag = HTTP_DEL_QDATA;
        [conn start];
    } else {
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService delPostAddress:id] delegate:self];
        conn.tag = HTTP_DEL_PDATA;
        [conn start];
    }
}

#pragma mark - UITableView Delegate & DataSource
/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.adrType == Zb_Adr_PerSon) {
        return 1;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (self.adrType == Zb_Adr_QiYe) {
        if (section == 0) {
            return 1;
        } else  {
            if (self.qiYeLimit == 0) {
                return self.dataArray.count;
            }
            return self.dataArray.count + 1;
        }
    } else {
        return self.dataArray.count + 1;
    }
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.adrType == Zb_Adr_PerSon) {
        return 60;
    } else {
        if (indexPath.section == 0) {
            return 70;
        }
        return 60;
    }
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (self.adrType == Zb_Adr_QiYe) {
        if (indexPath.section == 0) {
            ZbQyInfo *info = [ZbWebService sharedQy];
            if (info.company_area.length != 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell" forIndexPath:indexPath];
                UILabel *topLab = (UILabel *)[cell viewWithTag:1];
                UILabel *bomLab = (UILabel *)[cell viewWithTag:2];
                UILabel *flagLab = (UILabel *)[cell viewWithTag:3];
                topLab.text = [NSString stringWithFormat:@"%@%@%@", info.company_country, info.company_prov, info.company_city];
                bomLab.text = info.company_area;
                flagLab.hidden = NO;
                flagLab.layer.cornerRadius = 2.0f;
                flagLab.layer.masksToBounds = YES;
            } else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"mainfooterCell" forIndexPath:indexPath];
            }
            
        } else if (indexPath.section == 1) {
            if (self.dataArray.count != 0) {
                if ( self.dataArray.count - 1 >= indexPath.row ) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"itemsCell" forIndexPath:indexPath];
                    UILabel *topLab = (UILabel *)[cell viewWithTag:1];
                    UILabel *bomLab = (UILabel *)[cell viewWithTag:2];
                    NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
                    topLab.text = [NSString stringWithFormat:@"%@%@%@", dict[@"country"], dict[@"prov"], dict[@"city"]];
                    bomLab.text = dict[@"area"];
                } else {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
                }
            } else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
            }
        }
    } else {
        
        if (self.dataArray.count != 0) {
            if ( self.dataArray.count - 1 >= indexPath.row ) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"itemsCell" forIndexPath:indexPath];
                UILabel *topLab = (UILabel *)[cell viewWithTag:1];
                UILabel *bomLab = (UILabel *)[cell viewWithTag:2];
                NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
                topLab.text = [NSString stringWithFormat:@"%@%@%@", dict[@"country"], dict[@"prov"], dict[@"city"]];
                bomLab.text = dict[@"area"];
            } else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
            }
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.adrType == Zb_Adr_QiYe) {
        if (indexPath.section == 0) {
            ZbQyInfo *info = [ZbWebService sharedQy];
            ZbNewQAdrViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbNewQAdrViewController"];
            controller.country = info.company_country;
            controller.prov = info.company_prov;
            controller.city = info.company_city;
            controller.area = info.company_area;
            [controller setControllerType:Zb_Controller_MAINEDIT];
            [self.navigationController pushViewController:controller animated:YES];
        } else {
            if (self.dataArray.count == indexPath.row) {
                ZbNewQAdrViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbNewQAdrViewController"];
                [controller setControllerType:Zb_Controller_ADDADR];
                [self.navigationController pushViewController:controller animated:YES];
            } else {
                ZbNewQAdrViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbNewQAdrViewController"];
                NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
                controller.dict = dict;
                [controller setControllerType:Zb_Controller_ADREDIT];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
    } else {
        ZbNewAdrViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbNewAdrViewController"];
        if (self.dataArray.count == indexPath.row) {
            [controller setContollerType:POST_ADD];
            [self.navigationController pushViewController:controller animated:YES];
            return;
        }
        NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
        if (self.isSelectMode) {
            self.selectBlock(dict);
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        [controller setContollerType:POST_EDIT];
        controller.detailDict = dict;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
        self.curDelIndex = indexPath.row;
        [self delData:dict[@"id"]];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.adrType == Zb_Adr_QiYe) {
        if (indexPath.section == 0) {
            return NO;
        } else {
            if (self.dataArray.count == indexPath.row) {
                return NO;
            }
            return YES;
        }
    } else {
        if (self.dataArray.count == indexPath.row) {
            return NO;
        }
        return YES;
    }
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSString *result = dict[@"errno"];
    if (connection.tag == HTTP_LOAD_PDATA) {
        if (result.integerValue == 0) {
            NSArray *array = dict[@"data"];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:array];
            [self.tv reloadData];
        }
    } else if (connection.tag == HTTP_LOAD_QDATA) {
        if (result.integerValue == 0) {
            NSArray *array = dict[@"data"];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:array];
            [self loadQiYeLimit];
        }
    }else if(connection.tag == HTTP_QIYE_LIMIT) {
        if (result.integerValue == 0) {
            NSDictionary *dataLimt = dict[@"data"];
            self.qiYeLimit = [[dataLimt objectForKey:@"left"] intValue];
            [self.tv reloadData];
        }
      
    }   else if (connection.tag == HTTP_DEL_QDATA) {
        if (result.integerValue == 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.curDelIndex inSection:1];
            [self.dataArray removeObjectAtIndex:self.curDelIndex];
            [self.tv deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self loadQiYeLimit];
        } else {
            self.hud.labelText = @"删除失败,请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        }
    }  else if (connection.tag == HTTP_DEL_PDATA) {
        if (result.integerValue == 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.curDelIndex inSection:0];
            [self.dataArray removeObjectAtIndex:self.curDelIndex];
            [self.tv deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self loadQiYeLimit];
        } else {
            self.hud.labelText = @"删除失败,请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        }
    }
}







@end
