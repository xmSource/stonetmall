//
//  ZbPersonInfoViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/10/11.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbPersonInfoViewController.h"
#import "ZbQyDetailViewController.h"
#import "ChatViewController.h"

#define HTTP_REQUEST_TAG_USER 0        // 用户信息
#define HTTP_REQUEST_TAG_COMPANY 1     // 公司

@interface ZbPersonInfoViewController () <IChatManagerDelegate, UIAlertViewDelegate>

// 头像
@property (nonatomic, strong) IBOutlet UIImageView *ivImage;
// 头像
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 地址
@property (nonatomic, strong) IBOutlet UILabel *labAddr;
// 微信容器
@property (nonatomic, strong) IBOutlet UIView *viewWechat;
// 微信图片
@property (nonatomic, strong) IBOutlet UIImageView *ivWechat;
// 微信状态文字
@property (nonatomic, strong) IBOutlet UILabel *labWechatStatus;
// 身份证容器
@property (nonatomic, strong) IBOutlet UIView *viewIdentify;
// 身份证图片
@property (nonatomic, strong) IBOutlet UIImageView *ivIdentify;
// 身份证状态文字
@property (nonatomic, strong) IBOutlet UILabel *labIdentifyStatus;
// 账号
@property (nonatomic, strong) IBOutlet UILabel *labAccount;
// 手机号
@property (nonatomic, strong) IBOutlet UIButton *btnPhone;
// 手机号图片
@property (nonatomic, strong) IBOutlet UIImageView *ivPhone;
// 手机号箭头图片
@property (nonatomic, strong) IBOutlet UIImageView *ivPhoneArrow;
// 企业容器
@property (nonatomic, strong) IBOutlet UIView *viewCompany;
// 企业容器高度约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcViewCompanyHeight;
// 公司logo
@property (nonatomic, strong) IBOutlet UIImageView *ivCompanyImage;
// 公司名
@property (nonatomic, strong) IBOutlet UIButton *btnCompanyName;
// 操作按钮view
@property (nonatomic, strong) IBOutlet UIView *btnOperView;
// 操作按钮
@property (nonatomic, strong) IBOutlet UIButton *btnOper;
// 添加好友的提示
@property (nonatomic, strong) IBOutlet UILabel *labAddFriendHint;

// 用户信息
@property (nonatomic, strong) NSDictionary *userDict;
// 公司信息
@property (nonatomic, strong) NSDictionary *companyDict;

@end

@implementation ZbPersonInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 圆角、边框
    self.viewWechat.layer.cornerRadius = 2;
    self.viewWechat.layer.masksToBounds = YES;
    self.viewIdentify.layer.cornerRadius = 2;
    self.viewIdentify.layer.masksToBounds = YES;
    self.ivCompanyImage.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    self.ivCompanyImage.layer.borderWidth = 1;
    self.ivImage.layer.cornerRadius = CGRectGetHeight(self.ivImage.frame) / 2;
    self.ivImage.layer.masksToBounds = YES;
    self.btnOper.layer.cornerRadius = 3;
    self.btnOper.layer.masksToBounds = YES;
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    if (self.nickName != nil) {
        self.title = [NSString stringWithFormat:@"%@的名片", self.nickName];
    } else {
        self.title = @"用户名片";
    }
    // 请求用户信息
    [self requestUserInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
    
    // 绘制操作状态
    [self drawOperStatus];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 界面绘制
// 绘制信息
- (void)drawInfo {
    // 头像
    [self.ivImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:self.userDict[@"user_image"]]]];
    // 昵称
    NSString *nickName = self.userDict[@"user_nickname"];
    if (nickName != nil && ![nickName isKindOfClass:[NSNull class]] && nickName.length > 0) {
        self.labName.text = nickName;
        self.title = [NSString stringWithFormat:@"%@的名片", nickName];
    } else {
        self.labName.text = self.userDict[@"user_name"];
        self.title = [NSString stringWithFormat:@"%@的名片", self.userDict[@"user_name"]];
    }
    // 地址
    NSString *province = self.userDict[@"user_province"];
    if ([province isKindOfClass:[NSNull class]]) {
        province = @"";
    }
    NSString *city = self.userDict[@"user_city"];
    if ([city isKindOfClass:[NSNull class]]) {
        city = @"";
    }
    self.labAddr.text = [NSString stringWithFormat:@"%@%@", province, city];
    // 账号
    self.labAccount.text = [NSString stringWithFormat:@"石猫号 %@", self.userDict[@"user_name"]];
    // 手机号状态
    NSString *phone = self.userDict[@"user_phone"];
    if (phone != nil && [phone isKindOfClass:[NSString class]] && phone.length > 0) {
        [self.btnPhone setTitle:[NSString stringWithFormat:@"手机号 %@", phone] forState:UIControlStateNormal];
        self.btnPhone.enabled = YES;
        self.ivPhone.highlighted = YES;
        self.ivPhoneArrow.hidden = NO;
    } else {
        [self.btnPhone setTitle:@"未绑定手机号" forState:UIControlStateNormal];
        self.btnPhone.enabled = NO;
        self.ivPhone.highlighted = NO;
        self.ivPhoneArrow.hidden = YES;
    }
    // 身份证验证状态
    NSNumber *card = self.userDict[@"user_card_ok"];
    if (![card isKindOfClass:[NSNull class]] && card.integerValue == Zb_User_Verify_Status_Status_Success) {
        self.viewIdentify.layer.borderWidth = 1;
        self.viewIdentify.layer.borderColor = UIColorFromHexString(@"A5E0FE").CGColor;
        self.labIdentifyStatus.text = @"身份证已验证";
        self.labIdentifyStatus.textColor = UIColorFromHexString(@"A5E0FE");
        self.ivIdentify.highlighted = YES;
    } else {
        self.viewIdentify.layer.borderWidth = 1;
        self.viewIdentify.layer.borderColor = UIColorFromHexString(@"AAAAAA").CGColor;
        self.labIdentifyStatus.text = @"身份证未验证";
        self.labIdentifyStatus.textColor = UIColorFromHexString(@"AAAAAA");
        self.ivIdentify.highlighted = NO;
    }
    // 微信验证状态
    NSNumber *user_weixin_openid = self.userDict[@"user_weixin_openid"];
    if (user_weixin_openid.integerValue == 1) {
        self.viewWechat.layer.borderWidth = 1;
        self.viewWechat.layer.borderColor = UIColorFromHexString(@"A6E9B9").CGColor;
        self.labWechatStatus.text = @"微信已认证";
        self.labWechatStatus.textColor = UIColorFromHexString(@"A6E9B9");
        self.ivWechat.highlighted = YES;
    } else {
        self.viewWechat.layer.borderWidth = 1;
        self.viewWechat.layer.borderColor = UIColorFromHexString(@"AAAAAA").CGColor;
        self.labWechatStatus.text = @"微信未认证";
        self.labWechatStatus.textColor = UIColorFromHexString(@"AAAAAA");
        self.ivWechat.highlighted = NO;
    }
    
    // 公司信息
    if (self.companyDict == nil) {
        self.viewCompany.hidden = YES;
        self.alcViewCompanyHeight.constant = 0;
        return;
    }
    self.viewCompany.hidden = NO;
    self.alcViewCompanyHeight.constant = 70;
    [self.ivCompanyImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:self.companyDict[@"company_image"]]]];
    [self.btnCompanyName setTitle:self.companyDict[@"company_name"] forState:UIControlStateNormal];
    return;
}

// 绘制操作状态
- (void)drawOperStatus {
    // 通讯录好友
    NSArray *buddyList = [[EaseMob sharedInstance].chatManager buddyList];
    BOOL isFriend = NO;
    for (EMBuddy *buddy in buddyList) {
        if ([buddy.username isEqualToString:self.userName] &&
            buddy.followState != eEMBuddyFollowState_NotFollowed) {
            isFriend = YES;
        }
    }
    if (isFriend) {
        // 已经是朋友
        self.labAddFriendHint.hidden = YES;
        [self.btnOper setTitle:@"发送消息" forState:UIControlStateNormal];
    } else {
        self.labAddFriendHint.hidden = NO;
        [self.btnOper setTitle:@"加为好友" forState:UIControlStateNormal];
    }
    return;
}

#pragma mark - 点击事件
// 电话
- (IBAction)btnPhoneOnClick:(id)sender {
    NSString *phone = self.userDict[@"user_phone"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", phone]]];
    return;
}

// 企业
- (IBAction)btnCompanyOnClick:(id)sender {
    ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
    controller.companyId = self.userDict[@"company_id"];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

// 操作按钮点击
- (IBAction)btnOperOnClick:(id)sender {
    BOOL isFriend = self.labAddFriendHint.hidden;
    if (isFriend) {
        // 好友直接聊天
        ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:self.userName isGroup:NO];
        [self.navigationController pushViewController:chatVC animated:YES];
    } else {
        NSString *loginUsername = [ZbWebService sharedUser][@"user_name"];
        if (loginUsername && loginUsername.length > 0) {
            if ([loginUsername isEqualToString:self.userName]) {
                [EMAlertView showAlertWithTitle:@"不能添加自己为好友"
                                        message:nil
                                completionBlock:nil
                              cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                              otherButtonTitles:nil];
                return;
            }
        }
        // 添加好友
        if([self hasSendBuddyRequest:self.userName])
        {
            NSString *message = [NSString stringWithFormat:NSLocalizedString(@"friend.repeatApply", @"you have send fridend request to '%@'!"), self.userName];
            [EMAlertView showAlertWithTitle:message
                                    message:nil
                            completionBlock:nil
                          cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                          otherButtonTitles:nil];
            return;
        }
        [self showMessageAlertView];
    }
}

#pragma mark - 添加好友
- (BOOL)hasSendBuddyRequest:(NSString *)buddyName
{
    NSArray *buddyList = [[[EaseMob sharedInstance] chatManager] buddyList];
    for (EMBuddy *buddy in buddyList) {
        if ([buddy.username isEqualToString:buddyName] &&
            buddy.followState == eEMBuddyFollowState_NotFollowed &&
            buddy.isPendingApproval) {
            return YES;
        }
    }
    return NO;
}

- (void)showMessageAlertView
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:NSLocalizedString(@"saySomething", @"say somthing")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel")
                                          otherButtonTitles:NSLocalizedString(@"ok", @"OK"), nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 2) {
        // 用户被删提示框
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if ([alertView cancelButtonIndex] != buttonIndex) {
        UITextField *messageTextField = [alertView textFieldAtIndex:0];
        
        NSString *messageStr = @"";
        if (messageTextField.text.length > 0) {
            //            messageStr = [NSString stringWithFormat:@"%@：%@", username, messageTextField.text];
            messageStr = [NSString stringWithFormat:@"%@", messageTextField.text];
        }
        else{
            messageStr = [NSString stringWithFormat:NSLocalizedString(@"friend.somebodyInvite", @"invite you as a friend")];
        }
        [self sendFriendApplyAtIndexPath:messageStr];
    }
}

- (void)sendFriendApplyAtIndexPath:(NSString *)message
{
    NSString *userNickName = [ZbWebService sharedUser][@"user_nickname"];
    if (!userNickName || [userNickName isKindOfClass:[NSNull class]] || userNickName.length <= 0) {
        userNickName = [ZbWebService sharedUser][@"user_name"];
    }
    NSString *headUrl = [ZbWebService sharedUser][@"user_image"];
    if (!headUrl || [headUrl isKindOfClass:[NSNull class]] || headUrl.length <= 0) {
        headUrl = @"";
    }
    NSString *reason = [NSString stringWithFormat:@"%@{fkhj}%@|%@", message, userNickName, headUrl];
    
    // 名字
    NSString *buddyName = self.userName;
    if (buddyName && buddyName.length > 0) {
        [self showHudInView:self.view hint:NSLocalizedString(@"friend.sendApply", @"sending application...")];
        EMError *error;
        [[EaseMob sharedInstance].chatManager addBuddy:buddyName message:reason error:&error];
        [self hideHud];
        if (error) {
            [self showHint:NSLocalizedString(@"friend.sendApplyFail", @"send application fails, please operate again")];
        }
        else{
            [self showHint:NSLocalizedString(@"friend.sendApplySuccess", @"send successfully")];
        }
    }
}

#pragma mark - 接口调用
// 请求用户信息
- (void)requestUserInfo {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getUserInfoByName:self.userName] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_USER;
    [conn start];
    return;
}

// 加载企业信息
- (void)requestCompanyInfo {
    NSString *companyId = self.userDict[@"company_id"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyShortInfo:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_COMPANY;
    [conn start];
    return;
}

#pragma mark - 接口 delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_TAG_USER) {
        if (result.integerValue != 0) {
            self.hud.labelText = @"加载失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        self.userDict = resultDic[@"data"];
        NSString *user_name = self.userDict[@"user_name"];
        if (user_name == nil || [user_name isKindOfClass:[NSNull class]]) {
            // 被删除的用户
            [self.hud hide:NO];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"该账号已被删除" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            alert.tag = 2;
            [alert show];
            return;
        }
        
        NSString *companyId = self.userDict[@"company_id"];
        if (companyId.integerValue > 0) {
            [self drawInfo];
            [self requestCompanyInfo];
            return;
        }
        [self.hud hide:NO];
        [self drawInfo];
    } else if (connection.tag == HTTP_REQUEST_TAG_COMPANY) {
        if (result.integerValue != 0) {
            self.hud.labelText = @"加载失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self.hud hide:NO];
        self.companyDict = resultDic[@"data"];
        [self drawInfo];
    }
    return;
}

#pragma mark - 微聊delegate
- (void)didAcceptedByBuddy:(NSString *)username
{
    // 别人接受我的好友请求
    if ([username isEqualToString:self.userName]) {
        // 已经是朋友
        self.labAddFriendHint.hidden = YES;
        [self.btnOper setTitle:@"发送消息" forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
