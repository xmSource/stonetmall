//
//  ZbPickGoodsTypeViewController.h
//  ddLogisticsGuest
//
//  Created by 张斌 on 15/5/27.
//  Copyright (c) 2015年 qianxx. All rights reserved.
//

#import <UIKit/UIKit.h>

// 选择block
typedef void (^selectBlock)(BOOL isCancel, NSDictionary *typeDict, NSDictionary *subTypeDict);

@interface ZbPickGoodsTypeViewController : UIViewController

// 类方法，创建弹出二级选择框
+ (void)showInViewController:(UIViewController *)controller typeArray:(NSArray *)typeArray typeDict:(NSDictionary *)typeDict selectBlock:(selectBlock)block;

@end
