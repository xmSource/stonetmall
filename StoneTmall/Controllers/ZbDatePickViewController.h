//
//  ZbDatePickViewController.h
//  StoneTmall
//
//  Created by Apple on 15/9/14.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

// 下拉点击类型
typedef enum {
    Zb_Dp_Click_Cancel,            // 确认
    Zb_Dp_Click_Sure,              // 取消
} Zb_Dp_Click_Type;

typedef void (^btnClickBlock)(Zb_Dp_Click_Type, NSString *str);

@interface ZbDatePickViewController : BaseViewController

// 提示文字
@property (nonatomic, strong) IBOutlet UILabel *labHint;

+ (ZbDatePickViewController *)showInViewController:(UIViewController *)controller dataArray:(NSArray *)array btnClickBlock:(btnClickBlock)block;

@end
