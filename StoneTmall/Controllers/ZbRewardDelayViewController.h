//
//  ZbRewardDelayViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/21.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbRewardDelayViewController : BaseViewController

// 当前悬赏信息
@property (nonatomic, weak) NSDictionary *curPostDict;

// 申请成功block
@property (nonatomic, copy) void (^applyOkBlock)(void);

@end
