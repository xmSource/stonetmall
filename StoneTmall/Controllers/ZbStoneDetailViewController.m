//
//  ZbStoneDetailViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbStoneDetailViewController.h"
#import "YKuImageInLoopScrollView.h"
#import "ZbQiyeTableViewCell.h"
#import "ZbStoneSimilarTableViewCell.h"
#import "ZbSeparateTableViewCell.h"
#import "ZbTypeTableViewCell.h"
#import <ShareSDK/ShareSDK.h>
#import "ZbLoginMainViewController.h"
#import "ZbQyDetailViewController.h"
#import "ZbProductDetailViewController.h"
#import "ZbDdOrderViewController.h"
#import "ZbSampleOrderViewController.h"
#import "LocationServiceManager.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "ZLPhoto.h"
#import "ChatViewController.h"

#define HTTP_REQUEST_TAG_COMPANY 1         // 相关公司
#define HTTP_REQUEST_TAG_R_COMPANY 2       // 推荐公司
#define HTTP_REQUEST_TAG_SIMILAR 3         // 相似石材
#define HTTP_REQUEST_TAG_SAMPLE 4          // 样品规格
#define HTTP_REQUEST_TAG_INFO 5            // 石材详情
#define HTTP_REQUEST_TAG_MARK 6               // 收藏
#define HTTP_REQUEST_TAG_UNMARK 7             // 取消收藏
#define HTTP_REQUEST_TAG_LIKE 8               // 点赞
#define HTTP_REQUEST_TAG_UNLIKE 9             // 取消赞
#define HTTP_REQUEST_TAG_JOIN 10              // 加入供应商
#define HTTP_REQUEST_TAG_COMPANY_MORE 11      // 更多相关公司

@interface ZbStoneDetailViewController () <UITableViewDataSource, UITableViewDelegate, YKuImageInLoopScrollViewDelegate, PopoverViewDelegate, ZLPhotoPickerBrowserViewControllerDataSource, ZLPhotoPickerBrowserViewControllerDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 滚动图片容器
@property (nonatomic, strong) IBOutlet UIView *imageLoopContainer;
// 滚动图片容器height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcImageLoopContainerHeight;
// 名称
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 有样品
@property (nonatomic, strong) IBOutlet UILabel *labHasSample;
// 种类
@property (nonatomic, strong) IBOutlet UILabel *labType;
// 自助拿样按钮
@property (nonatomic, strong) IBOutlet UIButton *btnBuySample;
// 赞按钮
@property (nonatomic, strong) IBOutlet UIButton *btnPraise;
// 交互
@property (nonatomic, strong) IBOutlet UILabel *labCommunicate;

// 滚动图片
@property (nonatomic, strong) YKuImageInLoopScrollView *imageLoopView;
// 添加弹出框
@property (nonatomic, strong) PopoverView *pv;

// 相关企业数组
@property (nonatomic, strong) NSMutableArray *companyArray;
// 推荐企业数组
@property (nonatomic, strong) NSMutableArray *recommandCompanyArray;
// 相似石材数组
@property (nonatomic, strong) NSMutableArray *similarStoneArray;
// 样品规格数组
@property (nonatomic, strong) NSMutableArray *sampleArray;

// 当前石材详情字典
@property (nonatomic, strong) NSMutableDictionary *curStoneDetailDict;
// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

@end

@implementation ZbStoneDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.companyArray = [NSMutableArray array];
    self.recommandCompanyArray = [NSMutableArray array];
    self.similarStoneArray = [NSMutableArray array];
    self.sampleArray = [NSMutableArray array];
    
    // 圆角
    self.labHasSample.layer.cornerRadius = 3;
    self.labHasSample.layer.masksToBounds = YES;
    self.btnBuySample.layer.cornerRadius = 3;
    self.btnPraise.layer.cornerRadius = 3;
    self.btnPraise.layer.borderWidth = 1;
    self.btnPraise.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbQiyeTableViewCell" bundle:nil] forCellReuseIdentifier:QiyeCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneSimilarTableViewCell" bundle:nil] forCellReuseIdentifier:StoneSimilarCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbTypeTableViewCell" bundle:nil] forCellReuseIdentifier:TypeHeaderIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSeparateTableViewCell" bundle:nil] forCellReuseIdentifier:SeparateCellIdentifier];
    
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    // 获取详情数据
    [self requestCompanyListData];
    [self requestRecommendCompanyListData];
    [self requestSimilarProductListData];
    [self requestStoneDetailData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 登录判断
- (void)requestLogin {
    self.hud.labelText = @"尚未登录，无法操作";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
        UIStoryboard *loginSb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        UINavigationController *nav = [loginSb instantiateViewControllerWithIdentifier:@"LoginNav"];
        ZbLoginMainViewController *loginController = (ZbLoginMainViewController *)nav.viewControllers[0];
        loginController.loginSucessBlock = ^(void) {
            [self.tv.header performSelectorOnMainThread:@selector(beginRefreshing) withObject:nil waitUntilDone:NO];
        };
        [self.view.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }];
    return;
}

#pragma mark - 界面绘制
// 信息绘制
- (void)drawInfo {
    // 石材名
    NSString *stoneName = self.curStoneDetailDict[@"stone_name"];
    NSString *stoneAlias = self.curStoneDetailDict[@"stone_alias"];
    self.title = stoneName;
    self.labName.text = stoneAlias;
    // 类别
    NSMutableString *des = [NSMutableString string];
    NSString *stoneType = self.curStoneDetailDict[@"stone_type"];
    [des appendFormat:@"种类：%@", stoneType];
    NSString *stoneColor = self.curStoneDetailDict[@"stone_color"];
    if ([stoneColor isKindOfClass:[NSString class]] && stoneColor.length > 0) {
        [des appendFormat:@"  颜色：%@", stoneColor];
    }
    NSString *stoneTexture = self.curStoneDetailDict[@"stone_texture"];
    if ([stoneTexture isKindOfClass:[NSString class]] && stoneTexture.length > 0) {
        [des appendFormat:@"  纹理：%@", stoneTexture];
    }
    NSString *stone_origin_prov = self.curStoneDetailDict[@"stone_origin_prov"];
    NSString *stone_origin_city = self.curStoneDetailDict[@"stone_origin_city"];
    NSString *stone_origin_area = self.curStoneDetailDict[@"stone_origin_area"];
    if ([stone_origin_prov isKindOfClass:[NSString class]] && [stone_origin_city isKindOfClass:[NSString class]] && [stone_origin_area isKindOfClass:[NSString class]] && (stone_origin_prov.length > 0 || stone_origin_city.length > 0 || stone_origin_area.length > 0)) {
        [des appendFormat:@"  产地：%@%@%@", stone_origin_prov, stone_origin_city, stone_origin_area];
    }
    self.labType.text = des;
    
    // 图片高度
    CGFloat tagImageHeight = DeviceWidth / 1.4;
    self.alcImageLoopContainerHeight.constant = tagImageHeight;
    
    // header高度
    [self calcTvHeader];
    
    // 添加图片滚动
    self.imageLoopView = [[YKuImageInLoopScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, tagImageHeight)];
    self.imageLoopView.delegate = self;
    // 设置yKuImageInLoopScrollView显示类型
    self.imageLoopView.scrollViewType = ScrollViewDefault;
    // 设置styledPageControl位置
    [self.imageLoopView.styledPageControl setPageControlSite:PageControlSiteMiddle];
    [self.imageLoopView.styledPageControl setBottomDistance:12];
    // 设置styledPageControl已选中下的内心圆颜色
    [self.imageLoopView.styledPageControl setCoreSelectedColor:MAIN_COLOR];
    [self.imageLoopView.styledPageControl setCoreNormalColor:[UIColor whiteColor]];
    [self.imageLoopContainer addSubview:self.imageLoopView];
    return;
}

// 详情绘制
- (void)drawDetailInfo {
    // 绘制信息
    [self drawInfo];
    // 是否有样品
    NSNumber *hasSample = self.curStoneDetailDict[@"stone_sample"];
    if (hasSample.integerValue == 0) {
        self.labHasSample.text = @"无样品";
        self.btnBuySample.enabled = NO;
        self.btnBuySample.backgroundColor = MAIN_GRAY_COLOR;
    } else {
        self.labHasSample.text = @"有样品";
        self.btnBuySample.enabled = YES;
        self.btnBuySample.backgroundColor = MAIN_ORANGE_COLOR;
    }
    self.labHasSample.hidden = NO;
    [self.btnBuySample setTitle:@"自助拿样" forState:UIControlStateNormal];
    if (![ZbSaveManager isSampleShow]) {
        self.labHasSample.hidden = YES;
        self.btnBuySample.enabled = NO;
        self.btnBuySample.backgroundColor = MAIN_GRAY_COLOR;
        [self.btnBuySample setTitle:@"微聊咨询" forState:UIControlStateNormal];
    }
    // 赞状态
    [self drawLikeStatus];
    return;
}

// 重算header高度
- (void)calcTvHeader {
    // header高度
    CGFloat tagHeaderHeight = [self.tvHeader systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect headerFrame = CGRectMake(0, 0, DeviceWidth, tagHeaderHeight);
    self.tvHeader.frame = headerFrame;
    [self.tv setTableHeaderView:self.tvHeader];
}

// 绘制交互状态
- (void)drawLikeStatus {
    // 赞状态
    NSNumber *like = self.curStoneDetailDict[@"stone_like"];
    self.btnPraise.selected = like.integerValue > 0;
    // 赞数量
    NSNumber *likes = self.curStoneDetailDict[@"stone_likes"];
    [self.btnPraise setTitle:[NSString stringWithFormat:@"%@", likes] forState:UIControlStateNormal];
    // 交互
    NSNumber *clicks = self.curStoneDetailDict[@"stone_clicks"];
    NSNumber *marks = self.curStoneDetailDict[@"stone_marks"];
    NSString *communicate = [NSString stringWithFormat:@"%@ 浏览  %@ 收藏", clicks, marks];
    self.labCommunicate.text = communicate;
    return;
}

#pragma mark - 接口调用
// 获取相关企业接口
- (void)requestCompanyListData {
    NSString *stoneName = self.stoneName;
    BMKUserLocation *curPos = [[LocationServiceManager shareManager] getCurLocation];
    NSString *lng = curPos == nil ? @"0" : [NSString stringWithFormat:@"%f", curPos.location.coordinate.longitude];
    NSString *lat = curPos == nil ? @"0" : [NSString stringWithFormat:@"%f", curPos.location.coordinate.latitude];
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyListByStone:stoneName pageIndex:page lng:lng lat:lat] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_COMPANY;
    [conn start];
    return;
}

// 获取更多相关企业接口
- (void)requestMoreCompanyListData {
    NSString *stoneName = self.stoneName;
    BMKUserLocation *curPos = [[LocationServiceManager shareManager] getCurLocation];
    NSString *lng = curPos == nil ? @"0" : [NSString stringWithFormat:@"%f", curPos.location.coordinate.longitude];
    NSString *lat = curPos == nil ? @"0" : [NSString stringWithFormat:@"%f", curPos.location.coordinate.latitude];
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyListByStone:stoneName pageIndex:page lng:lng lat:lat] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_COMPANY_MORE;
    [conn start];
    return;
}

// 获取推荐企业接口
- (void)requestRecommendCompanyListData {
    NSString *stoneName = self.stoneName;
    BMKUserLocation *curPos = [[LocationServiceManager shareManager] getCurLocation];
    NSString *lng = curPos == nil ? @"0" : [NSString stringWithFormat:@"%f", curPos.location.coordinate.longitude];
    NSString *lat = curPos == nil ? @"0" : [NSString stringWithFormat:@"%f", curPos.location.coordinate.latitude];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getRecommendCompanyListByStone:stoneName lng:lng lat:lat] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_R_COMPANY;
    [conn start];
    return;
}

// 获取相似石材接口
- (void)requestSimilarProductListData {
    NSString *stoneName = self.stoneName;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getSimilarStoneListByStone:stoneName] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_SIMILAR;
    [conn start];
    return;
}

// 获取样品规格接口
- (void)requestSampleListData {
    NSString *stoneName = self.stoneName;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getSampleProductSpecList:stoneName] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_SAMPLE;
    [conn start];
    return;
}

// 获取详情接口
- (void)requestStoneDetailData {
    NSString *stoneName = self.stoneName;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneInfoByName:stoneName] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_INFO;
    [conn start];
    return;
}

// 收藏接口
- (void)requestMark {
    self.hud.labelText = @"收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *stoneId = self.curStoneDetailDict[@"stone_id"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService markStone:stoneId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MARK;
    [conn start];
    return;
}

// 取消收藏接口
- (void)requestUnMark {
    self.hud.labelText = @"取消收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *stoneId = self.curStoneDetailDict[@"stone_id"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService unmarkStone:stoneId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNMARK;
    [conn start];
    return;
}

// 点赞接口
- (void)requestLike {
    NSString *stoneId = self.curStoneDetailDict[@"stone_id"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService likeStone:stoneId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_LIKE;
    [conn start];
    return;
}

// 点赞接口
- (void)requestUnlike {
    NSString *stoneId = self.curStoneDetailDict[@"stone_id"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService unlikeStone:stoneId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNLIKE;
    [conn start];
    return;
}

// 加入供应商
- (void)requestJoin {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *stoneName = self.stoneName;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService joinStoneToCompany:stoneName] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_JOIN;
    [conn start];
    return;
}

#pragma mark - 点击事件
// 右上角操作按钮点击
- (IBAction)btnOperOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    // 收藏状态
    NSNumber *mark = self.curStoneDetailDict[@"stone_mark"];
    NSString *markText = mark.integerValue > 0 ? @"取消收藏" : @"收藏";
    UIImage *markImage = [UIImage imageNamed:@"public_icn_normal_favor"];
    
    // 弹出框
    CGPoint point = CGPointMake(DeviceWidth - 10, 60);
    NSArray *imageArray = @[[UIImage imageNamed:@"nav_btn_add"], [UIImage imageNamed:@"nav_btn_share"], markImage];
    self.pv = [PopoverView showPopoverAtPoint:point inView:self.navigationController.view withStringArray:@[@"加入供应商", @"分享", markText] withImageArray:imageArray delegate:self];
    return;
}

// 赞按钮点击
- (IBAction)btnPraiseOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    NSNumber *like = self.curStoneDetailDict[@"stone_like"];
    NSNumber *likes = self.curStoneDetailDict[@"stone_likes"];
    if (like.integerValue == 0) {
        // 未赞，则赞s
        [self requestLike];
        [self.curStoneDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"stone_like"];
        [self.curStoneDetailDict setValue:[NSNumber numberWithInteger:(likes.integerValue + 1)] forKey:@"stone_likes"];
    } else {
        // 已赞，则取消
        [self requestUnlike];
        [self.curStoneDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"stone_like"];
        [self.curStoneDetailDict setValue:[NSNumber numberWithInteger:(likes.integerValue - 1)] forKey:@"stone_likes"];
    }
    [self drawLikeStatus];
    return;
}

// 样品下单按钮点击
- (IBAction)btnMakeOrderOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    if (self.sampleArray.count <= 0) {
        self.hud.labelText = @"无样品信息";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSMutableDictionary *sampleDict in self.sampleArray) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:sampleDict];
        NSString *stoneName = self.curStoneDetailDict[@"stone_name"];
        NSString *specId = sampleDict[@"spec_id"];
        [dict setValue:[NSString stringWithFormat:@"%@_%@", stoneName, specId] forKey:@"mainKey"];
        [dict setValue:self.curStoneDetailDict[@"stone_image"] forKey:@"stone_image"];
        [dict setValue:self.curStoneDetailDict[@"stone_name"] forKey:@"stone_name"];
        [dict setValue:@0 forKey:@"count"];
        [array addObject:dict];
    }
    [ZbDdOrderViewController showInViewController:self stoneName:self.stoneName dataArray:array btnClickBlock:^(Zb_Dd_Order_Click_Type type, NSMutableArray *tagArray) {
        switch (type) {
            case Zb_Dd_Order_Click_Cancel:
                return;
            case Zb_Dd_Order_Click_Buy:
            {
                ZbSampleOrderViewController *sampleOrderController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSampleOrderViewController"];
                sampleOrderController.cartSamplyArray = tagArray;
                [self.navigationController pushViewController:sampleOrderController animated:YES];
                return;
            }
            case Zb_Dd_Order_Click_Cart:
            {
                self.hud.labelText = @"添加成功";
                [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
                return;
            }
            case Zb_Dd_Order_Click_Chat:
                // 客服
            {
                // 聊天系统登录判断
                if (![[[EaseMob sharedInstance] chatManager] isLoggedIn]) {
                    self.hud.labelText = @"聊天系统数据正在加载中..请稍候";
                    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                    return;
                };
                NSDictionary *kefuDict = [ZbSaveManager getKefuDict];
                if (kefuDict == nil) {
                    return;
                }
                NSString *userName = kefuDict[@"user_name"];
                ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:userName isGroup:NO];
                [self.navigationController pushViewController:chatVC animated:YES];
                return;
            }
            default:
                break;
        }
    }];
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
        {
            // 相似石材
            return self.similarStoneArray.count > 0 ? 3 : 2;
        }
        case 1:
        {
            // 推荐企业
            return self.recommandCompanyArray.count + 2;
        }
        case 2:
            // 相关企业
            return self.companyArray.count + 1;
        default:
            return 0;
    }
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 相似石材
            NSInteger stoneCount = self.similarStoneArray.count > 0 ? 1 : 0;
            if (indexPath.row == 0) {
                return [ZbTypeTableViewCell getRowHeight];
            } else if (indexPath.row == (stoneCount + 2 - 1)) {
                return [ZbSeparateTableViewCell getRowHeight];
            }
            return [ZbStoneSimilarTableViewCell getRowHeight];
        }
        case 1:
        case 2:
        {
            // 推荐企业，相关企业
            NSArray *dataArray = indexPath.section == 1 ? self.recommandCompanyArray : self.companyArray;
            if (indexPath.row == 0) {
                return [ZbTypeTableViewCell getRowHeight];
            } else if (indexPath.row == (dataArray.count + 2 - 1)) {
                return [ZbSeparateTableViewCell getRowHeight];
            }
            return [ZbQiyeTableViewCell getRowHeight];
        }
        default:
            return 0;
    }
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 相似石材
            NSInteger stoneCount = self.similarStoneArray.count > 0 ? 1 : 0;
            if (indexPath.row == 0) {
                // 头
                ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
                headerCell.labTypeName.text = @"相似石材";
                headerCell.btnSelect.hidden = YES;
                headerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return headerCell;
            } else if (indexPath.row == (stoneCount + 2 - 1)) {
                // 尾
                UITableViewCell *footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
                footerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return footerCell;
            }
            ZbStoneSimilarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:StoneSimilarCellIdentifier forIndexPath:indexPath];
            [cell setStoneArray:self.similarStoneArray];
            cell.stoneClickBlock = ^(NSInteger productIndex, NSDictionary *stoneDict) {
                ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
                NSString *stoneName = stoneDict[@"stone_name"];
                controller.stoneName = stoneName;
                [self.navigationController pushViewController:controller animated:YES];
            };
            return cell;
        }
        case 1:
        {
            // 推荐企业
            if (indexPath.row == 0) {
                // 头
                ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
                headerCell.labTypeName.text = @"推荐企业";
                headerCell.btnSelect.hidden = YES;
                headerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return headerCell;
            } else if (indexPath.row == (self.recommandCompanyArray.count + 2 - 1)) {
                // 尾
                UITableViewCell *footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
                footerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return footerCell;
            }
            ZbQiyeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:QiyeCellIdentifier forIndexPath:indexPath];
            NSDictionary *curDict = [self.recommandCompanyArray objectAtIndex:(indexPath.row - 1)];
            [cell setInfoDict:curDict];
            [cell showDistance:curDict];
            return cell;
        }
        case 2:
        {
            // 相关企业
            if (indexPath.row == 0) {
                // 头
                ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
                headerCell.labTypeName.text = @"相关企业";
                headerCell.btnSelect.hidden = NO;
                headerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return headerCell;
            } else if (indexPath.row == (self.companyArray.count + 2 - 1)) {
                // 尾
                UITableViewCell *footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
                footerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return footerCell;
            }
            ZbQiyeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:QiyeCellIdentifier forIndexPath:indexPath];
            NSDictionary *curDict = [self.companyArray objectAtIndex:(indexPath.row - 1)];
            [cell setInfoDict:curDict];
            [cell showDistance:curDict];
            return cell;
        }
        default:
            return nil;
    }
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            // 相似石材
            NSInteger stoneCount = self.similarStoneArray.count > 0 ? 1 : 0;
            if (indexPath.row == 0) {
                // 头
                break;
            } else if (indexPath.row == (stoneCount + 2 - 1)) {
                // 尾
                break;
            }
            break;
        }
        case 1:
        {
            // 推荐企业
            if (indexPath.row == 0) {
                // 头
                break;
            } else if (indexPath.row == (self.recommandCompanyArray.count + 2 - 1)) {
                // 尾
                break;
            }
            NSDictionary *curDict = [self.recommandCompanyArray objectAtIndex:(indexPath.row - 1)];
            ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
            NSString *companyId = curDict[@"company_id"];
            controller.companyId = companyId;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case 2:
        {
            // 相关企业
            if (indexPath.row == 0) {
                // 头
                break;
            }
            NSDictionary *curDict = [self.companyArray objectAtIndex:(indexPath.row - 1)];
            // 推荐企业
            ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
            NSString *companyId = curDict[@"company_id"];
            controller.companyId = companyId;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        default:
            break;
    }
    return;
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 清石材大小图缓存
    NSString *smallImgUrl = [ZbWebService getImageFullUrl:self.curStoneDetailDict[@"stone_image_small"]];
    [[SDWebImageManager sharedManager].imageCache removeImageForKey:smallImgUrl withCompletion:nil];
    NSString *imgUrl = [ZbWebService getImageFullUrl:self.curStoneDetailDict[@"stone_image"]];
    [[SDWebImageManager sharedManager].imageCache removeImageForKey:imgUrl withCompletion:nil];
    
    // 获取详情数据
    [self requestCompanyListData];
    [self requestRecommendCompanyListData];
    [self requestSimilarProductListData];
    [self requestStoneDetailData];
    return;
}

- (void)footerRefresh {
    [self requestMoreCompanyListData];
}

#pragma mark - YKuImageInLoopScrollViewDelegate
- (int)numOfPageForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    NSArray *stoneImages = self.curStoneDetailDict[@"stone_images"];
    return (int)(1 + stoneImages.count);
}

- (int)widthForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    int width = DeviceWidth;
    return width;
}

- (UIView *)scrollView:(YKuImageInLoopScrollView *)ascrollView viewAtPageIndex:(int)apageIndex
{
    // 图片数据
    NSString *imgUrl;
    if (apageIndex == 0) {
        imgUrl = self.curStoneDetailDict[@"stone_image"];
    } else {
        NSArray *stoneImages = self.curStoneDetailDict[@"stone_images"];
        imgUrl = [stoneImages objectAtIndex:(apageIndex - 1)];
    }
    
    // 图片控件
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    UIImageView *curImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CGRectGetHeight(ascrollView.frame))];
    [curImageView sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]]];
    curImageView.contentMode = UIViewContentModeScaleToFill;
    [bgView addSubview:curImageView];
    return bgView;
}

- (void)scrollView:(YKuImageInLoopScrollView*) ascrollView didTapIndex:(int)apageIndex{
    DLog(@"Clicked page%d",apageIndex);
    // 图片游览器
    ZLPhotoPickerBrowserViewController *pickerBrowser = [[ZLPhotoPickerBrowserViewController alloc] init];
    // 淡入淡出效果
    // pickerBrowser.status = UIViewAnimationAnimationStatusFade;
    //    pickerBrowser.toView = btn;
    // 数据源/delegate
    pickerBrowser.delegate = self;
    pickerBrowser.dataSource = self;
    // 当前选中的值
    pickerBrowser.currentIndexPath = [NSIndexPath indexPathForItem:apageIndex inSection:0];
    // 展示控制器
    [pickerBrowser showPickerVc:self.navigationController];
}

/*
 选中第几页
 @param didSelectedPageIndex 选中的第几项，[0-numOfPageForScrollView];
 */
-(void) scrollView:(YKuImageInLoopScrollView*) ascrollView didSelectedPageIndex:(int) apageIndex
{
    //    NSLog(@"didSelectedPageIndex:%d",apageIndex);
}

#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.1f];
    if (index == 0) {
        // 加入供应商
        if ([ZbWebService sharedQy] == nil) {
            self.hud.labelText = @"成为企业用户，即可加入供应商";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        [self requestJoin];
    } else if (index == 1) {
        // 分享
        // 石材信息
        NSString *stoneName = self.stoneName;
        NSString *stoneId = self.curStoneDetailDict[@"stone_id"];
        NSString *stoneType = self.curStoneDetailDict[@"stone_type"];
        NSString *stoneColor = self.curStoneDetailDict[@"stone_color"];
        NSString *stoneOriginCountry = self.curStoneDetailDict[@"stone_origin_country"];
        NSString *des = [NSString stringWithFormat:@"%@ %@ %@", stoneType, stoneColor, stoneOriginCountry];
        if (des.length > SHARE_CONTENT_MAX_LEN) {
            des = [des substringToIndex:SHARE_CONTENT_MAX_LEN];
            des = [NSString stringWithFormat:@"%@...", des];
        }
        NSString *imgUrl = [ZbWebService getImageFullUrl:self.curStoneDetailDict[@"stone_image_small"]];
        // 分享信息
        NSString *title = [NSString stringWithFormat:@"石猫石材搜索-%@", stoneName];
        NSString *shareUrl = [ZbWebService shareStoneUrl:stoneId];
//        NSDictionary *appDataDict = @{@"tag":[NSNumber numberWithInt:Zb_Share_Ext_Type_Stone], @"stone_name":self.stoneName};
//        NSString *appDataJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:appDataDict options:0 error:nil] encoding:NSUTF8StringEncoding];
        
        //构造分享内容
        id<ISSContent> publishContent = [ShareSDK content:des
                                           defaultContent:@""
                                                    image:[ShareSDK imageWithUrl:imgUrl]
                                                    title:title
                                                      url:shareUrl
                                              description:@"石材详情"
                                                mediaType:SSPublishContentMediaTypeNews];
        // 微信好友数据
        [publishContent addWeixinSessionUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeNews] content:des title:title url:shareUrl image:[ShareSDK imageWithUrl:imgUrl] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
        // 微信朋友圈数据
        [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeNews] content:des title:title url:shareUrl image:[ShareSDK imageWithUrl:imgUrl] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
        // 短信数据
        [publishContent addSMSUnitWithContent:[NSString stringWithFormat:@"[石猫]向你推荐： %@ ，链接为：%@", stoneName, shareUrl]];
        
        // 分享按钮列表
        NSArray *shareList;
        if ([WXApi isWXAppInstalled] && [QQApiInterface isQQInstalled]) {
            shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeQQ, ShareTypeSMS, nil];
        } else if ([WXApi isWXAppInstalled]) {
            shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeSMS, nil];
        } else if ([QQApiInterface isQQInstalled]) {
            shareList = [ShareSDK getShareListWithType:ShareTypeQQ, ShareTypeSMS, nil];
        } else {
            shareList = [ShareSDK getShareListWithType:ShareTypeSMS, nil];
        }
        
        //创建弹出菜单容器
        id<ISSContainer> container = [ShareSDK container];
        //弹出分享菜单
        [ShareSDK showShareActionSheet:container
                             shareList:shareList
                               content:publishContent
                         statusBarTips:YES
                           authOptions:nil
                          shareOptions:nil
                                result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                    
                                    if (state == SSResponseStateSuccess)
                                    {
                                        NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                    }
                                    else if (state == SSResponseStateFail)
                                    {
                                        NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                    }
                                }];
        return;
    } else {
        // 收藏
        NSNumber *mark = self.curStoneDetailDict[@"stone_mark"];
        NSNumber *marks = self.curStoneDetailDict[@"stone_marks"];
        if (mark.integerValue == 0) {
            // 未收藏，则收藏
            [self requestMark];
            [self.curStoneDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"stone_mark"];
            [self.curStoneDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue + 1)] forKey:@"stone_marks"];
        } else {
            // 已收藏，则取消
            [self requestUnMark];
            [self.curStoneDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"stone_mark"];
            [self.curStoneDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue - 1)] forKey:@"stone_marks"];
        }
        [self drawLikeStatus];
    }
}

- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    self.pv = nil;
}

#pragma mark - <ZLPhotoPickerBrowserViewControllerDataSource>
- (long)numberOfSectionInPhotosInPickerBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser{
    return 1;
}

- (long)photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser numberOfItemsInSection:(NSUInteger)section{
    NSArray *stoneImages = self.curStoneDetailDict[@"stone_images"];
    return (int)(1 + stoneImages.count);
}

#pragma mark - 每个组展示什么图片,需要包装下ZLPhotoPickerBrowserPhoto
- (ZLPhotoPickerBrowserPhoto *) photoBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser photoAtIndexPath:(NSIndexPath *)indexPath{
    // 图片数据
    NSString *imgUrl;
    if (indexPath.row == 0) {
        imgUrl = self.curStoneDetailDict[@"stone_image"];
    } else {
        NSArray *stoneImages = self.curStoneDetailDict[@"stone_images"];
        imgUrl = [stoneImages objectAtIndex:(indexPath.row - 1)];
    }
    
    ZLPhotoPickerBrowserPhoto *photo = [[ZLPhotoPickerBrowserPhoto alloc] init];
    photo.photoURL = [NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]];
    
//    photo.toView = self.imageLoopContainer;
    return photo;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.footer endRefreshing];
    [self.tv.header endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.footer endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 加载失败
    if (connection.tag != HTTP_REQUEST_TAG_JOIN && result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        [self.tv.header endRefreshing];
        return;
    }
    
    if (connection.tag == HTTP_REQUEST_TAG_COMPANY) {
        // 相关企业
        NSMutableDictionary *dataDict = resultDic[@"data"];
        NSMutableArray *array = dataDict[@"rows"];
        [self.companyArray removeAllObjects];
        [self.companyArray addObjectsFromArray:array];
        
        if (array.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
        
//        [self requestRecommendCompanyListData];
    } else if (connection.tag == HTTP_REQUEST_TAG_COMPANY_MORE) {
        // 更多相关企业
        [self.tv.footer endRefreshing];
        NSMutableArray *array = resultDic[@"data"][@"rows"];
        [self.companyArray addObjectsFromArray:array];
        if (array.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_R_COMPANY) {
        // 推荐企业
        NSMutableArray *array = resultDic[@"data"];
        [self.recommandCompanyArray removeAllObjects];
        [self.recommandCompanyArray addObjectsFromArray:array];
        
        [self.tv reloadData];
//        [self requestSimilarProductListData];
    } else if (connection.tag == HTTP_REQUEST_TAG_SIMILAR) {
        // 相似石材
        NSMutableArray *array = resultDic[@"data"];
        [self.similarStoneArray removeAllObjects];
        [self.similarStoneArray addObjectsFromArray:array];
        
        [self.tv reloadData];
//        [self requestStoneDetailData];
    } else if (connection.tag == HTTP_REQUEST_TAG_SAMPLE) {
        // 样品规格
        NSMutableArray *array = resultDic[@"data"];
        [self.sampleArray removeAllObjects];
        [self.sampleArray addObjectsFromArray:array];
    } else if (connection.tag == HTTP_REQUEST_TAG_INFO) {
        self.curStoneDetailDict = resultDic[@"data"];
        
        [self.tv.header endRefreshing];
        [self.hud hide:NO];
        [self drawDetailInfo];
        [self.tv reloadData];
        [self.imageLoopView reloadData];
        
        NSNumber *hasSample = self.curStoneDetailDict[@"stone_sample"];
        if (hasSample.integerValue != 0) {
            // 有样品，加载样品信息
            [self requestSampleListData];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_JOIN) {
        // 加入供应商
        if (result.integerValue != 0) {
            if (result.integerValue == -15) {
                self.hud.labelText = @"您已是该石材供应商";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        self.hud.labelText = @"加入供应商成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
