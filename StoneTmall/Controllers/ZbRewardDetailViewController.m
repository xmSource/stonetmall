//
//  ZbRewardDetailViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardDetailViewController.h"
#import "ZbTypeTableViewCell.h"
#import <ShareSDK/ShareSDK.h>
#import "ZbLoginMainViewController.h"
#import "ZbPostOwnerTableViewCell.h"
#import "ZbPostCommentTableViewCell.h"
#import "UITableView+FDTemplateLayoutCell.h"
#import "ZbRewardDelayViewController.h"
#import "ZbRewardJoinViewController.h"
#import "ZbRewardJoinTableViewCell.h"
#import "ZbRewardApplyDelayTableViewCell.h"
#import "ZbRewardSelectTableViewCell.h"
#import "ZbRewardManager.h"
#import "ZbSeparateTableViewCell.h"
#import "ZLPhoto.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "ChatViewController.h"
#import "ZbPersonInfoViewController.h"

#define HTTP_REQUEST_TAG_IMAGE 1          // 图片
#define HTTP_REQUEST_TAG_COMMENT 2        // 回答
#define HTTP_REQUEST_TAG_WIN 3            // 中标回答
#define HTTP_REQUEST_TAG_INFO 4           // 详情
#define HTTP_REQUEST_TAG_USE 7                // 采用回答
#define HTTP_REQUEST_TAG_MARK 8               // 收藏
#define HTTP_REQUEST_TAG_UNMARK 9             // 取消收藏

#define ALERT_TAG_ERRORPOST 0             // 无效帖子
#define ALERT_TAG_USE 10                  // 采用回答

@interface ZbRewardDetailViewController () <UITableViewDataSource, UITableViewDelegate, ZLPhotoPickerBrowserViewControllerDataSource, ZLPhotoPickerBrowserViewControllerDelegate, UIAlertViewDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 帖子类型
@property (nonatomic, strong) IBOutlet UILabel *labPostType;
// 帖子类型背景
@property (nonatomic, strong) IBOutlet UIView *bgPostType;
// 标题
@property (nonatomic, strong) IBOutlet UILabel *labTitle;
// 时间
@property (nonatomic, strong) IBOutlet UILabel *labDate;
// 截止
@property (nonatomic, strong) IBOutlet UILabel *labTimeRemain;
// 参与
@property (nonatomic, strong) IBOutlet UILabel *labJoins;
// 内容
@property (nonatomic, strong) IBOutlet UILabel *labContent;
// 收藏按钮
@property (nonatomic, strong) IBOutlet UIButton *btnMark;
// 图片按钮数组
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *ivArray;

// 计时器
@property (nonatomic, strong) NSTimer * curTimer;

// 图片数组
@property (nonatomic, strong) NSMutableArray *imageArray;
// 回答数组
@property (nonatomic, strong) NSMutableArray *replyArray;
// 中标回答
@property (nonatomic, strong) NSMutableDictionary *winCommentDict;
// 当前悬赏详情
@property (nonatomic, strong) NSDictionary *curPostDetailDict;

@end

@implementation ZbRewardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.replyArray = [NSMutableArray array];
    self.imageArray = [NSMutableArray array];
    
    // 圆角
    self.bgPostType.layer.cornerRadius = 3;
    self.bgPostType.clipsToBounds = YES;
    
    // 边框
    for (UIImageView *ivImage in self.ivArray) {
        ivImage.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
        ivImage.layer.borderWidth = 1;
        ivImage.layer.masksToBounds = YES;
    }
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbPostOwnerTableViewCell" bundle:nil] forCellReuseIdentifier:SupplyDemandOwnerCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbPostCommentTableViewCell" bundle:nil] forCellReuseIdentifier:PostCommentCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbTypeTableViewCell" bundle:nil] forCellReuseIdentifier:TypeHeaderIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSeparateTableViewCell" bundle:nil] forCellReuseIdentifier:SeparateCellIdentifier];
    [self.tv setTableHeaderView:nil];
    
    // 上拉重新加载
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    // 获取数据
    [self requestCommentListData];
    [self requestDetailData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 定时器
    self.curTimer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.curTimer invalidate];
    self.curTimer = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 登录判断
- (void)requestLogin {
    self.hud.labelText = @"尚未登录，无法操作";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
        UIStoryboard *loginSb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        UINavigationController *nav = [loginSb instantiateViewControllerWithIdentifier:@"LoginNav"];
        ZbLoginMainViewController *loginController = (ZbLoginMainViewController *)nav.viewControllers[0];
        loginController.loginSucessBlock = ^(void) {
            [self.tv.header performSelectorOnMainThread:@selector(beginRefreshing) withObject:nil waitUntilDone:NO];
        };
        [self.view.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }];
    return;
}

#pragma mark - 界面绘制
// 绘制悬赏概括
- (void)drawInfo {
    // 类型
    [ZbRewardManager setTypeText:self.labPostType typeBg:self.bgPostType postDict:self.curPostDetailDict];
    // 标题
    NSString *title = self.curPostDetailDict[@"post_title"];
    NSNumber *cent = self.curPostDetailDict[@"post_price"];
    NSNumber *yuan = [NSNumber numberWithInteger:(cent.integerValue / 100)];
    NSString *price = [NSString stringWithFormat:@"¥%@ ", yuan];
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", price, title]];
    // 价格字属性
    [attriString addAttribute:NSForegroundColorAttributeName
                        value:UIColorFromHexString(@"EF684D")
                        range:NSMakeRange(0, price.length)];
    [attriString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:16] range:NSMakeRange(0, price.length)];
    // 标题字属性
    [attriString addAttribute:NSForegroundColorAttributeName
                        value:UIColorFromHexString(@"333333")
                        range:NSMakeRange(price.length, title.length)];
    [attriString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(price.length, title.length)];
    self.labTitle.attributedText = attriString;
    
    // 时间
    NSString *date = [CommonCode getTimeShowText:self.curPostDetailDict[@"post_modified"]];
    self.labDate.text = date;
    // 悬赏状态
    [ZbRewardManager setStatusText:self.labTimeRemain postDict:self.curPostDetailDict];
    // 参与人数
    self.labJoins.text = [NSString stringWithFormat:@"%@人参与", self.curPostDetailDict[@"post_joins"]];
    // 内容
    self.labContent.text = self.curPostDetailDict[@"post_content"];
    // header高度
    [self calcTvHeader];
    // 图片
    NSInteger hasImageCount = 1 + self.imageArray.count;
    for (NSInteger i = 0; i < self.ivArray.count; i++) {
        UIImageView *ivImage = [self.ivArray objectAtIndex:i];
        ivImage.tag = i;
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageOnClick:)];
        [ivImage addGestureRecognizer:ges];
        if (i >= hasImageCount) {
            ivImage.hidden = YES;
            continue;
        }
        ivImage.hidden = NO;
        if (i == 0) {
            NSString *imageUrl = self.curPostDetailDict[@"post_image"];
            [ivImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imageUrl]]];
        } else {
            NSString *imageUrl = [self.imageArray objectAtIndex:(i - 1)];
            [ivImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imageUrl]]];
        }
    }
    // 收藏状态
    [self drawMarkStatus];
    return;
}

// 重算header高度
- (void)calcTvHeader {
    self.labTitle.preferredMaxLayoutWidth = DeviceWidth - (53 + 15);
    self.labContent.preferredMaxLayoutWidth = DeviceWidth - 15*2;
    // header高度
    CGFloat tagHeaderHeight = [self.tvHeader systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect headerFrame = CGRectMake(0, 0, DeviceWidth, tagHeaderHeight);
    self.tvHeader.frame = headerFrame;
    [self.tv setTableHeaderView:self.tvHeader];
}

// 绘制收藏状态
- (void)drawMarkStatus {
    // 收藏状态
    NSNumber *mark = self.curPostDetailDict[@"post_mark"];
    self.btnMark.selected = mark.integerValue > 0;
    return;
}

#pragma mark - 计时器
// 定时器
- (void)onTimer {
    DLog(@"悬赏详情onTimer()");
    
    // 未加载完详情
    if (self.curPostDetailDict == nil) {
        return;
    }
    
    // 进行中才需要倒计时
    NSString *post_status = self.curPostDetailDict[@"post_status"];
    BOOL isCanJoin = post_status.intValue == Zb_Post_Status_Type_Join || post_status.intValue == Zb_Post_Status_Type_Delay;
    if (isCanJoin) {
        BOOL isCountDownEnd = [ZbRewardManager isCountDownEnd:self.curPostDetailDict];
        if (isCountDownEnd) {
            // 重新加载数据
            [self.tv.header beginRefreshing];
        } else {
            // 显示倒计时
            NSArray *visibleCells = self.tv.visibleCells;
            for (UITableViewCell *cell in visibleCells) {
                if ([cell isKindOfClass:[ZbRewardJoinTableViewCell class]]) {
                    ZbRewardJoinTableViewCell *joinCell = (ZbRewardJoinTableViewCell *)cell;
                    [joinCell setCounDownDate:self.curPostDetailDict];
                } else if ([cell isKindOfClass:[ZbRewardApplyDelayTableViewCell class]]) {
                    ZbRewardApplyDelayTableViewCell *delayCell = (ZbRewardApplyDelayTableViewCell *)cell;
                    [delayCell setCounDownDate:self.curPostDetailDict];
                }
            }
            
        }
    }
    
    return;
}

#pragma mark - 点击事件
// 图片点击
- (void)imageOnClick:(UITapGestureRecognizer *)ges {
    UIImageView *iv = (UIImageView *)ges.view;
    
    // 图片游览器
    ZLPhotoPickerBrowserViewController *pickerBrowser = [[ZLPhotoPickerBrowserViewController alloc] init];
    // 淡入淡出效果
    // pickerBrowser.status = UIViewAnimationAnimationStatusFade;
    //    pickerBrowser.toView = btn;
    // 数据源/delegate
    pickerBrowser.delegate = self;
    pickerBrowser.dataSource = self;
    // 当前选中的值
    pickerBrowser.currentIndexPath = [NSIndexPath indexPathForItem:iv.tag inSection:0];
    // 展示控制器
    [pickerBrowser showPickerVc:self.navigationController];
    return;
}

// 收藏按钮点击
- (IBAction)btnMarkOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    NSNumber *mark = self.curPostDetailDict[@"post_mark"];
    NSNumber *marks = self.curPostDetailDict[@"post_marks"];
    if (mark.integerValue == 0) {
        // 未收藏，则收藏
        [self requestMark];
        [self.curPostDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"post_mark"];
        [self.curPostDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue + 1)] forKey:@"post_marks"];
    } else {
        // 已收藏，则取消
        [self requestUnMark];
        [self.curPostDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"post_mark"];
        [self.curPostDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue - 1)] forKey:@"post_marks"];
    }
    [self drawMarkStatus];
    return;
}

// 分享按钮点击
- (IBAction)btnShareOnClick:(id)sender {
    // 产品信息
    NSString *productTitle = self.curPostDetailDict[@"post_title"];
    NSString *des = self.curPostDetailDict[@"post_content"];
    if (des.length > SHARE_CONTENT_MAX_LEN) {
        des = [des substringToIndex:SHARE_CONTENT_MAX_LEN];
        des = [NSString stringWithFormat:@"%@...", des];
    }
    NSString *imgUrl = [ZbWebService getImageFullUrl:self.curPostDetailDict[@"post_image"]];
    // 分享信息
    NSString *title;
    title = [NSString stringWithFormat:@"[悬赏]-%@", productTitle];
    NSString *shareUrl = [ZbWebService shareSAndDUrl:self.curPostId];
//    NSDictionary *appDataDict = @{@"tag":[NSNumber numberWithInt:Zb_Share_Ext_Type_Reward], @"post_id":self.curPostId};
//    NSString *appDataJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:appDataDict options:0 error:nil] encoding:NSUTF8StringEncoding];
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:des
                                       defaultContent:@""
                                                image:[ShareSDK imageWithUrl:imgUrl]
                                                title:title
                                                  url:shareUrl
                                          description:@"悬赏详情"
                                            mediaType:SSPublishContentMediaTypeNews];
    // 微信好友数据
    [publishContent addWeixinSessionUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeApp] content:des title:title url:shareUrl image:[ShareSDK imageWithUrl:imgUrl] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
    // 微信朋友圈数据
    [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeNews] content:des title:title url:shareUrl image:[ShareSDK imageWithUrl:imgUrl] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
    // 短信数据
    [publishContent addSMSUnitWithContent:[NSString stringWithFormat:@"[石猫]向你推荐： %@ ，链接为：%@", productTitle, shareUrl]];
    
    // 分享按钮列表
    NSArray *shareList;
    if ([WXApi isWXAppInstalled] && [QQApiInterface isQQInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeQQ, ShareTypeSMS, nil];
    } else if ([WXApi isWXAppInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeSMS, nil];
    } else if ([QQApiInterface isQQInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeQQ, ShareTypeSMS, nil];
    } else {
        shareList = [ShareSDK getShareListWithType:ShareTypeSMS, nil];
    }
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:shareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];
    return;
}

#pragma mark - 接口调用
// 获取图片接口
- (void)requestImageListData {
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getSupplyDemandImageList:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_IMAGE;
    [conn start];
    return;
}

// 获取回答列表接口
- (void)requestCommentListData {
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getPaidDemandCommentList:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_COMMENT;
    [conn start];
    return;
}

// 获取中标回答接口
- (void)requestWinData {
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getWinBidComment:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_WIN;
    [conn start];
    return;
}

// 详情接口
- (void)requestDetailData {
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getPaidDemandInfo:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_INFO;
    [conn start];
}

// 采用回答接口
- (void)requestUseComment:(NSString *)commentId {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService usePaidDemandComment:postId commentId:commentId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_USE;
    [conn start];
    return;
}

// 收藏接口
- (void)requestMark {
    self.hud.labelText = @"收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService markPost:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MARK;
    [conn start];
    return;
}

// 取消收藏接口
- (void)requestUnMark {
    self.hud.labelText = @"取消收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService unmarkPost:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNMARK;
    [conn start];
    return;
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 0) {
        // 无效帖子
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    
    NSString *commentId = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:alertView.tag]];
    [self requestUseComment:commentId];
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            // 悬赏状态
            return 2;
        case 1:
            // 悬赏者
            return 2;
        case 2:
            // 中标回答
            return self.winCommentDict == nil ? 0 : 3;
        case 3:
            // 投标回答
            return self.replyArray.count + 1;
            
        default:
            break;
    }
    return 0;
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            // 悬赏状态
            if (indexPath.row == 0) {
                return [ZbRewardManager getStatusRowHeight:self.curPostDetailDict];
            } else {
                return [ZbSeparateTableViewCell getRowHeight];
            }
        case 1:
        {
            // 悬赏者
            if (indexPath.row == 0) {
                return [ZbPostOwnerTableViewCell getRowHeight];
            }
            return [ZbSeparateTableViewCell getRowHeight];
        }
        case 2:
        {
            // 中标回答
            if (indexPath.row == 0) {
                return [ZbTypeTableViewCell getRowHeight];
            } else if (indexPath.row == 1) {
                CGFloat height = [tableView fd_heightForCellWithIdentifier:PostCommentCellIdentifier configuration:^(ZbPostCommentTableViewCell *cell) {
                    [cell setCommentDictForCalc:self.winCommentDict];
                }];
                return height;
            } else {
                return [ZbSeparateTableViewCell getRowHeight];
            }
        }
        case 3:
        {
            // 回复
            if (indexPath.row == 0) {
                return [ZbTypeTableViewCell getRowHeight];
            }
            CGFloat height = [tableView fd_heightForCellWithIdentifier:PostCommentCellIdentifier configuration:^(ZbPostCommentTableViewCell *cell) {
                NSDictionary *curDict = [self.replyArray objectAtIndex:(indexPath.row - 1)];
                [cell setCommentDictForCalc:curDict];
            }];
            return height;
        }
        default:
            break;
    }
    return 0;
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 悬赏状态
            if (indexPath.row == 0) {
                NSString *post_status = self.curPostDetailDict[@"post_status"];
                NSString *post_role = self.curPostDetailDict[@"post_role"];
                if (post_status.intValue == Zb_Post_Status_Type_Pay) {
                    // 待支付，只有悬赏人才能看到
                    ZbRewardSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell" forIndexPath:indexPath];
                    cell.labDate.text = @"待支付";
                    return cell;
                } else if (post_status.intValue == Zb_Post_Status_Type_Join || post_status.intValue == Zb_Post_Status_Type_Delay) {
                    // 进行中
                    
                    if (post_role.intValue == Zb_Post_Role_Type_Owner) {
                        // 悬赏人看到延长cell
                        ZbRewardApplyDelayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ApplyDelayCell" forIndexPath:indexPath];
                        [cell setPostDict:self.curPostDetailDict];
                        cell.delayClickBlock = ^(void) {
                            // 延长按钮点击
                            ZbRewardDelayViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardDelayViewController"];
                            controller.curPostDict = self.curPostDetailDict;
                            controller.applyOkBlock = ^(void) {
                                // 申请成功block，重刷数据
                                [self.tv.header beginRefreshing];
                                [self.tv.header drawRect:CGRectZero];
                            };
                            [self.navigationController pushViewController:controller animated:YES];
                        };
                        
                        return cell;
                    } else {
                        // 其他人看到参与cell
                        ZbRewardJoinTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JoinCell" forIndexPath:indexPath];
                        [cell setPostDict:self.curPostDetailDict];
                        cell.joinClickBlock = ^(void) {
                            // 登录判断
                            if (![ZbWebService isLogin]) {
                                [self requestLogin];
                                return;
                            }
                            // 参与按钮点击
                            ZbRewardJoinViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardJoinViewController"];
                            controller.curPostId = self.curPostId;
                            controller.pubOkBlock = ^(void) {
                                // 发布成功block，重刷数据
                                [self.tv.header beginRefreshing];
                                [self.tv.header drawRect:CGRectZero];
                            };
                            [self.navigationController pushViewController:controller animated:YES];
                        };
                        return cell;
                    }
                } else if (post_status.intValue == Zb_Post_Status_Type_Select) {
                    // 选标中，看到文字cell
                    if (post_role.intValue == Zb_Post_Role_Type_Owner) {
                        // 悬赏人
                        ZbRewardSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell" forIndexPath:indexPath];
                        [cell setSelectDate:self.curPostDetailDict isOwner:YES];
                        return cell;
                    } else {
                        // 其他人
                        ZbRewardSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell" forIndexPath:indexPath];
                        [cell setSelectDate:self.curPostDetailDict isOwner:NO];
                        return cell;
                    }
                } else {
                    // 其它状态，看到描述文字
                    if (post_status.intValue < Zb_Post_Status_Type_Finish) {
                        // 悬赏结束
                        ZbRewardSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell" forIndexPath:indexPath];
                        cell.labDate.text = @"选标已结束";
                        return cell;
                    } else {
                        // 选标结束
                        ZbRewardSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectCell" forIndexPath:indexPath];
                        cell.labDate.text = @"悬赏已结束";
                        return cell;
                    }
                }
            } else {
                ZbSeparateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier forIndexPath:indexPath];
                return cell;
            }
        }
        case 1:
        {
            // 悬赏者
            if (indexPath.row == 0) {
                ZbPostOwnerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandOwnerCellIdentifier forIndexPath:indexPath];
                [cell setPostDict:self.curPostDetailDict];
                cell.labOwnerType.text = @"悬赏者信息";
                // 微聊点击
                NSString *userName = self.curPostDetailDict[@"user_name"];
                cell.chatClickBlock = ^(void) {
                    // 登录判断
                    if (![ZbWebService isLogin]) {
                        [self requestLogin];
                        return;
                    }
                    // 聊天系统登录判断
                    if (![[[EaseMob sharedInstance] chatManager] isLoggedIn]) {
                        self.hud.labelText = @"聊天系统数据正在加载中..请稍候";
                        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                        return;
                    };
                    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
                    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
                    if (loginUsername && loginUsername.length > 0) {
                        if ([loginUsername isEqualToString:userName]) {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notChatSelf", @"can't talk to yourself") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
                            [alertView show];
                            
                            return;
                        }
                    }
                    // 有可能不是好友，因此添加此用户昵称信息
                    [ZbSaveManager addUserInfo:@{@"user_name":userName, @"user_nickname":self.curPostDetailDict[@"user_nickname"], @"user_image":self.curPostDetailDict[@"user_image"]}];
                    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:userName isGroup:NO];
                    [self.navigationController pushViewController:chatVC animated:YES];
                };
                return cell;
            }
            ZbSeparateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier forIndexPath:indexPath];
            return cell;
        }
        case 2:
        {
            // 中标回答
            if (indexPath.row == 0) {
                ZbTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier forIndexPath:indexPath];
                cell.labTypeName.text = [NSString stringWithFormat:@"中标回答"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            } else if (indexPath.row == 1) {
                ZbPostCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PostCommentCellIdentifier forIndexPath:indexPath];
                [cell setCommentDict:self.winCommentDict];
                
                cell.btnUse.hidden = YES;
                cell.ivWin.hidden = NO;
                return cell;
            } else {
                ZbSeparateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier forIndexPath:indexPath];
                return cell;
            }
        }
        case 3:
        {
            // 回复
            if (indexPath.row == 0) {
                ZbTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier forIndexPath:indexPath];
                cell.labTypeName.text = [NSString stringWithFormat:@"投标回答（%lu）", (unsigned long)self.replyArray.count];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            ZbPostCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PostCommentCellIdentifier forIndexPath:indexPath];
            NSDictionary *curDict = [self.replyArray objectAtIndex:(indexPath.row - 1)];
            [cell setCommentDict:curDict];
            
            __weak typeof(self) weakSelf = self;
            NSString *commontId = curDict[@"post_id"];
            // 采用回复
            cell.useClickBlock = ^(NSDictionary *commentDict) {
                // 名字
                NSString *name = commentDict[@"user_nickname"];
                if (name != nil && ![name isKindOfClass:[NSNull class]] && name.length > 0) {
                } else {
                    name = commentDict[@"user_name"];
                }
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"确定选择%@的回答吗？", name] message:nil delegate:weakSelf cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alert show];
                alert.tag = commontId.integerValue;
            };
            
            cell.ivWin.hidden = YES;
            cell.btnUse.hidden = YES;
            NSString *post_status = self.curPostDetailDict[@"post_status"];
            NSString *post_role = self.curPostDetailDict[@"post_role"];
            if (post_status.intValue < Zb_Post_Status_Type_AnswerOk) {
                // 选标结束之前，都能采用回复
                if (post_role.intValue == Zb_Post_Role_Type_Owner) {
                    // 悬赏人看到采用按钮
                    cell.btnUse.hidden = NO;
                }
            }
            return cell;
        }
        default:
            return nil;
    }
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
            // 悬赏状态
            break;
        case 1:
        {
            // 悬赏者
            if (indexPath.row == 0) {
                NSString *userName = self.curPostDetailDict[@"user_name"];
                NSString *nickName = self.curPostDetailDict[@"user_nickname"];
                ZbPersonInfoViewController *person = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbPersonInfoViewController"];
                person.userName = userName;
                if (nickName != nil && [nickName isKindOfClass:[NSString class]] && nickName.length > 0) {
                    person.nickName = nickName;
                }
                [self.navigationController pushViewController:person animated:YES];
            }
            break;
        }
        case 2:
        {
            // 中标回答
            if (indexPath.row == 0) {
                break;
            } else if (indexPath.row == 1) {
                NSString *userName = self.winCommentDict[@"user_name"];
                NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
                NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
                if (loginUsername && loginUsername.length > 0) {
                    if ([loginUsername isEqualToString:userName]) {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notChatSelf", @"can't talk to yourself") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
                        [alertView show];
                        
                        return;
                    }
                }
                NSString *nickName = self.winCommentDict[@"user_nickname"];
                ZbPersonInfoViewController *person = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbPersonInfoViewController"];
                person.userName = userName;
                if (nickName != nil && [nickName isKindOfClass:[NSString class]] && nickName.length > 0) {
                    person.nickName = nickName;
                }
                [self.navigationController pushViewController:person animated:YES];
                break;
            } else {
                break;
            }
        }
        case 3:
        {
            // 回复
            if (indexPath.row != 0) {
                NSDictionary *curDict = [self.replyArray objectAtIndex:(indexPath.row - 1)];
                NSString *userName = curDict[@"user_name"];
                NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
                NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
                if (loginUsername && loginUsername.length > 0) {
                    if ([loginUsername isEqualToString:userName]) {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notChatSelf", @"can't talk to yourself") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
                        [alertView show];
                        
                        return;
                    }
                }
                NSString *nickName = curDict[@"user_nickname"];
                ZbPersonInfoViewController *person = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbPersonInfoViewController"];
                person.userName = userName;
                if (nickName != nil && [nickName isKindOfClass:[NSString class]] && nickName.length > 0) {
                    person.nickName = nickName;
                }
                [self.navigationController pushViewController:person animated:YES];
            }
            break;
        }
        default:
            break;
    }
    return;
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    [self requestCommentListData];
    [self requestDetailData];
}

#pragma mark - <ZLPhotoPickerBrowserViewControllerDataSource>
- (long)numberOfSectionInPhotosInPickerBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser{
    return 1;
}

- (long)photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser numberOfItemsInSection:(NSUInteger)section{
    NSInteger hasImageCount = 1 + self.imageArray.count;
    return hasImageCount;
}

#pragma mark - 每个组展示什么图片,需要包装下ZLPhotoPickerBrowserPhoto
- (ZLPhotoPickerBrowserPhoto *) photoBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser photoAtIndexPath:(NSIndexPath *)indexPath{
    // 图片数据
    NSString *imgUrl;
    if (indexPath.row == 0) {
        imgUrl = self.curPostDetailDict[@"post_image"];
    } else {
        imgUrl = [self.imageArray objectAtIndex:(indexPath.row - 1)];
    }
    
    ZLPhotoPickerBrowserPhoto *photo = [[ZLPhotoPickerBrowserPhoto alloc] init];
    photo.photoURL = [NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]];
    
    //    photo.toView = [self.ivArray objectAtIndex:indexPath.row];
    return photo;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    [self.hud hide:NO];
    [self.tv.header endRefreshing];
    // 加载失败
    if (result.integerValue != 0) {
        NSString *msg = resultDic[@"msg"];
        if (connection.tag == HTTP_REQUEST_TAG_INFO) {
            if ([msg hasPrefix:@"invalid postId"]) {
                [self.hud hide:NO];
                // 无效帖子
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"帖子异常，或已被删除。确定返回" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
                return;
            }
        }
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        [self.tv.header endRefreshing];
        return;
    }
    
    if (connection.tag == HTTP_REQUEST_TAG_IMAGE) {
        // 图片
        NSMutableArray *array = resultDic[@"data"];
        [self.imageArray removeAllObjects];
        [self.imageArray addObjectsFromArray:array];
        
//        [self requestCommentListData];
    } else if (connection.tag == HTTP_REQUEST_TAG_COMMENT) {
        // 回答列表
        NSMutableArray *array = resultDic[@"data"];
        [self.replyArray removeAllObjects];
        if (![array isKindOfClass:[NSNull class]]) {
            [self.replyArray addObjectsFromArray:array];
        }
        
        [self.tv reloadData];
//        [self requestDetailData];
    } else if (connection.tag == HTTP_REQUEST_TAG_WIN) {
        NSMutableArray *array = resultDic[@"data"];
        if (array != nil && [array isKindOfClass:[NSMutableArray class]]) {
            self.winCommentDict = [array firstObject];
        }
        
        // 中标回答
        [self.tv reloadData];
//        [self.tv scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    } else if (connection.tag == HTTP_REQUEST_TAG_INFO) {
        // 详情
        NSMutableDictionary *curDict = resultDic[@"data"];
        self.curPostDetailDict = curDict;
        [self.imageArray removeAllObjects];
        NSArray *images = curDict[@"post_images"];
        [self.imageArray addObjectsFromArray:images];
        
        NSString *post_role = self.curPostDetailDict[@"post_role"];
        if (post_role.intValue == Zb_Post_Role_Type_Owner) {
            [self requestWinData];
        }
        [self drawInfo];
        [self.tv reloadData];
        [self.tv scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    } else if (connection.tag == HTTP_REQUEST_TAG_USE) {
        // 采用回答
        self.hud.labelText = @"采用成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
            [self.tv.header beginRefreshing];
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
