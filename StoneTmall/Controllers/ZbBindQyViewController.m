//
//  ZbBindQyViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/17.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbBindQyViewController.h"
#import "ZbQiyeTableViewCell.h"
#import "ZbQyDetailViewController.h"
#import "ZbBindQySendOkViewController.h"
#import "ZbInputQyNameViewController.h"

#define HTTP_REQUEST_LOADDATA 0        // 下拉刷新数据
#define HTTP_REQUEST_LOADMORE 1        // 上拉加载更多数据
#define HTTP_REQUEST_BIND 2            // 绑定企业

@interface ZbBindQyViewController () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

// 导航栏titleView
@property (nonatomic, strong) IBOutlet UIView *navTitleView;
// 导航栏搜索栏
@property (nonatomic, strong) IBOutlet UITextField *tfSearch;
// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// header
@property (nonatomic, strong) IBOutlet UIView *tvHeaderView;

// 所有数据
@property (nonatomic, strong) NSMutableArray *dataArray;

// 是否搜索状态
@property (nonatomic, assign) BOOL isSearching;
// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;
// 当前选择绑定企业id
@property (nonatomic, strong) NSString *curCompanyId;

@end

@implementation ZbBindQyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    
    // 圆角
    self.navTitleView.layer.cornerRadius = CGRectGetHeight(self.navTitleView.frame) / 2;
    self.navTitleView.layer.masksToBounds = YES;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbQiyeTableViewCell" bundle:nil] forCellReuseIdentifier:QiyeCellIdentifier];
    
    //添加上拉加载更多
    __weak typeof(self) weakSelf = self;
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self textFieldChanged:self.tfSearch];
    [self.tfSearch becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 点击事件
// 取消按钮点击
- (IBAction)leftBarItemOnClick:(id)sender {
    self.isSearching = NO;
    [self.tfSearch resignFirstResponder];
    UIViewController *present = self.navigationController.presentingViewController;
    if (present != nil) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
    return;
}

// 创建企业按钮点击
- (IBAction)btnCreateOnClick:(id)sender {
    [self.tfSearch resignFirstResponder];
    
    ZbInputQyNameViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbInputQyNameViewController"];
    controller.preCompanyName = self.tfSearch.text;
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

#pragma mark - 接口调用
// 查询数据接口
- (void)requestSearchData:(NSString *)key {
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyList:key type:@"" area:@"" sortName:nil sortOrder:nil pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_LOADDATA;
    [conn start];
    return;
}

// 上拉加载更多数据列表接口
- (void)requestMoreSearchData:(NSString *)key {
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyList:key type:@"" area:@"" sortName:nil sortOrder:nil pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_LOADMORE;
    [conn start];
    return;
}

// 绑定企业
- (void)requestBindQy {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService applyForBindCompany:self.curCompanyId] delegate:self];
    conn.tag = HTTP_REQUEST_BIND;
    [conn start];
    return;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)textFieldChanged:(UITextField *)sender {
    if (sender.text.length <= 0) {
        // 清空
        self.isSearching = NO;
        self.tv.footer.hidden = YES;
        [self.dataArray removeAllObjects];
        [self.tv setTableHeaderView:nil];
        [self.tv reloadData];
    } else {
        // 搜索
        if (sender.markedTextRange != nil) {
            return;
        }
        self.isSearching = YES;
        [self requestSearchData:sender.text];
        [self.tv setTableHeaderView:self.tvHeaderView];
        [self.tv reloadData];
    }
    return;
}

#pragma mark - UITableViewDataSource
// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.isSearching) {
        return 1;
    }
    return self.dataArray.count;
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearching) {
        return 197;
    }
    return [ZbQiyeTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isSearching) {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NoCompanyCell"];
        UILabel *labDes1 = (UILabel *)[cell viewWithTag:1];
        UILabel *labDes2 = (UILabel *)[cell viewWithTag:2];
        UILabel *labDes3 = (UILabel *)[cell viewWithTag:3];
        labDes1.adjustsFontSizeToFitWidth = YES;
        labDes2.adjustsFontSizeToFitWidth = YES;
        labDes3.adjustsFontSizeToFitWidth = YES;
        return cell;
    }
    ZbQiyeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:QiyeCellIdentifier];
    NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
    [cell setInfoDict:curDict];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (!self.isSearching) {
        return;
    }
    [self.tfSearch resignFirstResponder];
    NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
    self.curCompanyId = curDict[@"company_id"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"您要绑定“%@”，升级为企业用户吗？", curDict[@"company_name"]] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"申请绑定", nil];
    [alert show];
}

#pragma mark - 上拉或下拉刷新
/**
 *  上拉刷新
 */
- (void)footerRefresh {
    [self requestMoreSearchData:self.tfSearch.text];
}

#pragma mark UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    
    [self requestBindQy];
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    // 用户已结束搜索，有些请求还在路上，不处理
    if (!self.isSearching) {
        return;
    }
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_LOADDATA) {
        // 加载失败
        if (result.integerValue != 0) {
            [self.tv reloadData];
            return;
        }
        
        NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
        // 下拉重刷
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:dataArray];
        if (dataArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
        if (self.dataArray.count > 0) {
            CGRect top = CGRectMake(0, 0, 1, 1);
            [self.tv scrollRectToVisible:top animated:YES];
        }
    } else if (connection.tag == HTTP_REQUEST_LOADMORE) {
        // 加载失败
        if (result.integerValue != 0) {
            [self.tv reloadData];
            return;
        }
        
        NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
        // 上拉加载更多
        [self.dataArray addObjectsFromArray:dataArray];
        if (dataArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_BIND) {
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        // 绑定成功
        ZbBindQySendOkViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbBindQySendOkViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
