//
//  ZbPickGoodsTypeViewController.m
//  ddLogisticsGuest
//
//  Created by 张斌 on 15/5/27.
//  Copyright (c) 2015年 qianxx. All rights reserved.
//

#import "ZbPickGoodsTypeViewController.h"
#import "CommonHeader.h"

@interface ZbPickGoodsTypeViewController ()

// 底部view
@property (nonatomic, strong) IBOutlet UIView *bottomView;
// 选择器
@property (nonatomic, strong) IBOutlet UIPickerView *picker1;
// 底部view的bottom约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcBottomViewBottom;

// 点击block
@property (nonatomic, copy) selectBlock curBlock;

@property (nonatomic, strong) NSArray *typeArray;   // 类型数组
@property (nonatomic, strong) NSDictionary *typeDict;         // 类型字典

@end

@implementation ZbPickGoodsTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 初始化时间数据
- (void)initDate {
}

#pragma 实例化方法
// 类方法，创建弹出二级选择框
+ (void)showInViewController:(UIViewController *)controller typeArray:(NSArray *)typeArray typeDict:(NSDictionary *)typeDict selectBlock:(selectBlock)block {
    
    UIStoryboard *pickSb = [UIStoryboard storyboardWithName:@"DropDown" bundle:nil];
    ZbPickGoodsTypeViewController *picker = [pickSb instantiateViewControllerWithIdentifier:@"ZbPickGoodsTypeViewController"];
    
    // 数据
    picker.typeArray = typeArray;
    picker.typeDict = typeDict;
    picker.curBlock = block;
    
    [controller addChildViewController:picker];
    [controller.view addSubview:picker.view];
    
    // 动画
    picker.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    picker.alcBottomViewBottom.constant = -182;
    
    [picker viewWillAppear:YES];
    [picker.view layoutIfNeeded];
    
    [UIView animateWithDuration:0.3 animations:^(void) {
        picker.view.backgroundColor = ColorWithRGBA(0, 0, 0, 0.5);
        picker.alcBottomViewBottom.constant = 0;
        [picker.view layoutIfNeeded];
    }];
}

// 关闭界面
- (void)dismiss:(BOOL)animated {
    if (!animated) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        return;
    }
    [UIView animateWithDuration:0.3 animations:^(void) {
        self.view.backgroundColor = [UIColor clearColor];
        self.alcBottomViewBottom.constant = -182;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
    }];
    return;
}

#pragma mark - 点击事件
// 背景点击
- (IBAction)btnBgOnClick:(id)sender {
    [self dismiss:YES];
    if (self.curBlock) {
        BOOL isCancle = YES;
        self.curBlock(isCancle, nil, nil);
    }
    return;
}

// 取消
- (IBAction)btnCancelOnClick:(id)sender {
    [self dismiss:YES];
    if (self.curBlock) {
        BOOL isCancle = YES;
        self.curBlock(isCancle, nil, nil);
    }
    return;
}

// 确定
- (IBAction)btnConfirmOnClick:(id)sender {
    [self dismiss:YES];
    if (self.curBlock) {
        
        BOOL isCancle = NO;
        // 类型
        NSInteger typeIndex = [self.picker1 selectedRowInComponent:0];
        NSDictionary *typeInfo = [self.typeArray objectAtIndex:typeIndex];
        
        // 二级
        NSString *classId = typeInfo[@"class_id"];
        NSArray *subArray = self.typeDict[classId];
        NSInteger subIndex = [self.picker1 selectedRowInComponent:1];
        NSDictionary *subDict = subArray[subIndex];
        
        self.curBlock(isCancle, typeInfo, subDict);
    }
}

#pragma mark - UIPickerView datasource && delegate
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return self.typeArray.count > 0 ? 2 : 0;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        
        // 类型列
        return self.typeArray.count;
    } else {
        
        // 单位列
        NSInteger typeIndex = [self.picker1 selectedRowInComponent:0];
        NSDictionary *typeInfo = [self.typeArray objectAtIndex:typeIndex];
        NSString *classId = typeInfo[@"class_id"];
        NSArray *unitArray = self.typeDict[classId];
        return unitArray.count;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 28;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        
        // 类型列
        NSDictionary *typeDict = [self.typeArray objectAtIndex:row];
        return typeDict[@"class_name"];
    } else {
        
        // 单位列
        NSInteger typeIndex = [self.picker1 selectedRowInComponent:0];
        NSDictionary *typeInfo = [self.typeArray objectAtIndex:typeIndex];
        NSString *classId = typeInfo[@"class_id"];
        NSArray *unitArray = self.typeDict[classId];
        if (row >= unitArray.count) {
            return nil;
        }
        NSDictionary *uniteDict = unitArray[row];
        return uniteDict[@"class_name"];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        [pickerView reloadComponent:1];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
