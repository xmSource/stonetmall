//
//  ZbGoodsPageController.h
//  StoneTmall
//
//  Created by chyo on 15/9/7.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"
#import "ZbMyFuncViewController.h"

@interface ZbGoodsPageController : BaseViewController

- (void)setControllerType:(ZbCxl_Type)ZbCxlType;


@end
