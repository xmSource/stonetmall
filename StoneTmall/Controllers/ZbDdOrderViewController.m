//
//  ZbDdOrderViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/11.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbDdOrderViewController.h"
#import "ZbSampleTableViewCell.h"

#define MAX_SHOW_HEIGHT_RATE (2/3.0)

@interface ZbDdOrderViewController () <UITableViewDataSource, UITableViewDelegate>

// 容器
@property (nonatomic, strong) IBOutlet UIView *container;
// 容器的bottom约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcContainerBottom;
// 容器的height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcContainerHeight;
// 汇总view
@property (nonatomic, strong) IBOutlet UIView *viewSummar;
// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// 遮罩层
@property (nonatomic, strong) IBOutlet UIControl *controlCover;
// 购买按钮
@property (nonatomic, strong) IBOutlet UIButton *btnBuy;
// 加入购物车按钮
@property (nonatomic, strong) IBOutlet UIButton *btnCart;
// 微聊按钮
@property (nonatomic, strong) IBOutlet UIButton *btnChat;
// 汇总label
@property (nonatomic, strong) IBOutlet UILabel *labSummar;

// 数据列表
@property (nonatomic, strong) NSMutableArray *array;

// 选中回调
@property (nonatomic, copy) btnClickBlock curBlock;

@end

@implementation ZbDdOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 圆角
    self.btnBuy.layer.cornerRadius = 3;
    self.btnCart.layer.cornerRadius = 3;
    self.btnChat.layer.cornerRadius = 3;
    
    // 汇总信息
    [self drawSummarInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//  类方法
+ (ZbDdOrderViewController *)showInViewController:(UIViewController *)controller stoneName:(NSString *)stoneName dataArray:(NSMutableArray *)array btnClickBlock:(btnClickBlock)block {
    UIStoryboard *pickSb = [UIStoryboard storyboardWithName:@"DropDown" bundle:nil];
    ZbDdOrderViewController *dropDown = [pickSb instantiateViewControllerWithIdentifier:@"ZbDdOrderViewController"];
    
    [controller addChildViewController:dropDown];
    [controller.view addSubview:dropDown.view];
    
    [dropDown viewWillAppear:YES];
    
    dropDown.array = array;
    dropDown.curBlock = block;
    for (NSMutableDictionary *curDict in array) {
        NSString *mainKey = curDict[@"mainKey"];
        NSDictionary *saveDict = [ZbSaveManager getSampleFromCart:mainKey];
        if (saveDict == nil) {
            continue;
        }
        NSNumber *saveCount = saveDict[@"count"];
        [curDict setValue:[NSNumber numberWithInteger:saveCount.integerValue] forKey:@"count"];
    }
    // 下拉框界面大小
    dropDown.view.frame = controller.view.bounds;
    // cell高度
    CGFloat cellHeight = 54;
    // 总高度
    CGFloat height = 45 + cellHeight * (array.count) + CGRectGetHeight(dropDown.viewSummar.frame);
    // 可显示高度
    CGFloat showHeight = MIN(height, CGRectGetHeight(controller.view.bounds) * MAX_SHOW_HEIGHT_RATE);
    dropDown.alcContainerBottom.constant = -showHeight;
    dropDown.alcContainerHeight.constant = showHeight;
    dropDown.controlCover.alpha = 0;
    [dropDown.view layoutIfNeeded];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.3 animations:^(void) {
        dropDown.alcContainerBottom.constant = 0;
        dropDown.controlCover.alpha = 1;
        [dropDown.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    return dropDown;
}

#pragma mark - 界面绘制
// 绘制汇总信息
- (void)drawSummarInfo {
    CGFloat sumMoney = 0;
    NSInteger sumCount = 0;
    // 遍历样品
    for (NSDictionary *curProductDict in self.array) {
        // 价格
        NSNumber *price = curProductDict[@"spec_price"];
        // 数量
        NSNumber *count = curProductDict[@"count"];
        if (count == nil) {
            continue;
        }
        // 计算
        sumMoney += price.floatValue * count.integerValue;
        sumCount += count.integerValue;
    }
    self.labSummar.text = [NSString stringWithFormat:@"总数量：%@，总金额：%@元", [NSNumber numberWithFloat:sumCount], [NSNumber numberWithFloat:(sumMoney / 100)]];
    return;
}

// 关闭界面
- (void)dismiss:(BOOL)animated {
    if (!animated) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        return;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.alcContainerBottom.constant = - CGRectGetHeight(self.container.frame);
        self.controlCover.alpha = 0;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    return;
}

#pragma mark - 点击事件
// 遮罩层点击
- (IBAction)btnCoverOnClick:(id)sender {
    self.curBlock(Zb_Dd_Order_Click_Cancel, nil);
    [self dismiss:YES];
    return;
}

// 购物按钮点击
- (IBAction)btnBuyOnClick:(id)sender {
    CGFloat sumMoney = 0;
    NSInteger sumCount = 0;
    
    NSMutableArray *hasCountArray = [NSMutableArray array];
    // 遍历样品
    for (NSDictionary *curProductDict in self.array) {
        // 价格
        NSNumber *price = curProductDict[@"spec_price"];
        // 数量
        NSNumber *count = curProductDict[@"count"];
        if (count == nil || count.integerValue <= 0) {
            continue;
        }
        // 计算
        sumMoney += price.floatValue * count.integerValue;
        sumCount += count.integerValue;
        [hasCountArray addObject:curProductDict];
    }
    if (sumCount <= 0) {
        self.hud.labelText = @"请选择规格";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    self.curBlock(Zb_Dd_Order_Click_Buy, hasCountArray);
    [self dismiss:YES];
    return;
}

// 加入购物车按钮点击
- (IBAction)btnCartOnClick:(id)sender {
    CGFloat sumMoney = 0;
    NSInteger sumCount = 0;
    // 遍历样品
    for (NSDictionary *curProductDict in self.array) {
        // 价格
        NSNumber *price = curProductDict[@"spec_price"];
        // 数量
        NSNumber *count = curProductDict[@"count"];
        if (count == nil) {
            continue;
        }
        // 计算
        sumMoney += price.floatValue * count.integerValue;
        sumCount += count.integerValue;
    }
    if (sumCount <= 0) {
        self.hud.labelText = @"请选择规格";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    // 保存购物车
    [ZbSaveManager addSamepleToCart:self.array];
    self.curBlock(Zb_Dd_Order_Click_Cart, self.array);
    [self dismiss:YES];
    return;
}

// 微聊按钮点击
- (IBAction)btnChatOnClick:(id)sender {
    self.curBlock(Zb_Dd_Order_Click_Chat, nil);
    [self dismiss:YES];
    return;
}

#pragma mark - UITableViewDataSource
// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count + 1;
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 45;
    } else {
        return 54;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"firstCell" forIndexPath:indexPath];
        return cell;
    } else {
        ZbSampleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sampleCell" forIndexPath:indexPath];
        // 规格名，价格，库存
        NSInteger index = indexPath.row - 1;
        NSMutableDictionary *curDict = [self.array objectAtIndex:index];
        cell.labSize.text = curDict[@"spec_name"];
        NSNumber *stockCount = curDict[@"spec_amount"];
        cell.labStockCount.text = [NSString stringWithFormat:@"库存%@件", stockCount];
        float price = [curDict[@"spec_price"] floatValue] / 100;
        cell.labPrice.text = [NSString stringWithFormat:@"%.2f元", price];
        // 数量
        NSNumber *count = curDict[@"count"];
        cell.labCount.text = count != nil ? [NSString stringWithFormat:@"%@", count] : @"0";
        // 增加
        cell.addBlock = ^(void) {
            NSInteger tagCount = count != nil ? (count.integerValue + 1) : 1;
            tagCount = MIN(tagCount, stockCount.integerValue);
            [curDict setObject:[NSNumber numberWithInteger:tagCount] forKey:@"count"];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self drawSummarInfo];
        };
        // 减少
        cell.cutBlock = ^(void) {
            if (count == nil) {
                return;
            }
            if (count.integerValue <= 0) {
                return;
            }
            NSNumber *tagCount = [NSNumber numberWithInteger:(count.integerValue - 1)];
            [curDict setObject:tagCount forKey:@"count"];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self drawSummarInfo];
        };
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
