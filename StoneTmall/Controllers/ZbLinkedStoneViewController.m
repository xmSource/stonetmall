//
//  ZbLinkedStoneViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/10/9.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbLinkedStoneViewController.h"
#import "BATableView.h"
#import "ZbSearchStoneTableViewCell.h"
#import "ZbIndexCharTableViewCell.h"
#import "ZbCompanyAddStoneViewController.h"

#define HTTP_REQUEST_STONE 0        // 主营石材
#define HTTP_REQUEST_LIMIT 1        // 关联石材数量限制
#define HTTP_REQUEST_DELETE 2       // 删除关联石材

@interface ZbLinkedStoneViewController () <BATableViewDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// 显示已关联数据
@property (nonatomic, strong) NSMutableArray *showArray;
// 原始已关联数据
@property (nonatomic, strong) NSMutableArray *rawArray;
// BATableView
@property (nonatomic, strong) BATableView *baTv;

// 关联石材数量限制
@property (nonatomic, assign) NSInteger addStoneLimit;

@end

@implementation ZbLinkedStoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.rawArray = [NSMutableArray array];
    self.showArray = [NSMutableArray array];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSearchStoneTableViewCell" bundle:nil] forCellReuseIdentifier:SearchStoneCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbIndexCharTableViewCell" bundle:nil] forCellReuseIdentifier:IndexCharCellIdentifier];
    
    //添加下拉刷新
    __weak typeof(self) weakSelf = self;
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self createTableView];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    [self requestStoneListData];
}

#pragma mark - 界面绘制
// 创建BATableView
- (void) createTableView {
    CGRect bounds = self.view.bounds;
    self.baTv = [[BATableView alloc] initWithTableView:self.tv frame:bounds];
    self.baTv.delegate = self;
}

#pragma mark - 点击事件
- (IBAction)btnAddOnClick:(id)sender {
    if (self.rawArray.count >= self.addStoneLimit) {
        self.hud.labelText = @"已达关联石材数量上限";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    // 剩余可添加数量
    NSInteger left = self.addStoneLimit - self.rawArray.count;
    // 已关联数据
    NSMutableArray *stones = [NSMutableArray array];
    for (NSDictionary *stoneDict in self.rawArray) {
        NSString *name = stoneDict[@"stone_name"];
        [stones addObject:name];
    }
    
    ZbCompanyAddStoneViewController *addStone = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbCompanyAddStoneViewController"];
    addStone.canAddCount = left;
    addStone.linkedArray = stones;
    // 添加完成block
    addStone.linkOkClickBlock = ^(void) {
        [self.tv.header beginRefreshing];
        [self.tv.header drawRect:CGRectZero];
    };
    [self.navigationController pushViewController:addStone animated:YES];
    return;
}

#pragma mark - 接口调用
// 获取主营石材接口
- (void)requestStoneListData {
    ZbQyInfo *curinfo = [ZbWebService sharedQy];
    NSString *companyId = curinfo.company_id;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyStoneList:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_STONE;
    [conn start];
    return;
}

// 获取关联石材数量限制
- (void)loadAddStoneLimit {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getAddStoneLimit] delegate:self];
    conn.tag = HTTP_REQUEST_LIMIT;
    [conn start];
}

// 删除关联石材
- (void)requestDeleteStone:(NSIndexPath *)indexPath {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    NSArray *sectionArray = self.showArray[indexPath.section][@"data"];
    NSDictionary *curStoneDict = [sectionArray objectAtIndex:indexPath.row];
    NSString *stoneName = curStoneDict[@"stone_name"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService delCompanyStone:stoneName] delegate:self];
    conn.tag = HTTP_REQUEST_DELETE;
    conn.userInfo = indexPath;
    [conn start];
}

#pragma mark - UITableViewDataSource
- (NSArray *) sectionIndexTitlesForABELTableView:(BATableView *)tableView {
    NSMutableArray * indexTitles = [NSMutableArray array];
    for (NSDictionary * sectionDictionary in self.showArray) {
        [indexTitles addObject:sectionDictionary[@"indexTitle"]];
    }
    return indexTitles;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.showArray.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionArray = self.showArray[section][@"data"];
    return sectionArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbSearchStoneTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZbSearchStoneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SearchStoneCellIdentifier];
    
    NSArray *sectionArray = self.showArray[indexPath.section][@"data"];
    NSDictionary *curStoneDict = [sectionArray objectAtIndex:indexPath.row];
    // 绘制
    [cell setInfoDict:curStoneDict];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [ZbIndexCharTableViewCell getRowHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZbIndexCharTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:IndexCharCellIdentifier];
    cell.labChar.text = self.showArray[section][@"indexTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self requestDeleteStone:indexPath];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - 上拉或下拉刷新
/**
 *  上拉刷新
 */
- (void)headerRefresh {
    // 获取详情数据
    [self requestStoneListData];
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    if (connection.tag == HTTP_REQUEST_STONE) {
        NSMutableArray *stoneArray = resultDic[@"data"];
        self.rawArray = stoneArray;
        [stoneArray enumerateObjectsUsingBlock:^(NSMutableDictionary *stoneDict, NSUInteger index, BOOL *stop) {
            NSString *name = stoneDict[@"stone_name"];
            NSString * firstPinyinLetter = [[NSString stringWithFormat:@"%c", pinyinFirstLetter([name characterAtIndex:0])] uppercaseString];
            [stoneDict setValue:firstPinyinLetter forKey:@"description"];
        }];
        NSMutableArray *tagArray = [ZbWebService sortByFirstCharact:stoneArray];
        [self.showArray removeAllObjects];
        [self.showArray addObjectsFromArray:tagArray];
        [self.baTv reloadData];
        [self loadAddStoneLimit];
    } else if (connection.tag == HTTP_REQUEST_LIMIT) {
        [self.hud hide:NO];
        
        NSNumber *limit = resultDic[@"data"][@"limit"];
        self.addStoneLimit = limit.integerValue;
    } else if (connection.tag == HTTP_REQUEST_DELETE) {
        // 删除关联石材
        self.hud.labelText = @"删除成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
        
        // 删除数据
        NSIndexPath *delIndexPath = connection.userInfo;
        NSArray *sectionArray = self.showArray[delIndexPath.section][@"data"];
        NSDictionary *curStoneDict = [sectionArray objectAtIndex:delIndexPath.row];
        [self.rawArray removeObject:curStoneDict];
        
        // 重新按字母检索
        NSMutableArray *tagArray = [ZbWebService sortByFirstCharact:self.rawArray];
        [self.showArray removeAllObjects];
        [self.showArray addObjectsFromArray:tagArray];
        [self.baTv reloadData];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
