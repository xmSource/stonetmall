//
//  ZbNewQAdrViewController.h
//  StoneTmall
//
//  Created by chyo on 15/9/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, Zb_Controller_Type) {
    Zb_Controller_MAINEDIT = 0,   //主地址修改
    Zb_Controller_ADDADR,         //新增副地址
    Zb_Controller_ADREDIT,       //副地址修改
} ;

@interface ZbNewQAdrViewController : BaseViewController

@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *prov;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSDictionary *dict;

- (void)setControllerType:(Zb_Controller_Type)zbControllerType;

@end
