//
//  ZbDropDownViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/30.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

// 下拉框类型
typedef enum {
    Zb_DropDown_Type_Text,          // 文字
    Zb_DropDown_Type_Color,         // 颜色
    Zb_DropDown_Type_Pic,           // 图文
    Zb_DropDown_Type_Sort,          // 排序
} Zb_DropDown_Type;

// 选中block
typedef void (^selectBlock)(NSInteger, NSString *, BOOL);

@interface ZbDropDownViewController : BaseViewController

// 当前下拉框类型
@property (nonatomic, assign) Zb_DropDown_Type curType;

// 关闭界面
- (void)dismiss:(BOOL)animated;

// 显示选择器
+ (ZbDropDownViewController *)showInViewController:(UIViewController *)controller startY:(CGFloat)startY dataArray:(NSArray *)array type:(Zb_DropDown_Type)type selectIndex:(NSInteger)index selectBlock:(selectBlock)block isNeedAll:(BOOL)isNeed;

@end
