//
//  ZbCompanyAddStoneViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/10/11.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbCompanyAddStoneViewController : BaseViewController

// 剩余可关联石材数量
@property (nonatomic, assign) NSInteger canAddCount;
//
@property (nonatomic, strong) NSArray *linkedArray;
// 完成点击block
@property (nonatomic, copy) void (^linkOkClickBlock)(void);

@end
