//
//  ZbSelectPayWayViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSelectPayWayViewController.h"

@interface ZbSelectPayWayViewController ()

@property (nonatomic, strong) IBOutlet UIView *container;

// 点击block
@property (nonatomic, copy) payWayClickBlock curBlock;

@end

@implementation ZbSelectPayWayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 圆角
    self.container.layer.cornerRadius = 5;
    
    [self.container.layer removeAllAnimations];
    self.view.backgroundColor = [UIColor clearColor];
    [UIView animateWithDuration:0.2 animations:^{
        self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    } completion:^(BOOL finished){
    }];
    
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.02f, 1.02f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.97f, 0.97f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.container.layer addAnimation:popAnimation forKey:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 关闭界面
- (void)dismiss:(BOOL)animated {
    if (!animated) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        return;
    }
    [UIView animateWithDuration:0.3 animations:^(void) {
        self.view.backgroundColor = [UIColor clearColor];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
    }];
    return;
}

#pragma 实例化方法
// 类方法，创建支付方式选择框
+ (void)showInViewController:(UIViewController *)controller block:(payWayClickBlock)block {
    
    UIStoryboard *pickSb = [UIStoryboard storyboardWithName:@"DropDown" bundle:nil];
    ZbSelectPayWayViewController *picker = [pickSb instantiateViewControllerWithIdentifier:@"ZbSelectPayWayViewController"];
    
    // 数据
    picker.curBlock = block;
    
    [controller addChildViewController:picker];
    [controller.view addSubview:picker.view];
    
    [picker viewWillAppear:YES];
}

#pragma mark - 点击事件
// 背景点击
- (IBAction)btnBgOnClick:(id)sender {
    [self dismiss:YES];
    return;
}

// 支付宝按钮点击
- (IBAction)btnZfbOnClick:(id)sender {
    self.curBlock(Zb_Pay_Way_Zfb);
    [self dismiss:YES];
    return;
}

// 微信按钮点击
- (IBAction)btnWechatOnClick:(id)sender {
    self.curBlock(Zb_Pay_Way_Wechat);
    [self dismiss:YES];
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
