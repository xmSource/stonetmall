//
//  ZbLinkStoneViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/24.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbLinkStoneViewController : BaseViewController

// 完成点击block
@property (nonatomic, copy) void (^linkOkClickBlock)(NSString *link);

// 公司主营石材
@property (nonatomic, weak) NSMutableArray *StoneArray;

@end
