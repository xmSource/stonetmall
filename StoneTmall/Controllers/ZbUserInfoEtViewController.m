//
//  ZbUserInfoEtViewController.m
//  StoneTmall
//
//  Created by chyo on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbUserInfoEtViewController.h"
#import "ZbUserNameEtViewController.h"

#define HEAD_EDIT 1
#define SEXY_EDIT 2
#define SEXY_EDIT_M 3
#define SEXY_EDIT_F 4
#define LOAD_DATA  5

@interface ZbUserInfoEtViewController ()<UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) IBOutlet UIImageView *headImg;
@property (nonatomic, strong) IBOutlet UILabel *nicknameLab;
@property (nonatomic, strong) IBOutlet UILabel *sexyLab;

@end

@implementation ZbUserInfoEtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headImg.layer.cornerRadius = 27.5f;
    self.headImg.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSDictionary *dict = [ZbWebService sharedUser];
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:dict[@"user_image"]]]];
    self.nicknameLab.text = dict[@"user_nickname"];
    if ([dict[@"user_sex"] isEqualToString:@"0"]) {
        self.sexyLab.text = @"";
    } else if ([dict[@"user_sex"] isEqualToString:@"1"]) {
        self.sexyLab.text = @"男";
    } else if ([dict[@"user_sex"] isEqualToString:@"2"]){
        self.sexyLab.text = @"女";
    }
    [self loadData];
}

- (void)loadData {
    NSDictionary *dic = [ZbWebService sharedUser];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getUserInfoParams:dic[@"user_name"]] delegate:self];
    conn.tag = LOAD_DATA;
    [conn start];
}

- (void)uploadImage:(UIImage *)image {
    
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    NSDictionary *params = [ZbWebService editMyLogoParams];
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
            // 图片压缩
        NSData* imageData = UIImageJPEGRepresentation(image, 0.4);
        NSString *fileName = [NSString stringWithFormat:@"sg_image"];
    
        // 图片文件流
        [formData appendPartWithFileData:imageData name:fileName fileName:fileName mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"%@", responseObject);
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = @"修改失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        self.hud.labelText = @"修改成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
            NSDictionary *dict = [ZbWebService sharedUser];
            NSString *imageUrl = [ZbWebService getImageFullUrl:dict[@"user_image"]];
            [[SDWebImageManager sharedManager].imageCache removeImageForKey:imageUrl];
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"修改失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
    return;
}

#pragma mark - 修改性别
- (void)changeSex:(NSString *)sex withTag:(int)tag {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editMySex:sex] delegate:self];
    conn.tag = tag;
    [conn start];
}

#pragma mark - 从相册选择图片和拍照
// 从相册选择图片
- (void)localPhoto {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.delegate = self;
        //        controller.allowsEditing = YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view.window.rootViewController presentViewController:controller animated:YES completion:nil];
        });
    }
}
// 拍照
- (void)takePhoto {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
//        controller.allowsEditing = YES;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view.window.rootViewController presentViewController:controller animated:YES completion:nil];
        });
    }
}

#pragma mark - 点击修改个人资料
- (IBAction)headClick:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从相册中选择", @"拍照", nil];
    sheet.tag = HEAD_EDIT;
    [sheet showInView:self.view];
}

- (IBAction)nicknameClick:(id)sender {
    ZbUserNameEtViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbUserNameEtViewController"];
    NSDictionary *dict = [ZbWebService sharedUser];
    controller.name = dict[@"user_nickname"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)sexyClick:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"男", @"女", nil];
    sheet.tag = SEXY_EDIT;
    [sheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // 设置用户头像
    if (actionSheet.tag == HEAD_EDIT) {
        switch (buttonIndex) {
            case 0:
                [self localPhoto];
                break;
                
            case 1:
                [self takePhoto];
                break;
                
            case 2:
                return;
                break;
                
            default:
                break;
        }
      // 设置用户性别
    } else if (actionSheet.tag == SEXY_EDIT) {
        switch (buttonIndex) {
            case 0:
                [self changeSex:@"1" withTag:SEXY_EDIT_M];
                break;
                
            case 1:
                [self changeSex:@"2" withTag:SEXY_EDIT_F];
                break;
                
            case 2:
                return;
                break;

            default:
                break;
        }
    }
}

#pragma mark - UIImagePickControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        CGSize imgSize = portraitImg.size;
        CGSize tagSize = imgSize;
        CGFloat rate = imgSize.width / imgSize.height;
        if (imgSize.width >= imgSize.height) {
            if (imgSize.width > 1000) {
                tagSize = CGSizeMake(1000, 1000 / rate);
            }
        } else {
            if (imgSize.height > 1000) {
                tagSize = CGSizeMake(1000 * rate, 1000);
            }
        }
        // 去除图片旋转属性
        UIGraphicsBeginImageContext(tagSize);
        [portraitImg drawInRect:CGRectMake(0, 0, tagSize.width, tagSize.height)];
        portraitImg = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.headImg.image = portraitImg;
        [self uploadImage:portraitImg];
    }];
    return;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    return;
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    if (connection.tag == SEXY_EDIT_M) {
        if (result.integerValue == 0) {
            self.sexyLab.text = @"男";
        }
    } else if (connection.tag == SEXY_EDIT_F) {
        if (result.integerValue == 0) {
            self.sexyLab.text = @"女";
        }
    } else if (connection.tag == LOAD_DATA) {
        if (result.integerValue != 0) {
            // 加载失败
            return;
        }
        NSMutableDictionary *userDict = resultDic[@"data"];
        [ZbWebService setUser:userDict];
        self.nicknameLab.text = userDict[@"user_nickname"];
    }
}



@end
