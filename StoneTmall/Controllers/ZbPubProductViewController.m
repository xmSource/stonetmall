//
//  ZbPubProductViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/1.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbPubProductViewController.h"
#import "ZbLinkStoneViewController.h"
#import "ZbQyInfo.h"
#import "AFNetworking.h"
#import "ZbTypeSelectViewController.h"
#import "IHKeyboardAvoiding.h"
#import "ZbPriceUnitViewController.h"

#define HTTP_REQUEST_TAG_TYPE 1                    // 产品类别列表
#define HTTP_REQUEST_TAG_ADD_IMAGE_LIMIT 2         // vip可增加图片限制
#define HTTP_REQUEST_TAG_EDIT_RECOM 3              // 修改推荐
#define HTTP_REQUEST_TAG_EDIT_TAG 4                // 修改产品标签
#define HTTP_REQUEST_TAG_EDIT_PRICE 5              // 修改产品参考价

@interface ZbPubProductViewController () <UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

// 名称背景
@property (nonatomic, strong) IBOutlet UIView *nameBg;
// 名称输入框
@property (nonatomic, strong) IBOutlet UITextField *tfName;
// 推荐按钮
@property (nonatomic, strong) IBOutlet UIButton *btnRecommand;
// 推荐按钮高度
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcBtnRecommandHeight;
// 推荐按钮top
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcBtnRecommandTop;
// 分类背景
@property (nonatomic, strong) IBOutlet UIView *typeBg;
// 分类输入框
@property (nonatomic, strong) IBOutlet UITextField *tfType;
// 参考价背景
@property (nonatomic, strong) IBOutlet UIView *priceBg;
// 参考价输入框
@property (nonatomic, strong) IBOutlet UITextField *tfPrice;
// 关联石材背景
@property (nonatomic, strong) IBOutlet UIView *linkBg;
// 关联石材输入框
@property (nonatomic, strong) IBOutlet UITextField *tfLink;
// 描述输入框
@property (nonatomic, strong) IBOutlet UITextView *tvDes;
// 描述输入框高度约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvDesHeight;
// 描述提示文字
@property (nonatomic, strong) IBOutlet UILabel *labDesPlaceHolder;
// 上传图片描述
@property (nonatomic, strong) IBOutlet UILabel *labUploadDes;
// 发布按钮
@property (nonatomic, strong) IBOutlet UIButton *btnPublic;

// 选择图片按钮数组
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *imgPickControlArray;
// 选择主图片勾选按钮数组
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *imgMainPickBtnArray;
// 图片数组数组
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *ivImageArray;

@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *pickBtnArray;

// 当前选中的主图片
@property (nonatomic, assign) NSInteger curMainImgIndex;
// 已选择图片列表
@property (nonatomic, strong) NSMutableArray *imageArray;
// 选中产品一级分类
@property (nonatomic, assign) NSInteger curTypeMain;
// 一级分类文字
@property (nonatomic, strong) NSString *curTypeMainText;
// 选中产品二级分类
@property (nonatomic, assign) NSInteger curTypeSub;
// 二级分类文字
@property (nonatomic, strong) NSString *curTypeSubText;
// 价格
@property (nonatomic, strong) NSString *curPrice;
// 单位
@property (nonatomic, strong) NSString *curPriceUnit;

// 可添加图片数量
@property (nonatomic, assign) NSInteger canAddImageCount;

// 店长推荐剩余次数
@property (nonatomic, assign) NSInteger curRecommandLeft;

@end

@implementation ZbPubProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageArray = [NSMutableArray array];
    self.canAddImageCount = 1;
    
    // 边框和圆角
    self.nameBg.layer.borderWidth = 1;
    self.nameBg.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.nameBg.layer.cornerRadius = 3;
    self.nameBg.layer.masksToBounds = YES;
    self.typeBg.layer.borderWidth = 1;
    self.typeBg.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.typeBg.layer.cornerRadius = 3;
    self.typeBg.layer.masksToBounds = YES;
    self.priceBg.layer.borderWidth = 1;
    self.priceBg.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.priceBg.layer.cornerRadius = 3;
    self.priceBg.layer.masksToBounds = YES;
    self.linkBg.layer.borderWidth = 1;
    self.linkBg.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.linkBg.layer.cornerRadius = 3;
    self.linkBg.layer.masksToBounds = YES;
    self.tvDes.layer.borderWidth = 1;
    self.tvDes.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.tvDes.layer.cornerRadius = 3;
    self.tvDes.layer.masksToBounds = YES;
    self.btnPublic.layer.cornerRadius = 3;
    self.btnPublic.layer.masksToBounds = YES;
    
    // 图片模式
    for (UIImageView *ivImage in self.ivImageArray) {
        ivImage.clipsToBounds = YES;
    }
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 适配iphone4s
    if (!Greater_Than_480_Height) {
        self.alcTvDesHeight.constant = 40;
    }
    
    if (self.curEditDict != nil) {
        self.title = @"编辑产品";
        self.tfName.text = self.curEditDict[@"product_title"];
        [self.curEditDict[@"product_recommend"] isEqualToString:@"1"] ? (self.btnRecommand.selected = YES) : (self.btnRecommand.selected = NO);
        self.tfLink.text = self.curEditDict[@"product_tag"];
        
        // 参考价
        NSString *price = self.curEditDict[@"product_price"];
        NSString *priceUnit = self.curEditDict[@"product_price_unit"];
        if ([priceUnit isKindOfClass:[NSNull class]] || priceUnit.length <= 0) {
            if (price.integerValue <= 0) {
                // 未选价格
            } else {
                // 兼容无价格单位的旧数据
                self.curPrice = [NSString stringWithFormat:
                                 @"%@", [NSNumber numberWithInteger:[price integerValue] / 100]];
                self.curPriceUnit = @"吨";
                self.tfPrice.text = [NSString stringWithFormat:@"%@元/%@", self.curPrice, self.curPriceUnit];
            }
        } else {
            self.curPrice = [NSString stringWithFormat:
                             @"%@", [NSNumber numberWithInteger:[price integerValue] / 100]];
            self.curPriceUnit = priceUnit;
            self.tfPrice.text = [NSString stringWithFormat:@"%@元/%@", self.curPrice, self.curPriceUnit];
        }
            
        self.tvDes.text = self.curEditDict[@"product_description"];
        self.labDesPlaceHolder.hidden = self.tvDes.text.length > 0;
        self.tfType.text = [NSString stringWithFormat:@"%@(%@)", self.curEditDict[@"product_ptype"], self.curEditDict[@"product_type"]];
        self.curTypeMainText = self.curEditDict[@"product_ptype"];
        self.curTypeSubText = self.curEditDict[@"product_type"];
    }
    
    if ([ZbSaveManager isCtrlHide]) {
        self.btnRecommand.hidden = YES;
        self.alcBtnRecommandHeight.constant = 0;
        self.alcBtnRecommandTop.constant = 0;
    }
    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    // 加载权限数据
    [self requestAddProductImageLimit];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self drawInfo];
    if (self.curEditDict == nil) {
        [self drawPickImage];
    } else {
        [self drawImage];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IHKeyboardAvoiding removeAll];
}

#pragma mark - 界面绘制
- (void)drawInfo {
    BOOL isVip = [ZbWebService isVipQy];
    // 店长推荐
    self.btnRecommand.enabled = isVip;
    // 上传图片描述文字
    if (isVip) {
        self.labUploadDes.text = @"上传产品图片（VIP专享上传多张图片）";
    } else {
        self.labUploadDes.text = @"上传产品图片（非VIP只能上传一张产品图）";
    }
    if ([ZbSaveManager isCtrlHide]) {
        self.labUploadDes.text = @"上传产品图片";
    }
    return;
}

// 选择图片信息绘制
- (void)drawPickImage {
    BOOL isVip = [ZbWebService isVipQy];
    NSInteger maxImageCount = isVip? 3 : 1;
    NSInteger showCount = MIN(MAX(1, self.imageArray.count + 1), maxImageCount);
    for (NSInteger i = 0; i < self.imgPickControlArray.count; i++) {
        // 显隐
        UIView *imgPickControl = [self.imgPickControlArray objectAtIndex:i];
        imgPickControl.hidden = i >= showCount;
        
        // 图片
        UIImageView *ivImage = [self.ivImageArray objectAtIndex:i];
        ivImage.image = i < self.imageArray.count ? [self.imageArray objectAtIndex:i] : [UIImage imageNamed:@"public_pic_add"];
        // 勾选
        UIButton *btnSelect = [self.imgMainPickBtnArray objectAtIndex:i];
        btnSelect.selected = i == self.curMainImgIndex;
        btnSelect.hidden = i < self.imageArray.count ? NO : YES;
    }
    return;
}

- (void)drawImage {
    for (NSInteger i = 0; i < self.imgPickControlArray.count; i ++) {
        UIView *imgPickControl = [self.imgPickControlArray objectAtIndex:i];
        imgPickControl.hidden = YES;
        UIImageView *ivImage = [self.ivImageArray objectAtIndex:i];
        ivImage.hidden = YES;
    }
    
    for (NSInteger i = 0; i < self.stoneImgArray.count; i++) {
        // 显隐
        UIView *imgPickControl = [self.imgPickControlArray objectAtIndex:i];
        imgPickControl.hidden = NO;
        // 图片
        UIImageView *ivImage = [self.ivImageArray objectAtIndex:i];
        ivImage.hidden = NO;
        NSString *imgUrl = [self.stoneImgArray objectAtIndex:i];
        [ivImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]]];
    }
    
    for (UIButton *btn in self.imgMainPickBtnArray) {
        btn.hidden = YES;
    }
    
    for (UIButton *btn in self.pickBtnArray) {
        btn.enabled = NO;
    }
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

// 推荐按钮点击
- (IBAction)btnRecommandOnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    sender.selected = !sender.selected;
    return;
}

// 分类按钮点击
- (IBAction)btnTypeOnClick:(id)sender {
    NSLog(@"btnTypeOnClick()");
    [self.view endEditing:YES];
    if (self.hud.showing) {
        return;
    }
    
    NSArray *typeArray = [[ZbSaveManager shareManager] getProductTypeArray];
    if (typeArray != nil) {
        // 类别数据已加载过
        NSDictionary *subTypeDict = [[ZbSaveManager shareManager] getProductSubTypeDict];
        ZbTypeSelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTypeSelectViewController"];
        controller.typeArray = typeArray;
        controller.subTypeDict = subTypeDict;
        controller.lastParentIndex = self.curTypeMain;
        controller.lastSubIndex = self.curTypeSub;
        controller.selectClickBlock = ^(NSString *typeMain, NSString *typeSub, NSInteger mainIndex, NSInteger subIndex) {
            self.curTypeMain = mainIndex;
            self.curTypeSub = subIndex;
            self.curTypeMainText = typeMain;
            self.curTypeSubText = typeSub;
            NSString *type = [NSString stringWithFormat:@"%@(%@)", typeMain, typeSub];
            self.tfType.text = type;
        };
        [self.navigationController pushViewController:controller animated:YES];
        return;
    }
    
    // 加载分类数据
    [self requestTypeData];
    return;
}

// 关联按钮点击
- (IBAction)btnLinkOnClick:(id)sender {
    [self.view endEditing:YES];
    if (self.hud.showing) {
        return;
    }
    
    ZbLinkStoneViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbLinkStoneViewController"];
    [self.navigationController pushViewController:controller animated:YES];
    controller.linkOkClickBlock = ^(NSString *linkStones) {
        self.tfLink.text = linkStones;
    };
    return;
}

// 参考价点击
- (IBAction)btnPriceSelectOnClick:(id)sender {
    NSLog(@"btnPriceSelectOnClick()");
    [self.view endEditing:YES];
    if (self.hud.showing) {
        return;
    }
    
    ZbPriceUnitViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbPriceUnitViewController"];
    [self.navigationController pushViewController:controller animated:YES];
    controller.selectOkBlock = ^(NSString *price, NSString *priceUnit) {
        NSString *showText = [NSString stringWithFormat:@"%@元/%@", price, priceUnit];
        self.tfPrice.text = showText;
        self.curPrice = price;
        self.curPriceUnit = priceUnit;
    };
    return;
}

// 选择图片点击
- (IBAction)controlPickImageOnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    
    NSInteger index = sender.tag;
    if (index < self.imageArray.count) {
        return;
    }
    
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册选择", nil];
    action.tag = index;
    [action showInView:self.navigationController.view];
    return;
}

// 选择主图片点击
- (IBAction)btnPickMainImageOnClick:(UIButton *)sender {
    NSInteger index = sender.tag;
    if (index == self.curMainImgIndex) {
        return;
    }
    self.curMainImgIndex = index;
    [self drawPickImage];
    return;
}

// 发布按钮点击
- (IBAction)btnPublicOnClick:(id)sender {
    [self.view endEditing:YES];
    
    NSString *name = [self.tfName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (name.length < 4) {
        self.hud.labelText = @"名称字数不足";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (name.length > 25) {
        self.hud.labelText = @"名称字数过长";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    NSString *des = [self.tvDes.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (des.length < 10) {
        self.hud.labelText = @"描述字数不足";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (des.length > 700) {
        self.hud.labelText = @"描述字数过长";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (self.tfType.text.length <= 0) {
        self.hud.labelText = @"请选择分类";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (self.curEditDict == nil) {
        if (self.imageArray.count <= 0) {
            self.hud.labelText = @"请选择图片";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        [self requestAddProduct];
    } else {
        [self requestEditProduct];
    }
}

#pragma mark - 接口调用
// 获取发布产品图片限制
- (void)requestAddProductImageLimit {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getVipLimits] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_ADD_IMAGE_LIMIT;
    [conn start];
}

// 获取产品类别列表
- (void)requestTypeData {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getProductTypeList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_TYPE;
    [conn start];
}

// 发布
- (void)requestAddProduct {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *name = [self.tfName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *des = [self.tvDes.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *select = self.btnRecommand.selected ? @"1" : @"0";
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.securityPolicy setAllowInvalidCertificates:YES];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    NSDictionary *params = [ZbWebService addProduct:name description:des price:self.curPrice amount:@"0" type:self.curTypeMainText subtype:self.curTypeSubText tag:self.tfLink.text recommend:select priceUnit:self.curPriceUnit];
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        // 图片
        for (NSInteger i = 0; i < self.imageArray.count; i++) {
            UIImage *image = [self.imageArray objectAtIndex:i];
            // 图片压缩
            NSData* imageData = UIImageJPEGRepresentation(image, 0.6);
            NSString *fileName = i == self.curMainImgIndex ? @"sg_image" : [NSString stringWithFormat:@"image_%ld", (long)i];
            // 图片文件流
            [formData appendPartWithFileData:imageData name:fileName fileName:fileName mimeType:@"image/jpeg"];
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        self.hud.labelText = @"发布成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"提交失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
    return;
}

// 编辑信息
- (void)requestEditProduct {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    [self EditProductTitle];
}

// 修改产品标题
- (void)EditProductTitle{
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    NSString *name = [self.tfName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSDictionary *params = [ZbWebService editProductTitle:self.curEditDict[@"product_id"] withTitle:name];
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        BOOL isVip = [ZbWebService isVipQy];
        if (isVip) {
            [self EditProductRecommend];
        } else {
            [self EditProductType];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"提交失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
    return;
}

// 修改产品推荐
- (void)EditProductRecommend {
    NSString *select = self.btnRecommand.selected ? @"1" : @"0";
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editProductRecommend:self.curEditDict[@"product_id"] withRecomand:select] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_EDIT_RECOM;
    [conn start];
}

// 修改产品分类
- (void)EditProductType {
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    NSDictionary *params = [ZbWebService editProductType:self.curEditDict[@"product_id"] withType:self.curTypeMainText withSubType:self.curTypeSubText];
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        [self EditProductPrice];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"提交失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
     return;
}

// 修改产品标签
- (void)EditProductTag {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editProductTag:self.curEditDict[@"product_id"] withTag:self.tfLink.text] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_EDIT_TAG;
    [conn start];
}

// 修改产品参考价
- (void)EditProductPrice {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editProductPrice:self.curEditDict[@"product_id"] withPrice:self.curPrice priceUnit:self.curPriceUnit] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_EDIT_PRICE;
    [conn start];
}

// 修改产品描述
- (void)EditProductDescription {
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    NSString *des = [self.tvDes.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSDictionary *params = [ZbWebService editProductDescription:self.curEditDict[@"product_id"] withDesc:des] ;
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        self.hud.labelText = @"修改成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
            if (self.ClickBlock) {
                self.ClickBlock();
            }
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"提交失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
}


#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.tfName]) {
        [self.tfPrice becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - UITextView delegate
- (void)textViewDidChange:(UITextView *)textView {
    self.labDesPlaceHolder.hidden = textView.text.length > 0;
    return;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }

    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.modalPresentationStyle= UIModalPresentationOverFullScreen;
    imgPicker.delegate = self;
    if (buttonIndex == 0) {
        // 拍照
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        // 相册选择
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
    });
    return;
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        CGSize imgSize = portraitImg.size;
        CGSize tagSize = imgSize;
        CGFloat rate = imgSize.width / imgSize.height;
        if (imgSize.width >= imgSize.height) {
            if (imgSize.width > 1000) {
                tagSize = CGSizeMake(1000, 1000 / rate);
            }
        } else {
            if (imgSize.height > 1000) {
                tagSize = CGSizeMake(1000 * rate, 1000);
            }
        }
        // 去除图片旋转属性
        UIGraphicsBeginImageContext(tagSize);
        [portraitImg drawInRect:CGRectMake(0, 0, tagSize.width, tagSize.height)];
        UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [self.imageArray addObject:reSizeImage];
        [self drawPickImage];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"获取分类失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    
    NSMutableArray *dataArray = resultDic[@"data"];
    if (connection.tag == HTTP_REQUEST_TAG_TYPE) {
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = @"获取分类失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        [self.hud hide:NO];
        NSArray *tagArray = [ZbWebService treeTypeParser:dataArray];
        NSArray *typeArray = [tagArray firstObject];
        NSDictionary *subTypeDict = [tagArray lastObject];
        [[ZbSaveManager shareManager] setProductTypeArray:typeArray];
        [[ZbSaveManager shareManager] setProductSubTypeDict:subTypeDict];
        
        ZbTypeSelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTypeSelectViewController"];
        controller.typeArray = typeArray;
        controller.subTypeDict = subTypeDict;
        controller.lastParentIndex = self.curTypeMain;
        controller.lastSubIndex = self.curTypeSub;
        controller.selectClickBlock = ^(NSString *typeMain, NSString *typeSub, NSInteger mainIndex, NSInteger subIndex) {
            self.curTypeMain = mainIndex;
            self.curTypeSub = subIndex;
            self.curTypeMainText = typeMain;
            self.curTypeSubText = typeSub;
            NSString *type = [NSString stringWithFormat:@"%@(%@)", typeMain, typeSub];
            self.tfType.text = type;
        };
        [self.navigationController  pushViewController:controller animated:YES];
    } else if (connection.tag == HTTP_REQUEST_TAG_ADD_IMAGE_LIMIT) {
        [self.hud hide:NO];
        // 加载失败
        if (result.integerValue != 0) {
            return;
        }
        NSDictionary *imageLimitDict = [dataArray objectAtIndex:1];
        NSNumber *limit = imageLimitDict[@"limit"];
        self.canAddImageCount = MIN(3, limit.integerValue);
        if(self.curEditDict == nil) {
            [self drawPickImage];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_EDIT_RECOM) {
        if (result.integerValue == 0) {
            [self EditProductType];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_EDIT_PRICE) {
        if (result.integerValue == 0) {
            [self EditProductTag];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_EDIT_TAG) {
        if (result.integerValue == 0) {
            [self EditProductDescription];
        }
    }
}

@end
