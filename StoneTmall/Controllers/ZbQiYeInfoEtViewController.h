//
//  ZbQiYeInfoEtViewController.h
//  StoneTmall
//
//  Created by chyo on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

// 编辑企业资料状态
typedef enum {
    Zb_Qy_Edit_Type_Default,        // 正常编辑
    Zb_Qy_Edit_Type_OnCreate,       // 创建企业时编辑
} Zb_Qy_Edit_Type;

@interface ZbQiYeInfoEtViewController : BaseViewController

@property (nonatomic, assign) Zb_Qy_Edit_Type curEditType;

@end
