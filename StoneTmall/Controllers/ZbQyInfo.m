//
//  ZbQyInfo.m
//  StoneTmall
//
//  Created by chyo on 15/9/6.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQyInfo.h"

@implementation ZbQyInfo

- (id)initWithDictionary:(NSMutableDictionary*)jsonObject {
    // 过滤null信息
    NSArray *keys = [jsonObject allKeys];
    for (int i = 0; i < keys.count; i ++)
    {
        id obj = [jsonObject objectForKey:keys[i]];
        if (![obj isKindOfClass:[NSNull class]]) {
            continue;
        }
        [jsonObject setObject:@"" forKey:keys[i]];
    }
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:jsonObject];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key  {
    if([key isEqualToString:@"errno"]){
        self.error = [NSString stringWithFormat:@"%@", value];
    }
}

@end
