//
//  ZbContantViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/16.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbContantViewController.h"
#import "ZbNewContantViewController.h"

#define HTTP_LOAD_PDATA 1
#define HTTP_LOAD_QDATA 2
#define HTTP_DEL_PDATA 3
#define HTTP_DEL_QDATA 4
#define HTTP_CONNACT_LIMIT 5

@interface ZbContantViewController ()<UITableViewDelegate, UITableViewDataSource>

// 收货地址数组
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (strong, nonatomic) IBOutlet UITableView *tv;
@property (nonatomic, assign) int contantLimit;

@property (nonatomic) NSInteger curDelIndex;

@end

@implementation ZbContantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contantLimit = 0;
    self.curDelIndex = -1;
    self.dataArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

#pragma mark - 读取数据
- (void)loadData {
    ZbQyInfo *info = [ZbWebService sharedQy];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyContactList:info.company_id] delegate:self];
    conn.tag = HTTP_LOAD_PDATA;
    [conn start];
}
// 允许添加企业其他联系方式的数量
- (void)loadLimitData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getAddCompanyContactLimit] delegate:self];
    conn.tag = HTTP_CONNACT_LIMIT;
    [conn start];
}


- (void)delData:(NSString *)id {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService delCompanyContact:id] delegate:self];
    conn.tag = HTTP_DEL_QDATA;
    [conn start];
}

#pragma mark - UITableView Delegate & DataSource
/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    } else  {
        if (self.contantLimit == 0) {
            return self.dataArray.count;
        } else {
            return self.dataArray.count + 1;
        }
    }
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 55;
    }
    return 45;
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        ZbQyInfo *info = [ZbWebService sharedQy];
        if (info.company_contact.length != 0 || info.company_phone.length != 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell" forIndexPath:indexPath];
            UILabel *topLab = (UILabel *)[cell viewWithTag:1];
            UILabel *bomLab = (UILabel *)[cell viewWithTag:2];
            UILabel *flagLab = (UILabel *)[cell viewWithTag:3];
            topLab.text = info.company_contact;
            bomLab.text = info.company_phone;
            flagLab.hidden = NO;
            flagLab.layer.cornerRadius = 2.0f;
            flagLab.layer.masksToBounds = YES;
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"mainfooterCell" forIndexPath:indexPath];
        }
        
    } else if (indexPath.section == 1) {
        if (self.dataArray.count != 0) {
            if ( self.dataArray.count - 1 >= indexPath.row ) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"itemsCell" forIndexPath:indexPath];
                UILabel *topLab = (UILabel *)[cell viewWithTag:1];
                UILabel *bomLab = (UILabel *)[cell viewWithTag:2];
                NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
                topLab.text = dict[@"name"];
                bomLab.text = dict[@"phone"];
            } else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
            }
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        ZbQyInfo *info = [ZbWebService sharedQy];
        ZbNewContantViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbNewContantViewController"];
        controller.name = info.company_contact;
        controller.phone = info.company_phone;
        [controller setControllerType:Zb_Controller_MAINEDIT];
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        if (self.dataArray.count == indexPath.row) {
            ZbNewContantViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbNewContantViewController"];
            [controller setControllerType:Zb_Controller_ADDCONACT];
            [self.navigationController pushViewController:controller animated:YES];
        } else {
            ZbNewContantViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbNewContantViewController"];
            NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
            controller.dict = dict;
            [controller setControllerType:Zb_Controller_OTHEREDIT];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
        self.curDelIndex = indexPath.row;
        [self delData:dict[@"id"]];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return NO;
    } else {
        if (self.dataArray.count == indexPath.row) {
            return NO;
        }
        return YES;
    }
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSString *result = dict[@"errno"];
    if (result.integerValue != 0) {
        // 加载失败
        return;
    }
    if (connection.tag == HTTP_LOAD_PDATA) {
        if (result.integerValue == 0) {
            NSArray *array = dict[@"data"];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:array];
            [self loadLimitData];
        }
    } else if (connection.tag == HTTP_CONNACT_LIMIT) {
        NSDictionary *dataLimt = dict[@"data"];
        self.contantLimit = [[dataLimt objectForKey:@"left"] intValue];
        [self.tv reloadData];
    } else if (connection.tag == HTTP_LOAD_QDATA) {
        if (result.integerValue == 0) {
            NSArray *array = dict[@"data"];
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:array];
            [self.tv reloadData];
        }
    } else if (connection.tag == HTTP_DEL_QDATA) {
        if (result.integerValue == 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.curDelIndex inSection:1];
            [self.dataArray removeObjectAtIndex:self.curDelIndex];
            [self.tv deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self loadLimitData];
        } else {
            self.hud.labelText = @"删除失败";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        }
    }
}

@end
