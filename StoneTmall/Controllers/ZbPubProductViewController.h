//
//  ZbPubProductViewController.h
//  StoneTmall
//
//  Created by chyo on 15/9/1.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbPubProductViewController : BaseViewController

// 编辑字典
@property (nonatomic, strong) NSDictionary *curEditDict;

@property (nonatomic, strong) NSMutableArray *stoneImgArray;

@property (nonatomic, strong) void (^ ClickBlock)(void);

@end
