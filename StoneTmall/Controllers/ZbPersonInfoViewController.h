//
//  ZbPersonInfoViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/10/11.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbPersonInfoViewController : BaseViewController

// 用户名
@property (nonatomic, strong) NSString *userName;
// 昵称
@property (nonatomic, strong) NSString *nickName;

@end
