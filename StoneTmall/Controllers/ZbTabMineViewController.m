//
//  ZbTabMineViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTabMineViewController.h"
#import "ZbSettingViewController.h"
#import "ZbNoticeViewController.h"
#import "ZbUserInfoEtViewController.h"
#import "ZbMyorderViewController.h"
#import "ZbShopCartViewController.h"
#import "ZbUserUnLoginHeader.h"
#import "ZbUserNormalHeader.h"
#import "ZbUserQiYeHeader.h"
#import "ZbQyInfo.h"
#import "ZbMyFuncViewController.h"
#import "ZbMyPubInfoViewController.h"
#import "AppDelegate.h"
#import "EaseMob.h"
#import "ZbAboutViewController.h"
#import "MainViewController.h"
#import "ZbTabViewController.h"

#define HTTP_REQUEST_TAG_USERINFO 1
#define HTTP_REQUEST_TAG_QIYEINFO 2

@interface ZbTabMineViewController () <UITableViewDataSource, UITableViewDelegate, ZbUserHeaderDelegate, UIAlertViewDelegate>

// 导航栏左
@property (nonatomic, strong) IBOutlet UIBarButtonItem *leftNavItem;
// 导航栏左
@property (nonatomic, strong) IBOutlet UIBarButtonItem *rightNavItem;

@property (nonatomic, strong) NSArray *userTArray;
@property (nonatomic, strong) NSArray *qiyeTArray;

@end

@implementation ZbTabMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userTArray = [NSArray arrayWithObjects:@"我的微聊", /*@"推荐给我",*/ @"我的收藏", /*@"我的发布", @"我参与的", @"我的浏览历史",*/ @"关于我们", /*@"注销",*/ nil];
    self.qiyeTArray = [NSArray arrayWithObjects:@"我的微聊", /*@"我的订单", */@"我的购物车",/* @"推荐给我",*/ @"我的订单", @"我的收藏", @"我的发布",/* @"我参与的", @"我的浏览历史", @"我的微数据", */ @"关于我们", /*@"注销",*/  nil];
    
    if (![ZbSaveManager isSampleShow]) {
        self.qiyeTArray = [NSArray arrayWithObjects:@"我的微聊", /*@"我的订单", @"我的购物车", @"推荐给我", @"我的订单",*/ @"我的收藏", @"我的发布",/* @"我参与的", @"我的浏览历史", @"我的微数据", */ @"关于我们", /*@"注销",*/  nil];
    }
    
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tabBarController.navigationItem.leftBarButtonItem = self.leftNavItem;
    self.tabBarController.navigationItem.rightBarButtonItem = self.rightNavItem;
    self.tabBarController.navigationItem.titleView = nil;
    self.tabBarController.title = @"我的";
    
    BOOL isLogin = [ZbWebService isLogin];
//    NSDictionary *userDict = [ZbWebService sharedUser];
    if (isLogin) {
        [self loadData];
    }
    // 绘制用户信息
    [self drawUserInfo];
    [self.tv reloadData];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
}

- (NSString *)isNullStr:(NSString *)str {
    NSNull *null = [[NSNull alloc] init];
    if ([str isEqual:null]) {
        return @"";
    }
    return str;
}

#pragma mark - 读取数据
//  读取数据
- (void)loadData {
//    self.hud.labelText = @"努力加载中..";
//    [self.hud show:NO];
    
    NSDictionary *dic = [ZbWebService sharedUser];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getUserInfoParams:dic[@"user_name"]] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_USERINFO;
    [conn start];
}

// 读取个人企业信息
- (void)loadQyData {
//    self.hud.labelText = @"努力加载中..";
//    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getBindCompanyInfo] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_QIYEINFO;
    [conn start];
}


#pragma mark - 绘制界面
// 绘制界面
- (void)drawUserInfo {
    BOOL isLogin = [ZbWebService isLogin];
    NSDictionary *user = [ZbWebService sharedUser];
    NSString *companyId = user[@"company_id"];
    if (!isLogin) {
        // 未登录
        ZbUserUnLoginHeader *unLoginView = [[[NSBundle mainBundle] loadNibNamed:@"ZbUserUnLoginHeader" owner:nil options:nil] firstObject];
        unLoginView.delegate = self;
        CGRect xFrame = CGRectMake(0, 0, DeviceWidth, 100);
        unLoginView.frame = xFrame;
        [self.tv setTableHeaderView:unLoginView];
        
    } else if (companyId == nil || companyId.length <= 0 || [companyId isEqualToString:@"0"]) {
        // 普通用户
        ZbUserNormalHeader *normalView = [[[NSBundle mainBundle] loadNibNamed:@"ZbUserNormalHeader" owner:nil options:nil] firstObject];
        normalView.delegate = self;
        CGRect xFrame = CGRectMake(0, 0, DeviceWidth, 180);
        normalView.orderView.hidden = NO;
        if (![ZbSaveManager isSampleShow]) {
            xFrame = CGRectMake(0, 0, DeviceWidth, 110);
            normalView.orderView.hidden = YES;
        }
        normalView.frame = xFrame;
        
        normalView.labName.text = user[@"user_nickname"];
        normalView.labProvince.text = [NSString stringWithFormat:@"%@%@", user[@"user_province"], user[@"user_city"]];
        [normalView.imgHead sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:user[@"user_image"]]]];
        
        // 手机绑定状态
        NSString *phone = user[@"user_phone"];
        if (phone != nil && [phone isKindOfClass:[NSString class]] && phone.length > 0) {
            normalView.userImgFlag.highlighted = YES;
        } else {
            normalView.userImgFlag.highlighted = NO;
        }
        // 身份证验证状态
        NSNumber *card = user[@"user_card_ok"];
        if (card.integerValue == Zb_User_Verify_Status_Status_Success) {
            normalView.userImgFlag3.highlighted = YES;
        } else {
            normalView.userImgFlag3.highlighted = NO;
        }
        
        [self.tv setTableHeaderView:normalView];
    } else {
        ZbQyInfo *info = [ZbWebService sharedQy];
        // 企业用户
        ZbUserQiYeHeader *qiyeView = [[[NSBundle mainBundle] loadNibNamed:@"ZbUserQiYeHeader" owner:nil options:nil] firstObject];
        qiyeView.delegate = self;
        CGRect xFrame = CGRectMake(0, 0, DeviceWidth, CGRectGetHeight(qiyeView.frame));
        qiyeView.frame = xFrame;
        
        [qiyeView.userImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:user[@"user_image"]]]];
        qiyeView.userName.text = user[@"user_nickname"];
        
        qiyeView.userProvince.text = [NSString stringWithFormat:@"%@%@",[self isNullStr:user[@"user_province"]], [self isNullStr:user[@"user_city"]]];
        [qiyeView.qiyeImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:info.company_image]]];
        qiyeView.qiyeImage.layer.masksToBounds = YES;
        qiyeView.qiyeName.text = info.company_name;
        qiyeView.qiyeLevel.text = [NSString stringWithFormat:@"V%@", info.company_level];
        qiyeView.qiyeLevel.backgroundColor = info.company_level.integerValue > 0 ? MAIN_ORANGE_COLOR : UIColorFromHexString(@"CCCCCC");
        
        qiyeView.qiyeAdr.text = [NSString stringWithFormat:@"%@ %@ %@",info.company_prov, info.company_city, info.company_area];
        qiyeView.qiyeImgFlag2.text = [NSString stringWithFormat:@"成长值%@", info.company_grow];
        qiyeView.qiyeImgFlag2.backgroundColor = info.company_grow.integerValue > 0 ? UIColorFromHexString(@"04A875") : UIColorFromHexString(@"CCCCCC");
        
        // vip状态
        if (info.company_level.integerValue <= 0 && ![ZbSaveManager isCtrlHide]) {
            // 非vip
            qiyeView.btnVip.hidden = NO;
            qiyeView.alcCompanyViewHeight.constant = 150;
            CGRect qyFrame = qiyeView.frame;
            qyFrame.size.height = 305;
            qiyeView.frame = qyFrame;
        } else {
            // vip
            qiyeView.btnVip.hidden = YES;
            qiyeView.alcCompanyViewHeight.constant = 115;
            CGRect qyFrame = qiyeView.frame;
            qyFrame.size.height = 270;
            qiyeView.frame = qyFrame;
        }
        
        // 手机绑定状态
        NSString *phone = user[@"user_phone"];
        if (phone != nil && [phone isKindOfClass:[NSString class]] && phone.length > 0) {
            qiyeView.userImgFlag.highlighted = YES;
        } else {
            qiyeView.userImgFlag.highlighted = NO;
        }
        // 身份证验证状态
        NSNumber *card = user[@"user_card_ok"];
        if (card.integerValue == Zb_User_Verify_Status_Status_Success) {
            qiyeView.userImgFlag3.highlighted = YES;
        } else {
            qiyeView.userImgFlag3.highlighted = NO;
        }
        // 营业执照
        NSString *company_licence_ok = info.company_licence_ok;
        if (company_licence_ok.intValue == Zb_Company_Licence_Status_Success) {
            qiyeView.qiyeImgFlag1.highlighted = YES;
        } else {
            qiyeView.qiyeImgFlag1.highlighted = NO;
        }
        
        [self.tv setTableHeaderView:qiyeView];
    }
}

#pragma mark - 点击事件
// 设置点击
- (IBAction)settingOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        self.hud.labelText = @"尚未登录，无法操作";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
        }];
        return;
    }
    UIStoryboard *noticesb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbSettingViewController *contoller = [noticesb instantiateViewControllerWithIdentifier:@"ZbSettingViewController"];
    [self.navigationController pushViewController:contoller animated:YES];
    return;
}
// 通知点击
- (IBAction)noficOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        self.hud.labelText = @"尚未登录，无法操作";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
        }];
        return;
    }
    UIStoryboard *noticesb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbNoticeViewController *contoller = [noticesb instantiateViewControllerWithIdentifier:@"ZbNoticeViewController"];
    [self.navigationController pushViewController:contoller animated:YES];
    return;
}

#pragma mark - ZbUserHeaderDelegate
- (void)presentToController:(UINavigationController *)controller {
    // 登录判断
    if (![ZbWebService isLogin]) {
//        self.hud.labelText = @"尚未登录，无法操作";
//        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
//        }];
        return;
    }
    [self.navigationController.tabBarController presentViewController:controller animated:YES completion:nil];
}

- (void)pushToController:(UIViewController *)controller {
    // 登录判断
    if (![ZbWebService isLogin]) {
        self.hud.labelText = @"尚未登录，无法操作";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
        }];
        return;
    }

    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark - UITableViewDataSource
// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// 行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    BOOL islogin = [ZbWebService isLogin];
    NSDictionary *user = [ZbWebService sharedUser];
    NSString *companyId = user[@"company_id"];
    if (!islogin) {
        return self.userTArray.count;
    } else {
        if ([companyId isEqualToString:@"0"]) {
            return self.userTArray.count + 1;
        } else {
            return self.qiyeTArray.count + 1;
        }
    }
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL islogin = [ZbWebService isLogin];
    NSDictionary *user = [ZbWebService sharedUser];
    NSString *companyId = user[@"company_id"];
    if (!islogin) {
        return 45.0f;
    } else {
        if ([companyId isEqualToString:@"0"]) {
            if (indexPath.row >= self.userTArray.count) {
                return 55.0f;
            }
            return 45.0f;
        } else {
            if (indexPath.row >= self.qiyeTArray.count) {
                return 55.0f;
            }
            return 45.0f;
        }
    }
    return 45.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *user = [ZbWebService sharedUser];
    NSString *companyId = user[@"company_id"];
    if ([ZbWebService isLogin]) {
        // 不是企业用户
        if ([companyId isEqualToString:@"0"]) {
            if (indexPath.row >= self.userTArray.count) {
                // 注销
                UITableViewCell *logOutCell = [tableView dequeueReusableCellWithIdentifier:@"bottomCell"];
                return logOutCell;
            }
        } else {
            if (indexPath.row >= self.qiyeTArray.count) {
                // 注销
                UITableViewCell *logOutCell = [tableView dequeueReusableCellWithIdentifier:@"bottomCell"];
                return logOutCell;
            }
        }
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myListCell" forIndexPath:indexPath];
    
    BOOL islogin = [ZbWebService isLogin];
    UIImageView *imgLeft = (UIImageView *)[cell viewWithTag:1];
    UILabel *labTit = (UILabel *)[cell viewWithTag:2];
    UILabel *labFlag = (UILabel *)[cell viewWithTag:3];
    labFlag.layer.cornerRadius = 7.5f;
    labFlag.layer.masksToBounds = YES;
    labFlag.text = @"";
    
    NSDictionary *imgDict = @{@"我的微聊":@"public_btn_chat", @"我的订单":@"public_btn_order", @"我的购物车":@"public_icn_cart", @"推荐给我":@"public_btn_recommended", @"我的收藏":@"public_btn_favor", @"我的发布":@"public_btn_release", @"我参与的":@"public_btn_participation", @"我的浏览历史":@"public_btn_time", @"我的微数据":@"public_btn_time", @"关于我们":@"public_btn_time", };
    
    if (!islogin) {
        labTit.text = [self.userTArray objectAtIndex:indexPath.row];
    } else {
        if ([companyId isEqualToString:@"0"]) {
            labTit.text = [self.userTArray objectAtIndex:indexPath.row];
        } else {
            labTit.text = [self.qiyeTArray objectAtIndex:indexPath.row];
        }
    }
    
    // 微聊红点
    if ([labTit.text isEqualToString:@"我的微聊"]) {
        if (self.tabBarItem.badgeValue.integerValue > 0) {
            labFlag.hidden = NO;
        } else {
            labFlag.hidden = YES;
        }
    } else {
        labFlag.hidden = YES;
    }
    imgLeft.image = [UIImage imageNamed:imgDict[labTit.text]];
   return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *user = [ZbWebService sharedUser];
    NSString *companyId = user[@"company_id"];
    NSString *listItem;
    // 不是企业用户
    if (![ZbWebService isLogin] || [companyId isEqualToString:@"0"]) {
        if (indexPath.row >= self.userTArray.count) {
            // 注销
            UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:@"确定注销？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
            return;
        }
        listItem = [self.userTArray objectAtIndex:indexPath.row];
    } else {
        if (indexPath.row >= self.qiyeTArray.count) {
            // 注销
            UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:@"确定注销？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
            return;
        }
        listItem = [self.qiyeTArray objectAtIndex:indexPath.row];
    }
    
    // 登录判断
    if (![ZbWebService isLogin] && ![listItem isEqualToString:@"关于我们"]) {
        self.hud.labelText = @"尚未登录，无法操作";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
        }];
        return;
    }
    
    if ([listItem isEqualToString:@"我的微聊"]) {
        // 聊天系统登录判断
        if (![[[EaseMob sharedInstance] chatManager] isLoggedIn]) {
            self.hud.labelText = @"聊天系统数据正在加载中..请稍候";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        };
        MainViewController *controller = [[MainViewController alloc] init];
        AppDelegate *applegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [controller networkChanged:applegate.connectionState];
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
    } else if ([listItem isEqualToString:@"我的订单"]) {
        UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
        ZbMyorderViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbMyorderViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([listItem isEqualToString:@"我的购物车"]) {
        UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
        ZbShopCartViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbShopCartViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([listItem isEqualToString:@"推荐给我"]) {
        ZbMyFuncViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbMyFuncViewController"];
        [controller setControllerType:ZbCxl_Type_comme];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([listItem isEqualToString:@"我的收藏"]) {
        ZbMyFuncViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbMyFuncViewController"];
        [controller setControllerType:ZbCXl_Type_colle];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([listItem isEqualToString:@"我的发布"]) {
        UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
        ZbMyPubInfoViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbMyPubInfoViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([listItem isEqualToString:@"我参与的"]) {
        ZbMyFuncViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbMyFuncViewController"];
        [controller setControllerType:ZbCxl_Type_invol];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if ([listItem isEqualToString:@"我的浏览历史"]) {
        
    } else if ([listItem isEqualToString:@"我的微数据"]) {
        
    } else if ([listItem isEqualToString:@"关于我们"]) {
        UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
        ZbAboutViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbAboutViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - UIALertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        return;
    }
    
    // 先异步登出环信
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:YES completion:^(NSDictionary *info, EMError *error) {
        if (!error) {
            DLog(@"环信退出成功");
        }
    } onQueue:nil];
    // 退出app
    AppDelegate *applegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [applegate logOut];
    // 清微聊红点
    ZbTabViewController *tab = (ZbTabViewController *)self.tabBarController;
    [tab setupUntreatedApplyCount];
}

#pragma mark - CTURLConnectionDelegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    if (connection.tag == HTTP_REQUEST_TAG_USERINFO) {
        NSMutableDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        [self.hud hide:NO];
        NSString *result = resultDic[@"errno"];
        if (result.integerValue != 0) {
            // 加载失败
            return;
        }
        NSMutableDictionary *userDict = resultDic[@"data"];
        [ZbWebService setUser:userDict];
        if (![userDict[@"company_id"] isEqualToString:@"0"]) {
            // 企业用户
            [self loadQyData];
        }
        [self drawUserInfo];
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_QIYEINFO) {
        [self.hud hide:NO];
        NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSString *result = dict[@"errno"];
        if (result.integerValue != 0) {
            // 加载失败
            return;
        }
        ZbQyInfo *info = [[ZbQyInfo alloc] initWithDictionary:dict[@"data"]];
        [ZbWebService setQy:info];
        [self drawUserInfo];
        [self.tv reloadData];
    }
}

@end
