//
//  ZbGuideViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/10/31.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbGuideViewController.h"

#define MAX_GUIDE_PAGE_COUNT 4

@interface ZbGuideViewController () <UIScrollViewDelegate>

// scrollview控件
@property (nonatomic, strong) IBOutlet UIScrollView *svContainer;

// 立即进入按钮
@property (nonatomic, strong) IBOutlet UIButton *btnEnter;

// 当前选择页面
@property (assign, nonatomic) NSInteger curPageIndex;

@end

@implementation ZbGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 立即进入按钮圆角
    self.btnEnter.layer.cornerRadius = 2;
    self.btnEnter.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self pageChange];
    // 创建图片
    for (NSInteger i = 0; i < MAX_GUIDE_PAGE_COUNT; i++) {
        UIImageView *ivImage = [[UIImageView alloc] initWithFrame:CGRectMake(i * DeviceWidth, 0, DeviceWidth, DeviceHeight)];
        ivImage.contentMode = UIViewContentModeScaleAspectFill;
        ivImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"guide%@", [NSNumber numberWithInt:(i + 1)]]];
        [self.svContainer addSubview:ivImage];
    }
    self.svContainer.contentSize = CGSizeMake(DeviceWidth * MAX_GUIDE_PAGE_COUNT, DeviceHeight);
    
    // 引导页已播放
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@1 forKey:SAVE_KEY_GUIDE_SEEN];
    [userDefault synchronize];
}

#pragma mark - 点击事件
- (IBAction)btnEnterOnClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    return;
}

#pragma mark - 切页逻辑
- (void)pageChange
{
    if (self.curPageIndex == (MAX_GUIDE_PAGE_COUNT - 1)) {
        // 最后一页显示进入
        self.btnEnter.hidden = NO;
    } else {
        self.btnEnter.hidden = YES;
    }
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 判断是否切页
    if (fmod(offsetX, scrollView.frame.size.width)  == 0) {
        int index = offsetX / scrollView.frame.size.width;
        self.curPageIndex = index;
        [self pageChange];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
