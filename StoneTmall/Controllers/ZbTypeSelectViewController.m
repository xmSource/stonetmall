//
//  ZbTypeSelectViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/8.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTypeSelectViewController.h"
#import "ZbTypeParentTableViewCell.h"
#import "ZbTypeSubTableViewCell.h"

@interface ZbTypeSelectViewController () <UITableViewDataSource, UITableViewDelegate>

// 一级分类tv
@property (nonatomic, strong) IBOutlet UITableView *tvMain;
// 二级分类tv
@property (nonatomic, strong) IBOutlet UITableView *tvSub;

// 当前选中的一级选项
@property (nonatomic, assign) NSInteger curTypeMainIndex;

@end

@implementation ZbTypeSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 注册cell
    [self.tvMain registerNib:[UINib nibWithNibName:@"ZbTypeParentTableViewCell" bundle:nil] forCellReuseIdentifier:TypeParentCellIdentifier];
    self.tvMain.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tvSub registerNib:[UINib nibWithNibName:@"ZbTypeSubTableViewCell" bundle:nil] forCellReuseIdentifier:TypeSubCellIdentifier];
    self.tvSub.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.curTypeMainIndex = self.lastParentIndex;
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:self.tvMain]) {
        return self.typeArray.count;
    } else {
        NSDictionary *typeInfo = [self.typeArray objectAtIndex:self.curTypeMainIndex];
        NSString *classId = typeInfo[@"class_id"];
        NSArray *subArray = self.subTypeDict[classId];
        return subArray.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.tvMain]) {
        return [ZbTypeParentTableViewCell getRowHeight];
    } else {
        return [ZbTypeSubTableViewCell getRowHeight];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.tvMain]) {
        ZbTypeParentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TypeParentCellIdentifier forIndexPath:indexPath];
        // 一级类名
        NSDictionary *typeDict = [self.typeArray objectAtIndex:indexPath.row];
        NSString *class_name = typeDict[@"class_name"];
        cell.labName.text = class_name;
        // 一级图标
        cell.ivIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@min", class_name]];
        // 二级类名
        NSString *classId = typeDict[@"class_id"];
        NSArray *subArray = self.subTypeDict[classId];
        NSMutableString *subClassname = [NSMutableString string];
        for (NSDictionary *subInfo in subArray) {
            [subClassname appendString:subInfo[@"class_name"]];
        }
        cell.labSubName.text = subClassname;
        
        BOOL isSelect = indexPath.row == self.curTypeMainIndex;
        [cell setIsCurSelect:isSelect];
        return cell;
    } else {
        ZbTypeSubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TypeSubCellIdentifier forIndexPath:indexPath];
        NSDictionary *typeInfo = [self.typeArray objectAtIndex:self.curTypeMainIndex];
        NSString *classId = typeInfo[@"class_id"];
        NSArray *subArray = self.subTypeDict[classId];
        NSDictionary *typeDict = [subArray objectAtIndex:indexPath.row];
        cell.labName.text = typeDict[@"class_name"];
        
        BOOL isLastSelect = (self.curTypeMainIndex == self.lastParentIndex && indexPath.row == self.lastSubIndex);
        [cell setIsCurSelect:isLastSelect];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([tableView isEqual:self.tvMain]) {
        self.curTypeMainIndex = indexPath.row;
        [self.tvMain reloadData];
        [self.tvSub reloadData];
    } else {
        if (self.selectClickBlock) {
            // 当前父类别信息
            NSDictionary *typeParentDict = [self.typeArray objectAtIndex:self.curTypeMainIndex];
            NSString *typeParent = typeParentDict[@"class_name"];
            
            // 当前子类别信息
            NSDictionary *typeInfo = [self.typeArray objectAtIndex:self.curTypeMainIndex];
            NSString *classId = typeInfo[@"class_id"];
            NSArray *subArray = self.subTypeDict[classId];
            NSDictionary *typeSubDict = [subArray objectAtIndex:indexPath.row];
            NSString *typeSub = typeSubDict[@"class_name"];
            self.selectClickBlock(typeParent, typeSub, self.curTypeMainIndex, indexPath.row);
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
