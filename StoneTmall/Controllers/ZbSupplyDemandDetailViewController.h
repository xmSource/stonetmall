//
//  ZbSupplyDemandDetailViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbSupplyDemandDetailViewController : BaseViewController

// 当前帖子id
@property (nonatomic, strong) NSString *curPostId;
@property (nonatomic, assign) BOOL isNews;

@end
