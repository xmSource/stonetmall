//
//  ZbNewContantViewController.m
//  StoneTmall
//
//  Created by Apple on 15/9/16.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbNewContantViewController.h"

#define HTTP_GET_COMPANYINFO 1
#define HTTP_EDIT_CONTACT 2

@interface ZbNewContantViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UIButton *commit;
@property (nonatomic) Zb_Controller_Type contactType;

@end

@implementation ZbNewContantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.nameField.text = self.name;
    self.phoneField.text = self.phone;
    if (self.contactType == Zb_Controller_OTHEREDIT) {
        self.nameField.text = self.dict[@"name"];
        self.phoneField.text = self.dict[@"phone"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.commit.layer.cornerRadius = 2.0f;
    self.commit.layer.masksToBounds = YES;
}

- (void)setControllerType:(Zb_Controller_Type)contactType {
    _contactType = contactType;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)loadqiYeData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getBindCompanyInfo] delegate:self];
    conn.tag = HTTP_GET_COMPANYINFO;
    [conn start];
}

#pragma mark - TextFiele Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.nameField]) {
        [self.phoneField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - 点击事件
//添加联系人
- (IBAction)commitOnClick:(id)sender {
    if (self.nameField.text.length == 0) {
        self.hud.labelText = @"请填写联系人";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    if (![CommonCode isPhoneNumber:self.phoneField.text]) {
        self.hud.labelText = @"手机号格式不正确";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    [self.view endEditing:YES];
    NSString *params;
    switch (_contactType) {
        case Zb_Controller_MAINEDIT:
            params = [ZbWebService editCompanyContact:self.nameField.text phone:self.phoneField.text];
            break;
            
        case Zb_Controller_ADDCONACT:
            params = [ZbWebService addCompanyContact:self.nameField.text phone:self.phoneField.text];
            break;
            
        case Zb_Controller_OTHEREDIT: 
            params = [ZbWebService editCompanyOtherContact:self.dict[@"id"] name:self.nameField.text phone:self.phoneField.text];
            break;

        default:
            break;
    }
    
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:params delegate:self];
    conn.tag = HTTP_EDIT_CONTACT;
    [conn start];
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = dict[@"errno"];
    if (connection.tag == HTTP_EDIT_CONTACT) {
         if (result.integerValue == 0) {
            self.hud.labelText = @"提交成功";
            if (self.contactType == Zb_Controller_MAINEDIT) {
                [self loadqiYeData];
            }
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            self.hud.labelText = dict[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        }
    } else if (connection.tag == HTTP_GET_COMPANYINFO) {
        ZbQyInfo *info = [[ZbQyInfo alloc] initWithDictionary:dict[@"data"]];
        [ZbWebService setQy:info];
    }

}


@end
