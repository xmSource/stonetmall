//
//  ZbRecommendQyViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/11/6.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbRecommendQyViewController : BaseViewController

// 推荐数组
@property (nonatomic, strong) NSMutableArray *array;

@end
