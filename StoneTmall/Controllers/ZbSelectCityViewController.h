//
//  ZbSelectCityViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/20.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbSelectCityViewController : BaseViewController

@property (nonatomic, copy) void (^selectBlock)(NSString *city);

@end
