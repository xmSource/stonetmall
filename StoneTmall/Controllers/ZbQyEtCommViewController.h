//
//  ZbQyEtCommViewController.h
//  StoneTmall
//
//  Created by Apple on 15/9/9.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, EditType) {
    EditType_TEL,        //编辑企业电话
    EditType_SITE,       //编辑企业网站
    EditType_DESC,       //编辑企业详情
};

@interface ZbQyEtCommViewController : BaseViewController

- (void)setControllerType:(EditType)type with:(NSString *)defaultStr;

@end
