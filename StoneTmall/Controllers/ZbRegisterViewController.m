//
//  ZbRegisterViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRegisterViewController.h"
#import "ZbBecomeQyViewController.h"
#import "WXApi.h"
#import "ConsWebController.h"

#define HTTP_REQUEST_TAG_CHECKEXIST 0        // 检测用户名是否存在
#define HTTP_REQUEST_TAG_REGISTER 1          // 注册

@interface ZbRegisterViewController () <UITextFieldDelegate>

// 账号输入框
@property (nonatomic, strong) IBOutlet UITextField *tfAcc;
// 密码输入框
@property (nonatomic, strong) IBOutlet UITextField *tfPsw;
// 注册按钮
@property (nonatomic, strong) IBOutlet UIButton *btnRegister;
// 已读注册条款按钮
@property (nonatomic, strong) IBOutlet UIButton *btnIsRead;

@end

@implementation ZbRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 注册收到微信登录请求回应的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotificationGetWxCodeResult:) name:NOTIFICATION_GET_WX_CODE_SUCCESS object:nil];
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 注册按钮初始不激活
    [self setBtnRegisterEnable:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 设置注册按钮激活状态
- (void)setBtnRegisterEnable:(BOOL)isEnable {
    self.btnRegister.enabled = isEnable;
    self.btnRegister.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

// 界面不显示
- (void)onNoShow {
    [self.tfAcc resignFirstResponder];
    [self.tfPsw resignFirstResponder];
}

#pragma mark - 微信
// 发起微信授权
- (void)sendWechatAuthRequest {
    //构造SendAuthReq结构体
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"register" ;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendAuthReq:req viewController:self delegate:nil];
    return;
}

- (void)onNotificationGetWxCodeResult:(NSNotification *)notify {
    SendAuthResp *resp = notify.object;
    if (resp.code == nil) {
        [self.hud hide:NO];
        return;
    }
    if (![resp.state isEqualToString:@"register"]) {
        return;
    }
    [self requestRegister:resp.code];
    return;
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

// 注册按钮点击
- (IBAction)btnRegisterOnClick:(id)sender {
    [self.view endEditing:YES];
    
    if (IS_IPAD && ![WXApi isWXAppInstalled]) {
        // ipad收不到短信，无法用微信sdk的免微信授权功能
        self.hud.labelText = @"请在石猫iPhone版上注册";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    [self reuqestCheckNameExist];
    return;
}

// 已读按钮点击
- (IBAction)btnIsReadOnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.tfAcc.text.length > 3 && self.tfPsw.text.length > 0 && self.btnIsRead.selected) {
        [self setBtnRegisterEnable:YES];
    } else {
        [self setBtnRegisterEnable:NO];
    }
    return;
}

// 免责协议点击
- (IBAction)btnItemsOnClick:(id)sender {
    ConsWebController *webController = [[ConsWebController alloc] init];
    webController.showTitle = @"用户协议";
    webController.mainUrl = APP_ITEMS_URL;
    UIViewController *loginContainer = self.parentViewController;
    [loginContainer.navigationController pushViewController:webController animated:YES];
    return;
}

#pragma mark - 接口调用
// 注册接口
- (void)requestRegister:(NSString *)code {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService registerParams:self.tfAcc.text password:self.tfPsw.text wxCode:code] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_REGISTER;
    conn.timeOut = 45;
    [conn start];
    return;
}

// 判断用户名是否存在
- (void)reuqestCheckNameExist {
    self.hud.labelText = @"提交中..";
    [self.hud show:NO];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService checkUserNameExistParams:self.tfAcc.text] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_CHECKEXIST;
    [conn start];
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.tfAcc]) {
        [self.tfPsw becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (IBAction)textFieldChanged:(id)sender {
    if (self.tfAcc.text.length > 3 && self.tfPsw.text.length > 0 && self.btnIsRead.selected) {
        [self setBtnRegisterEnable:YES];
    } else {
        [self setBtnRegisterEnable:NO];
    }
    return;
}

#pragma mark - 接口 delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"注册失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_TAG_CHECKEXIST) {
        if (result.integerValue != 0) {
            if (result.integerValue == -1) {
                self.hud.labelText = @"该账号已存在";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        [self sendWechatAuthRequest];
    } else if (connection.tag == HTTP_REQUEST_TAG_REGISTER) {
        if (result.integerValue != 0) {
            if (result.integerValue == -59) {
                self.hud.labelText = @"微信账号已注册，请直接登录";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        // 设置用户信息
        NSMutableDictionary *user = resultDic[@"data"];
        [ZbWebService setUser:user];
        
        // 保存cookie到本地
        [ZbWebService saveCookie];
        
        //跳转到主界面
        self.hud.labelText = @"注册成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
            // 跳转成为企业用户界面
            ZbBecomeQyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbBecomeQyViewController"];
            UIViewController *loginContainer = self.parentViewController;
            [loginContainer.navigationController pushViewController:controller animated:YES];
        }];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
