//
//  ZbTelValidateViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/21.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

// 手机验证类型
typedef enum {
    Zb_TelValidate_Type_FindPsw,                    // 找回密码
    Zb_TelValidate_Type_BindTel,                    // 单独绑定手机
    Zb_TelValidate_Type_BindTel_BecommonQy,         // 注册企业前绑定手机
} Zb_TelValidate_Type;

@interface ZbTelValidateViewController : BaseViewController

@property (nonatomic, assign) Zb_TelValidate_Type curValidateType;

@end
