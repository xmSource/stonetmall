//
//  ZbDemandViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbDemandViewController.h"
#import "YKuImageInLoopScrollView.h"
#import "ZbSeeMoreTableViewCell.h"
#import "ZbSupplyDemandTableViewCell.h"
#import "ZbQyDetailViewController.h"
#import "ZbRewardContainerViewController.h"
#import "ZbSupplyDemandDetailViewController.h"
#import "ZbRewardDetailViewController.h"

#define HTTP_REQUEST_TAG_AD 1               // 广告数据
#define HTTP_REQUEST_TAG_REWARD 2           // 悬赏
#define HTTP_REQUEST_TAG_DEMAND 3           // 需求
#define HTTP_REQUEST_TAG_DEMAND_MORE 4      // 更多需求

@interface ZbDemandViewController () <UITableViewDataSource, UITableViewDelegate, YKuImageInLoopScrollViewDelegate>

// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 广告滚动控件
@property (nonatomic, strong) YKuImageInLoopScrollView *imageLoopView;

// 广告数组
@property (nonatomic, strong) NSMutableArray *adArray;
// 求购数据
@property (nonatomic, strong) NSMutableArray *dataArray;
// 悬赏数据
@property (nonatomic, strong) NSMutableArray *rewardArray;

// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

@end

@implementation ZbDemandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    self.adArray = Mutable_ArrayInit;
    self.dataArray = [NSMutableArray array];
    self.rewardArray = [NSMutableArray array];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSupplyDemandTableViewCell" bundle:nil] forCellReuseIdentifier:SupplyDemandCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSeeMoreTableViewCell" bundle:nil] forCellReuseIdentifier:SeeMoreFooterIdentifier];
    
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
    
    // 设置tableview头部高度
    CGRect tvHeaderFrame = self.tvHeader.frame;
    tvHeaderFrame.size.height = DeviceWidth / 3;
    self.tvHeader.frame = tvHeaderFrame;
    [self.tv setTableHeaderView:self.tvHeader];
    self.tvHeader.hidden = YES;
    
    // 添加滚动广告
    CGFloat adHeight = DeviceWidth / 3;
    self.imageLoopView = [[YKuImageInLoopScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, adHeight)];
    self.imageLoopView.delegate = self;
    // 设置yKuImageInLoopScrollView显示类型
    self.imageLoopView.scrollViewType = ScrollViewDefault;
    // 设置styledPageControl位置
    [self.imageLoopView.styledPageControl setPageControlSite:PageControlSiteMiddle];
    [self.imageLoopView.styledPageControl setBottomDistance:12];
    // 设置styledPageControl已选中下的内心圆颜色
    [self.imageLoopView.styledPageControl setCoreSelectedColor:MAIN_COLOR];
    [self.imageLoopView.styledPageControl setCoreNormalColor:[UIColor whiteColor]];
    [self.tvHeader addSubview:self.imageLoopView];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    [self requestAdListData];
    [self requestDemandListData];
    [self requestRewardListData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)onPageShowChange:(BOOL)isShow {
    self.tv.scrollsToTop = isShow;
}

#pragma mark - 接口调用
// 获取需求广告列表
- (void)requestAdListData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getDemandSearchAdList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_AD;
    [conn start];
}

// 悬赏列表的第一页列表
- (void)requestRewardListData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getPaidDemandListTopN] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_REWARD;
    [conn start];
}

// 需求第一页列表
- (void)requestDemandListData {
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getDemandListByQuery:@"" pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DEMAND;
    [conn start];
}

// 加载更多需求数据
- (void)requestMoreData {
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getDemandListByQuery:@"" pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DEMAND_MORE;
    [conn start];
    return;
}

#pragma mark - 点击事件
// xx按钮点击
- (IBAction)btnXxOnClick:(UIButton *)sender {
    return;
}

#pragma mark - UITableViewDataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger secCount = 0;
    if (self.rewardArray.count > 0) {
        secCount += 1;
    }
    if (self.dataArray.count > 0) {
        secCount += 1;
    }
    return secCount;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        {
            // 悬赏或需求
            if (self.rewardArray.count > 0) {
                return self.rewardArray.count + 1;
            } else {
                return self.dataArray.count;
            }
        }
        case 1:
            // 需求
            return self.dataArray.count;
            
        default:
            return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 悬赏或需求
            if (self.rewardArray.count > 0) {
                if (indexPath.row >= self.rewardArray.count) {
                    return [ZbSeeMoreTableViewCell getRowHeight:NO];
                }
                break;
            }
            break;
        }
        case 1:
            // 需求
            break;
            
        default:
            break;
    }
    return [ZbSupplyDemandTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 悬赏或需求
            if (self.rewardArray.count > 0) {
                // 悬赏
                if (indexPath.row >= self.rewardArray.count) {
                    // 查看更多
                    ZbSeeMoreTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SeeMoreFooterIdentifier];
                    cell.labDes.text = @"查看更多悬赏找石";
                    cell.clickBlock = ^(void) {
                        ZbRewardContainerViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardContainerViewController"];
                        [self.navigationController pushViewController:controller animated:YES];
                    };
                    return cell;
                }
                // 悬赏
                ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
                [cell setPostDict:[self.rewardArray objectAtIndex:indexPath.row]];
                return cell;
            } else {
                // 需求
                ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
                [cell setPostDict:[self.dataArray objectAtIndex:indexPath.row]];
                return cell;
            }
        }
        case 1:
        {
            // 需求
            ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
            [cell setPostDict:[self.dataArray objectAtIndex:indexPath.row]];
            return cell;
        }
        default:
        {
            ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
            [cell setPostDict:[self.dataArray objectAtIndex:indexPath.row]];
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            // 悬赏或需求
            if (self.rewardArray.count > 0) {
                if (indexPath.row >= self.rewardArray.count) {
                    // 查看更多
                    return;
                }
                // 悬赏
                NSDictionary *curDict = [self.rewardArray objectAtIndex:indexPath.row];
                ZbRewardDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardDetailViewController"];
                controller.curPostId = curDict[@"post_id"];
                [self.navigationController pushViewController:controller animated:YES];
                break;
            } else {
                // 需求
                ZbSupplyDemandDetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
                NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
                detail.curPostId = curDict[@"post_id"];
                [self.navigationController pushViewController:detail animated:YES];
                break;
            }
        }
        case 1:
        {
            // 需求
            ZbSupplyDemandDetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
            NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
            detail.curPostId = curDict[@"post_id"];
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 重置选项
    [self requestAdListData];
    [self requestDemandListData];
    [self requestRewardListData];
}

- (void)footerRefresh {
    [self requestMoreData];
}

#pragma mark - YKuImageInLoopScrollViewDelegate
- (int)numOfPageForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    return (int)self.adArray.count;
}

- (int)widthForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    int width = DeviceWidth;
    return width;
}

- (UIView *)scrollView:(YKuImageInLoopScrollView *)ascrollView viewAtPageIndex:(int)apageIndex
{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    UIImageView *curImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    NSDictionary *adDict = [self.adArray objectAtIndex:apageIndex];
    NSString *imgUrl = adDict[@"ad_image"];
    [curImageView sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]]];
    curImageView.contentMode = UIViewContentModeScaleToFill;
    [bgView addSubview:curImageView];
    return bgView;
}

- (void)scrollView:(YKuImageInLoopScrollView*) ascrollView didTapIndex:(int)apageIndex{
//    DLog(@"Clicked page%d",apageIndex);
    NSDictionary *adDict = [self.adArray objectAtIndex:apageIndex];
    ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
    controller.companyId = adDict[@"company_id"];
    [self.navigationController pushViewController:controller animated:YES];
    
}

/*
 选中第几页
 @param didSelectedPageIndex 选中的第几项，[0-numOfPageForScrollView];
 */
-(void) scrollView:(YKuImageInLoopScrollView*) ascrollView didSelectedPageIndex:(int) apageIndex
{
    //    NSLog(@"didSelectedPageIndex:%d",apageIndex);
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    [self.hud hide:NO];
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    NSMutableArray *dataArray = resultDic[@"data"];
    if (connection.tag == HTTP_REQUEST_TAG_AD) {
        [self.adArray removeAllObjects];
        [self.adArray addObjectsFromArray:dataArray];
        
        [self.imageLoopView reloadData];
//        [self requestRewardListData];
    } else if (connection.tag == HTTP_REQUEST_TAG_REWARD) {
        NSMutableArray *tagArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.rewardArray removeAllObjects];
        [self.rewardArray addObjectsFromArray:tagArray];
        
        [self.tv reloadData];
//        [self requestDemandListData];
    } else if (connection.tag == HTTP_REQUEST_TAG_DEMAND) {
        NSMutableArray *tagArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:tagArray];
        if (tagArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        
        self.tvHeader.hidden = NO;
        [self.tv reloadData];
        if (self.dataArray.count > 0) {
            CGRect rectTop = CGRectMake(0, 0, 1, 1);
            [self.tv scrollRectToVisible:rectTop animated:YES];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_DEMAND_MORE) {
        // 上拉加载更多
        [self.tv.footer endRefreshing];
        NSMutableArray *tagArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.dataArray addObjectsFromArray:tagArray];
        if (tagArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
