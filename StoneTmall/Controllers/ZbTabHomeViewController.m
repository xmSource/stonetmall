//
//  ZbHomeViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTabHomeViewController.h"
#import "YKuImageInLoopScrollView.h"
#import "UIImageView+WebCache.h"
#import "CTScannerController.h"
#import "LocationServiceManager.h"
#import "ZbSelectCityViewController.h"
#import "ZbSearchViewController.h"
#import "ZbSupplyDemandTableViewCell.h"
#import "ZbSupplyDemandViewController.h"
#import "ZbRewardContainerViewController.h"
#import "ZbQyDetailViewController.h"
#import "ZbQyListTableViewCell.h"
#import "ZbProductDetailViewController.h"
#import "ZbStoneDetailViewController.h"
#import "ZbPubSupViewController.h"
#import "ZbPubProductViewController.h"
#import "ZbSupplyDemandDetailViewController.h"
#import "ZbRewardDetailViewController.h"
#import "MainViewController.h"
#import "AppDelegate.h"
#import "ConsWebController.h"
#import "ZbPersonInfoViewController.h"
#import "ZbRecommendQyViewController.h"

#define HTTP_REQUEST_COMPANY 0        // 品牌列表
#define HTTP_REQUEST_AD 1             // 广告列表
#define HTTP_REQUEST_NEWS 2           // 行业头条
#define HTTP_REQUEST_S_AND_D 3        // 供需列表
#define HTTP_REQUEST_PRODUCT 4        // 推荐品种

#define MAX_AD_COUNT 6

@interface ZbTabHomeViewController () <UITableViewDataSource, UITableViewDelegate, YKuImageInLoopScrollViewDelegate, PopoverViewDelegate, CTScannerControllerDelegate>

// 当前城市名
@property (nonatomic, strong) IBOutlet UILabel *labCurCity;
// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 悬赏按钮
@property (nonatomic, strong) IBOutlet UIControl *controlReward;
// 悬赏按钮height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcControlRewardWidth;
// 搜索背景
@property (nonatomic, strong) IBOutlet UIView *searchBg;
// 导航栏左
@property (nonatomic, strong) IBOutlet UIBarButtonItem *leftNavItem;
// 导航栏左容器
@property (nonatomic, strong) IBOutlet UIView *leftNavItemView;
// 导航栏右
@property (nonatomic, strong) IBOutlet UIBarButtonItem *rightNavItem;

// 广告滚动控件
@property (nonatomic, strong) YKuImageInLoopScrollView *imageLoopView;
// 添加弹出框
@property (nonatomic, strong) PopoverView *pv;

// 广告数组
@property (nonatomic, strong) NSMutableArray *adArray;
// 推荐石材公司数组
@property (nonatomic, strong) NSMutableArray *companyArray;
// 推荐石材品种品种数组
@property (nonatomic, strong) NSMutableArray *productArray;
// 供需信息数组
@property (nonatomic, strong) NSMutableArray *supplyArray;
// 行业头条数组
@property (nonatomic, strong) NSMutableArray *newsArray;

// 是否将弹出登录界面
@property (nonatomic, assign) BOOL willPresentLogin;

@end

@implementation ZbTabHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.adArray = Mutable_ArrayInit;
    self.companyArray = Mutable_ArrayInit;
    self.productArray = Mutable_ArrayInit;
    self.supplyArray = Mutable_ArrayInit;
    self.newsArray = Mutable_ArrayInit;
    
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv setTableFooterView:[[UIView alloc] init]];
    
    // 设置tableview头部高度
    CGRect tvHeaderFrame = self.tvHeader.frame;
    tvHeaderFrame.size.height = tvHeaderFrame.size.height + DeviceWidth / 3;
    self.tvHeader.frame = tvHeaderFrame;
    [self.tv setTableHeaderView:self.tvHeader];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSupplyDemandTableViewCell" bundle:nil] forCellReuseIdentifier:SupplyDemandCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbQyListTableViewCell" bundle:nil] forCellReuseIdentifier:QyListCellIdentifier];
    
    // 圆角、边框
    self.searchBg.layer.cornerRadius = 22;
    self.searchBg.layer.masksToBounds = YES;
    self.searchBg.layer.borderColor = UIColorFromHexString(@"999999").CGColor;
    self.searchBg.layer.borderWidth = 1;
    self.rightNavItem.tintColor = UIColorFromHexString(@"999999");
    self.leftNavItemView.layer.cornerRadius = 10;
    self.leftNavItemView.layer.masksToBounds = YES;
    self.leftNavItemView.layer.borderColor = UIColorFromHexString(@"999999").CGColor;
    self.leftNavItemView.layer.borderWidth = 1;
    
    // 添加滚动广告
    CGFloat adHeight = DeviceWidth / 3;
    self.imageLoopView = [[YKuImageInLoopScrollView alloc]initWithFrame:CGRectMake(0, tvHeaderFrame.size.height - adHeight, DeviceWidth, adHeight)];
    self.imageLoopView.delegate = self;
    // 设置yKuImageInLoopScrollView显示类型
    self.imageLoopView.scrollViewType = ScrollViewDefault;
    // 设置styledPageControl位置
    [self.imageLoopView.styledPageControl setPageControlSite:PageControlSiteMiddle];
    [self.imageLoopView.styledPageControl setBottomDistance:12];
    // 设置styledPageControl已选中下的内心圆颜色
    [self.imageLoopView.styledPageControl setCoreSelectedColor:MAIN_COLOR];
    [self.imageLoopView.styledPageControl setCoreNormalColor:[UIColor whiteColor]];
    [self.tvHeader addSubview:self.imageLoopView];
    
//    // 悬赏默认不显示
//    self.controlReward.hidden = YES;
//    self.alcControlRewardWidth.constant = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    // 加载首页信息
    [self requestCompanyData];
    [self requestAdData];
    [self requestProductData];
    [self requestSupplyDemandData];
    [self requestNewsData];
    
    // 当前城市名
    BMKPoiInfo *curPoi = [[LocationServiceManager shareManager] getCurLocationPoiInfo];
    NSString *city = curPoi ? curPoi.city : @"未知城市";
    self.labCurCity.text = city;
    [[LocationServiceManager shareManager] updateLocationBlock:^(void) {
        BMKPoiInfo *curPoi = [[LocationServiceManager shareManager] getCurLocationPoiInfo];
        NSString *city = curPoi ? curPoi.city : @"未知城市";
        self.labCurCity.text = city;
        [[LocationServiceManager shareManager] updateLocationBlock:nil];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.titleView = nil;
    self.tabBarController.title = @"";
    self.tabBarController.navigationItem.leftBarButtonItem = self.leftNavItem;
    self.tabBarController.navigationItem.rightBarButtonItem = self.rightNavItem;
    
    // 白色nav，黑色状态栏
    self.tabBarController.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // 登录界面的标题栏颜色由其自行处理
    if (self.willPresentLogin) {
        self.willPresentLogin = NO;
        return;
    }
    
    // 蓝色色nav，白色状态栏
    self.tabBarController.navigationController.navigationBar.barTintColor = MAIN_COLOR;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

// 悬赏开关绘制
- (void)setRewardOpenStatus {
    // 首页顶部按钮已变化，无工序按钮，不用控制了
    return;
//    BOOL isShow = [ZbSaveManager isPaiddemandShow];
//    if (isShow) {
//        self.controlReward.hidden = NO;
//        UIView *container = self.controlReward.superview.superview;
//        self.alcControlRewardWidth.constant = CGRectGetWidth(container.frame) / 4;
//    } else {
//        self.controlReward.hidden = YES;
//        self.alcControlRewardWidth.constant = 0;
//    }
}

#pragma mark - 点击事件
// 选择城市点击
- (IBAction)btnSelectAddrOnClick:(id)sender {
    return;
//    ZbSelectCityViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSelectCityViewController"];
//    controller.selectBlock = ^(NSString *city) {
//        self.labCurCity.text = city;
//    };
//    [self.navigationController pushViewController:controller animated:YES];
}

// 添加按钮点击
- (IBAction)btnAddOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        self.willPresentLogin = YES;
        self.hud.labelText = @"尚未登录，无法操作";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
        }];
        return;
    }
    CGPoint point = CGPointMake(DeviceWidth - 10, 60);
//    self.pv = [PopoverView showPopoverAtPoint:point
//                                  inView:self.navigationController.view
//                         withStringArray:@[@"发布产品", @"发布供求", ]
//                                     delegate:self];
    self.pv = [PopoverView showPopoverAtPoint:point
                                       inView:self.navigationController.view
                              withStringArray:@[@"发布产品", @"发布供求", @"扫一扫", ]
                                     delegate:self];
}

// 搜索按钮点击
- (IBAction)btnSearchOnClick:(id)sender {
    ZbSearchViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSearchViewController"];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

// 石材图库按钮点击
- (IBAction)btnStoneOnClick:(id)sender {
    [self.tabBarController setSelectedIndex:2];
    return;
}

// 产品按钮点击
- (IBAction)btnProductOnClick:(id)sender {
    [self.tabBarController setSelectedIndex:1];
    return;
}

// 供求信息按钮点击
- (IBAction)btnSupplyOnClick:(id)sender {
    // 暂为企业
    [self.tabBarController setSelectedIndex:3];
//    ZbSupplyDemandViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
    return;
}

// 悬赏找石按钮点击
- (IBAction)btnRewardOnClick:(id)sender {
    // 暂为供需
    ZbSupplyDemandViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandViewController"];
    [self.navigationController pushViewController:controller animated:YES];
//    ZbRewardContainerViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardContainerViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
    return;
}

// section头点击
- (void)btnSectionOnClick:(ZbIndexButton *)btnSection {
    if (btnSection.index == 0) {
        // 品牌
        ZbRecommendQyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRecommendQyViewController"];
        controller.array = self.companyArray;
        controller.title = @"推荐品牌";
        [self.navigationController pushViewController:controller animated:YES];
    } else if (btnSection.index == 1) {
        // 推荐品种
        ZbRecommendQyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRecommendQyViewController"];
        controller.array = self.productArray;
        controller.title = @"推荐品种";
        [self.navigationController pushViewController:controller animated:YES];
    } else if (btnSection.index == 2) {
        // 供需
        ZbSupplyDemandViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    return;
}

#pragma mark - 接口调用
// 获取品牌列表接口
- (void)requestCompanyData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyShowAdList] delegate:self];
    conn.tag = HTTP_REQUEST_COMPANY;
    [conn start];
    return;
}

// 获取广告列表接口
- (void)requestAdData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCoverRollAdList] delegate:self];
    conn.tag = HTTP_REQUEST_AD;
    [conn start];
    return;
}

// 获取行业头条列表接口
- (void)requestNewsData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getLatestNewsList] delegate:self];
    conn.tag = HTTP_REQUEST_NEWS;
    [conn start];
    return;
}

// 获取供需列表接口
- (void)requestSupplyDemandData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getLatestSupplyDemandList] delegate:self];
    conn.tag = HTTP_REQUEST_S_AND_D;
    [conn start];
    return;
}

// 获取推荐品种列表接口
- (void)requestProductData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getNewProductAdList] delegate:self];
    conn.tag = HTTP_REQUEST_PRODUCT;
    [conn start];
    return;
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
        {
            // 公司品牌
            NSArray *showArray = [self.companyArray subarrayWithRange:NSMakeRange(0, MIN(MAX_AD_COUNT, self.companyArray.count))];
            return [ZbQyListTableViewCell getRowCount:showArray];
        }
        case 1:
        {
            // 推荐品种
            NSArray *showArray = [self.productArray subarrayWithRange:NSMakeRange(0, MIN(MAX_AD_COUNT, self.productArray.count))];
            return [ZbQyListTableViewCell getRowCount:showArray];
        }
        case 2:
            return self.supplyArray.count;
            break;
        case 3:
            return self.newsArray.count;
            break;
            
        default:
            return 0;
            break;
    }
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        case 1:
        {
            // 公司品牌，推荐品种
            return [ZbQyListTableViewCell getRowHeight];
        }
        case 2:
            // 供求信息高度
            return [ZbSupplyDemandTableViewCell getRowHeight];
        case 3:
            // 行业头条高度
            if (indexPath.row == 0) {
                return 110;
            }
            return 37;
            
        default:
            return 0;
    }
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case 0:
        case 1:
        {
            // 石材公司品牌、推荐品种
            ZbQyListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:QyListCellIdentifier];
            NSArray *dataArray = indexPath.section == 0 ? self.companyArray : self.productArray;
            [cell setInfoRow:indexPath.row dataArray:dataArray];
            cell.iconClickBlock = ^(NSInteger index, NSDictionary *infoDict) {
                ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
                controller.companyId = infoDict[@"company_id"];
                [self.navigationController pushViewController:controller animated:YES];
            };
            return cell;
        }
        case 2:
        {
            // 供求信息cell
            ZbSupplyDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandCellIdentifier];
            [cell setPostDict:[self.supplyArray objectAtIndex:indexPath.row]];
            return cell;
        }
        case 3:
            // 行业头条信息cell
        {
            NSDictionary *curNews = [self.newsArray objectAtIndex:indexPath.row];
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"newsFirstCell" forIndexPath:indexPath];
                // 置顶新闻
                UIImageView *ivIcon = (UIImageView *)[cell viewWithTag:1];
                ivIcon.clipsToBounds = YES;
                UILabel *labTitle = (UILabel *)[cell viewWithTag:2];
                UILabel *labContent = (UILabel *)[cell viewWithTag:3];
                UILabel *labDate = (UILabel *)[cell viewWithTag:4];
                
                // 图片
                NSString *imageUrl = curNews[@"post_image"];
                [ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imageUrl]]];
                // 标题
                labTitle.text = curNews[@"post_title"];
                // 内容
                labContent.text = curNews[@"post_content"];
                // 时间
                labDate.text = [CommonCode getTimeShowText:curNews[@"post_modified"]];
            } else {
                // 普通新闻
                cell = [tableView dequeueReusableCellWithIdentifier:@"newsCell" forIndexPath:indexPath];
                UILabel *labTitle = (UILabel *)[cell viewWithTag:1];
                UILabel *labDate = (UILabel *)[cell viewWithTag:2];
                // 标题
                labTitle.text = curNews[@"post_title"];
                // 时间
                labDate.text = [CommonCode getTimeShowText:curNews[@"post_modified"]];
            }
            break;
        }
        default:
            break;
    }
    
    return cell;
}

// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
        case 2:
            return 54;
        case 3:
            return 54;
            
        default:
            return 0;
    }
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *headerCell;
    headerCell = [tableView dequeueReusableCellWithIdentifier:@"sectionHeaderCell"];
    // section名控件
    UILabel *labSectionName = (UILabel *)[headerCell viewWithTag:1];
    // 底部线
    UIView *line = (UIView *)[headerCell viewWithTag:2];
    // 箭头
    UIImageView *arrow = (UIImageView *)[headerCell viewWithTag:3];
    // 按钮
    ZbIndexButton *btnSection = (ZbIndexButton *)[headerCell viewWithTag:4];
    btnSection.index = section;
    [btnSection addTarget:self action:@selector(btnSectionOnClick:) forControlEvents:UIControlEventTouchUpInside];
    switch (section) {
        case 0:
        {
            labSectionName.text = @"品牌";
            line.hidden = YES;
            arrow.hidden = NO;
            btnSection.enabled = YES;
            break;
        }
        case 1:
        {
            labSectionName.text = @"推荐品种";
            line.hidden = YES;
            arrow.hidden = NO;
            btnSection.enabled = YES;
            break;
        }
        case 2:
        {
            labSectionName.text = @"最新供求信息";
            line.hidden = NO;
            arrow.hidden = NO;
            btnSection.enabled = YES;
            break;
        }
        case 3:
        {
            labSectionName.text = @"行业头条";
            line.hidden = NO;
            arrow.hidden = YES;
            btnSection.enabled = NO;
            break;
        }
            
        default:
            break;
    }
    return headerCell;
}

// section尾部高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
            return 6;
        case 2:
            return 0;
        case 3:
            return 3;
            
        default:
            return 0;
    }
}

// section尾部view
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UITableViewCell *footerCell;
    switch (section) {
        case 0:
        case 1:
        {
            footerCell = [tableView dequeueReusableCellWithIdentifier:@"sectionFooterCell"];
            break;
        }
        case 2:
            break;
        case 3:
            footerCell = [tableView dequeueReusableCellWithIdentifier:@"newsFooterCell"];
            break;
            
        default:
            break;
    }
    return footerCell;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 2:
        {
            // 供求信息
            NSDictionary *curPost = [self.supplyArray objectAtIndex:indexPath.row];
            NSString *postType = curPost[@"post_type"];
            if (postType.intValue == Zb_Home_Message_Type_Reward) {
                // 悬赏
                ZbRewardDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbRewardDetailViewController"];
                controller.curPostId = curPost[@"post_id"];
                [self.navigationController pushViewController:controller animated:YES];
            } else {
                // 供需
                ZbSupplyDemandDetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
                detail.curPostId = curPost[@"post_id"];
                [self.navigationController pushViewController:detail animated:YES];
            }
            break;
        }
        case 3:
        {
            // 行业头条
            NSDictionary *curPost = [self.newsArray objectAtIndex:indexPath.row];
            ZbSupplyDemandDetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
            detail.curPostId = curPost[@"post_id"];
            detail.isNews = YES;
            [self.navigationController pushViewController:detail animated:YES];
            break;
        }
            
        default:
            break;
    }
    return;
}

#pragma mark - 上拉或下拉刷新
/**
 *  下拉刷新
 */
- (void)headerRefresh {
    [self requestCompanyData];
    [self requestAdData];
    [self requestProductData];
    [self requestSupplyDemandData];
    [self requestNewsData];
}

#pragma mark - YKuImageInLoopScrollViewDelegate
- (int)numOfPageForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    return (int)self.adArray.count;
}

- (int)widthForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    int width = DeviceWidth;
    return width;
}

- (UIView *)scrollView:(YKuImageInLoopScrollView *)ascrollView viewAtPageIndex:(int)apageIndex
{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    UIImageView *curImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    NSDictionary *adDict = [self.adArray objectAtIndex:apageIndex];
    NSString *imgUrl = [ZbWebService getImageFullUrl:adDict[@"ad_image"]];
    [curImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    curImageView.contentMode = UIViewContentModeScaleToFill;
    [bgView addSubview:curImageView];
    return bgView;
}

- (void)scrollView:(YKuImageInLoopScrollView*) ascrollView didTapIndex:(int)apageIndex{
//    DLog(@"Clicked page%d",apageIndex);
    ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
    NSDictionary *infoDict = [self.adArray objectAtIndex:apageIndex];
    controller.companyId = infoDict[@"company_id"];
    [self.navigationController pushViewController:controller animated:YES];
    
}

/*
 选中第几页
 @param didSelectedPageIndex 选中的第几项，[0-numOfPageForScrollView];
 */
-(void) scrollView:(YKuImageInLoopScrollView*) ascrollView didSelectedPageIndex:(int) apageIndex
{
    //    NSLog(@"didSelectedPageIndex:%d",apageIndex);
}

#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            // 发布产品
            if (![ZbWebService sharedQy]) {
                self.hud.labelText = @"企业用户才能发布产品";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            } else {
                UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
                ZbPubProductViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbPubProductViewController"];
                [self.navigationController pushViewController:controller animated:YES];
            }
            break;
        }
        case 1:
        {
            // 发布供求
//            if (![ZbWebService sharedQy]) {
//                self.hud.labelText = @"企业用户才能发布供求";
//                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
//            } else {
                UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
                ZbPubSupViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbPubSupViewController"];
                [self.navigationController pushViewController:controller animated:YES];
//            }
            break;
        }
        case 2:
        {
            // 扫一扫
            CTScannerController *scan = [[CTScannerController alloc] initWithNibName:nil bundle:nil];
            scan.delegate = self;
            [self.navigationController pushViewController:scan animated:YES];
            break;
        }
        default:
            break;
    }
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.1f];
}

- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    self.pv = nil;
}

#pragma mark - 扫码delegate
- (void)ctScannerController:(CTScannerController *)controller didReceivedScanResult:(NSString *)result {
    [self.navigationController popViewControllerAnimated:NO];
    
    if ([result hasPrefix:@"http://"] || [result hasPrefix:@"https://"]) {
        // 网页
        ConsWebController *webController = [[ConsWebController alloc] init];
        webController.mainUrl = result;
        [self.navigationController pushViewController:webController animated:YES];
    } else if ([result hasPrefix:@"stonetmall://"]) {
        // 其它信息
        NSString *others = [result stringByReplacingOccurrencesOfString:@"stonetmall://" withString:@""];
        NSArray *othersArray = [others componentsSeparatedByString:@"/"];
        if (othersArray.count != 3) {
            // 错误数据
            self.hud.labelText = @"不支持的二维码类型";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        NSString *type = [othersArray firstObject];
        NSString *idInfo = [othersArray lastObject];
        if ([type isEqualToString:@"company"]) {
            // 企业
            ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
            controller.companyId = idInfo;
            [self.navigationController pushViewController:controller animated:YES];
        } else if ([type isEqualToString:@"product"]) {
            // 产品
            ZbProductDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
            controller.productId = idInfo;
            [self.navigationController pushViewController:controller animated:YES];
        } else if ([type isEqualToString:@"stone"]) {
            // 石材
//            ZbSupplyDemandDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbSupplyDemandDetailViewController"];
//            controller.curPostId = idInfo;
//            [self.navigationController pushViewController:controller animated:YES];
        } else if ([type isEqualToString:@"user"]) {
            // 用户
            ZbPersonInfoViewController *person = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbPersonInfoViewController"];
            person.userName = idInfo;
            [self.navigationController pushViewController:person animated:YES];
        } else {
            self.hud.labelText = @"不支持的二维码类型";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        }
    } else {
        // 不支持类型
        self.hud.labelText = @"不支持的二维码类型";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv reloadData];
    [self.imageLoopView reloadData];
    
    [self.hud hide:NO];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    [self.hud hide:NO];
    // 任一加载失败，中断请求，重刷数据
    if (result.integerValue != 0) {
        [self.tv.header endRefreshing];
        [self.tv reloadData];
        [self.imageLoopView reloadData];
        
        return;
    }
    
    if (connection.tag == HTTP_REQUEST_COMPANY) {
        // 品牌列表
        NSArray *dataArray = resultDic[@"data"];
        // 添加品牌数据
        [self.companyArray removeAllObjects];
        [self.companyArray addObjectsFromArray:dataArray];
        
        [self.tv.header endRefreshing];
        [self.tv reloadData];
//        // 请求下一个数据
//        [self requestAdData];
    } else if (connection.tag == HTTP_REQUEST_AD) {
        // 广告
        NSMutableArray *dataArray = resultDic[@"data"];
        // 添加广告数据
        [self.adArray removeAllObjects];
        [self.adArray addObjectsFromArray:dataArray];
        
        [self.imageLoopView reloadData];
//        // 请求下一个数据
//        [self requestProductData];
    } else if (connection.tag == HTTP_REQUEST_NEWS) {
        // 头条
        NSMutableArray *dataArray = resultDic[@"data"];
        // 添加头条数据
        [self.newsArray removeAllObjects];
        [self.newsArray addObjectsFromArray:dataArray];
        
        // 获取完全部数据，重刷界面
        [self.tv.header endRefreshing];
        [self.tv reloadData];
//        [self.imageLoopView reloadData];
        
//        [self.hud hide:NO];
    } else if (connection.tag == HTTP_REQUEST_S_AND_D) {
        // 供需
        NSMutableArray *dataArray = resultDic[@"data"];
        // 添加供需数据
        [self.supplyArray removeAllObjects];
        [self.supplyArray addObjectsFromArray:dataArray];
        
        [self.tv.header endRefreshing];
        [self.tv reloadData];
//        // 请求下一个数据
//        [self requestNewsData];
    } else if (connection.tag == HTTP_REQUEST_PRODUCT) {
        // 推荐品种
        NSArray *dataArray = resultDic[@"data"];
        // 添加推荐品种数据
        [self.productArray removeAllObjects];
        [self.productArray addObjectsFromArray:dataArray];
        
        [self.tv.header endRefreshing];
        [self.tv reloadData];
//        [self requestSupplyDemandData];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
