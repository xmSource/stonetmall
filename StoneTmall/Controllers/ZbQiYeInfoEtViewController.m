//
//  ZbQiYeInfoEtViewController.m
//  StoneTmall
//
//  Created by chyo on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQiYeInfoEtViewController.h"
#import "ZbQyEtCommViewController.h"
#import "ZbQyLocateViewController.h"
#import "ZbShipAdrViewController.h"
#import "ZbContantViewController.h"
#import "ZbBeVipViewController.h"
#import "ZbLinkedStoneViewController.h"
#import "ZbCompanyTypeViewController.h"
#import "ChatViewController.h"
#import "RSKImageCropper.h"

#define EDIT_COMPANYLOGO 1
#define EDIT_COMPANYIMAGES 2
#define DEL_COMPANYIMAGES 3
#define EDIT_COMPLICENSE 4

#define HTTP_DEL_IMAGE 4
#define HTTP_GET_COMPANYINFO 5
#define HTTP_GET_COMPANYIMAGELIST 6
#define HTTP_ADD_COMPANYIMAGE 7
#define HTTP_GET_COMPANYLIMIT 8
#define HTTP_GET_COMPANYLICENLIMIT 9
#define HTTP_GET_COMPANYSTONE 10
#define HTTP_EDIT_COMPANY_LOCATION 11

@interface ZbQiYeInfoEtViewController ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tv;
// 底部按钮容器view
@property (nonatomic, strong) IBOutlet UIView *bottomView;
// 底部按钮容器heihgt约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcBottomViewHeight;
@property (nonatomic, strong) IBOutlet UIButton *finshbtn;
@property (nonatomic, strong) IBOutlet UIButton *laterbtn;
@property (nonatomic, strong) UIActionSheet *actionsheet;
@property (nonatomic, strong) UIImageView *companyImgs;  //封面图片
@property (nonatomic, strong) UITapGestureRecognizer *tapGes;

@property (nonatomic, assign) int imgLimit;
@property (nonatomic, strong) NSMutableArray *imgDataArray;  //有封面的图片数组
@property (nonatomic) int clickIndex;
@property (nonatomic, strong) NSString *delImageId;

@property (nonatomic, strong) NSMutableArray *stoneArray;  // 主营石材

@property (nonatomic) BOOL isHaveImg;

@end

@implementation ZbQiYeInfoEtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imgLimit = 0;
    self.imgDataArray = [[NSMutableArray alloc] init];
    self.stoneArray = [NSMutableArray array];
    self.delImageId = @"";
    self.clickIndex = -1;
    self.isHaveImg = NO;
    
    // 显隐底部按钮view
    BOOL isHideBottomView = self.curEditType == Zb_Qy_Edit_Type_Default;
    if (isHideBottomView) {
        self.bottomView.hidden = YES;
        self.alcBottomViewHeight.constant = 0;
    } else {
        self.bottomView.hidden = NO;
        self.alcBottomViewHeight.constant = 45;
        // 隐藏返回按钮
        self.navigationItem.leftBarButtonItem.title = @" ";
        self.navigationItem.leftBarButtonItem.image = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.finshbtn.layer.cornerRadius = 2.0f;
    self.finshbtn.layer.masksToBounds = YES;
    self.laterbtn.layer.cornerRadius = 2.0f;
    self.laterbtn.layer.masksToBounds = YES;
    [self loadData];
}

- (void)beVip:(UIButton *)btn {
    ZbBeVipViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbBeVipViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 点击事件
// 返回按钮点击
- (IBAction)leftBarItemOnClick:(id)sender {
    if (self.curEditType == Zb_Qy_Edit_Type_OnCreate) {
        // 创建企业后直接编辑资料，不让返回
        return;
    }
    [super leftBarItemOnClick:sender];
}

// 图片点击
- (void)imgClicked:(UITapGestureRecognizer *)tapGes {
    [self.view endEditing:YES];
    UIImageView *imgView = (UIImageView*)[self.view viewWithTag:tapGes.view.tag];
    if (![imgView.image isEqual:[UIImage imageNamed:@"public_pic_add"]]) {
        self.actionsheet = [[UIActionSheet alloc] initWithTitle:@"确定删除图片？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"删除图片", nil];
        NSDictionary *dict = [self.imgDataArray objectAtIndex:tapGes.view.tag - 100];
        self.clickIndex = (int)tapGes.view.tag;
        self.delImageId = dict[@"image_id"];
        self.actionsheet.tag = DEL_COMPANYIMAGES;
        [self.actionsheet showInView:self.navigationController.view];
        return;
    }
    self.actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册选择", nil];
    self.clickIndex = (int)tapGes.view.tag;
    self.actionsheet.tag = EDIT_COMPANYIMAGES;
    [self.actionsheet showInView:self.navigationController.view];
    return;
}

// 完成按钮点击
- (IBAction)btnCompleteOnClick:(id)sender {
    UIViewController *present = self.navigationController.presentingViewController;
    if (present != nil) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
    return;
}

// 客服电话点击
- (IBAction)btnAppPhoneOnClick:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", APP_CONTACT_PHONE]]];
    return;
}

// 微聊电话点击
- (IBAction)btnChatOnClick:(id)sender {
    // 聊天系统登录判断
    if (![[[EaseMob sharedInstance] chatManager] isLoggedIn]) {
        self.hud.labelText = @"聊天系统数据正在加载中..请稍候";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    };
    // 客服
    NSDictionary *kefuDict = [ZbSaveManager getKefuDict];
    if (kefuDict == nil) {
        return;
    }
    NSString *name = kefuDict[@"user_name"];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:name isGroup:NO];
    [self.navigationController pushViewController:chatVC animated:YES];
    return;
}

#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    if (actionSheet.tag == EDIT_COMPANYLOGO) {
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.delegate = self;
        if (buttonIndex == 0) {
            // 拍照
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else {
            // 相册选择
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
        });
        return;

    } else if(actionSheet.tag == EDIT_COMPANYIMAGES) {
    
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.delegate = self;
        if (buttonIndex == 0) {
            // 拍照
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else {
            // 相册选择
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
        });
        return;
    } else if (actionSheet.tag == DEL_COMPANYIMAGES) {
        
        [self delImages];
        
    } else if (actionSheet.tag == EDIT_COMPLICENSE) {
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.delegate = self;
        if (buttonIndex == 0) {
            // 拍照
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else {
            // 相册选择
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
        });
        return;
    }
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        CGSize imgSize = portraitImg.size;
        
        self.hud.labelText = @"努力提交中..";
        [self.hud show:YES];
        
        CGSize tagSize = imgSize;
        CGFloat rate = imgSize.width / imgSize.height;
        if (imgSize.width >= imgSize.height) {
            if (imgSize.width > 1000) {
                tagSize = CGSizeMake(1000, 1000 / rate);
            }
        } else {
            if (imgSize.height > 1000) {
                tagSize = CGSizeMake(1000 * rate, 1000);
            }
        }
        
        // 去除图片旋转属性
        UIGraphicsBeginImageContext(tagSize);
        [portraitImg drawInRect:CGRectMake(0, 0, tagSize.width, tagSize.height)];
        portraitImg = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (self.actionsheet.tag == EDIT_COMPANYLOGO) {
            RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:portraitImg cropMode:RSKImageCropModeCustom];
            imageCropVC.delegate = self;
            imageCropVC.dataSource = self;
            imageCropVC.view.tag = EDIT_COMPANYLOGO;
            [self.navigationController pushViewController:imageCropVC animated:YES];
        } else if (self.actionsheet.tag == EDIT_COMPANYIMAGES) {
            RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:portraitImg cropMode:RSKImageCropModeCustom];
            imageCropVC.delegate = self;
            imageCropVC.dataSource = self;
            imageCropVC.view.tag = EDIT_COMPANYIMAGES;
            [self.navigationController pushViewController:imageCropVC animated:YES];
        } else if (self.actionsheet.tag == EDIT_COMPLICENSE) {
            UITableViewCell *cell = [self.tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
            UIImageView *imgLicense = (UIImageView *)[cell viewWithTag:2];
            imgLicense.image = portraitImg;
            self.isHaveImg = YES;
            [self uploadImage:portraitImg with:3];
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    return;
}

#pragma mark - RSKImageCropViewControllerDelegate
- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)imageCropViewController:(RSKImageCropViewController *)controller didCropImage:(UIImage *)croppedImage usingCropRect:(CGRect)cropRect
{
    if (controller.view.tag == EDIT_COMPANYLOGO) {
    
        UITableViewCell *cell = [self.tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        UIImageView *imgLogo = (UIImageView *)[cell viewWithTag:2];
        imgLogo.image = croppedImage;
        [self uploadImage:croppedImage with:1];
    } else if (controller.view.tag == EDIT_COMPANYIMAGES) {
    
        UITableViewCell *cell = [self.tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        UIScrollView *sv = (UIScrollView *)[cell viewWithTag:1];
        self.companyImgs = (UIImageView *)[sv viewWithTag:self.clickIndex];
        self.companyImgs.image = croppedImage;
        self.isHaveImg = YES;
        [self uploadImage:croppedImage with:2];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGRect)imageCropViewControllerCustomMaskRect:(RSKImageCropViewController *)controller
{
    CGFloat viewWidth = CGRectGetWidth(controller.view.frame);
    CGFloat viewHeight = CGRectGetHeight(controller.view.frame);
    
    CGSize maskSize;
    if (controller.view.tag == EDIT_COMPANYLOGO) {
        CGFloat tagWidth = viewWidth - 15 - 15;
        maskSize = CGSizeMake(tagWidth, tagWidth / 1.4);
    } else {
        CGFloat tagWidth = viewWidth - 15 - 15;
        maskSize = CGSizeMake(tagWidth, tagWidth / 1.6);
    }
    
    CGRect maskRect = CGRectMake((viewWidth - maskSize.width) * 0.5f,
                                 (viewHeight - maskSize.height) * 0.5f,
                                 maskSize.width,
                                 maskSize.height);
    
    return maskRect;
}

// Returns a custom path for the mask.
- (UIBezierPath *)imageCropViewControllerCustomMaskPath:(RSKImageCropViewController *)controller
{
    CGRect rect = controller.maskRect;
    CGPoint point1 = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPoint point2 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGPoint point3 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPoint point4 = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    
    UIBezierPath *triangle = [UIBezierPath bezierPath];
    [triangle moveToPoint:point1];
    [triangle addLineToPoint:point2];
    [triangle addLineToPoint:point3];
    [triangle addLineToPoint:point4];
    [triangle closePath];
    
    return triangle;
}

// Returns a custom rect in which the image can be moved.
- (CGRect)imageCropViewControllerCustomMovementRect:(RSKImageCropViewController *)controller
{
    // If the image is not rotated, then the movement rect coincides with the mask rect.
    return controller.maskRect;
}

#pragma mark - 读取数据
// 加载企业信息
- (void)loadData {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getBindCompanyInfo] delegate:self];
    conn.tag = HTTP_GET_COMPANYINFO;
    [conn start];
}

//加载最多上传图片限制
- (void)loadUpLoadImgLimit {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getAddCompanyImageLimit] delegate:self];
    conn.tag = HTTP_GET_COMPANYLIMIT;
    [conn start];

}
//是否允许启用企业营运执照认证
- (void)loadCompanyLicenceLimit {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getAddCompanyLicenceLimit] delegate:self];
    conn.tag = HTTP_GET_COMPANYLICENLIMIT;
    [conn start];
}

// 加载企业封面图
- (void)loadCompImgList {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getBindCompanyImageList] delegate:self];
    conn.tag = HTTP_GET_COMPANYIMAGELIST;
    [conn start];
}

// 加载主营石材
- (void)loadCompanyStoneList {
    ZbQyInfo *curinfo = [ZbWebService sharedQy];
    NSString *companyId = curinfo.company_id;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyStoneList:companyId] delegate:self];
    conn.tag = HTTP_GET_COMPANYSTONE;
    [conn start];
}

// 单个删除企业封面图
- (void)delImages {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService delBindCompanyImage:self.delImageId] delegate:self];
    conn.tag = HTTP_DEL_IMAGE;
    [conn start];
}

// 单个上传企业封面图
- (void)uploadImage:(UIImage *)image with:(NSInteger)tag{
    
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    NSDictionary *params;
    if (tag == 1) {
        // 清除logo缓存
        ZbQyInfo *info = [ZbWebService sharedQy];
        if (info.company_image.length > 0) {
            [[SDWebImageManager sharedManager].imageCache removeImageForKey:[ZbWebService getImageFullUrl:info.company_image] withCompletion:nil];
            [[SDWebImageManager sharedManager] removeFailedForURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:info.company_image]]];
        }
        params = [ZbWebService editCompanyLogo];
    } else if (tag == 2) {
        params = [ZbWebService addBindCompanyImage];
    } else if (tag == 3) {
        params = [ZbWebService bindCompanyLicense];
    }
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        // 图片压缩
        NSData* imageData = UIImageJPEGRepresentation(image, 0.4);
        NSString *fileName = [NSString stringWithFormat:@"sg_image"];
        
        // 图片文件流
        [formData appendPartWithFileData:imageData name:fileName fileName:fileName mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = @"修改失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        if (tag == 1) {
        } else if (tag == 2) {
            [self loadCompImgList];
        } else if (tag == 3) {
            // 审核地址变更
            ZbQyInfo *info = [ZbWebService sharedQy];
            info.company_licence = resultDic[@"data"][@"fullname"];
            info.company_licence_ok = [NSString stringWithFormat:@"%@", [NSNumber numberWithInt:Zb_User_Verify_Status_Status_Watting]];
        }
        self.hud.labelText = @"修改成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^{
            [self.tv reloadData];
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"修改失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
    return;
}

// 编辑企业位置
- (void)requestEditCompanyLocation:(CLLocationCoordinate2D)loc {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *lng = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:loc.longitude]];
    NSString *lat = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:loc.latitude]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editCompanyLocation:lng latitude:lat] delegate:self];
    conn.tag = HTTP_EDIT_COMPANY_LOCATION;
    conn.userInfo = [NSString stringWithFormat:@"%@|%@", lng, lat];
    [conn start];
}


#pragma mark - UITableViewDataSource
// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45.0f;
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"sectionHeaderCell" ];
    // 父控件名称
    UILabel *labSectionName = (UILabel *)[headerCell viewWithTag:1];
    UIButton *btnVip = (UIButton *)[headerCell viewWithTag:2];
    btnVip.layer.cornerRadius = 3.0f;
    btnVip.layer.masksToBounds = YES;
    [btnVip addTarget:self action:@selector(beVip:) forControlEvents:UIControlEventTouchUpInside];
    
    BOOL isVip = [ZbWebService isVipQy];

    switch (section) {
        case 0:
        {
            labSectionName.text = @"基本资料";
            btnVip.hidden = YES;
            break;
        }
        case 1:
        {
            labSectionName.text = [NSString stringWithFormat:@"企业封面图(最多上传%d张)", self.imgLimit];
            if (isVip) {
                btnVip.hidden = YES;
            } else {
                btnVip.hidden = NO;
            }
            if ([ZbSaveManager isCtrlHide]) {
                btnVip.hidden = YES;
            }
            break;
        }
        case 2:
        {
            labSectionName.text = @"关于企业";
            btnVip.hidden = YES;
            break;
        }
        case 3:
        {
            if (isVip) {
                labSectionName.text = @"VIP资料";
                labSectionName.text = @"企业认证";
                btnVip.hidden = YES;
            } else {
                labSectionName.text = @"VIP资料(需升级VIP方可填写)";
                labSectionName.text = @"企业认证";
                btnVip.hidden = NO;
            }
            if ([ZbSaveManager isCtrlHide]) {
                btnVip.hidden = YES;
            }
            break;
        }
    }
    return headerCell.contentView;
}

// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowofSec = 0;
    switch (section) {
        case 0:
            rowofSec = 7;
            break;
            
        case 1:
            rowofSec = 1;
            break;
            
        case 2:
            rowofSec = 3;
            break;
            
        case 3:
            rowofSec = 3;
            break;
            
        default:
            break;
    }
    return rowofSec;
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
            case 0:
                if (indexPath.row == 0) {
                    return  70;
                } else {
                    return  45;
                }
                break;

            
            case 1:
                return  90;
                break;
            
            case 2:
                return 65;
                break;

            case 3:
                if (indexPath.row == 0) {
                    return  70;
                } else if(indexPath.row == 1) {
                    return  45;
                } else {
                    return 60;
                }
                break;

        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    ZbQyInfo *info = [ZbWebService sharedQy];
    BOOL isVip = [ZbWebService isVipQy];
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"qiyeLogoCell" forIndexPath:indexPath];
                UILabel *labName = (UILabel *)[cell viewWithTag:1];
                UIImageView *imgLogo = (UIImageView *)[cell viewWithTag:2];
                UILabel *labPlace = (UILabel *)[cell viewWithTag:3];
                UILabel *falgLab = (UILabel *)[cell viewWithTag:88];
                if (info.company_image.length > 0) {
                    NSURL *logoUrl = [NSURL URLWithString:[ZbWebService getImageFullUrl:info.company_image] ];
                    [imgLogo sd_setImageWithURL:logoUrl];
                    imgLogo.layer.masksToBounds = YES;
                }
                imgLogo.layer.borderWidth = 1;
                imgLogo.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
                imgLogo.hidden = NO;
                labName.text = @"企业Logo";
                labName.textColor = UIColorFromHexString(@"333333");
                labPlace.hidden = YES;
                falgLab.hidden = YES;
            } else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"qiyeInfoCell" forIndexPath:indexPath];
                UILabel *labName = (UILabel *)[cell viewWithTag:1];
                UILabel *labCont = (UILabel *)[cell viewWithTag:2];
                UIImageView *forwardFlag = (UIImageView *)[cell viewWithTag:3];
                forwardFlag.hidden = NO;
                switch (indexPath.row) {
                    case 1:
                    {
                        labName.text = @"企业名称";
                        labCont.text = info.company_name;
                        forwardFlag.hidden = YES;
                        break;
                    }
                    case 2:
                    {
                        labName.text = @"地址";
                        labCont.text = [NSString stringWithFormat:@"%@%@", info.company_prov, info.company_city];
                        break;
                    }
                    case 3:
                    {
                        labName.text = @"位置";
                        info.company_latitude.intValue != 0 || info.company_longitude.intValue != 0 ? (labCont.text = @"已标注") : (labCont.text = @"未标注");
                        break;
                    }
                    case 4:
                    {
                        labName.text = @"联系人";
                        NSString *companyPhone = info.company_phone;
                        if (companyPhone != nil && companyPhone.length > 0) {
                            companyPhone = [[companyPhone componentsSeparatedByString:@","] firstObject];
                        }
                        labCont.text = [NSString stringWithFormat:@"%@ %@", info.company_contact, companyPhone];
                        labName.textColor = UIColorFromHexString(@"333333");
                        labCont.textColor = UIColorFromHexString(@"888888");
                        break;
                    }
                    case 5:
                    {
                        labName.text = @"固定电话";
                        labName.textColor = UIColorFromHexString(@"333333");
                        labCont.text = info.company_tel;
                        labCont.textColor = UIColorFromHexString(@"888888");
                        break;
                    }
                    case 6:
                    {
                        labName.text = @"网站";
                        labName.textColor = UIColorFromHexString(@"333333");
                        labCont.text = info.company_site;
                        labCont.textColor = UIColorFromHexString(@"888888");
                        break;
                    }
                    default:
                        break;
                }
            }
            break;
            
        case 1:{
            cell = [tableView dequeueReusableCellWithIdentifier:@"qiyePhotoCell" forIndexPath:indexPath];
            UIScrollView *sv = (UIScrollView *)[cell viewWithTag:1];
            sv.contentSize = CGSizeMake(10 + self.imgLimit * 84, 90);
            sv.showsVerticalScrollIndicator = NO;
            sv.showsHorizontalScrollIndicator = NO;
            
            
            if (isVip) {
                if (self.companyImgs == nil) {
                    for (int i = 0; i < self.imgLimit; i ++) {
                             self.companyImgs = [[UIImageView alloc] initWithFrame:CGRectMake(10 + i * 84, 8, 74, 74)];
                            self.companyImgs.contentMode = UIViewContentModeScaleToFill;
                            self.companyImgs.image = [UIImage imageNamed:@"public_pic_add"];
                            self.companyImgs.tag = 100 + i;
                            self.companyImgs.userInteractionEnabled = YES;
                            self.tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgClicked:)];
                            self.tapGes.view.tag = self.companyImgs.tag;
                            [self.companyImgs addGestureRecognizer:self.tapGes];
                            [sv addSubview:self.companyImgs];
                        }
                }
                
                for (int i = 0; i < self.imgLimit; i ++ ) {
                    self.companyImgs = (UIImageView *)[cell viewWithTag:100 + i];
                    self.companyImgs.hidden = YES;
                    if (i < self.imgDataArray.count) {
                        NSDictionary *dict = self.imgDataArray[i];
                        [self.companyImgs sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:dict[@"company_image"]]]];
                        self.companyImgs.hidden = NO;
                    } else if(i == self.imgDataArray.count){
                        self.companyImgs.image = [UIImage imageNamed:@"public_pic_add"];
                        self.companyImgs.hidden = NO;
                    }
                }
            } else {
                if (self.companyImgs == nil) {
                    for (int i = 0; i < self.imgLimit; i ++) {
                        self.companyImgs = [[UIImageView alloc] initWithFrame:CGRectMake(10 + i * 84, 8, 74, 74)];
                        self.companyImgs.contentMode = UIViewContentModeScaleToFill;
                        self.companyImgs.image = [UIImage imageNamed:@"public_pic_add"];
                        [sv addSubview:self.companyImgs];
                    }
                }
            }
            break;
        }
            
            
        case 2:{
            cell = [tableView dequeueReusableCellWithIdentifier:@"qiyeAboutCell" forIndexPath:indexPath];
            UILabel *labName = (UILabel *)[cell viewWithTag:1];
            UILabel *labCont = (UILabel *)[cell viewWithTag:2];
            switch (indexPath.row) {
                case 0:
                    labName.text = @"企业简介";
                    labCont.text = info.company_description;
                    break;
                    
                case 1:{
                    labName.text = @"经营范围";
                    if (info.company_types.count <= 0) {
                        labCont.text = @"请填写";
                    } else {
                        NSString *linkedStr = [info.company_types componentsJoinedByString:@"、"];
                        labCont.text = linkedStr;
                    }
                    break;
                }
                    
                case 2:
                    labName.text = @"关联石材";
                    if (self.stoneArray.count <= 0) {
                        labCont.text = @"请填写";
                    } else {
                        NSMutableArray *stoneNameArray = [NSMutableArray array];
                        for (NSDictionary *stoneDict in self.stoneArray) {
                            [stoneNameArray addObject:stoneDict[@"stone_name"]];
                        }
                        labCont.text = [stoneNameArray componentsJoinedByString:@"、"];
                    }
                    break;
                    
                default:
                    break;
            }
        }
            break;

        case 3:
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"qiyeLogoCell" forIndexPath:indexPath];
                UILabel *labName = (UILabel *)[cell viewWithTag:1];
                UIImageView *imgLogo = (UIImageView *)[cell viewWithTag:2];
                imgLogo.layer.borderWidth = 1;
                imgLogo.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
                UILabel *labPlace = (UILabel *)[cell viewWithTag:3];
                UILabel *falgLab = (UILabel *)[cell viewWithTag:88];
                falgLab.hidden = NO;
                labName.text = @"企业营业执照";
                if (isVip) {
                    [imgLogo sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:info.company_licence]]];
                    imgLogo.layer.masksToBounds = YES;
                    labPlace.hidden = YES;
                    labName.textColor = UIColorFromHexString(@"333333");
                    NSString *company_licence_ok = info.company_licence_ok;
                    if (company_licence_ok.intValue == Zb_Company_Licence_Status_Default) {
                        falgLab.hidden = YES;
                    } else if (company_licence_ok.intValue == Zb_Company_Licence_Status_Success) {
                        falgLab.hidden = NO;
                        falgLab.text = @"审核通过";
                    } else if (company_licence_ok.intValue == Zb_Company_Licence_Status_Fail) {
                        falgLab.hidden = NO;
                        falgLab.text = @"审核失败";
                    } else if (company_licence_ok.intValue == Zb_Company_Licence_Status_Watting) {
                        falgLab.hidden = NO;
                        falgLab.text = @"正在审核";
                    }
                } else {
                    imgLogo.hidden = YES;
                    labPlace.text = @"未认证";
                    labPlace.hidden = NO;
                    falgLab.hidden = YES;
                    labName.textColor = UIColorFromHexString(@"CCCCCC");
                }
                
            } else if (indexPath.row == 1) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"qiyeInfoCell" forIndexPath:indexPath];
                UILabel *labName = (UILabel *)[cell viewWithTag:1];
                UILabel *labCont = (UILabel *)[cell viewWithTag:2];
                UIImageView *forwardFlag = (UIImageView *)[cell viewWithTag:3];
                forwardFlag.hidden = YES;
                labName.text = @"VIP认证等级";
                labName.text = @"石猫认证等级";
                if (isVip) {
                    labCont.text = [NSString stringWithFormat:@"%@级", info.company_level];
                    labName.textColor =  UIColorFromHexString(@"333333");
                } else {
                    labCont.text = @"未升级VIP";
                    labCont.text = @"未升级石猫认证等级";
                    labCont.textColor = UIColorFromHexString(@"CCCCCC");
                    labName.textColor =  UIColorFromHexString(@"CCCCCC");
                }

            } else {
                if (self.curEditType == Zb_Qy_Edit_Type_OnCreate) {
                    // 创建企业后直接编辑资料，无微聊
                    cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
                } else {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell1" forIndexPath:indexPath];
                }
                
            }
            break;

            
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ZbQyInfo *info = [ZbWebService sharedQy];
    
    ZbQyEtCommViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyEtCommViewController"];
    if (indexPath.section == 0 && indexPath.row == 0) {
       self.actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册中选择", nil];
        self.actionsheet.tag = EDIT_COMPANYLOGO;
        [self.actionsheet showInView:self.view];
        
    } else if (indexPath.section == 0 && indexPath.row == 2) {
        ZbShipAdrViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbShipAdrViewController"];
        [controller setControllerType:Zb_Adr_QiYe];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 0 && indexPath.row == 3) {
        
        // 企业位置定位
        ZbQyLocateViewController *qyLocationController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyLocateViewController"];
        qyLocationController.markLocationBlock = ^(CLLocationCoordinate2D pos) {
            [self requestEditCompanyLocation:pos];
            [self.navigationController popViewControllerAnimated:YES];
        };
        [self.navigationController pushViewController:qyLocationController animated:YES];
        
    } else if (indexPath.section == 0 && indexPath.row == 4) {
        
        ZbContantViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbContantViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 0 && indexPath.row == 5) {
        
        [controller setControllerType:EditType_TEL with:info.company_tel];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 0 && indexPath.row == 6) {
        
        [controller setControllerType:EditType_SITE with:info.company_site];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        
        [controller setControllerType:EditType_DESC with:info.company_description];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 2 && indexPath.row == 1) {
        ZbCompanyTypeViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbCompanyTypeViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 2 && indexPath.row == 2) {
        ZbLinkedStoneViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbLinkedStoneViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.section == 3 && indexPath.row == 0) {
        NSString *company_licence_ok = info.company_licence_ok;
        if (company_licence_ok.intValue == Zb_Company_Licence_Status_Success || company_licence_ok.intValue == Zb_Company_Licence_Status_Watting) {
            return;
        }
        BOOL isVip = [ZbWebService isVipQy];
        if (!isVip) {
            return;
        }
        self.actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册中选择", nil];
        self.actionsheet.tag = EDIT_COMPLICENSE;
        [self.actionsheet showInView:self.view];
    }
}

#pragma mark - CTURLConnectionDelegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}
- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.hud hide:NO];
    NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSString *result = dict[@"errno"];
    if (result.integerValue != 0 && connection.tag != HTTP_EDIT_COMPANY_LOCATION) {
        // 加载失败
        return;
    }
    if (connection.tag == HTTP_GET_COMPANYINFO) {
        ZbQyInfo *info = [[ZbQyInfo alloc] initWithDictionary:dict[@"data"]];
        [ZbWebService setQy:info];
        [self loadUpLoadImgLimit];
    } else if (connection.tag == HTTP_GET_COMPANYLIMIT) {
        NSDictionary *dataLimt = dict[@"data"];
        self.imgLimit = [[dataLimt objectForKey:@"limit"] intValue];
        [self loadCompanyStoneList];
    } else if (connection.tag == HTTP_GET_COMPANYIMAGELIST) {
        [self.imgDataArray removeAllObjects];
        [self.imgDataArray addObjectsFromArray:(NSArray *)dict[@"data"]];
        [self loadCompanyLicenceLimit];
        [self.tv reloadData];
    } else if (connection.tag == HTTP_DEL_IMAGE) {
        [self loadCompImgList];
    } else if (connection.tag == HTTP_GET_COMPANYLICENLIMIT) {
        
    } else if (connection.tag == HTTP_GET_COMPANYSTONE) {
        NSMutableArray *stones = dict[@"data"];
        if ([stones isKindOfClass:[NSMutableArray class]]) {
            self.stoneArray = stones;
        }
        [self loadCompImgList];
    } else if (connection.tag == HTTP_EDIT_COMPANY_LOCATION) {
        if (result.integerValue != 0) {
            // 标注失败
            self.hud.labelText = @"标注失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        // 标注成功
        self.hud.labelText = @"标注成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
        // 修改位置数据
        ZbQyInfo *info = [ZbWebService sharedQy];
        NSString *posStr = connection.userInfo;
        NSArray *posArray = [posStr componentsSeparatedByString:@"|"];
        info.company_longitude = posArray.firstObject;
        info.company_latitude = posArray.lastObject;
        // 重刷位置
        [self.tv reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

@end
