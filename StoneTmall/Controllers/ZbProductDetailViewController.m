//
//  ZbProductDetailViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbProductDetailViewController.h"
#import "YKuImageInLoopScrollView.h"
#import "ZbQiyeTableViewCell.h"
#import "ZbStoneLinkTableViewCell.h"
#import "ZbFindProductTableViewCell.h"
#import "ZbSeparateTableViewCell.h"
#import "ZbTypeTableViewCell.h"
#import <ShareSDK/ShareSDK.h>
#import "ZbLoginMainViewController.h"
#import "ZbQyDetailViewController.h"
#import "ZbStoneDetailViewController.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "ZLPhoto.h"
#import "ZbPubProductViewController.h"
#import "ChatViewController.h"

#define HTTP_REQUEST_TAG_INFO 1               // 产品详情
#define HTTP_REQUEST_TAG_IMAGE 2              // 产品图片列表
#define HTTP_REQUEST_TAG_COMPANY 3            // 相关公司
#define HTTP_REQUEST_TAG_MARK 4               // 收藏
#define HTTP_REQUEST_TAG_UNMARK 5             // 取消收藏
#define HTTP_REQUEST_TAG_SIMILAR 6            // 相似产品
#define HTTP_REQUEST_TAG_SIMILAR_MORE 7       // 相似产品更多
#define HTTP_REQUEST_TAG_LIKE 8               // 点赞
#define HTTP_REQUEST_TAG_UNLIKE 9             // 取消赞
#define HTTP_REQUEST_TAG_DELPRODUCT 10        // 删除产品信息
#define HTTP_REQUEST_TAG_STONE 100            // 关联石材

@interface ZbProductDetailViewController () <UITableViewDataSource, UITableViewDelegate, YKuImageInLoopScrollViewDelegate, ZLPhotoPickerBrowserViewControllerDataSource, ZLPhotoPickerBrowserViewControllerDelegate, PopoverViewDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 滚动图片容器
@property (nonatomic, strong) IBOutlet UIView *imageLoopContainer;
// 滚动图片容器height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcImageLoopContainerHeight;
// 名称
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 参考价格
@property (nonatomic, strong) IBOutlet UILabel *labPrice;
// 参考价格单位
@property (nonatomic, strong) IBOutlet UILabel *labPriceUnit;
// 分类
@property (nonatomic, strong) IBOutlet UILabel *labType;
// 描述
@property (nonatomic, strong) IBOutlet UILabel *labDes;
// 微聊咨询按钮
@property (nonatomic, strong) IBOutlet UIButton *btnChat;
// 赞按钮
@property (nonatomic, strong) IBOutlet UIButton *btnPraise;
// 交互
@property (nonatomic, strong) IBOutlet UILabel *labCommunicate;
// 收藏按钮
@property (nonatomic, strong) IBOutlet UIButton *btnMark;

// 滚动图片
@property (nonatomic, strong) YKuImageInLoopScrollView *imageLoopView;

// 企业信息
@property (nonatomic, strong) NSDictionary *curQyDict;
// 图片数组
@property (nonatomic, strong) NSMutableArray *imgArray;
// 关联石材数组
@property (nonatomic, strong) NSMutableArray *stoneArray;
// 相似产品数组
@property (nonatomic, strong) NSMutableArray *productArray;

// 当前石材详情字典
@property (nonatomic, strong) NSMutableDictionary *curProductDetailDict;

// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

@property (nonatomic, strong) PopoverView *pv;

@end

@implementation ZbProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.curQyDict = @{};
    self.imgArray = [NSMutableArray array];
    self.stoneArray = [NSMutableArray array];
    self.productArray = [NSMutableArray array];
    
    // 圆角
    self.btnChat.layer.cornerRadius = 3;
    self.btnChat.layer.masksToBounds = YES;
    self.btnChat.layer.cornerRadius = 3;
    self.btnPraise.layer.cornerRadius = 3;
    self.btnPraise.layer.borderWidth = 1;
    self.btnPraise.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    
    // 默认微聊不激活
    [self setBtnChatEnable:NO];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbFindProductTableViewCell" bundle:nil] forCellReuseIdentifier:FindProductCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneLinkTableViewCell" bundle:nil] forCellReuseIdentifier:StoneLinkCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbQiyeTableViewCell" bundle:nil] forCellReuseIdentifier:QiyeCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbTypeTableViewCell" bundle:nil] forCellReuseIdentifier:TypeHeaderIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSeparateTableViewCell" bundle:nil] forCellReuseIdentifier:SeparateCellIdentifier];
    
    // 控制右上角状态
    ZbQyInfo *qyinfo = [ZbWebService sharedQy];
    if (![qyinfo.company_id isEqualToString:self.curProductDetailDict[@"company_id"]]) {
        [self.btnMark setImage:[UIImage imageNamed:@"public_icn_normal_favor"] forState:UIControlStateNormal];
    } else {
        [self.btnMark setImage:[UIImage imageNamed:@"public_icn_more"] forState:UIControlStateNormal];
    }
    
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    // 获取详情数据
    [self requestInfoData];
    [self requestImageListData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 登录判断
- (void)requestLogin {
    self.hud.labelText = @"尚未登录，无法操作";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
        UIStoryboard *loginSb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        UINavigationController *nav = [loginSb instantiateViewControllerWithIdentifier:@"LoginNav"];
        ZbLoginMainViewController *loginController = (ZbLoginMainViewController *)nav.viewControllers[0];
        loginController.loginSucessBlock = ^(void) {
            [self.tv.header performSelectorOnMainThread:@selector(beginRefreshing) withObject:nil waitUntilDone:NO];
        };
        [self.view.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }];
    return;
}

#pragma mark - 界面绘制
// 信息绘制
- (void)drawInfo {
    // 产品标题
    NSString *productTitle = self.curProductDetailDict[@"product_title"];
    self.labName.text = productTitle;
    
    // 图片高度
    CGFloat tagImageHeight = DeviceWidth / 1.4;
    self.alcImageLoopContainerHeight.constant = tagImageHeight;;
    
    // 添加图片滚动
    self.imageLoopView = [[YKuImageInLoopScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, tagImageHeight)];
    self.imageLoopView.delegate = self;
    // 设置yKuImageInLoopScrollView显示类型
    self.imageLoopView.scrollViewType = ScrollViewDefault;
    // 设置styledPageControl位置
    [self.imageLoopView.styledPageControl setPageControlSite:PageControlSiteMiddle];
    [self.imageLoopView.styledPageControl setBottomDistance:12];
    // 设置styledPageControl已选中下的内心圆颜色
    [self.imageLoopView.styledPageControl setCoreSelectedColor:MAIN_COLOR];
    [self.imageLoopView.styledPageControl setCoreNormalColor:[UIColor whiteColor]];
    [self.imageLoopContainer addSubview:self.imageLoopView];
    return;
}

// 微聊按钮绘制
- (void)drawBtnChat {
    // 微聊按钮状态
    NSString *userName = self.curQyDict[@"user_name"];
    if (userName && [userName isKindOfClass:[NSString class]] && userName.length > 0) {
        [self setBtnChatEnable:YES];
    } else {
        [self setBtnChatEnable:NO];
    }
}

// 详情绘制
- (void)drawDetailInfo {
    [self drawInfo];
    // 参考价
    NSString *price = self.curProductDetailDict[@"product_price"];
    self.labPrice.text = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:([price integerValue] / 100)]];
    NSString *priceUnit = self.curProductDetailDict[@"product_price_unit"];
    if ([priceUnit isKindOfClass:[NSNull class]] || priceUnit.length <= 0) {
        self.labPriceUnit.text = @"元/吨";
    } else {
        self.labPriceUnit.text = [NSString stringWithFormat:@"元/%@", priceUnit];
    }
    // 分类
    NSString *type = self.curProductDetailDict[@"product_type"];
    self.labType.text = [NSString stringWithFormat:@"%@", type];
    // 描述
    NSString *des = self.curProductDetailDict[@"product_description"];
    self.labDes.text = des;
    // header高度
    [self calcTvHeader];
    // 收藏状态
    [self drawMarkStatus];
    // 赞状态
    [self drawLikeStatus];
    
    [self drawBtnChat];
//    // zb test 暂时隐藏微聊
//    [self setBtnChatEnable:NO];
}

// 重算header高度
- (void)calcTvHeader {
    self.labDes.preferredMaxLayoutWidth = DeviceWidth - 15*2;
    // header高度
    CGFloat tagHeaderHeight = [self.tvHeader systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect headerFrame = CGRectMake(0, 0, DeviceWidth, tagHeaderHeight);
    self.tvHeader.frame = headerFrame;
    [self.tv setTableHeaderView:self.tvHeader];
}

// 绘制收藏状态
- (void)drawMarkStatus {
    // 收藏状态
    NSNumber *mark = self.curProductDetailDict[@"product_mark"];
    ZbQyInfo *qyinfo = [ZbWebService sharedQy];
    if (![qyinfo.company_id isEqualToString:self.curProductDetailDict[@"company_id"]]) {
        [self.btnMark setImage:[UIImage imageNamed:@"public_icn_normal_favor"] forState:UIControlStateNormal];
        self.btnMark.selected = mark.integerValue > 0;
    } else {
        [self.btnMark setImage:[UIImage imageNamed:@"public_icn_more"] forState:UIControlStateNormal];
    }
    // 交互
    NSNumber *clicks = self.curProductDetailDict[@"product_clicks"];
    NSNumber *marks = self.curProductDetailDict[@"product_marks"];
    NSString *communicate = [NSString stringWithFormat:@"%@ 浏览  %@ 收藏", clicks, marks];
    self.labCommunicate.text = communicate;
    return;
}

// 绘制点赞状态
- (void)drawLikeStatus {
    // 赞状态
    NSNumber *like = self.curProductDetailDict[@"product_like"];
    self.btnPraise.selected = like.integerValue > 0;
    // 赞数量
    NSNumber *likes = self.curProductDetailDict[@"product_likes"];
    [self.btnPraise setTitle:[NSString stringWithFormat:@"%@", likes] forState:UIControlStateNormal];
    return;
}

// 设置微聊按钮激活状态
- (void)setBtnChatEnable:(BOOL)isEnable {
    self.btnChat.enabled = isEnable;
    self.btnChat.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

#pragma mark - 接口调用
// 获取产品详情接口
- (void)requestInfoData {
    NSString *productId = self.productId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getProductInfo:productId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_INFO;
    [conn start];
    return;
}

// 获取产品图片列表接口
- (void)requestImageListData {
    NSString *productId = self.productId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getProductImageList:productId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_IMAGE;
    [conn start];
    return;
}

// 获取产品相关公司接口
- (void)requestCompanyData {
    NSString *companyId = self.curProductDetailDict[@"company_id"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyShortInfo:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_COMPANY;
    [conn start];
    return;
}

// 获取相似产品接口
- (void)requestSimilarProductData {
    // 关联石材数据
    NSString *productLinks = self.curProductDetailDict[@"product_tag"];
    // 关联石材列表
    NSArray *linksArray = [productLinks componentsSeparatedByString:@"|"];
    // 取第一个关联石材，来查询相似产品
    NSString *stoneName = [linksArray firstObject];
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getSimilarProductListByStone:stoneName pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_SIMILAR;
    [conn start];
    return;
}

// 获取更多相似产品接口
- (void)requestMoreSimilarProductData {
    // 关联石材数据
    NSString *productLinks = self.curProductDetailDict[@"product_tag"];
    // 关联石材列表
    NSArray *linksArray = [productLinks componentsSeparatedByString:@"|"];
    // 取第一个关联石材，来查询相似产品
    NSString *stoneName = [linksArray firstObject];
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getSimilarProductListByStone:stoneName pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_SIMILAR_MORE;
    [conn start];
    return;
}

// 获取关联石材产品信息接口
- (void)requestLinkStoneData:(NSInteger)index {
    // 关联石材数据
    NSString *productLinks = self.curProductDetailDict[@"product_tag"];
    // 关联石材列表
    NSArray *linksArray = [productLinks componentsSeparatedByString:@"|"];
    NSString *stoneName = [linksArray objectAtIndex:index];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneShortInfo:stoneName] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_STONE + (int)index;
    [conn start];
    return;
}

// 收藏接口
- (void)requestMark {
    self.hud.labelText = @"收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *productId = self.productId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService markProduct:productId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MARK;
    [conn start];
    return;
}

// 取消收藏接口
- (void)requestUnMark {
    self.hud.labelText = @"取消收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *productId = self.productId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService unmarkProduct:productId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNMARK;
    [conn start];
    return;
}

// 点赞接口
- (void)requestLike {
    NSString *productId = self.productId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService likeProduct:productId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_LIKE;
    [conn start];
    return;
}

// 点赞接口
- (void)requestUnlike {
    NSString *productId = self.productId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService unlikeProduct:productId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNLIKE;
    [conn start];
    return;
}

#pragma mark - 点击事件
// 分享按钮点击
- (IBAction)btnShareOnClick:(id)sender {
    // 产品信息
    NSString *productTitle = self.curProductDetailDict[@"product_title"];
    NSString *des = self.curProductDetailDict[@"product_description"];
    if (des.length > SHARE_CONTENT_MAX_LEN) {
        des = [des substringToIndex:SHARE_CONTENT_MAX_LEN];
        des = [NSString stringWithFormat:@"%@...", des];
    }
    NSString *imgUrl = [ZbWebService getImageFullUrl:self.curProductDetailDict[@"product_image_small"]];
    // 分享信息
    NSString *title = [NSString stringWithFormat:@"石猫石材搜索-%@", productTitle];
    NSString *shareUrl = [ZbWebService shareProductUrl:self.productId];
//    NSDictionary *appDataDict = @{@"tag":[NSNumber numberWithInt:Zb_Share_Ext_Type_Product], @"product_id":self.productId};
//    NSString *appDataJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:appDataDict options:0 error:nil] encoding:NSUTF8StringEncoding];
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:des
                                       defaultContent:@""
                                                image:[ShareSDK imageWithUrl:imgUrl]
                                                title:title
                                                  url:shareUrl
                                          description:@"企业详情"
                                            mediaType:SSPublishContentMediaTypeNews];
    // 微信好友数据
    [publishContent addWeixinSessionUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeApp] content:des title:title url:shareUrl image:[ShareSDK imageWithUrl:imgUrl] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
    // 微信朋友圈数据
    [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeNews] content:des title:title url:shareUrl image:[ShareSDK imageWithUrl:imgUrl] musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
    // 短信数据
    [publishContent addSMSUnitWithContent:[NSString stringWithFormat:@"[石猫]向你推荐： %@ ，链接为：%@", productTitle, shareUrl]];
    
    // 分享按钮列表
    NSArray *shareList;
    if ([WXApi isWXAppInstalled] && [QQApiInterface isQQInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeQQ, ShareTypeSMS, nil];
    } else if ([WXApi isWXAppInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeSMS, nil];
    } else if ([QQApiInterface isQQInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeQQ, ShareTypeSMS, nil];
    } else {
        shareList = [ShareSDK getShareListWithType:ShareTypeSMS, nil];
    }
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:shareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];
    return;
}

// 收藏按钮点击
- (IBAction)btnMarkOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    NSNumber *mark = self.curProductDetailDict[@"product_mark"];
    NSString *statusStr;
    if (mark.integerValue == 0) {
        // 未收藏，则收藏
        statusStr = @"收藏";
    } else {
        // 已收藏，则取消
        statusStr = @"取消收藏";
    }
    // 弹出框
    CGPoint point = CGPointMake(DeviceWidth - 10, 60);
    ZbQyInfo *qyinfo = [ZbWebService sharedQy];
    // 发布产品的拥有者是本人，多一个编辑按钮, 删除按钮
    NSArray *imageArray = @[[UIImage imageNamed:@"nav_btn_favor"], [UIImage imageNamed: @"nav_btn_edit"], [UIImage imageNamed:@"nav_but_delete"]];
    
    if ([qyinfo.company_id isEqualToString:self.curProductDetailDict[@"company_id"]]) {
        self.pv = [PopoverView showPopoverAtPoint:point inView:self.navigationController.view withStringArray:@[statusStr, @"编辑", @"删除"] withImageArray:imageArray delegate:self];
        
    } else {
    // 发布产品的拥有者不是本人，保持原状态

        NSNumber *mark = self.curProductDetailDict[@"product_mark"];
        NSNumber *marks = self.curProductDetailDict[@"product_marks"];
        if (mark.integerValue == 0) {
            // 未收藏，则收藏
            [self requestMark];
            [self.curProductDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"product_mark"];
            [self.curProductDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue + 1)] forKey:@"product_marks"];
        } else {
            // 已收藏，则取消
            [self requestUnMark];
            [self.curProductDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"product_mark"];
            [self.curProductDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue - 1)] forKey:@"product_marks"];
        }
        [self drawMarkStatus];
    }

    return;

}

// 赞按钮点击
- (IBAction)btnPraiseOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    NSNumber *like = self.curProductDetailDict[@"product_like"];
    NSNumber *likes = self.curProductDetailDict[@"product_likes"];
    if (like.integerValue == 0) {
        // 未赞，则收藏
        [self requestLike];
        [self.curProductDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"product_like"];
        [self.curProductDetailDict setValue:[NSNumber numberWithInteger:(likes.integerValue + 1)] forKey:@"product_likes"];
    } else {
        // 已赞，则取消
        [self requestUnlike];
        [self.curProductDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"product_like"];
        [self.curProductDetailDict setValue:[NSNumber numberWithInteger:(likes.integerValue - 1)] forKey:@"product_likes"];
    }
    [self drawLikeStatus];
    return;
}

// 微聊按钮点击
- (IBAction)btnChatOnClick:(id)sender {
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    // 聊天系统登录判断
    if (![[[EaseMob sharedInstance] chatManager] isLoggedIn]) {
        self.hud.labelText = @"聊天系统数据正在加载中..请稍候";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    };
    NSString *userName = self.curQyDict[@"user_name"];
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
    if (loginUsername && loginUsername.length > 0) {
        if ([loginUsername isEqualToString:userName]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notChatSelf", @"can't talk to yourself") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
            [alertView show];
            
            return;
        }
    }
//    // 有可能不是好友，因此添加此用户昵称信息
//    [ZbSaveManager addUserInfo:@{@"user_name":userName, @"user_nickname":self.curQyDict[@"user_nickname"], @"user_image":self.curQyDict[@"user_image"]}];
    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:userName isGroup:NO];
    [self.navigationController pushViewController:chatVC animated:YES];
}

#pragma mark - PopoverViewDelegate Methods

- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index {
    
    // 登录判断
    if (![ZbWebService isLogin]) {
        self.hud.labelText = @"尚未登录，无法操作";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
        }];
        return;
    }
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.1f];
    if (index == 0) {
        NSNumber *mark = self.curProductDetailDict[@"product_mark"];
        NSNumber *marks = self.curProductDetailDict[@"product_marks"];
        if (mark.integerValue == 0) {
            // 未收藏，则收藏
            [self requestMark];
            [self.curProductDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"product_mark"];
            [self.curProductDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue + 1)] forKey:@"product_marks"];
        } else {
            // 已收藏，则取消
            [self requestUnMark];
            [self.curProductDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"product_mark"];
            [self.curProductDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue - 1)] forKey:@"product_marks"];
        }
    } else if (index == 1) {
        UIStoryboard *minesb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
        ZbPubProductViewController *controller = [minesb instantiateViewControllerWithIdentifier:@"ZbPubProductViewController"];
        NSMutableArray *array = [NSMutableArray array];
        [array addObject:self.curProductDetailDict[@"product_image"]];
        [array addObjectsFromArray:self.imgArray];
        controller.stoneImgArray = array;
        controller.curEditDict = self.curProductDetailDict;
        controller.ClickBlock = ^(void){
            // 申请成功block，重刷数据
            [self.tv.header beginRefreshing];
            [self.tv.header drawRect:CGRectZero];
        };
        [self.navigationController pushViewController:controller animated:YES];
    } else if (index == 2) {
        NSString *productId = self.curProductDetailDict[@"product_id"];
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService delProduct:productId] delegate:self];
        conn.tag = HTTP_REQUEST_TAG_DELPRODUCT;
        [conn start];
    }
}

- (void)popoverViewDidDismiss:(PopoverView *)popoverView {
    self.pv = nil;
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
        {
            // 相关企业
            return self.curQyDict != nil ? 3 : 2;
        }
        case 1:
        {
            // 关联石材
            return self.stoneArray.count + 2;
        }
        case 2:
        {
            // 相似产品
            return self.productArray.count + 1;
        }
        default:
            return 0;
    }
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 相关企业
            NSInteger qyCount = self.curQyDict != nil ? 1 : 0;
            if (indexPath.row == 0) {
                return [ZbTypeTableViewCell getRowHeight];
            } else if (indexPath.row == (qyCount + 2 - 1)) {
                return [ZbSeparateTableViewCell getRowHeight];
            }
            return [ZbQiyeTableViewCell getRowHeight];
        }
        case 1:
        {
            // 关联石材
            if (indexPath.row == 0) {
                return [ZbTypeTableViewCell getRowHeight];
            } else if (indexPath.row == (self.stoneArray.count + 2 - 1)) {
                return [ZbSeparateTableViewCell getRowHeight];
            }
            return [ZbStoneLinkTableViewCell getRowHeight];
        }
        case 2:
        {
            // 相似产品
            if (indexPath.row == 0) {
                return [ZbTypeTableViewCell getRowHeight];
            }
            return [ZbFindProductTableViewCell getRowHeight];
        }
            
        default:
            return 0;
    }
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 相关企业
            NSInteger qyCount = self.curQyDict != nil ? 1 : 0;
            if (indexPath.row == 0) {
                // 头
                ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
                headerCell.labTypeName.text = @"企业信息";
                headerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return headerCell;
            } else if (indexPath.row == (qyCount + 2 - 1)) {
                // 尾
                UITableViewCell *footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
                footerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return footerCell;
            }
            // 企业
            ZbQiyeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:QiyeCellIdentifier forIndexPath:indexPath];
            [cell setInfoDict:self.curQyDict];
            return cell;
        }
        case 1:
        {
            // 关联石材
            if (indexPath.row == 0) {
                // 头
                ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
                headerCell.labTypeName.text = @"关联石材";
                headerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return headerCell;
            } else if (indexPath.row == (self.stoneArray.count + 2 - 1)) {
                // 尾
                UITableViewCell *footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
                footerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return footerCell;
            }
            // 石材
            ZbStoneLinkTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:StoneLinkCellIdentifier forIndexPath:indexPath];
            NSDictionary *curDict = [self.stoneArray objectAtIndex:(indexPath.row - 1)];
            [cell setInfoDict:curDict];
            return cell;
        }
        case 2:
        {
            // 相似产品
            if (indexPath.row == 0) {
                // 头
                ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
                headerCell.labTypeName.text = @"相似产品";
                headerCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return headerCell;
            }
            // 产品
            ZbFindProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:FindProductCellIdentifier forIndexPath:indexPath];
            NSDictionary *curDict = [self.productArray objectAtIndex:(indexPath.row - 1)];
            [cell setInfoDict:curDict];
            return cell;
        }
        default:
            return nil;
    }
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            // 相关企业
            NSInteger qyCount = self.curQyDict != nil ? 1 : 0;
            if (indexPath.row == 0) {
                // 头
                break;
            } else if (indexPath.row == (qyCount + 2 - 1)) {
                // 尾
                break;
            }
            // 企业
            ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
            controller.companyId = self.curQyDict[@"company_id"];
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case 1:
        {
            // 关联石材
            if (indexPath.row == 0) {
                // 头
                break;
            } else if (indexPath.row == (self.stoneArray.count + 2 - 1)) {
                // 尾
                break;
            }
            // 石材
            NSDictionary *curDict = [self.stoneArray objectAtIndex:(indexPath.row - 1)];
            ZbStoneDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
            NSString *stoneName = curDict[@"stone_name"];
            controller.stoneName = stoneName;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        case 2:
        {
            // 相似产品
            if (indexPath.row == 0) {
                // 头
                break;
            }
            // 产品
            NSDictionary *curDict = [self.productArray objectAtIndex:(indexPath.row - 1)];
            ZbProductDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbProductDetailViewController"];
            NSString *productId = curDict[@"product_id"];
            controller.productId = productId;
            [self.navigationController pushViewController:controller animated:YES];
            break;
        }
        default:
            break;
    }
    return;
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 获取详情数据
    [self requestInfoData];
    [self requestImageListData];
    return;
}

- (void)footerRefresh {
    [self requestMoreSimilarProductData];
}

#pragma mark - YKuImageInLoopScrollViewDelegate
- (int)numOfPageForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    return (int)(self.imgArray.count + 1);
}

- (int)widthForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    int width = DeviceWidth;
    return width;
}

- (UIView *)scrollView:(YKuImageInLoopScrollView *)ascrollView viewAtPageIndex:(int)apageIndex
{
    // 图片数据
    NSString *imgUrl;
    if (apageIndex == 0) {
        imgUrl = self.curProductDetailDict[@"product_image"];
    } else {
        imgUrl = [self.imgArray objectAtIndex:(apageIndex - 1)];
    }
    
    // 图片控件
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    UIImageView *curImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CGRectGetHeight(ascrollView.frame))];
    [curImageView sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]]];
    curImageView.contentMode = UIViewContentModeScaleAspectFill;
    [bgView addSubview:curImageView];
    return bgView;
}

- (void)scrollView:(YKuImageInLoopScrollView*) ascrollView didTapIndex:(int)apageIndex{
    DLog(@"Clicked page%d",apageIndex);
    // 图片游览器
    ZLPhotoPickerBrowserViewController *pickerBrowser = [[ZLPhotoPickerBrowserViewController alloc] init];
    // 淡入淡出效果
    // pickerBrowser.status = UIViewAnimationAnimationStatusFade;
    //    pickerBrowser.toView = btn;
    // 数据源/delegate
    pickerBrowser.delegate = self;
    pickerBrowser.dataSource = self;
    // 当前选中的值
    pickerBrowser.currentIndexPath = [NSIndexPath indexPathForItem:apageIndex inSection:0];
    // 展示控制器
    [pickerBrowser showPickerVc:self.navigationController];
}

/*
 选中第几页
 @param didSelectedPageIndex 选中的第几项，[0-numOfPageForScrollView];
 */
-(void) scrollView:(YKuImageInLoopScrollView*) ascrollView didSelectedPageIndex:(int) apageIndex
{
    //    NSLog(@"didSelectedPageIndex:%d",apageIndex);
}

#pragma mark - <ZLPhotoPickerBrowserViewControllerDataSource>
- (long)numberOfSectionInPhotosInPickerBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser{
    return 1;
}

- (long)photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser numberOfItemsInSection:(NSUInteger)section{
    return (long)(self.imgArray.count + 1);
}

#pragma mark - 每个组展示什么图片,需要包装下ZLPhotoPickerBrowserPhoto
- (ZLPhotoPickerBrowserPhoto *) photoBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser photoAtIndexPath:(NSIndexPath *)indexPath{
    // 图片数据
    NSString *imgUrl;
    if (indexPath.row == 0) {
        imgUrl = self.curProductDetailDict[@"product_image"];
    } else {
        imgUrl = [self.imgArray objectAtIndex:(indexPath.row - 1)];
    }
    
    ZLPhotoPickerBrowserPhoto *photo = [[ZLPhotoPickerBrowserPhoto alloc] init];
    photo.photoURL = [NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]];
    
    //    photo.toView = self.imageLoopContainer;
    return photo;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    if (connection.tag == HTTP_REQUEST_TAG_MARK || connection.tag == HTTP_REQUEST_TAG_UNMARK || connection.tag == HTTP_REQUEST_TAG_LIKE || connection.tag == HTTP_REQUEST_TAG_UNLIKE) {
        return;
    }
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    if (connection.tag == HTTP_REQUEST_TAG_MARK || connection.tag == HTTP_REQUEST_TAG_UNMARK || connection.tag == HTTP_REQUEST_TAG_LIKE || connection.tag == HTTP_REQUEST_TAG_UNLIKE) {
        return;
    }
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    [self.hud hide:NO];
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        [self drawDetailInfo];
        [self.tv reloadData];
        [self.imageLoopView reloadData];
        [self.tv.footer endRefreshing];
        [self.tv.header endRefreshing];
        return;
    }
    
    if (connection.tag == HTTP_REQUEST_TAG_INFO) {
        // 产品详情
        self.curProductDetailDict = resultDic[@"data"];
        
        [self drawDetailInfo];
        [self.imageLoopView reloadData];
        
        [self requestCompanyData];
        [self requestSimilarProductData];
        NSString *productLinks = self.curProductDetailDict[@"product_tag"];
        if (productLinks != nil && [productLinks isKindOfClass:[NSString class]] && productLinks.length > 0) {
            // 继续加载关联石材
            [self.stoneArray removeAllObjects];
            [self requestLinkStoneData:0];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_IMAGE) {
        // 图片列表
        NSMutableArray *array = resultDic[@"data"];
        [self.imgArray removeAllObjects];
        [self.imgArray addObjectsFromArray:array];
        
        [self.imageLoopView reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_COMPANY) {
        // 公司信息
        self.curQyDict = resultDic[@"data"];
        
        [self drawBtnChat];
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_SIMILAR) {
        // 相似产品
        NSMutableArray *similarArray = resultDic[@"data"][@"rows"];
        NSMutableArray *array = [NSMutableArray array];
        // 过滤自己
        NSString *myProductId = self.curProductDetailDict[@"product_id"];
        for (NSMutableDictionary *curProduct in similarArray) {
            if ([curProduct[@"product_id"] isEqualToString:myProductId]) {
                continue;
            }
            [array addObject:curProduct];
        }
        [self.productArray removeAllObjects];
        [self.productArray addObjectsFromArray:array];
        
        if (array.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv.header endRefreshing];
        [self.tv reloadData];
//        CGRect rectTop = CGRectMake(0, 0, 1, 1);
//        [self.tv scrollRectToVisible:rectTop animated:YES];
        
    } else if (connection.tag == HTTP_REQUEST_TAG_SIMILAR_MORE) {
        // 更多相似产品
        [self.tv.footer endRefreshing];
        NSMutableArray *similarArray = resultDic[@"data"][@"rows"];
        NSMutableArray *array = [NSMutableArray array];
        // 过滤自己
        NSString *myProductId = self.curProductDetailDict[@"product_id"];
        for (NSMutableDictionary *curProduct in similarArray) {
            if ([curProduct[@"product_id"] isEqualToString:myProductId]) {
                continue;
            }
            [array addObject:curProduct];
        }
        [self.productArray addObjectsFromArray:array];
        if (array.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    } else if (connection.tag >= HTTP_REQUEST_TAG_STONE) {
        // 关联石材
        NSInteger index= connection.tag - HTTP_REQUEST_TAG_STONE;
        [self.stoneArray addObject:resultDic[@"data"]];
        
        // 关联石材字段
        NSString *productLinks = self.curProductDetailDict[@"product_tag"];
        // 关联石材列表
        NSArray *linksArray = [productLinks componentsSeparatedByString:@"|"];
        
        if ((index + 1) < linksArray.count) {
            // 继续加载下一个
            [self requestLinkStoneData:(index + 1)];
        } else {
            // 加载完毕
            [self.tv reloadData];
            CGRect rectTop = CGRectMake(0, 0, 1, 1);
            [self.tv scrollRectToVisible:rectTop animated:YES];
        }
        
    } else if (connection.tag == HTTP_REQUEST_TAG_DELPRODUCT) {
        if (result.integerValue == 0) {
            self.hud.labelText = @"删除成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
