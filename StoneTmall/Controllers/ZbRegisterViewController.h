//
//  ZbRegisterViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbRegisterViewController : BaseViewController

// 界面不显示
- (void)onNoShow;

@end
