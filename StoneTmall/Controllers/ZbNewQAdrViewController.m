//
//  ZbNewQAdrViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbNewQAdrViewController.h"
#import "ZbTypeSelectViewController.h"

#define HTTP_LOAD_TYPEDATA 1
#define HTTP_UPLOAD_ADR 2
#define HTTP_GET_COMPANYINFO 3

@interface ZbNewQAdrViewController ()<UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UILabel *placehloderLab;
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) IBOutlet UIButton *commitBtn;

@property (nonatomic, strong) IBOutlet UITextField *countryField;
@property (nonatomic, strong) IBOutlet UITextField *provField;
@property (nonatomic, strong) IBOutlet UITextField *cityField;

// 选中省份一级分类
@property (nonatomic, assign) NSInteger curTypeMain;
// 选中城市二级分类
@property (nonatomic, assign) NSInteger curTypeSub;

@property (nonatomic, readonly) Zb_Controller_Type zbControllerType;

@end

@implementation ZbNewQAdrViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.commitBtn.layer.cornerRadius = 2.0f;
    self.commitBtn.layer.masksToBounds = YES;
    self.countryField.text = self.country;
    self.provField.text = self.prov;
    self.cityField.text = self.city;
    self.textView.text = self.area;
    if (_zbControllerType == Zb_Controller_ADREDIT) {
        self.provField.text = self.dict[@"prov"];
        self.cityField.text = self.dict[@"city"];
        self.textView.text = self.dict[@"area"];
    }
    if (self.textView.text.length > 0) {
        self.placehloderLab.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-  (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.countryField.text = @"中国";
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)setControllerType:(Zb_Controller_Type)zbControllerType {
    _zbControllerType = zbControllerType;
}

#pragma mark - 点击事件
// 国家选择
- (IBAction)countryClick:(id)sender {
//    ZbCitySelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbCitySelectViewController"];
//    [self.navigationController pushViewController:controller animated:YES];
}
// 省份选择
- (IBAction)provClick:(id)sender {
    [self.view endEditing:YES];
    NSArray *typeArray = [[ZbSaveManager shareManager] getProvTypeArray];
    if (typeArray != nil) {
        // 类别数据已加载过
        NSDictionary *subTypeDict = [[ZbSaveManager shareManager] getCitySubTypeDict];
        ZbTypeSelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTypeSelectViewController"];
        controller.title = @"省市选择";
        controller.typeArray = typeArray;
        controller.subTypeDict = subTypeDict;
        controller.lastParentIndex = self.curTypeMain;
        controller.lastSubIndex = self.curTypeSub;
        controller.selectClickBlock = ^(NSString *typeMain, NSString *typeSub, NSInteger mainIndex, NSInteger subIndex) {
            self.curTypeMain = mainIndex;
            self.curTypeSub = subIndex;
            self.provField.text = typeMain;
            self.cityField.text = typeSub;
        };
        [self.navigationController  pushViewController:controller animated:YES];
        return;
    }
    
    // 加载分类数据
    [self requestTypeData];
    return;
}

// 城市选择
- (IBAction)cityClick:(id)sender {
    [self.view endEditing:YES];
    NSArray *typeArray = [[ZbSaveManager shareManager] getProvTypeArray];
    if (typeArray != nil) {
        // 类别数据已加载过
        NSDictionary *subTypeDict = [[ZbSaveManager shareManager] getCitySubTypeDict];
        ZbTypeSelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTypeSelectViewController"];
        controller.title = @"省市选择";
        controller.typeArray = typeArray;
        controller.subTypeDict = subTypeDict;
        controller.lastParentIndex = self.curTypeMain;
        controller.lastSubIndex = self.curTypeSub;
        controller.selectClickBlock = ^(NSString *typeMain, NSString *typeSub, NSInteger mainIndex, NSInteger subIndex) {
            self.curTypeMain = mainIndex;
            self.curTypeSub = subIndex;
            self.provField.text = typeMain;
            self.cityField.text = typeSub;
        };
        [self.navigationController  pushViewController:controller animated:YES];
        return;
    }
    
    // 加载分类数据
    [self requestTypeData];
    return;
}
//保存地址
- (IBAction)commOnClick:(id)sender {
    if (self.provField.text.length == 0) {
        self.hud.labelText = @"请选择省份";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (self.cityField.text.length == 0) {
        self.hud.labelText = @"请选择城市";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (self.textView.text.length == 0) {
        self.hud.labelText = @"请填写详细地址";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;

    }
    [self.view endEditing:YES];
    self.hud.labelText = @"提交中...";
    [self.hud show:NO];
    
    NSString *params;
    
    switch (_zbControllerType) {
        case Zb_Controller_MAINEDIT:
            {
                params = [ZbWebService editCompanyAddress:self.countryField.text prov:self.provField.text city:self.cityField.text area:self.textView.text];
            }
            break;
            
        case Zb_Controller_ADDADR:
            {
            params = [ZbWebService addCompanyAddress:self.countryField.text prov:self.provField.text city:self.cityField.text area:self.textView.text];
            }
            break;
            
        case Zb_Controller_ADREDIT:
            {
            params = [ZbWebService editCompanyOtherAddress:self.dict[@"id"] country:self.countryField.text prov:self.provField.text city:self.cityField.text area:self.textView.text];
            }
            break;

            
        default:
            break;
    }
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:params delegate:self];
    conn.tag = HTTP_UPLOAD_ADR;
    [conn start];
}

- (void)loadqiYeData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getBindCompanyInfo] delegate:self];
    conn.tag = HTTP_GET_COMPANYINFO;
    [conn start];
}

#pragma mark - TextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    self.placehloderLab.hidden = textView.text.length > 0;
    return;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}

#pragma mark - 读取数据
- (void)requestTypeData {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];

    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getRegionList] delegate:self];
    conn.tag = HTTP_LOAD_TYPEDATA;
    [conn start];
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"获取分类失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = dict[@"errno"];
    
    if (connection.tag == HTTP_LOAD_TYPEDATA) {
        if (result.integerValue == 0) {
            [self.hud hide:NO];
            NSArray *array = dict[@"data"];
            NSArray *tagArray = [ZbWebService treeSTypeParser:array];
            NSArray *typeArray = [tagArray firstObject];
            NSDictionary *subTypeDict = [tagArray lastObject];
            [[ZbSaveManager shareManager] setProvTypeArray:typeArray];
            [[ZbSaveManager shareManager] setCitySubTypeDict:subTypeDict];
            
            ZbTypeSelectViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTypeSelectViewController"];
            controller.title = @"省市选择";
            controller.typeArray = typeArray;
            controller.subTypeDict = subTypeDict;
            controller.lastParentIndex = self.curTypeMain;
            controller.lastSubIndex = self.curTypeSub;
            controller.selectClickBlock = ^(NSString *typeMain, NSString *typeSub, NSInteger mainIndex, NSInteger subIndex) {
                self.curTypeMain = mainIndex;
                self.curTypeSub = subIndex;
                self.provField.text = typeMain;
                self.cityField.text = typeSub;
            };
            [self.navigationController  pushViewController:controller animated:YES];
        }
    } else if (connection.tag == HTTP_UPLOAD_ADR) {
        if (result.integerValue == 0) {
            self.hud.labelText = @"提交成功";
            if (self.zbControllerType == Zb_Controller_MAINEDIT) {
                [self loadqiYeData];
            }
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            self.hud.labelText = dict[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        }
    } else if (connection.tag == HTTP_GET_COMPANYINFO) {
        ZbQyInfo *info = [[ZbQyInfo alloc] initWithDictionary:dict[@"data"]];
        [ZbWebService setQy:info];
    }
}


@end
