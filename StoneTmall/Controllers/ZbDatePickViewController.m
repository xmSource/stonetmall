//
//  ZbDatePickViewController.m
//  StoneTmall
//
//  Created by Apple on 15/9/14.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbDatePickViewController.h"

@interface ZbDatePickViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>

// 容器
@property (nonatomic, strong) IBOutlet UIView *container;

// 遮罩层
@property (nonatomic, strong) IBOutlet UIControl *controlCover;

// 数据列表
@property (nonatomic, strong) NSArray *array;

// 选中回调
@property (nonatomic, copy) btnClickBlock curBlock;

@property (nonatomic, strong) IBOutlet UIPickerView *picker;

@property (nonatomic, strong) NSString *xtitle;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcContainerBottom;


@end

@implementation ZbDatePickViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (ZbDatePickViewController *)showInViewController:(UIViewController *)controller dataArray:(NSArray *)array btnClickBlock:(btnClickBlock)block {
    UIStoryboard *pickSb = [UIStoryboard storyboardWithName:@"DropDown" bundle:nil];
    ZbDatePickViewController *dropDown = [pickSb instantiateViewControllerWithIdentifier:@"ZbDatePickViewController"];
    
    [controller addChildViewController:dropDown];
    [controller.view addSubview:dropDown.view];
    
    [dropDown viewWillAppear:YES];
    
    dropDown.array = array;
    dropDown.curBlock = block;
    
    
    // 下拉框界面大小
    dropDown.view.frame = controller.view.bounds;
    dropDown.controlCover.alpha = 0;
    dropDown.alcContainerBottom.constant = -220;
    [dropDown.picker reloadAllComponents];
    [dropDown.view layoutIfNeeded];
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.3 animations:^(void) {
        dropDown.alcContainerBottom.constant = 0;
        dropDown.controlCover.alpha = 1;
        [dropDown.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    
    return dropDown;
}

- (IBAction)cancelOnClick:(id)sender {
    if (self.curBlock) {
        self.curBlock(Zb_Dp_Click_Cancel, nil);
    }
    [self dismiss:YES];
}

- (IBAction)sureOnClick:(id)sender {
    if (self.curBlock) {
        NSInteger typeIndex = [self.picker selectedRowInComponent:0];
        self.xtitle = [self.array objectAtIndex:typeIndex];
        self.curBlock(Zb_Dp_Click_Sure, self.xtitle);
    }
    [self dismiss:YES];
}

// 关闭界面
- (void)dismiss:(BOOL)animated {
    if (!animated) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        return;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.alcContainerBottom.constant = - CGRectGetHeight(self.container.frame);
        self.controlCover.alpha = 0;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    return;
}


#pragma mark - UIPickViewDelegate
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.array.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.array objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
}


@end
