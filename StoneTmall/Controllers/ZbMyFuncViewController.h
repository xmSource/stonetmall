//
//  ZbMyFuncViewController.h
//  StoneTmall
//
//  Created by chyo on 15/9/6.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, ZbCxl_Type) {
    ZbCxl_Type_comme = 0,   // 推荐给我
    ZbCXl_Type_colle,       // 我的收藏
    ZbCxl_Type_invol        // 我的参与
};

@interface ZbMyFuncViewController : BaseViewController

- (void)setControllerType:(ZbCxl_Type)type;

@end
