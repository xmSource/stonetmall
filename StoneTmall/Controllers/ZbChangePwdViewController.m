//
//  ZbChangePwdViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/4.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbChangePwdViewController.h"

@interface ZbChangePwdViewController ()<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *finshBtn;

@property (nonatomic, strong) IBOutlet UITextField *oldpwd;
@property (nonatomic, strong) IBOutlet UITextField *newpwd;

@end

@implementation ZbChangePwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.finshBtn.layer.cornerRadius = 2.0f;
    self.finshBtn.layer.masksToBounds = YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - textField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.oldpwd]) {
        [self.newpwd becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark  - 点击事件
- (IBAction)subMit:(id)sender {
    
    if (self.oldpwd.text.length == 0) {
        self.hud.labelText = @"请输入旧密码";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    } else if (self.newpwd.text.length == 0) {
        self.hud.labelText = @"请输入新密码";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    [self.view endEditing:YES];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editMyPasswd:self.oldpwd.text newPwd:self.newpwd.text] delegate:self];
    self.hud.labelText = @"修改中...";
    [self.hud show:YES];
    [conn start];
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"修改失败";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
    }];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (result.integerValue == 0) {
        self.hud.labelText = @"修改成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } else {
        self.hud.labelText = resultDic[@"friendmsg"];
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];

    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
