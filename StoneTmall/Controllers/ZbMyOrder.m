//
//  ZbMyOrder.m
//  StoneTmall
//
//  Created by chyo on 15/9/6.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbMyOrder.h"

@implementation ZbMyOrder

- (id)initWithDictionary:(NSMutableDictionary*)jsonObject {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:jsonObject];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key  {
    if([key isEqualToString:@"errno"]){
        self.error = [NSString stringWithFormat:@"%@", value];
    }
}


@end
