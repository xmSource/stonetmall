//
//  ZbHomeViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbTabHomeViewController : BaseViewController

// 微聊按钮
@property (nonatomic, strong) IBOutlet UIButton *btnChat;

// 悬赏开关绘制
- (void)setRewardOpenStatus;

@end
