//
//  ZbLoginMainViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbLoginMainViewController : BaseViewController

@property (nonatomic, copy) void (^loginSucessBlock)(void);

@end
