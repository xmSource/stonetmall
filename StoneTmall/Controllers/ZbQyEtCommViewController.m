//
//  ZbQyEtCommViewController.m
//  StoneTmall
//
//  Created by Apple on 15/9/9.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQyEtCommViewController.h"

#define HTTP_REQUST_LOADDATA 1
#define HTTP_REQUEST_EDITCOMPANYTEL 2
#define HTTP_REQUEST_EDITCOMPANYSITE 3
#define HTTP_REQUEST_EDITCOMPANYDESC 5
#define HTTP_REQUEST_EDIT 6


@interface ZbQyEtCommViewController ()<UITextFieldDelegate, UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *contField;
@property (weak, nonatomic) IBOutlet UIButton *commBtn;
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcheight;
@property (nonatomic, strong) NSString *defaultString;
@property (nonatomic)EditType editType;

@end

@implementation ZbQyEtCommViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.commBtn.layer.cornerRadius = 2.0f;
    self.commBtn.layer.masksToBounds = YES;
}

- (void)setControllerType:(EditType)type with:(NSString *)defaultStr {
    self.defaultString = defaultStr;
    _editType = type;
    switch (_editType) {
        case EditType_TEL:
            self.title = @"修改企业电话";
            break;
            
        case EditType_SITE:
            self.title = @"修改企业网站";
            break;
            
        case EditType_DESC:
            self.title = @"修改企业简介";
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.contField.text = self.defaultString;
    self.textView.text = self.defaultString;
    
    switch (_editType) {
        case EditType_TEL:
            self.textView.hidden = YES;
            self.contField.hidden = NO;
            self.alcheight.constant = 10;
            break;
            
        case EditType_SITE:
            self.textView.hidden = YES;
            self.contField.hidden = NO;
            self.alcheight.constant = 10;
            break;
            
        case EditType_DESC:
            self.textView.hidden = NO;
            self.contField.hidden = YES;
            self.alcheight.constant = 50;
            break;
            
        default:
            break;
    }

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - 读取数据
//修改企业电话
- (void)editCompanyTel {
    if (self.contField.text.length <= 0) {
        self.hud.labelText = @"请填写内容，再提交";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    [self.view endEditing:YES];
    self.hud.labelText = @"修改中...";
    [self.hud show:YES];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editCompanyTel:self.contField.text] delegate:self];
    conn.tag = HTTP_REQUEST_EDITCOMPANYTEL;
    [conn start];
}
// 修改企业网站
- (void)editCompanySite {
    if (self.contField.text.length <= 0) {
        self.hud.labelText = @"请填写内容，再提交";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    [self.view endEditing:YES];
    self.hud.labelText = @"修改中...";
    [self.hud show:YES];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editCompanySite:self.contField.text] delegate:self];
    conn.tag = HTTP_REQUEST_EDITCOMPANYSITE;
    [conn start];
}
// 修改企业简介
- (void)editCompanyDesc {
    if (self.textView.text.length <= 0 ) {
        self.hud.labelText = @"请填写内容，再提交";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    if (self.textView.text.length < 20 && self.contField.text.length > 0) {
        self.hud.labelText = @"描述字数不足20字符";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
        return;
    }
    [self.view endEditing:YES];
    self.hud.labelText = @"修改中...";
    [self.hud show:YES];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editCompanyDescription:self.textView.text] delegate:self];
    conn.tag = HTTP_REQUEST_EDITCOMPANYDESC;
    [conn start];
}

#pragma mark - UITextFiled Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}


#pragma mark - 点击保存数据
- (IBAction)comOnClick:(id)sender {
    switch (_editType) {
        case EditType_TEL:
            [self editCompanyTel];
            break;
        case EditType_SITE:
            [self editCompanySite];
            break;
        case EditType_DESC:
            [self editCompanyDesc];
            break;
            
        default:
            break;
    }
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"修改失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (result.integerValue != 0) {
        self.hud.labelText = resultDic[@"friendmsg"];
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }

    if (connection.tag == HTTP_REQUEST_EDITCOMPANYTEL) {
        
        if (result.integerValue == 0) {
            self.hud.labelText = @"修改成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }

    } else if (connection.tag == HTTP_REQUEST_EDITCOMPANYSITE) {
        
        if (result.integerValue == 0) {
            self.hud.labelText = @"修改成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        
    } else if (connection.tag == HTTP_REQUEST_EDITCOMPANYDESC) {
        
        if (result.integerValue == 0) {
            self.hud.labelText = @"修改成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        
    }
}



@end
