//
//  ZbInputQyNameViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/22.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbInputQyNameViewController.h"
#import "ZbPickGoodsTypeViewController.h"
#import "ZbCreateQyOkViewController.h"

#define HTTP_REQUEST_TAG_REGION 1         // 省市
#define HTTP_REQUEST_TAG_CREATE 2         // 创建

@interface ZbInputQyNameViewController () <UITextFieldDelegate>

// 企业名称输入框
@property (nonatomic, strong) IBOutlet UITextField *tfQyName;
// 联系人姓名
@property (nonatomic, strong) IBOutlet UITextField *tfContactName;
// 联系人电话
@property (nonatomic, strong) IBOutlet UITextField *tfContactPhone;
// 省份、城市
@property (nonatomic, strong) IBOutlet UITextField *tfCity;
// 详细地址
@property (nonatomic, strong) IBOutlet UITextField *tfAddr;
// 下一步按钮
@property (nonatomic, strong) IBOutlet UIButton *btnNext;

// 省字典
@property (nonatomic, strong) NSDictionary *curTypeDict;
// 市字典
@property (nonatomic, strong) NSDictionary *curSubTypeDict;

@end

@implementation ZbInputQyNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 按钮圆角
    self.btnNext.layer.cornerRadius = 3;
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 下一步按钮初始不激活
    [self setBtnNextEnable:NO];
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    if (self.preCompanyName && self.preCompanyName.length > 0) {
        self.tfQyName.text = self.preCompanyName;
    }
    [self.tfQyName becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 设置下一步按钮激活状态
- (void)setBtnNextEnable:(BOOL)isEnable {
    self.btnNext.enabled = isEnable;
    self.btnNext.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

#pragma mark - 省市选择
- (void)showTypePicker:(NSArray *)typeArray typeDict:(NSDictionary *)typeDict {
    [ZbPickGoodsTypeViewController showInViewController:self.navigationController typeArray:typeArray typeDict:typeDict selectBlock:^(BOOL isCancel, NSDictionary *typeDict, NSDictionary *subTypeDict) {
        if (isCancel) {
            return;
        }
        self.curTypeDict = typeDict;
        self.curSubTypeDict = subTypeDict;
        self.tfCity.text = [NSString stringWithFormat:@"%@ %@", typeDict[@"class_name"], subTypeDict[@"class_name"]];
        [self textFieldChanged:self.tfCity];
    }];
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

// 下一步按钮点击
- (IBAction)btnNextOnClick:(id)sender {
    [self.view endEditing:YES];
    
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *qyName = [self.tfQyName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *contactName = [self.tfContactName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *prov = self.curTypeDict[@"class_name"];
    NSString *city = self.curSubTypeDict[@"class_name"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService newCompany:qyName description:DEFAULT_COMPANY_DESCRIPT contact:contactName phone:self.tfContactPhone.text country:DEFAULT_COUNTRY_NAME prov:prov city:city area:self.tfAddr.text] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_CREATE;
    [conn start];
    return;
}

// 城市选择
- (IBAction)btnCityPickOnClick:(id)sender {
    [self.view endEditing:YES];
    
    NSArray *typeArray = [[ZbSaveManager shareManager] getProvTypeArray];
    if (typeArray == nil) {
        // 数据未加载过
        self.hud.labelText = @"努力加载中..";
        [self.hud show:NO];
        
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getRegionList] delegate:self];
        conn.tag = HTTP_REQUEST_TAG_REGION;
        [conn start];
        return;
    }
    
    NSDictionary *typeDict = [[ZbSaveManager shareManager] getCitySubTypeDict];
    [self showTypePicker:typeArray typeDict:typeDict];
    return;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.tfQyName]) {
        [self.tfContactName becomeFirstResponder];
    } else if ([textField isEqual:self.tfContactName]) {
        [self.tfContactPhone becomeFirstResponder];
    } else if ([textField isEqual:self.tfContactPhone]) {
        [textField resignFirstResponder];
    } else if ([textField isEqual:self.tfCity]) {
        [textField resignFirstResponder];
    } else if ([textField isEqual:self.tfAddr]) {
        [textField resignFirstResponder];
    }
    return YES;
}

// 文本内容变化
- (IBAction)textFieldChanged:(id)sender {
    if ((![CommonCode isStringEmpty:self.tfQyName.text]) && (![CommonCode isStringEmpty:self.tfContactName.text]) && [CommonCode isPhoneNumber:self.tfContactPhone.text] && self.tfCity.text.length > 0 && self.tfAddr.text.length > 0) {
        [self setBtnNextEnable:YES];
    } else {
        [self setBtnNextEnable:NO];
    }
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    if (connection.tag == HTTP_REQUEST_TAG_REGION) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    } else if (connection.tag == HTTP_REQUEST_TAG_CREATE) {
        self.hud.labelText = @"提交失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    NSMutableArray *dataArray = resultDic[@"data"];
    if (connection.tag == HTTP_REQUEST_TAG_REGION) {
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = @"加载失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self.hud hide:NO];
        NSArray *tagArray = [ZbWebService treeSTypeParser:dataArray];
        NSArray *typeArray = [tagArray firstObject];
        NSDictionary *subTypeDict = [tagArray lastObject];
        [[ZbSaveManager shareManager] setProvTypeArray:typeArray];
        [[ZbSaveManager shareManager] setCitySubTypeDict:subTypeDict];
        [self showTypePicker:typeArray typeDict:subTypeDict];
    } else if (connection.tag == HTTP_REQUEST_TAG_CREATE) {
        // 提交失败
        if (result.integerValue != 0) {
            
            if (result.integerValue == -15) {
                self.hud.labelText = @"该企业已存在，请返回上一页申请绑定";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        self.hud.labelText = @"创建成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
            ZbCreateQyOkViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbCreateQyOkViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        }];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
