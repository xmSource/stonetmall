//
//  ZbInputQyNameViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/22.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbInputQyNameViewController : BaseViewController

// 用户上一页搜索的企业关键字
@property (nonatomic, strong) NSString *preCompanyName;

@end
