//
//  ZbBecomeQyViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/22.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbBecomeQyViewController.h"
#import "ZbTelValidateViewController.h"
#import "ZbBindQyViewController.h"

@interface ZbBecomeQyViewController () <UITableViewDataSource, UITableViewDelegate>

// 企业用户label
@property (nonatomic, strong) IBOutlet UILabel *labQy;
@property (nonatomic, strong) IBOutlet UITableView *tv;

// 权限数组
@property (nonatomic, strong) NSMutableArray *rightsArray;

@end

@implementation ZbBecomeQyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.rightsArray = Mutable_ArrayInit;
    
    // 企业用户label圆角
    self.labQy.layer.cornerRadius = 3;
    self.labQy.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

#pragma mark - 读取数据
- (void)loadData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getComUserRightsInfo] delegate:self];
    [conn start];
}

#pragma mark - 点击事件
// 确定成为企业用户
- (void)btnConfirmOnClick {
    // 检查手机绑定状态
    NSDictionary *user = [ZbWebService sharedUser];
    NSString *phone = user[@"user_phone"];
    if ([phone isKindOfClass:[NSNull class]] || phone.length <= 0) {
        // 需绑定手机
        ZbTelValidateViewController *validateController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTelValidateViewController"];
        validateController.curValidateType = Zb_TelValidate_Type_BindTel_BecommonQy;
        [self.navigationController pushViewController:validateController animated:YES];
    } else {
        // 已绑定手机
        ZbBindQyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbBindQyViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    return;
}

// 关闭按钮点击
- (IBAction)btnClockOnClick:(id)sender {
    UIViewController *present = self.navigationController.presentingViewController;
    if (present != nil) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
    return;
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.rightsArray.count + 1;
}

/**
 *  设置cell的内容
 *
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if (indexPath.row < self.rightsArray.count) {
        // 权限
        cell = [tableView dequeueReusableCellWithIdentifier:@"rightsCell" forIndexPath:indexPath];
        
        // 权限label
        UILabel *labRights = (UILabel *)[cell viewWithTag:1];
        NSString *rightsName = [NSString stringWithFormat:@"%@", [self.rightsArray objectAtIndex:indexPath.row]];
        labRights.adjustsFontSizeToFitWidth = YES;
        labRights.text = rightsName;
    } else {
        // 确定按钮
        cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
        
        UIButton *btnConfirm = (UIButton *)[cell viewWithTag:1];;
        btnConfirm.layer.cornerRadius = 3;
        [btnConfirm addTarget:self action:@selector(btnConfirmOnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

/**
 *  设置cell的高度
 *
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.rightsArray.count) {
        // 权限
        return 40;
    } else {
        // 确定按钮
        return 80;
    }
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    return;
}


#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败,请稍后再试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSString *result = dict[@"errno"];
    if (result.integerValue != 0) {
        return;
    }
    [self.rightsArray removeAllObjects];
    [self.rightsArray addObjectsFromArray:(NSArray *)dict[@"data"]];
    [self.tv reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
