//
//  ZbLinkStoneViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/24.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbLinkStoneViewController.h"
#import "BATableView.h"
#import "ZbStoneTableViewCell.h"
#import "ZbIndexCharTableViewCell.h"
#import "ZbQyInfo.h"
#import "ZbStoneDetailViewController.h"
#import "ZbCompanyAddStoneViewController.h"

#define HTTP_REQUEST_STONE 0        // 主营石材
#define HTTP_REQUEST_LIMIT 1        // 关联石材数量限制

@interface ZbLinkStoneViewController () <BATableViewDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// 无记录view
@property (nonatomic, strong) IBOutlet UIView *noResultView;
// headerView
@property (nonatomic, strong) IBOutlet UIView *headerView;
// 顶部描述label
@property (nonatomic, strong) IBOutlet UILabel *labDes;
// 底部添加主营石材view
@property (nonatomic, strong) IBOutlet UIView *tvFooterView;
// 已关联石材容器
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *linkedStoneContainerArray;
// 已关联石材名背景
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *linkedStoneNameBgArray;
// 已关联石材名label
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *linkedStoneNameLabelArray;
// BATableView
@property (nonatomic, strong) BATableView *baTv;
// 完成按钮
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btnOk;

// 显示的数据
@property (nonatomic, strong) NSMutableArray *showDataArray;
// 原始已关联数据
@property (nonatomic, strong) NSMutableArray *rawArray;
// 已关联石材
@property (nonatomic, strong) NSMutableArray *linkedArray;

// 关联石材数量限制
@property (nonatomic, assign) NSInteger addStoneLimit;

@end

@implementation ZbLinkStoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.rawArray = [NSMutableArray array];
    self.showDataArray = [NSMutableArray array];
    self.linkedArray = [NSMutableArray array];
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneTableViewCell" bundle:nil] forCellReuseIdentifier:StoneCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbIndexCharTableViewCell" bundle:nil] forCellReuseIdentifier:IndexCharCellIdentifier];
    
    // 已关联石材背景圆角
    for (UIView *bg in self.linkedStoneNameBgArray) {
        bg.layer.cornerRadius = 3;
        bg.layer.masksToBounds = YES;
    }
    [self drawLinkedStoneInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self createTableView];
    [self.tv setTableHeaderView:[[UIView alloc] init]];
    if (self.StoneArray != nil) {
        // 从企业详情跳过来已带上主营石材数据
        NSMutableArray *stoneArray = self.StoneArray;
        [stoneArray enumerateObjectsUsingBlock:^(NSMutableDictionary *stoneDict, NSUInteger index, BOOL *stop) {
            NSString *name = stoneDict[@"stone_name"];
            NSString * firstPinyinLetter = [[NSString stringWithFormat:@"%c", pinyinFirstLetter([name characterAtIndex:0])] uppercaseString];
            [stoneDict setValue:firstPinyinLetter forKey:@"description"];
        }];
        NSMutableArray *tagArray = [ZbWebService sortByFirstCharact:stoneArray];
        [self.showDataArray removeAllObjects];
        [self.showDataArray addObjectsFromArray:tagArray];
        [self.baTv reloadData];
        self.title = @"主营石材";
    } else {
        [self requestStoneListData];
        [self loadAddStoneLimit];
    }
}

#pragma mark - 界面绘制
// 创建BATableView
- (void) createTableView {
    CGRect bounds = self.view.bounds;
    self.baTv = [[BATableView alloc] initWithTableView:self.tv frame:bounds];
    self.baTv.delegate = self;
}

// 绘制已关联石材信息
- (void)drawLinkedStoneInfo {
    self.labDes.hidden = self.linkedArray.count != 0;
    self.navigationItem.rightBarButtonItem = self.linkedArray.count > 0 ? self.btnOk : nil;
    
    for (NSInteger i = 0; i < 4; i++) {
        UIView *container = self.linkedStoneContainerArray[i];
        if (i >= self.linkedArray.count) {
            container.hidden = YES;
            continue;
        }
        container.hidden = NO;
        UILabel *labStoneName = self.linkedStoneNameLabelArray[i];
        labStoneName.text = self.linkedArray[i];
    }
    return;
}

#pragma mark - 接口调用
// 获取主营石材接口
- (void)requestStoneListData {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    ZbQyInfo *curinfo = [ZbWebService sharedQy];
    NSString *companyId = curinfo.company_id;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyStoneList:companyId] delegate:self];
    conn.tag = HTTP_REQUEST_STONE;
    [conn start];
    return;
}

// 获取关联石材数量限制
- (void)loadAddStoneLimit {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getAddStoneLimit] delegate:self];
    conn.tag = HTTP_REQUEST_LIMIT;
    [conn start];
}

#pragma mark - 点击事件
// 删除已关联石材
- (IBAction)btnDeleteLinkedStoneOnClick:(UIButton *)sender {
    [self.linkedArray removeObjectAtIndex:sender.tag];
    [self drawLinkedStoneInfo];
    return;
}

// 完成
- (IBAction)btnOkOnClick:(id)sender {
    NSString *linkStones = [self.linkedArray componentsJoinedByString:@"|"];
    if (self.linkOkClickBlock) {
        self.linkOkClickBlock(linkStones);
    }
    [self.navigationController popViewControllerAnimated:YES];
    return;
}

// 添加主营石材
- (IBAction)btnAddOnClick:(id)sender {
    if (self.rawArray.count >= self.addStoneLimit) {
        self.hud.labelText = @"已达关联石材数量上限";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    // 剩余可添加数量
    NSInteger left = self.addStoneLimit - self.rawArray.count;
    // 已关联数据
    NSMutableArray *stones = [NSMutableArray array];
    for (NSDictionary *stoneDict in self.rawArray) {
        NSString *name = stoneDict[@"stone_name"];
        [stones addObject:name];
    }
    
    ZbCompanyAddStoneViewController *addStone = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbCompanyAddStoneViewController"];
    addStone.canAddCount = left;
    addStone.linkedArray = stones;
    // 添加完成block
    addStone.linkOkClickBlock = ^(void) {
        [self performSelector:@selector(requestStoneListData) withObject:nil];
    };
    [self.navigationController pushViewController:addStone animated:YES];
    return;
}

#pragma mark - UITableViewDataSource
- (NSArray *) sectionIndexTitlesForABELTableView:(BATableView *)tableView {
    NSMutableArray * indexTitles = [NSMutableArray array];
    for (NSDictionary * sectionDictionary in self.showDataArray) {
        [indexTitles addObject:sectionDictionary[@"indexTitle"]];
    }
    return indexTitles;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.showDataArray.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ZbStoneTableViewCell getRowCount:self.showDataArray[section][@"data"]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbStoneTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZbStoneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:StoneCellIdentifier];
    
    // 绘制
    [cell setStoneRow:indexPath.row dataArray:self.showDataArray[indexPath.section][@"data"] ];
    if (self.StoneArray != nil) {
        // 企业详情跳过来显示主营石材
        // 石材点击
        cell.stoneClickBlock = ^(NSInteger stoneIndex, NSDictionary *stoneDict) {
            UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ZbStoneDetailViewController *controller = [main instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
            NSString *stoneName = stoneDict[@"stone_name"];
            controller.stoneName = stoneName;
            [self.navigationController pushViewController:controller animated:YES];
        };
    } else {
        // 发布产品关联石材
        // 石材点击
        cell.stoneClickBlock = ^(NSInteger stoneIndex, NSDictionary *stoneDict) {
            //        if (self.linkedArray.count >= 1) {
            //            self.hud.labelText = @"产品职能关联一种石材";
            //            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            //            return;
            //        }
            //        [self.linkedArray addObject:stoneDict[@"stone_name"]];
            //        [self drawLinkedStoneInfo];
            if (self.linkOkClickBlock) {
                self.linkOkClickBlock(stoneDict[@"stone_name"]);
            }
            [self.navigationController popViewControllerAnimated:YES];
        };
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [ZbIndexCharTableViewCell getRowHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZbIndexCharTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:IndexCharCellIdentifier];
    cell.labChar.text = self.showDataArray[section][@"indexTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_STONE) {
        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = @"加载失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self.hud hide:NO];
        NSMutableArray *stoneArray = resultDic[@"data"];
        
        [stoneArray enumerateObjectsUsingBlock:^(NSMutableDictionary *stoneDict, NSUInteger index, BOOL *stop) {
            NSString *name = stoneDict[@"stone_name"];
            NSString * firstPinyinLetter = [[NSString stringWithFormat:@"%c", pinyinFirstLetter([name characterAtIndex:0])] uppercaseString];
            [stoneDict setValue:firstPinyinLetter forKey:@"description"];
        }];
        NSMutableArray *tagArray = [ZbWebService sortByFirstCharact:stoneArray];
        [self.showDataArray removeAllObjects];
        [self.showDataArray addObjectsFromArray:tagArray];
        [self.baTv reloadData];
        self.noResultView.hidden = tagArray.count > 0;
        if (tagArray.count > 0) {
            [self.tv setTableFooterView:self.tvFooterView];
        }
    } else if (connection.tag == HTTP_REQUEST_LIMIT) {
        // 加载失败
        if (result.integerValue != 0) {
            return;
        }
        
        NSNumber *limit = resultDic[@"data"][@"limit"];
        self.addStoneLimit = limit.integerValue;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
