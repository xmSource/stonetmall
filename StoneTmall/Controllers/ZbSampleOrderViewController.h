//
//  ZbSampleOrderViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/12.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbSampleOrderViewController : BaseViewController

@property (nonatomic, strong) NSMutableArray *cartSamplyArray;

// 下单成功block
@property (nonatomic, copy) void (^payOkBlock)(void);

@end
