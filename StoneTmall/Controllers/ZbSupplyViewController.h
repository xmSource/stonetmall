//
//  ZbSupplyViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

@interface ZbSupplyViewController : BaseViewController

- (void)onPageShowChange:(BOOL)isShow;

@end
