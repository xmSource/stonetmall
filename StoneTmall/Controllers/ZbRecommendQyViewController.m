//
//  ZbRecommendQyViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/11/6.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbRecommendQyViewController.h"
#import "ZbQyListTableViewCell.h"
#import "ZbQyDetailViewController.h"

@interface ZbRecommendQyViewController () <UITableViewDataSource, UITableViewDelegate>

// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;

@end

@implementation ZbRecommendQyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.tv.tableFooterView = [[UIView alloc] init];
    [self.tv setTableFooterView:[[UIView alloc] init]];
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbQyListTableViewCell" bundle:nil] forCellReuseIdentifier:QyListCellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [ZbQyListTableViewCell getRowCount:self.array];
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbQyListTableViewCell getRowHeight];
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // 石材公司品牌、推荐品种
    ZbQyListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:QyListCellIdentifier];
    [cell setInfoRow:indexPath.row dataArray:self.array];
    cell.iconClickBlock = ^(NSInteger index, NSDictionary *infoDict) {
        ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
        controller.companyId = infoDict[@"company_id"];
        [self.navigationController pushViewController:controller animated:YES];
    };
    return cell;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
