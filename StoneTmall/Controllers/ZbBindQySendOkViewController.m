//
//  ZbBindQySendOkViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/18.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbBindQySendOkViewController.h"

@interface ZbBindQySendOkViewController ()

// 电话
@property (nonatomic, strong) IBOutlet UILabel *labTel;
// 进入应用
@property (nonatomic, strong) IBOutlet UIButton *btnEnter;

@end

@implementation ZbBindQySendOkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 圆角
    self.btnEnter.layer.cornerRadius = 3;
    
    NSDictionary *kefuDict = [ZbSaveManager getKefuDict];
    NSString *tel = kefuDict[@"user_phone"];
    if (tel != nil) {
        NSString *first = @"客服电话 ";
        // 设置文字
        NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", first, tel]];
        // 电话文字属性
        [attriString addAttribute:NSFontAttributeName
                            value:[UIFont systemFontOfSize:16]
                            range:NSMakeRange(first.length, tel.length)];
        // 电话颜色属性
        [attriString addAttribute:NSForegroundColorAttributeName
                            value:UIColorFromHexString(@"3B88E1")
                            range:NSMakeRange(first.length, tel.length)];
        self.labTel.attributedText = attriString;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 点击事件
// 进入应用按钮点击
- (IBAction)btnEnterOnClick:(id)sender {
    UIViewController *present = self.navigationController.presentingViewController;
    if (present != nil) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
