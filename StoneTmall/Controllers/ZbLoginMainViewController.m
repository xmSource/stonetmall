//
//  ZbLoginMainViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbLoginMainViewController.h"
#import "ZbLoginViewController.h"
#import "ZbRegisterViewController.h"

@interface ZbLoginMainViewController () <UIScrollViewDelegate>

// scrollview容器
@property (strong, nonatomic) IBOutlet UIScrollView *svMain;

// 登录页按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageLogin;
// 注册页按钮
@property (strong, nonatomic) IBOutlet UIButton *btnPageRegister;
// 登陆controller
@property (strong, nonatomic) ZbLoginViewController *leftPageController;
// 注册controller
@property (strong, nonatomic) ZbRegisterViewController *rightPageController;
// 选中线leading
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcSelectLineLeading;

// 当前选择页面
@property (assign, nonatomic) NSInteger curPageIndex;

@end

@implementation ZbLoginMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.svMain.scrollsToTop = NO;
    self.curPageIndex = 0;
    
    // 友盟统计用
    self.myName = @"查询容器";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 蓝色色nav，白色状态栏
    self.tabBarController.navigationController.navigationBar.barTintColor = MAIN_COLOR;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 切页逻辑
- (void)pageChange
{
    // 按钮激活状态
    self.btnPageLogin.enabled = !(self.curPageIndex == 0);
    self.btnPageRegister.enabled = (self.curPageIndex == 0);
    self.title = self.curPageIndex == 0 ? @"登录" : @"注册";
    
    // 界面显示状态
    if (self.curPageIndex == 0) {
        [self.rightPageController onNoShow];
    } else {
        [self.leftPageController onNoShow];
    }
    return;
}

#pragma mark - 点击事件
// 返回按钮点击
- (IBAction)backBarItemOnClick:(id)sender {
    [self.leftPageController onNoShow];
    [self.rightPageController onNoShow];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    return;
}

// 登录页按钮点击
- (IBAction)btnPageLoginOnClick:(id)sender {
    self.curPageIndex = 0;
    [self pageChange];
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

// 注册页按钮点击
- (IBAction)btnPageRegisterOnClick:(id)sender {
    self.curPageIndex = 1;
    [self pageChange];
    CGPoint tagPoint = CGPointMake(CGRectGetWidth(self.svMain.frame) * self.curPageIndex, 0);
    [self.svMain setContentOffset:tagPoint animated:YES];
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 选中线条跟随滚动
    CGFloat lineScrollRange = DeviceWidth / 2;
    CGFloat percent = offsetX / DeviceWidth;
    self.alcSelectLineLeading.constant = lineScrollRange * percent;
    
//    // 判断是否切页
//    if (fmod(offsetX, scrollView.frame.size.width)  == 0) {
//        int index = offsetX / scrollView.frame.size.width;
//        self.curPageIndex = index;
//        [self pageChange];
//    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Register"]) {
        self.rightPageController = segue.destinationViewController;
    } else {
        self.leftPageController = segue.destinationViewController;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
