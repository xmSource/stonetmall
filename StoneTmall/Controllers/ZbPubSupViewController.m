//
//  ZbPubSupViewController.m
//  StoneTmall
//
//  Created by chyo on 15/9/1.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbPubSupViewController.h"
#import "ZbDatePickViewController.h"
#import "ZbSelectPayWayViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"
#import "ConsWebController.h"

#define HTTP_REQUEST_LIMIT 1          // 悬赏天数、价格范围限制
#define HTTP_REQUEST_TAG_PayOrder 2   // 支付订单

@interface ZbPubSupViewController ()<UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UITextField *titleField;
@property (nonatomic, strong) IBOutlet UITextField *priceField;
@property (nonatomic, strong) IBOutlet UITextField *expireField;
@property (strong, nonatomic) IBOutlet UILabel *labCity;
@property (nonatomic, strong) IBOutlet UITextView *contView;
// 描述输入框高度约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvDesHeight;
@property (nonatomic, strong) IBOutlet UIButton *comitBtn;
@property (strong, nonatomic) IBOutlet UIView *titleView;
@property (nonatomic, strong) IBOutlet UILabel *labDesPlaceHolder;
@property (strong, nonatomic) IBOutlet UIButton *typeBtn;
@property (nonatomic, strong) IBOutlet UIView *priceView;
@property (nonatomic, strong) IBOutlet UIView *dateView;


// 选择图片按钮数组
@property (nonatomic, strong) IBOutletCollection(UIControl) NSArray *imgPickControlArray;
// 选择主图片勾选按钮数组
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *imgMainPickBtnArray;
// 图片数组数组
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *ivImageArray;

// 当前选中的主图片
@property (nonatomic, assign) NSInteger curMainImgIndex;
// 已选择图片列表
@property (nonatomic, strong) NSMutableArray *imageArray;


@property (nonatomic, strong) IBOutlet NSLayoutConstraint *contHeiConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *topConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *heightConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (nonatomic, strong) IBOutlet UIView * xsView;

// 底部按钮约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *consBotHeiConstraint;
@property (nonatomic, strong) IBOutlet UILabel *bottomLab;
@property (strong, nonatomic) IBOutlet UIButton *mianzeBtn;
@property (strong, nonatomic) IBOutlet UIButton *xieyiBtn;

@property (nonatomic) int clickIndex;

// 限制字典
@property (nonatomic, strong) NSDictionary *limitDict;

@end

@implementation ZbPubSupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.imageArray = [[NSMutableArray alloc] init];
    self.clickIndex = 0;
    self.title = @"发布信息";
    if (self.curMessageIndex != 0) {
        switch (self.curMessageIndex) {
            case Zb_Home_Message_Type_Supply:
                [self btn:self.typeBtn setTitle:@"供货信息" index:Zb_Home_Message_Type_Supply];
                self.title = @"发布供货信息";
                break;
                
            case Zb_Home_Message_Type_Buy:
                [self btn:self.typeBtn setTitle:@"求购信息" index:Zb_Home_Message_Type_Buy];
                self.title = @"发布求购信息";
                break;
                
            case Zb_Home_Message_Type_Reward:
                [self btn:self.typeBtn setTitle:@"悬赏找石" index:Zb_Home_Message_Type_Reward];
                self.title = @"发布悬赏信息";
                break;
                
            default:
                break;
        }

    } else {
        self.curMessageIndex = 0;
    }
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 注册收到微信登录请求回应的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotificationWxPayResult:) name:NOTIFICATION_WX_PAY_RESULT object:nil];
    
    // 适配iphone4s
    if (!Greater_Than_480_Height) {
        self.alcTvDesHeight.constant = 100;
    }
    
    // 加载限制信息
    [self getLimitRangeForPaidDemand];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.titleView.layer.borderWidth = 1;
    self.titleView.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.titleView.layer.cornerRadius = 2;
    self.titleView.layer.masksToBounds = YES;
    self.contView.layer.borderWidth = 1;
    self.contView.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.contView.layer.cornerRadius = 2;
    self.contView.layer.masksToBounds = YES;
    self.comitBtn.layer.cornerRadius = 2;
    self.comitBtn.layer.masksToBounds = YES;
    self.priceView.layer.borderWidth = 1;
    self.priceView.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.dateView.layer.borderWidth = 1;
    self.dateView.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    
    // 当前城市名
    BMKPoiInfo *curPoi = [[LocationServiceManager shareManager] getCurLocationPoiInfo];
    NSString *city = curPoi ? curPoi.city : @"未知城市";
    self.labCity.text = city;
    [[LocationServiceManager shareManager] updateLocationBlock:^(void) {
        BMKPoiInfo *curPoi = [[LocationServiceManager shareManager] getCurLocationPoiInfo];
        NSString *city = curPoi ? curPoi.city : @"未知城市";
        self.labCity.text = city;
        [[LocationServiceManager shareManager] updateLocationBlock:nil];
    }];
    [self loadConstract];
    [self drawPickImage];
    
    // 图片模式
    for (UIImageView *ivImage in self.ivImageArray) {
        ivImage.clipsToBounds = YES;
    }
}

// 选择图片信息绘制
- (void)drawPickImage {
    NSInteger showCount = MIN(MAX(1, self.imageArray.count + 1), 3);
    for (NSInteger i = 0; i < self.imgPickControlArray.count; i++) {
        // 显隐
        UIControl *imgPickControl = [self.imgPickControlArray objectAtIndex:i];
        imgPickControl.hidden = i >= showCount;
        
        // 图片
        UIImageView *ivImage = [self.ivImageArray objectAtIndex:i];
        ivImage.image = i < self.imageArray.count ? [self.imageArray objectAtIndex:i] : [UIImage imageNamed:@"public_pic_add"];
        // 勾选
        UIButton *btnSelect = [self.imgMainPickBtnArray objectAtIndex:i];
        btnSelect.selected = i == self.curMainImgIndex;
        btnSelect.hidden = i < self.imageArray.count ? NO : YES;
    }
    return;
}


- (void)loadConstract {
    if (self.curMessageIndex == 0 || self.curMessageIndex == 1 || self.curMessageIndex == 2) {
        self.xsView.hidden = YES;
        self.topConstraint.constant = 0;
        self.heightConstraint.constant = 0;
        self.bottomConstraint.constant = 0;
        self.contHeiConstraint.constant = 315;
        self.consBotHeiConstraint.constant = 0;
        self.bottomLab.hidden = YES;
        self.xieyiBtn.hidden = YES;
        self.mianzeBtn.hidden = YES;
    } else {
        self.xsView.hidden = NO;
        self.topConstraint.constant = 0;
        self.heightConstraint.constant = 40;
        self.bottomConstraint.constant = 10;
        self.contHeiConstraint.constant = 365;
        self.consBotHeiConstraint.constant = 25;
        self.mianzeBtn.hidden = NO;
        self.xieyiBtn.hidden = NO;
        self.bottomLab.hidden = NO;
    }
}

- (void)btn:(UIButton *)btn setTitle:(NSString *)title index:(Zb_Home_Message_Type)index   {
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateHighlighted];
    
    self.curMessageIndex = index;
    [self loadConstract];
}

#pragma mark - 接口调用
// 价格范围、天数
- (void)getLimitRangeForPaidDemand {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getLimitRangeForPaidDemand] delegate:self];
    conn.tag = HTTP_REQUEST_LIMIT;
    [conn start];
    return;
}

// 支付接口
- (void)requestPayOrder:(NSString *)orderNumber payType:(NSString *)payType {
    self.hud.labelText = @"支付中..";
    [self.hud show:NO];
    
    // 支付类型
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService payOrder:orderNumber payType:payType] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_PayOrder;
    conn.userInfo = payType;
    [conn start];
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

- (IBAction)selectType:(UIButton *)sender {
    [self.view endEditing:YES];
    
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"供货信息", @"求购信息", @"悬赏找石", nil];
    if (![ZbSaveManager isPaiddemandShow]) {
        action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"供货信息", @"求购信息", nil];
    }
    action.tag = 1;
    [action showInView:self.navigationController.view];
    return;
}

// 选择图片点击
- (IBAction)controlPickImageOnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    
    NSInteger index = sender.tag;
    if (index < self.imageArray.count) {
        return;
    }
    
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册选择", nil];
    action.tag = index;
    [action showInView:self.navigationController.view];
    return;
}

// 选择主图片点击
- (IBAction)btnPickMainImageOnClick:(UIButton *)sender {
    NSInteger index = sender.tag;
    if (index == self.curMainImgIndex) {
        return;
    }
    self.curMainImgIndex = index;
    [self drawPickImage];
    return;
}


- (IBAction)selecExpired:(id)sender {
    [self.view endEditing:YES];
    NSInteger minDay = 1;
    NSInteger maxDay = 30;
    if (self.limitDict != nil) {
        NSNumber *minNumber = self.limitDict[@"expired_day_min"];
        NSNumber *maxNumber = self.limitDict[@"expired_day_max"];
        minDay = minNumber.integerValue;
        maxDay = maxNumber.integerValue;
    }
    NSMutableArray *dayMArray = [NSMutableArray array];
    for (NSInteger i = minDay; i <= maxDay; i++) {
        [dayMArray addObject:[NSString stringWithFormat:@"%@天", [NSNumber numberWithInteger:i]]];
    }
    
    [ZbDatePickViewController showInViewController:self dataArray:dayMArray btnClickBlock:^(Zb_Dp_Click_Type ZbClickType, NSString *str) {
        switch (ZbClickType) {
            case Zb_Dp_Click_Cancel:
                return ;
                break;
              
            case Zb_Dp_Click_Sure:
                self.expireField.text = str;
                break;

            default:
                break;
        }
    }];
}
- (IBAction)isSelectXieYiClick:(UIButton *)sender {
    [self.view endEditing:YES];
    sender.selected = !sender.selected;
}

- (IBAction)xieyiOnClick:(id)sender {
    ConsWebController *webController = [[ConsWebController alloc] init];
    webController.showTitle = @"免责协议";
    webController.mainUrl = JOIN_REWARD_ITEMS_URL;
    [self.navigationController pushViewController:webController animated:YES];
    return;
}


- (IBAction)pubOnClick:(id)sender {
    [self.view endEditing:YES];
    
    if ([self.typeBtn.titleLabel.text isEqualToString:@"发布版块"]) {
        self.hud.labelText = @"请选择发布版块";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    NSString *title = [self.titleField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (title.length <= 0) {
        self.hud.labelText = @"请填写标题";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    if (self.curMessageIndex == Zb_Home_Message_Type_Reward) {
        NSInteger price = self.priceField.text.integerValue;
        NSInteger minPrice = 1;
        NSInteger maxPrice = 10000;
        if (self.limitDict != nil) {
            NSNumber *minNumber = self.limitDict[@"price_min"];
            NSNumber *maxNumber = self.limitDict[@"price_max"];
            minPrice = minNumber.integerValue / 100;
            maxPrice = maxNumber.integerValue / 100;
        }
        if (price < minPrice || price > maxPrice) {
            self.hud.labelText = [NSString stringWithFormat:@"请输入正确的金额（%@元-%@元）", [NSNumber numberWithInteger:minPrice], [NSNumber numberWithInteger:maxPrice]];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        if (self.expireField.text.length == 0) {
            self.hud.labelText = @"请填写悬赏期限";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        if (self.mianzeBtn.selected == NO) {
            self.hud.labelText = @"请勾选免责协议";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
    }
    
    NSString *content = [self.contView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (content.length <= 0) {
        self.hud.labelText = @"请填写描述";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }

    if (self.imageArray.count <= 0) {
        self.hud.labelText = @"请选择图片";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    
    // 上传请求
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置头信息
    [CTURLConnection setAFNetHeaderInfo:manager];
    
    
    NSDictionary *params;
    switch (self.curMessageIndex) {
        case Zb_Home_Message_Type_Supply:
            params = [ZbWebService addSupply:title content:content location:self.labCity.text];
            break;
            
        case Zb_Home_Message_Type_Buy:
            params = [ZbWebService addDemand:title content:content location:self.labCity.text];
            break;
            
        case Zb_Home_Message_Type_Reward:{
            NSString *expire = [self.expireField.text stringByReplacingOccurrencesOfString:@"天" withString:@""];
            params = [ZbWebService addPaidDemand:title content:content location:self.labCity.text price:self.priceField.text expired:expire];
            break;
        }
            
        default:
            break;
    }
    
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    __weak typeof(self) weakSelf = self;
    AFHTTPRequestOperation *op = [manager POST:ZB_WEBSERVICE_HOST parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        // 图片
        for (NSInteger i = 0; i < self.imageArray.count; i++) {
            UIImage *image = [self.imageArray objectAtIndex:i];
            // 图片压缩
            NSData* imageData = UIImageJPEGRepresentation(image, 0.4);
            NSString *fileName = i == self.curMainImgIndex ? @"sg_image" : [NSString stringWithFormat:@"image_%ld", (long)i];
            // 图片文件流
            [formData appendPartWithFileData:imageData name:fileName fileName:fileName mimeType:@"image/jpeg"];
        }

    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *resultDic = (NSDictionary *)responseObject;
        NSString *result = resultDic[@"errno"];
        DLog(@"%@", resultDic);

        // 加载失败
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        if (weakSelf.curMessageIndex != Zb_Home_Message_Type_Reward) {
            self.hud.labelText = @"发布成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }];
            return;
        }
        
        // 悬赏贴，继续支付
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSString *order_number = resultDic[@"data"];
            [ZbSelectPayWayViewController showInViewController:weakSelf.navigationController block:^(Zb_Pay_Way_Type type) {
                NSString *payType = [NSString stringWithFormat:@"%u", type];
                [weakSelf requestPayOrder:order_number payType:payType];
            }];
        });
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.hud.labelText = @"发布失败，请稍候重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    [op start];
    return;

}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.titleField]) {
        [self.contView becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - UITextView delegatect
- (void)textViewDidChange:(UITextView *)textView {
    self.labDesPlaceHolder.hidden = textView.text.length > 0;
    return;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    if (actionSheet.tag == 1) {
        switch (buttonIndex) {
            case 0:
                [self btn:self.typeBtn setTitle:@"供货信息" index:Zb_Home_Message_Type_Supply];
                self.title = @"发布供货信息";
                break;
                
            case 1:
                [self btn:self.typeBtn setTitle:@"求购信息" index:Zb_Home_Message_Type_Buy];
                self.title = @"发布求购信息";
                break;
                
            case 2:
                [self btn:self.typeBtn setTitle:@"悬赏找石" index:Zb_Home_Message_Type_Reward];
                self.title = @"发布悬赏信息";
                break;

            default:
                break;
        }
    } else {
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.delegate = self;
        if (buttonIndex == 0) {
            // 拍照
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else {
            // 相册选择
            imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
        });
        return;
    }
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        CGSize imgSize = portraitImg.size;
        CGSize tagSize = imgSize;
        CGFloat rate = imgSize.width / imgSize.height;
        if (imgSize.width >= imgSize.height) {
            if (imgSize.width > 1000) {
                tagSize = CGSizeMake(1000, 1000 / rate);
            }
        } else {
            if (imgSize.height > 1000) {
                tagSize = CGSizeMake(1000 * rate, 1000);
            }
        }
        // 去除图片旋转属性
        UIGraphicsBeginImageContext(tagSize);
        [portraitImg drawInRect:CGRectMake(0, 0, tagSize.width, tagSize.height)];
        UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        [self.imageArray addObject:reSizeImage];
        [self drawPickImage];
   }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    return;
}

#pragma mark - 微信
// 微信授权结果通知
- (void)onNotificationWxPayResult:(NSNotification *)notify {
    [self.hud hide:NO];
    PayResp *resp = notify.object;
    switch(resp.errCode){
        case WXSuccess:
        {
            //服务器端查询支付通知或查询API返回的结果再提示成功
            NSString *info = [NSString stringWithFormat:@"恭喜您支付成功"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:info delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            break;
        }
        default:
        {
            DLog(@"支付失败，retcode=%d",resp.errCode);
            // 支付失败
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            break;
        }
    }
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    if (connection.tag == HTTP_REQUEST_TAG_PayOrder) {
        self.hud.labelText = @"提交失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    } else {
        [self.hud hide:NO];
    }
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_TAG_PayOrder) {
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self.hud hide:NO];
        Zb_Pay_Way_Type payType = ((NSString *)connection.userInfo).intValue;
        NSDictionary *pay_charge = resultDic[@"data"][@"pay_charge"];
        if (payType == Zb_Pay_Way_Zfb) {
            NSString *orderString = pay_charge[@"order_string"];
            // 支付
            [[AlipaySDK defaultService] payOrder:orderString fromScheme:WX_URL_SCHEME callback:^(NSDictionary *resultDic) {
                DLog(@"##order detail alipay reslut = %@",resultDic);
                NSString *resultStatus = resultDic[@"resultStatus"];
                if (resultStatus.integerValue != 9000) {
                    // 支付失败
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
                
                NSString *info = [NSString stringWithFormat:@"恭喜您支付成功"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:info delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
            }];
        } else {
            PayReq *request = [[PayReq alloc] init];
            request.openID = pay_charge[@"appid"];
            request.partnerId = pay_charge[@"partnerid"];
            request.prepayId = pay_charge[@"prepayid"];
            request.package = pay_charge[@"package"];
            request.nonceStr = pay_charge[@"noncestr"];
            NSString *time = pay_charge[@"timestamp"];
            request.timeStamp = time.integerValue;
            request.sign = pay_charge[@"sign"];
            [WXApi sendReq:request];
        }
    } else if (connection.tag == HTTP_REQUEST_LIMIT) {
        if (result.integerValue != 0) {
            return;
        }
        
        self.limitDict = resultDic[@"data"];
    }
    return;
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
