//
//  ZbSupplyDemandDetailViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSupplyDemandDetailViewController.h"
#import "ZbTypeTableViewCell.h"
#import <ShareSDK/ShareSDK.h>
#import "ZbLoginMainViewController.h"
#import "ZbPostOwnerTableViewCell.h"
#import "ZbPostCommentTableViewCell.h"
#import "UITableView+FDTemplateLayoutCell.h"
#import "ZbSeparateTableViewCell.h"
#import "ZLPhoto.h"
#import "ZbPersonInfoViewController.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "ChatViewController.h"

#define HTTP_REQUEST_TAG_IMAGE 1          // 图片
#define HTTP_REQUEST_TAG_COMMENT 2        // 评论
#define HTTP_REQUEST_TAG_INFO 3           // 详情
#define HTTP_REQUEST_TAG_MARK 6               // 收藏
#define HTTP_REQUEST_TAG_UNMARK 7             // 取消收藏
#define HTTP_REQUEST_TAG_SEND 8               // 评论

// section显示的内容类型
typedef enum {
    Zb_Message_Section_Type_Owner,        // 发帖人
    Zb_Message_Section_Type_Reply,        // 回复
} Zb_Message_Section_Type;

@interface ZbSupplyDemandDetailViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, ZLPhotoPickerBrowserViewControllerDataSource, ZLPhotoPickerBrowserViewControllerDelegate, UIAlertViewDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 类型背景
@property (nonatomic, strong) IBOutlet UIView *typeBg;
// 类型名
@property (nonatomic, strong) IBOutlet UILabel *labTypeName;
// 标题
@property (nonatomic, strong) IBOutlet UILabel *labTitle;
// 地点
@property (nonatomic, strong) IBOutlet UILabel *labLocaition;
// 时间
@property (nonatomic, strong) IBOutlet UILabel *labDate;
// 浏览
@property (nonatomic, strong) IBOutlet UILabel *labClicks;
// 内容
@property (nonatomic, strong) IBOutlet UILabel *labContent;
// 图片数组
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *ivArray;
// 收藏按钮
@property (nonatomic, strong) IBOutlet UIButton *btnMark;
// 输入框背景
@property (nonatomic, strong) IBOutlet UIView *bgInput;
// 回复输入框
@property (nonatomic, strong) IBOutlet UITextField *tfComment;
// 回复按钮
@property (nonatomic, strong) IBOutlet UIButton *btnComment;
// 输入区域bottom的约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcBottomViewBottom;

// 显示的section类型数组
@property (nonatomic, strong) NSArray *showSectionArray;

// 回复数组
@property (nonatomic, strong) NSMutableArray *replyArray;
// 图片数组
@property (nonatomic, strong) NSMutableArray *imageArray;

// 当前帖子详情字典
@property (nonatomic, strong) NSMutableDictionary *curPostDetailDict;

@end

@implementation ZbSupplyDemandDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.replyArray = [NSMutableArray array];
    self.imageArray = [NSMutableArray array];
    self.showSectionArray = [NSArray array];
    
    // 圆角
    self.typeBg.layer.cornerRadius = 3;
    self.typeBg.clipsToBounds = YES;
    self.bgInput.layer.cornerRadius = 3;
    self.bgInput.layer.masksToBounds = YES;
    self.bgInput.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    self.bgInput.layer.borderWidth = 1;
    self.btnComment.layer.cornerRadius = 3;
    
    // 边框
    for (UIImageView *ivImage in self.ivArray) {
        ivImage.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
        ivImage.layer.borderWidth = 1;
        ivImage.layer.masksToBounds = YES;
    }
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbPostOwnerTableViewCell" bundle:nil] forCellReuseIdentifier:SupplyDemandOwnerCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbPostCommentTableViewCell" bundle:nil] forCellReuseIdentifier:PostCommentCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbTypeTableViewCell" bundle:nil] forCellReuseIdentifier:TypeHeaderIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSeparateTableViewCell" bundle:nil] forCellReuseIdentifier:SeparateCellIdentifier];
    [self.tv setTableHeaderView:nil];
    
    // 上拉重新加载
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    
    // 键盘通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    // 获取数据
    [self requestCommentListData];
//    [self requestImageListData];
    [self requestDetailData];
}

// 登录判断
- (void)requestLogin {
    self.hud.labelText = @"尚未登录，无法操作";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:^(void) {
        UIStoryboard *loginSb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        UINavigationController *nav = [loginSb instantiateViewControllerWithIdentifier:@"LoginNav"];
        ZbLoginMainViewController *loginController = (ZbLoginMainViewController *)nav.viewControllers[0];
        loginController.loginSucessBlock = ^(void) {
            [self.tv.header performSelectorOnMainThread:@selector(beginRefreshing) withObject:nil waitUntilDone:NO];
        };
        [self.view.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }];
    return;
}

#pragma mark - 界面绘制
// 信息绘制
- (void)drawInfo {
    // 类型名
    NSString *postType = self.curPostDetailDict[@"post_type"];
    if (postType.intValue == Zb_Home_Message_Type_Supply) {
        self.labTypeName.text = @"供应";
        self.typeBg.backgroundColor = UIColorFromHexString(@"4A90E2");
        self.title = @"供货信息";
        
        self.showSectionArray = @[[NSNumber numberWithInt:Zb_Message_Section_Type_Owner], [NSNumber numberWithInt:Zb_Message_Section_Type_Reply]];
    } else if (postType.intValue == Zb_Home_Message_Type_Buy) {
        self.labTypeName.text = @"求购";
        self.typeBg.backgroundColor = UIColorFromHexString(@"00CFA0");
        self.title = @"求购信息";
        
        self.showSectionArray = @[[NSNumber numberWithInt:Zb_Message_Section_Type_Owner], [NSNumber numberWithInt:Zb_Message_Section_Type_Reply]];
    } else {
        self.labTypeName.text = @"行业";
        self.typeBg.backgroundColor = UIColorFromHexString(@"00CFA0");
        self.title = @"行业新闻";
        
        self.showSectionArray = @[[NSNumber numberWithInt:Zb_Message_Section_Type_Reply]];
    }
    // 标题
    self.labTitle.text = self.curPostDetailDict[@"post_title"];
    self.labContent.text = self.curPostDetailDict[@"post_content"];
    // 地点
    self.labLocaition.text = self.curPostDetailDict[@"post_location"];
    // 时间
    self.labDate.text = [CommonCode getTimeShowText:self.curPostDetailDict[@"post_modified"]];
    // 浏览量
    self.labClicks.text = [NSString stringWithFormat:@"%@浏览", self.curPostDetailDict[@"post_clicks"]];
    
    // header高度
    [self calcTvHeader];
    return;
}

// 详情绘制
- (void)drawDetailInfo {
    // 绘制信息
    [self drawInfo];
    // 收藏状态
    [self drawMarkStatus];
    // 图片
    NSInteger hasImageCount = 1 + self.imageArray.count;
    for (NSInteger i = 0; i < self.ivArray.count; i++) {
        UIImageView *ivImage = [self.ivArray objectAtIndex:i];
        ivImage.tag = i;
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageOnClick:)];
        [ivImage addGestureRecognizer:ges];
        if (i >= hasImageCount) {
            ivImage.hidden = YES;
            continue;
        }
        ivImage.hidden = NO;
        if (i == 0) {
            NSString *imageUrl = self.curPostDetailDict[@"post_image"];
            [ivImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imageUrl]]];
        } else {
            NSString *imageUrl = [self.imageArray objectAtIndex:(i - 1)];
            [ivImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imageUrl]]];
        }
    }
    return;
}

// 重算header高度
- (void)calcTvHeader {
    self.labTitle.preferredMaxLayoutWidth = DeviceWidth - (15*2 + 30 + 8);
    self.labContent.preferredMaxLayoutWidth = DeviceWidth - 15*2;
    // header高度
    CGFloat tagHeaderHeight = [self.tvHeader systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect headerFrame = CGRectMake(0, 0, DeviceWidth, tagHeaderHeight);
    self.tvHeader.frame = headerFrame;
    [self.tv setTableHeaderView:self.tvHeader];
}

// 绘制收藏状态
- (void)drawMarkStatus {
    NSNumber *mark = self.curPostDetailDict[@"post_mark"];
    self.btnMark.selected = mark.integerValue > 0;
    return;
}

#pragma mark - 接口调用
// 获取图片接口
- (void)requestImageListData {
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getSupplyDemandImageList:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_IMAGE;
    [conn start];
    return;
}

// 获取评论接口
- (void)requestCommentListData {
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCommentList:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_COMMENT;
    [conn start];
    return;
}

// 详情接口
- (void)requestDetailData {
    NSString *postId = self.curPostId;
    CTURLConnection *conn;
    if (self.isNews) {
        // 新闻
        conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getNewsInfo:postId] delegate:self];
    } else {
        // 供需
        conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getSupplyDemandInfo:postId] delegate:self];
    }
    conn.tag = HTTP_REQUEST_TAG_INFO;
    [conn start];
}

// 收藏接口
- (void)requestMark {
    self.hud.labelText = @"收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService markPost:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MARK;
    [conn start];
    return;
}

// 取消收藏接口
- (void)requestUnMark {
    self.hud.labelText = @"取消收藏成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService unmarkPost:postId] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_UNMARK;
    [conn start];
    return;
}

// 评论帖子
- (void)requestComment {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    NSString *postId = self.curPostId;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService addComment:postId content:self.tfComment.text] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_SEND;
    [conn start];
    return;
}

#pragma mark - 点击事件
// 图片点击
- (void)imageOnClick:(UITapGestureRecognizer *)ges {
    UIImageView *iv = (UIImageView *)ges.view;
    
    // 图片游览器
    ZLPhotoPickerBrowserViewController *pickerBrowser = [[ZLPhotoPickerBrowserViewController alloc] init];
    // 淡入淡出效果
    // pickerBrowser.status = UIViewAnimationAnimationStatusFade;
    //    pickerBrowser.toView = btn;
    // 数据源/delegate
    pickerBrowser.delegate = self;
    pickerBrowser.dataSource = self;
    // 当前选中的值
    pickerBrowser.currentIndexPath = [NSIndexPath indexPathForItem:iv.tag inSection:0];
    // 展示控制器
    [pickerBrowser showPickerVc:self.navigationController];
    return;
}

// 收藏按钮点击
- (IBAction)btnMarkOnClick:(id)sender {
    [self.tfComment resignFirstResponder];
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    NSNumber *mark = self.curPostDetailDict[@"post_mark"];
    NSNumber *marks = self.curPostDetailDict[@"post_marks"];
    if (mark.integerValue == 0) {
        // 未收藏，则收藏
        [self requestMark];
        [self.curPostDetailDict setValue:[NSNumber numberWithInteger:1] forKey:@"post_mark"];
        [self.curPostDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue + 1)] forKey:@"post_marks"];
    } else {
        // 已收藏，则取消
        [self requestUnMark];
        [self.curPostDetailDict setValue:[NSNumber numberWithInteger:0] forKey:@"post_mark"];
        [self.curPostDetailDict setValue:[NSNumber numberWithInteger:(marks.integerValue - 1)] forKey:@"post_marks"];
    }
    [self drawMarkStatus];
    return;
}
// 分享按钮点击
- (IBAction)btnShareOnClick:(id)sender {
    [self.tfComment resignFirstResponder];
    NSString *postType = self.curPostDetailDict[@"post_type"];
    // 产品信息
    NSString *productTitle = self.curPostDetailDict[@"post_title"];
    NSString *des = self.curPostDetailDict[@"post_content"];
    if (des.length > SHARE_CONTENT_MAX_LEN) {
        des = [des substringToIndex:SHARE_CONTENT_MAX_LEN];
        des = [NSString stringWithFormat:@"%@...", des];
    }
    id imgUrl = [ZbWebService getImageFullUrl:self.curPostDetailDict[@"post_image"]];
    UIImage *shareImage = [[SDWebImageManager sharedManager].imageCache imageFromDiskCacheForKey:[[SDWebImageManager sharedManager] cacheKeyForURL:[NSURL URLWithString:imgUrl]]];
    if (shareImage != nil) {
        CGSize imgSize = CGSizeMake(120, 120);
        // 去除图片旋转属性
        UIGraphicsBeginImageContext(imgSize);
        [shareImage drawInRect:CGRectMake(0, 0, imgSize.width, imgSize.height)];
        UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        imgUrl = [ShareSDK jpegImageWithImage:reSizeImage quality:0.8];
    } else {
        imgUrl = [ShareSDK imageWithUrl:imgUrl];
    }
    // 分享信息
    NSString *title;
    int tag;
    if (postType.intValue == Zb_Home_Message_Type_Supply) {
        title = [NSString stringWithFormat:@"[供应]-%@", productTitle];
        tag = Zb_Share_Ext_Type_SAndD;
    } else if (postType.intValue == Zb_Home_Message_Type_Buy) {
        title = [NSString stringWithFormat:@"[求购]-%@", productTitle];
        tag = Zb_Share_Ext_Type_SAndD;
    } else {
        title = [NSString stringWithFormat:@"[行业]-%@", productTitle];
        tag = Zb_Share_Ext_Type_News;
    }
    NSString *shareUrl = [ZbWebService shareSAndDUrl:self.curPostId];
//    NSDictionary *appDataDict = @{@"tag":[NSNumber numberWithInt:tag], @"post_id":self.curPostId};
//    NSString *appDataJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:appDataDict options:0 error:nil] encoding:NSUTF8StringEncoding];
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:des
                                       defaultContent:@""
                                                image:imgUrl
                                                title:title
                                                  url:shareUrl
                                          description:@"供需详情"
                                            mediaType:SSPublishContentMediaTypeNews];
    // 微信好友数据
    [publishContent addWeixinSessionUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeApp] content:des title:title url:shareUrl image:imgUrl musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
    // 微信朋友圈数据
    [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInt:SSPublishContentMediaTypeNews] content:des title:title url:shareUrl image:imgUrl musicFileUrl:nil extInfo:nil fileData:nil emoticonData:nil];
    // 短信数据
    [publishContent addSMSUnitWithContent:[NSString stringWithFormat:@"[石猫]向你推荐： %@ ，链接为：%@", productTitle, shareUrl]];
    
    // 分享按钮列表
    NSArray *shareList;
    if ([WXApi isWXAppInstalled] && [QQApiInterface isQQInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeQQ, ShareTypeSMS, nil];
    } else if ([WXApi isWXAppInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeWeixiSession, ShareTypeWeixiTimeline, ShareTypeSMS, nil];
    } else if ([QQApiInterface isQQInstalled]) {
        shareList = [ShareSDK getShareListWithType:ShareTypeQQ, ShareTypeSMS, nil];
    } else {
        shareList = [ShareSDK getShareListWithType:ShareTypeSMS, nil];
    }
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:shareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];
    return;
}

// 评论按钮点击
- (IBAction)btnCommentOnClick:(id)sender {
    [self.tfComment resignFirstResponder];
    // 登录判断
    if (![ZbWebService isLogin]) {
        [self requestLogin];
        return;
    }
    
    if (self.tfComment.text.length < 4) {
        self.hud.labelText = @"回复内容过短";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (self.tfComment.text.length > 700) {
        self.hud.labelText = @"回复内容过长";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    [self requestComment];
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self btnCommentOnClick:nil];
    return YES;
}

# pragma mark - keyboard Notification
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    CGFloat bottomSpace = keyboardBounds.size.height;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    self.alcBottomViewBottom.constant = bottomSpace;
    [self.view layoutIfNeeded];
    
    // commit animations
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    CGFloat bottomSpace = 0;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    self.alcBottomViewBottom.constant = bottomSpace;
    [self.view layoutIfNeeded];
    
    // commit animations
    [UIView commitAnimations];
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return self.showSectionArray.count;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSNumber *sectionNumber = [self.showSectionArray objectAtIndex:section];
    switch (sectionNumber.intValue) {
        case Zb_Message_Section_Type_Owner:
            // 求购者信息
            return 2;
        case Zb_Message_Section_Type_Reply:
            // 回复
            return self.replyArray.count + 1;
            
        default:
            break;
    }
    return 0;
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *sectionNumber = [self.showSectionArray objectAtIndex:indexPath.section];
    switch (sectionNumber.intValue) {
        case Zb_Message_Section_Type_Owner:
            // 求购者信息
            if (indexPath.row == 0) {
                return [ZbPostOwnerTableViewCell getRowHeight];
            }
            return [ZbSeparateTableViewCell getRowHeight];
        case Zb_Message_Section_Type_Reply:
        {
            // 回复
            if (indexPath.row == 0) {
                return [ZbTypeTableViewCell getRowHeight];
            }
            CGFloat height = [tableView fd_heightForCellWithIdentifier:PostCommentCellIdentifier configuration:^(ZbPostCommentTableViewCell *cell) {
                NSDictionary *curDict = [self.replyArray objectAtIndex:(indexPath.row - 1)];
                [cell setCommentDictForCalc:curDict];
            }];
            return height;
        }
        default:
            break;
    }
    return 0;
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *sectionNumber = [self.showSectionArray objectAtIndex:indexPath.section];
    switch (sectionNumber.intValue) {
        case Zb_Message_Section_Type_Owner:
        {
            // 求购者信息
            if (indexPath.row == 0) {
                ZbPostOwnerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SupplyDemandOwnerCellIdentifier forIndexPath:indexPath];
                [cell setPostDict:self.curPostDetailDict];
                NSString *postType = self.curPostDetailDict[@"post_type"];
                if (postType.intValue == Zb_Home_Message_Type_Supply) {
                    cell.labOwnerType.text = @"供货人信息";
                } else {
                    cell.labOwnerType.text = @"求购者信息";
                }
                // 微聊点击
                NSString *userName = self.curPostDetailDict[@"user_name"];
                cell.chatClickBlock = ^(void) {
                    // 登录判断
                    if (![ZbWebService isLogin]) {
                        [self requestLogin];
                        return;
                    }
                    // 聊天系统登录判断
                    if (![[[EaseMob sharedInstance] chatManager] isLoggedIn]) {
                        self.hud.labelText = @"聊天系统数据正在加载中..请稍候";
                        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                        return;
                    };
                    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
                    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
                    if (loginUsername && loginUsername.length > 0) {
                        if ([loginUsername isEqualToString:userName]) {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notChatSelf", @"can't talk to yourself") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
                            [alertView show];
                            
                            return;
                        }
                    }
                    // 有可能不是好友，因此添加此用户昵称信息
                    [ZbSaveManager addUserInfo:@{@"user_name":userName, @"user_nickname":self.curPostDetailDict[@"user_nickname"], @"user_image":self.curPostDetailDict[@"user_image"]}];
                    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:userName isGroup:NO];
                    [self.navigationController pushViewController:chatVC animated:YES];
                };
                return cell;
            }
            ZbSeparateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier forIndexPath:indexPath];
            return cell;
        }
        case Zb_Message_Section_Type_Reply:
        {
            // 回复
            if (indexPath.row == 0) {
                ZbTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier forIndexPath:indexPath];
                cell.labTypeName.text = [NSString stringWithFormat:@"回复（%lu）", (unsigned long)self.replyArray.count];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            ZbPostCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PostCommentCellIdentifier forIndexPath:indexPath];
            NSDictionary *curDict = [self.replyArray objectAtIndex:(indexPath.row - 1)];
            [cell setCommentDict:curDict];
            cell.replyClickBlock = ^(NSDictionary *commentDict) {
                // 评论
                
            };
            return cell;
        }
        default:
            return nil;
    }
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSNumber *sectionNumber = [self.showSectionArray objectAtIndex:indexPath.section];
    switch (sectionNumber.intValue) {
        case Zb_Message_Section_Type_Owner:
        {
            // 发帖人信息
            if (indexPath.row == 0) {
                [self.tfComment resignFirstResponder];
                // 登录判断
                if (![ZbWebService isLogin]) {
                    [self requestLogin];
                    return;
                }
                
                NSString *userName = self.curPostDetailDict[@"user_name"];
                NSString *nickName = self.curPostDetailDict[@"user_nickname"];
                ZbPersonInfoViewController *person = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbPersonInfoViewController"];
                person.userName = userName;
                if (nickName != nil && [nickName isKindOfClass:[NSString class]] && nickName.length > 0) {
                    person.nickName = nickName;
                }
                [self.navigationController pushViewController:person animated:YES];
            }
            break;
        }
        case Zb_Message_Section_Type_Reply:
        {
            // 回复
            if (indexPath.row == 0) {
            } else {
                [self.tfComment resignFirstResponder];
                // 登录判断
                if (![ZbWebService isLogin]) {
                    [self requestLogin];
                    return;
                }
                
                NSDictionary *curDict = [self.replyArray objectAtIndex:(indexPath.row - 1)];
                NSString *userName = curDict[@"user_name"];
                NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
                NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
                if (loginUsername && loginUsername.length > 0) {
                    if ([loginUsername isEqualToString:userName]) {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notChatSelf", @"can't talk to yourself") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
                        [alertView show];
                        
                        return;
                    }
                }
                NSString *nickName = self.curPostDetailDict[@"user_nickname"];
                ZbPersonInfoViewController *person = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbPersonInfoViewController"];
                person.userName = userName;
                if (nickName != nil && [nickName isKindOfClass:[NSString class]] && nickName.length > 0) {
                    person.nickName = nickName;
                }
                [self.navigationController pushViewController:person animated:YES];
            }
            break;
        }
        default:
            break;
    }
    return;
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    [self requestCommentListData];
//    [self requestImageListData];
    [self requestDetailData];
}

#pragma mark - <ZLPhotoPickerBrowserViewControllerDataSource>
- (long)numberOfSectionInPhotosInPickerBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser{
    return 1;
}

- (long)photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser numberOfItemsInSection:(NSUInteger)section{
    NSInteger hasImageCount = 1 + self.imageArray.count;
    return hasImageCount;
}

#pragma mark - 每个组展示什么图片,需要包装下ZLPhotoPickerBrowserPhoto
- (ZLPhotoPickerBrowserPhoto *) photoBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser photoAtIndexPath:(NSIndexPath *)indexPath{
    // 图片数据
    NSString *imgUrl;
    if (indexPath.row == 0) {
        imgUrl = self.curPostDetailDict[@"post_image"];
    } else {
        imgUrl = [self.imageArray objectAtIndex:(indexPath.row - 1)];
    }
    
    ZLPhotoPickerBrowserPhoto *photo = [[ZLPhotoPickerBrowserPhoto alloc] init];
    photo.photoURL = [NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]];

//    photo.toView = [self.ivArray objectAtIndex:indexPath.row];
    return photo;
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    [self.hud hide:NO];
    // 加载失败
    if (result.integerValue != 0) {
        NSString *msg = resultDic[@"msg"];
        if (connection.tag == HTTP_REQUEST_TAG_INFO) {
            if ([msg hasPrefix:@"invalid postId"]) {
                [self.hud hide:NO];
                // 无效帖子
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"帖子异常，或已被删除。确定返回" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
                return;
            }
        }
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        [self.tv.header endRefreshing];
        return;
    }
    
    if (connection.tag == HTTP_REQUEST_TAG_IMAGE) {
        // 图片
        NSMutableArray *array = resultDic[@"data"];
        [self.imageArray removeAllObjects];
        [self.imageArray addObjectsFromArray:array];
        
//        if (self.curPostDetailDict != nil) {
//            [self drawDetailInfo];
//        }
    } else if (connection.tag == HTTP_REQUEST_TAG_COMMENT) {
        // 评论列表
        NSMutableArray *array = resultDic[@"data"];
        [self.replyArray removeAllObjects];
        [self.replyArray addObjectsFromArray:array];
        
        [self.tv reloadData];
//        [self requestDetailData];
    } else if (connection.tag == HTTP_REQUEST_TAG_INFO) {
        // 详情
        NSMutableDictionary *curDict = resultDic[@"data"];
        self.curPostDetailDict = curDict;
        NSArray *images = curDict[@"post_images"];
        [self.imageArray removeAllObjects];
        [self.imageArray addObjectsFromArray:images];
        
        [self.tv.header endRefreshing];
        [self drawDetailInfo];
        [self.tv reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_SEND) {
        // 回复帖子
        self.tfComment.text = @"";
        self.hud.labelText = @"回复成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
            [self.tv.header beginRefreshing];
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
