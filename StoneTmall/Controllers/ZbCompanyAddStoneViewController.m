//
//  ZbCompanyAddStoneViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/10/11.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbCompanyAddStoneViewController.h"
#import "ZbStoneTableViewCell.h"
#import "ZbStoneDetailViewController.h"
#import "BATableView.h"
#import "ZbIndexCharTableViewCell.h"

#define HTTP_REQUEST_LOADDATA 0        // 下拉刷新数据
#define HTTP_REQUEST_LOADMORE 1        // 上拉加载更多数据
#define HTTP_REQUEST_ADD 10            // 添加关联石材

@interface ZbCompanyAddStoneViewController () <UITextFieldDelegate, BATableViewDelegate>

// 导航栏titleView
@property (nonatomic, strong) IBOutlet UIView *navTitleView;
// 导航栏搜索栏
@property (nonatomic, strong) IBOutlet UITextField *tfSearch;
// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// headerView
@property (nonatomic, strong) IBOutlet UIView *headerView;
// 顶部描述label
@property (nonatomic, strong) IBOutlet UILabel *labDes;
// 已关联石材容器
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *linkedStoneContainerArray;
// 已关联石材名背景
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *linkedStoneNameBgArray;
// 已关联石材名label
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *linkedStoneNameLabelArray;
// BATableView
@property (nonatomic, strong) BATableView *baTv;
// 完成按钮
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btnOk;

// 所有数据
@property (nonatomic, strong) NSMutableArray *dataArray;
// 已关联石材
@property (nonatomic, strong) NSMutableArray *selectArray;

// 是否搜索状态
@property (nonatomic, assign) BOOL isSearching;
// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

@end

@implementation ZbCompanyAddStoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    self.selectArray = [NSMutableArray array];
    
    // 圆角
    self.navTitleView.layer.cornerRadius = CGRectGetHeight(self.navTitleView.frame) / 2;
    self.navTitleView.layer.masksToBounds = YES;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneTableViewCell" bundle:nil] forCellReuseIdentifier:StoneCellIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbIndexCharTableViewCell" bundle:nil] forCellReuseIdentifier:IndexCharCellIdentifier];
    
    //添加上拉加载更多
    __weak typeof(self) weakSelf = self;
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
    
    // 已关联石材背景圆角
    for (UIView *bg in self.linkedStoneNameBgArray) {
        bg.layer.cornerRadius = 3;
        bg.layer.masksToBounds = YES;
    }
    [self drawLinkedStoneInfo];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self createTableView];
    
    self.hud.labelText = @"加载中..";
    [self.hud show:NO];
    [self requestSearchData:self.tfSearch.text];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 界面绘制
// 创建BATableView
- (void) createTableView {
    CGRect bounds = self.view.bounds;
    bounds.origin.y = 45;
    bounds.size.height -= 45;
    self.baTv = [[BATableView alloc] initWithTableView:self.tv frame:bounds];
    self.baTv.delegate = self;
}

// 绘制已关联石材信息
- (void)drawLinkedStoneInfo {
    self.labDes.hidden = self.selectArray.count != 0;
    self.navigationItem.rightBarButtonItem = self.selectArray.count > 0 ? self.btnOk : nil;
    
    for (NSInteger i = 0; i < 4; i++) {
        UIView *container = self.linkedStoneContainerArray[i];
        if (i >= self.selectArray.count) {
            container.hidden = YES;
            continue;
        }
        container.hidden = NO;
        UILabel *labStoneName = self.linkedStoneNameLabelArray[i];
        labStoneName.text = self.selectArray[i];
    }
    return;
}

#pragma mark - 点击事件
- (IBAction)leftBarItemOnClick:(id)sender {
    self.isSearching = NO;
    [self.tfSearch resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}
// 删除已关联石材
- (IBAction)btnDeleteLinkedStoneOnClick:(UIButton *)sender {
    [self.selectArray removeObjectAtIndex:sender.tag];
    [self drawLinkedStoneInfo];
    return;
}

// 完成
- (IBAction)btnOkOnClick:(id)sender {
    [self requestAddStone:0];
    return;
}

#pragma mark - 接口调用
// 查询数据接口
- (void)requestSearchData:(NSString *)key {
    self.isSearching = YES;
    // 类别
    NSString *type = @"";
    // 颜色
    NSString *color = @"";
    // 纹理
    NSString *texture = @"";
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneList:key type:type color:color texture:texture pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_LOADDATA;
    [conn start];
    return;
}

// 上拉加载更多数据列表接口
- (void)requestMoreSearchData:(NSString *)key {
    // 类别
    NSString *type = @"";
    // 颜色
    NSString *color = @"";
    // 纹理
    NSString *texture = @"";
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getStoneList:key type:type color:color texture:texture pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_LOADMORE;
    [conn start];
    return;
}

// 添加关联石材
- (void)requestAddStone:(NSInteger)index {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *stoneName = [self.selectArray objectAtIndex:index];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService addCompanyStone:stoneName] delegate:self];
    conn.tag = HTTP_REQUEST_ADD + index;
    [conn start];
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)textFieldChanged:(UITextField *)sender {
    if (sender.markedTextRange != nil) {
        return;
    }
    [self requestSearchData:sender.text];
    return;
}

#pragma mark - UITableViewDataSource
- (NSArray *) sectionIndexTitlesForABELTableView:(BATableView *)tableView {
    NSMutableArray * indexTitles = [NSMutableArray array];
    for (NSDictionary * sectionDictionary in self.dataArray) {
        [indexTitles addObject:sectionDictionary[@"indexTitle"]];
    }
    return indexTitles;
}

// section数
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

// 行数
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ZbStoneTableViewCell getRowCount:self.dataArray[section][@"data"]];
}

// 行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbStoneTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZbStoneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:StoneCellIdentifier];
    [cell setStoneRow:indexPath.row dataArray:self.dataArray[indexPath.section][@"data"] ];
    // 石材点击
    cell.stoneClickBlock = ^(NSInteger stoneIndex, NSDictionary *stoneDict) {
        [self.tfSearch resignFirstResponder];
        if (self.selectArray.count >= self.canAddCount) {
            self.hud.labelText = @"已达关联石材数量上限";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        if (self.selectArray.count >= 4) {
            self.hud.labelText = @"单次最多关联4种石材";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        NSString *stoneName = stoneDict[@"stone_name"];
        if ([self.selectArray containsObject:stoneName]) {
            self.hud.labelText = @"请勿重复添加";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        if ([self.linkedArray containsObject:stoneName]) {
            self.hud.labelText = @"该石材已经关联";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self.selectArray addObject:stoneName];
        [self drawLinkedStoneInfo];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [ZbIndexCharTableViewCell getRowHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZbIndexCharTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:IndexCharCellIdentifier];
    cell.labChar.text = self.dataArray[section][@"indexTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 上拉或下拉刷新
/**
 *  上拉刷新
 */
- (void)footerRefresh {
    [self requestMoreSearchData:self.tfSearch.text];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.hud hide:NO];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    // 用户已结束搜索，有些请求还在路上，不处理
    if (!self.isSearching) {
        return;
    }
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_LOADDATA) {
        [self.hud hide:NO];
        // 加载失败
        if (result.integerValue != 0) {
            return;
        }
        
        NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
        // 下拉重刷
        [dataArray enumerateObjectsUsingBlock:^(NSMutableDictionary *stoneDict, NSUInteger index, BOOL *stop) {
            NSString *name = stoneDict[@"stone_name"];
            NSString * firstPinyinLetter = [[NSString stringWithFormat:@"%c", pinyinFirstLetter([name characterAtIndex:0])] uppercaseString];
            [stoneDict setValue:firstPinyinLetter forKey:@"description"];
        }];
        NSMutableArray *tagArray = [ZbWebService sortByFirstCharact:dataArray];
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:tagArray];
        if (dataArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        [self.baTv reloadData];
        if (self.dataArray.count > 0) {
            [self.tv scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    } else if (connection.tag == HTTP_REQUEST_LOADMORE) {
        // 加载失败
        if (result.integerValue != 0) {
            return;
        }
        
        NSMutableArray *dataArray = resultDic[@"data"][@"rows"];
        // 上拉加载更多
        [dataArray enumerateObjectsUsingBlock:^(NSMutableDictionary *stoneDict, NSUInteger index, BOOL *stop) {
            NSString *name = stoneDict[@"stone_name"];
            NSString * firstPinyinLetter = [[NSString stringWithFormat:@"%c", pinyinFirstLetter([name characterAtIndex:0])] uppercaseString];
            [stoneDict setValue:firstPinyinLetter forKey:@"description"];
        }];
        NSMutableArray *tagArray = [ZbWebService sortByFirstCharact:dataArray];
        // 拼接首字母数组
        [ZbWebService firstCharactArrayJoin:self.dataArray addArray:tagArray];
        if (dataArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.baTv reloadData];
    } else if (connection.tag >= HTTP_REQUEST_ADD) {
        // 添加关联石材
        if (result.integerValue != 0) {
            self.hud.labelText = @"提交失败，请稍候重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        NSInteger index = connection.tag - HTTP_REQUEST_ADD;
        NSInteger next = index + 1;
        if (next >= self.selectArray.count) {
            // 已添加完毕
            self.hud.labelText = @"添加成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
                if (self.linkOkClickBlock) {
                    self.linkOkClickBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
        } else {
            // 添加下一个
            [self requestAddStone:next];
        }
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
