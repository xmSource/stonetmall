//
//  ZbQyLocateViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/13.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQyLocateViewController.h"
#import "LocationServiceManager.h"

@interface ZbQyLocateViewController () <BMKMapViewDelegate, BMKGeoCodeSearchDelegate>

// 地图
@property (nonatomic, strong) IBOutlet BMKMapView *mapView;
// 中心点图标
@property (strong, nonatomic) IBOutlet UIImageView *ivLocation;

// 百度反地址搜索
@property (strong, nonatomic) BMKGeoCodeSearch *geoSearch;
// 当前屏幕中心点地址
@property (assign, nonatomic) CLLocationCoordinate2D centerCoordinate;

@end

@implementation ZbQyLocateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView.zoomLevel = 16;
    // 反地址检索
    _geoSearch =[[BMKGeoCodeSearch alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.mapView viewWillAppear];
    self.mapView.delegate = self;
    _geoSearch.delegate = self;
    
    // 位置变化
    [[LocationServiceManager shareManager] updateLocationBlock:^(void) {
        [self drawMapView];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.mapView viewWillDisappear];
    self.mapView.delegate = nil;
    _geoSearch.delegate = nil;
    
    // 位置变化
    [[LocationServiceManager shareManager] updateLocationBlock:nil];
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    BMKUserLocation *curPos = [[LocationServiceManager shareManager] getCurLocation];
    if (curPos == nil) {
        self.hud.labelText = @"获取当前地址定位失败";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    } else {
        [self drawMapView];
    }
}

#pragma mark - 点击事件
// 标注按钮点击
- (IBAction)btnMarkOnClick:(id)sender {
    BMKUserLocation *curPos = [[LocationServiceManager shareManager] getCurLocation];
    if (curPos == nil) {
        self.hud.labelText = @"获取当前地址定位失败";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (self.markLocationBlock) {
        self.markLocationBlock(self.mapView.centerCoordinate);
    }
    return;
}

#pragma mark - MapView
// 设置当前位置
- (void)drawMapView {
    BMKUserLocation *curPos = [[LocationServiceManager shareManager] getCurLocation];
    if (curPos == nil) {
        return;
    }
    [_mapView setCenterCoordinate:curPos.location.coordinate];
}

- (void)mapView:(BMKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    //        NSLog(@"regionWillChangeAnimated");
//    self.curLocationView.hidden = YES;
}

- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    //        NSLog(@"regionDidChangeAnimated lat=%f long=%f", self.mapView.centerCoordinate.latitude, self.mapView.centerCoordinate.longitude);
    
    self.centerCoordinate = self.mapView.centerCoordinate;
//    [self onGetNewLocation];
}

#pragma mark - 拖动结束，位置更新
- (void)onGetNewLocation {
//    self.ivLocation.hidden = NO;
    
    //发起反向地理编码检索
    BMKReverseGeoCodeOption *reverseGeoCodeSearchOption = [[
                                                            BMKReverseGeoCodeOption alloc]init];
    reverseGeoCodeSearchOption.reverseGeoPoint = self.centerCoordinate;
    BOOL flag = [_geoSearch reverseGeoCode:reverseGeoCodeSearchOption];
    if(flag)
    {
        DLog(@"周边检索发送成功");
    }
    else
    {
        DLog(@"周边检索发送失败");
    }
    return;
}

#pragma mark - 反向地理编码检索结果
//接收反向地理编码结果
-(void) onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:
(BMKReverseGeoCodeResult *)result
                        errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        if (result.poiList.count > 0) {
//            self.curLocationView.hidden = NO;
//            BMKPoiInfo *firstPoi = result.poiList.firstObject;
//            self.labLocationName.text = firstPoi.name;
//            self.curPoiInfo = firstPoi;
        }
    }
    else {
        DLog(@"抱歉，未找到结果");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
