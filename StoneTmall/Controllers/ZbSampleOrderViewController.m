//
//  ZbSampleOrderViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/12.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSampleOrderViewController.h"
#import "ZbSeparateTableViewCell.h"
#import "ZbTypeTableViewCell.h"
#import "ZbPayWayTableViewCell.h"
#import "ZbAddrChooseTableViewCell.h"
#import "ZbShipAdrViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"

#define HTTP_REQUEST_TAG_MakeOrdr 1         // 下单
#define HTTP_REQUEST_TAG_PayOrder 2         // 支付

@interface ZbSampleOrderViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

// tableView
@property (nonatomic, strong) IBOutlet UITableView *tv;

// 汇总价格
@property (nonatomic, strong) IBOutlet UILabel *labMoneySum;
// 确认按钮
@property (nonatomic, strong) IBOutlet UIButton *btnConfirm;

// 已选地址
@property (nonatomic, strong) NSDictionary *addrDict;
// 已选支付方式
@property (nonatomic, assign) Zb_Pay_Way_Type curPayWay;

@end

@implementation ZbSampleOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.curPayWay = Zb_Pay_Way_Zfb;
    
    // 圆角
    self.btnConfirm.layer.cornerRadius = 3;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbTypeTableViewCell" bundle:nil] forCellReuseIdentifier:TypeHeaderIdentifier];
    [self.tv registerNib:[UINib nibWithNibName:@"ZbSeparateTableViewCell" bundle:nil] forCellReuseIdentifier:SeparateCellIdentifier];
    self.tv.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tv.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // 注册收到微信登录请求回应的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotificationWxPayResult:) name:NOTIFICATION_WX_PAY_RESULT object:nil];
    
    [self drawInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 界面绘制
// 汇总信息
- (void)drawInfo {
    CGFloat sumMoney = 0;
    NSInteger sumCount = 0;
    // 遍历样品
    for (NSDictionary *curProductDict in self.cartSamplyArray) {
        // 价格
        NSNumber *price = curProductDict[@"spec_price"];
        // 数量
        NSNumber *count = curProductDict[@"count"];
        if (count == nil) {
            continue;
        }
        // 计算
        sumMoney += price.floatValue * count.integerValue;
        sumCount += count.integerValue;
    }
    self.labMoneySum.text = [NSString stringWithFormat:@"%@", [NSNumber numberWithFloat:(sumMoney / 100)]];
}

#pragma mark - 点击事件
// 确认按钮点击
- (IBAction)btnConfirmOnClick:(id)sender {
    if (self.addrDict == nil) {
        self.hud.labelText = @"请选择收货地址";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    [self requestAddOrderData];
    return;
}

#pragma mark - 接口调用
// 添加订单接口
- (void)requestAddOrderData {
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    // 支付类型
    NSString *payType = [NSString stringWithFormat:@"%u", self.curPayWay];
    // 产品
    NSMutableArray *productArray = [NSMutableArray array];
    // 遍历样品
    for (NSDictionary *curSampleDict in self.cartSamplyArray) {
        NSNumber *count = curSampleDict[@"count"];
        [productArray addObject:[NSString stringWithFormat:@"%@|%@", curSampleDict[@"spec_id"], count]];
    }
    // 地址
    NSString *addrId = self.addrDict[@"id"];
    NSString *products = [productArray componentsJoinedByString:@"|"];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService addSampleProductOrder:addrId payType:payType buyerRemark:@"" products:products] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MakeOrdr;
    [conn start];
    return;
}

// 支付接口
- (void)requestPayOrder:(NSString *)orderNumber {
    // 支付类型
    NSString *payType = [NSString stringWithFormat:@"%u", self.curPayWay];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService payOrder:orderNumber payType:payType] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_PayOrder;
    [conn start];
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
        {
            // 地址
            return 1;
        }
        case 1:
        {
            // 支付方式
            return 2;
        }
        case 2:
        {
            // 样品
            return self.cartSamplyArray.count;
        }
        case 3:
            // 物流
            return 2;
        default:
            return 0;
    }
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            // 地址
            return 45;
        case 1:
            // 支付方式
            return 45;
        case 2:
            // 样品
            return 84;
        case 3:
            // 物流
            return 35;
            
        default:
            return 0;
    }
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            // 地址
            if (self.addrDict == nil) {
                // 未选地址
                ZbAddrChooseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddAddrCell" forIndexPath:indexPath];
                cell.addrChooseClickBlock = ^(void) {
                    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
                    ZbShipAdrViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbShipAdrViewController"];
                    [controller setControllerType:Zb_Adr_PerSon];
                    controller.isSelectMode = YES;
                    controller.selectBlock = ^(NSDictionary *addrDict) {
                        // 选择地址
                        self.addrDict = addrDict;
                        [self.tv reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
                    };
                    [self.navigationController pushViewController:controller animated:YES];
                };
                return cell;
            } else {
                // 已选地址
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddrCell" forIndexPath:indexPath];
                // 控件
                UILabel *labContact = (UILabel *)[cell viewWithTag:1];
                UILabel *labAddr = (UILabel *)[cell viewWithTag:2];
                // 绘制
                NSString *contact = [NSString stringWithFormat:@"%@，%@", self.addrDict[@"name"], self.addrDict[@"phone"]];
                NSString *addr = [NSString stringWithFormat:@"%@%@%@", self.addrDict[@"prov"], self.addrDict[@"city"], self.addrDict[@"area"]];
                labContact.text = contact;
                labAddr.text = addr;
                return cell;
            }
        }
        case 1:
            // 支付方式
        {
            ZbPayWayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PayWayCell" forIndexPath:indexPath];
            if (indexPath.row == 0) {
                // 支付宝
                cell.ivPayWay.image = [UIImage imageNamed:@"public_icn_zhifubao"];
                cell.labPayWay.text = @"支付宝";
                cell.curPayWay = Zb_Pay_Way_Zfb;
                cell.btnPayWay.selected = self.curPayWay == Zb_Pay_Way_Zfb;
                cell.payWayClickBlock = ^(Zb_Pay_Way_Type type) {
                    self.curPayWay = type;
                    [tableView reloadRowsAtIndexPaths:@[indexPath, [NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
//                    [tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
                };
            } else {
                // 微信
                cell.ivPayWay.image = [UIImage imageNamed:@"public_icn_wechat_selection"];
                cell.labPayWay.text = @"微信";
                cell.curPayWay = Zb_Pay_Way_Wechat;
                cell.btnPayWay.selected = self.curPayWay == Zb_Pay_Way_Wechat;
                cell.payWayClickBlock = ^(Zb_Pay_Way_Type type) {
                    self.curPayWay = type;
                    [tableView reloadRowsAtIndexPaths:@[indexPath, [NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
//                    [tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
                };
            }
            return cell;
        }
        case 2:
        {
            // 样品
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SampleCell" forIndexPath:indexPath];
            
            // 控件
            UIImageView *ivImage = (UIImageView *)[cell viewWithTag:1];
            UILabel *labStoneName = (UILabel *)[cell viewWithTag:2];
            UILabel *labSpecName = (UILabel *)[cell viewWithTag:3];
            UILabel *labPrice = (UILabel *)[cell viewWithTag:4];
            UILabel *labCount = (UILabel *)[cell viewWithTag:5];
            
            // 样品信息
            NSDictionary *sampleDict = [self.cartSamplyArray objectAtIndex:indexPath.row];
            // 图片
            NSString *imageUrl = sampleDict[@"stone_image"];
            [ivImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imageUrl]]];
            // 石材名
            labStoneName.text = sampleDict[@"stone_name"];
            // 规格名
            labSpecName.text = sampleDict[@"spec_name"];
            // 价格
            float price = [sampleDict[@"spec_price"] floatValue] / 100;
            labPrice.text = [NSString stringWithFormat:@"%.2f元", price];
            // 数量
            NSNumber *cnt =  sampleDict[@"count"];
            labCount.text = [NSString stringWithFormat:@"元 x %@个", cnt];
            return cell;
        }
        case 3:
        {
            // 物流
            if (indexPath.row == 0) {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LogisticsCell" forIndexPath:indexPath];
                return cell;
            } else {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeliveryCell" forIndexPath:indexPath];
                return cell;
            }
        }
        default:
            return nil;
    }
}

// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            // 地址
            return 0;
        case 1:
            // 支付方式
            return [ZbTypeTableViewCell getRowHeight];
        case 2:
            // 样品
            return [ZbTypeTableViewCell getRowHeight];
        case 3:
            // 物流
            return 0;
            
        default:
            return 0;
    }
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZbTypeTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TypeHeaderIdentifier];
    switch (section) {
        case 0:
        {
            return [[UIView alloc] initWithFrame:CGRectZero];
        }
        case 1:
        {
            headerCell.labTypeName.text = @"支付方式";
            break;
        }
        case 2:
        {
            headerCell.labTypeName.text = @"确认信息";
            break;
        }
        case 3:
            return [[UIView alloc] initWithFrame:CGRectZero];
            
        default:
            break;
    }
    return headerCell;
}

// section尾部高度
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [ZbSeparateTableViewCell getRowHeight];
        case 1:
            return [ZbSeparateTableViewCell getRowHeight];
        case 2:
            return [ZbSeparateTableViewCell getRowHeight];
        case 3:
            return 0;
            
        default:
            return 0;
    }
}

// section尾部view
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UITableViewCell *footerCell;
    switch (section) {
        case 0:
        {
            footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
            break;
        }
        case 1:
        {
            footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
            break;
        }
        case 2:
        {
            footerCell = [tableView dequeueReusableCellWithIdentifier:SeparateCellIdentifier];
            break;
        }
        case 3:
        {
            return [[UIView alloc] init];
        }
        default:
            break;
    }
    return footerCell;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            // 地址
            UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
            ZbShipAdrViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbShipAdrViewController"];
            [controller setControllerType:Zb_Adr_PerSon];
            controller.isSelectMode = YES;
            controller.selectBlock = ^(NSDictionary *addrDict) {
                // 选择地址
                self.addrDict = addrDict;
                [self.tv reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            [self.navigationController pushViewController:controller animated:YES];
            return;
        }
        default:
            return;
    }
    return;
}

#pragma mark - 微信
// 微信授权结果通知
- (void)onNotificationWxPayResult:(NSNotification *)notify {
    [self.hud hide:NO];
    PayResp *resp = notify.object;
    switch(resp.errCode){
        case WXSuccess:
        {
            //服务器端查询支付通知或查询API返回的结果再提示成功
            NSString *info = [NSString stringWithFormat:@"恭喜您支付成功"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:info delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            break;
        }
        default:
        {
            DLog(@"支付失败，retcode=%d",resp.errCode);
            // 支付失败
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            break;
        }
    }
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"提交失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_TAG_MakeOrdr) {
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        NSDictionary *dataDict = resultDic[@"data"];
        NSString *order_number = dataDict[@"order_number"];
        [self requestPayOrder:order_number];
    } else if (connection.tag == HTTP_REQUEST_TAG_PayOrder) {
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self.hud hide:NO];
        NSDictionary *pay_charge = resultDic[@"data"][@"pay_charge"];
        if (self.curPayWay == Zb_Pay_Way_Zfb) {
            NSString *orderString = pay_charge[@"order_string"];
            // 支付
            [[AlipaySDK defaultService] payOrder:orderString fromScheme:WX_URL_SCHEME callback:^(NSDictionary *resultDic) {
                DLog(@"##order detail alipay reslut = %@",resultDic);
                NSString *resultStatus = resultDic[@"resultStatus"];
                if (resultStatus.integerValue != 9000) {
                    // 支付失败
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:@"支付失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
                
                NSString *info = [NSString stringWithFormat:@"恭喜您支付成功"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"支付结果" message:info delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
            }];
        } else {
            PayReq *request = [[PayReq alloc] init];
            request.openID = pay_charge[@"appid"];
            request.partnerId = pay_charge[@"partnerid"];
            request.prepayId = pay_charge[@"prepayid"];
            request.package = pay_charge[@"package"];
            request.nonceStr = pay_charge[@"noncestr"];
            NSString *time = pay_charge[@"timestamp"];
            request.timeStamp = time.integerValue;
            request.sign = pay_charge[@"sign"];
            [WXApi sendReq:request];
        }
    }
    return;
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (self.payOkBlock) {
        self.payOkBlock();
    }
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
