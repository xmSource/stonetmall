//
//  ZbDropTreeDownViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/8.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbDropTreeDownViewController.h"
#import "ZbTypeParentTableViewCell.h"
#import "ZbTypeSubTableViewCell.h"

#define MAX_SHOW_HEIGHT_RATE (3/4.0)

@interface ZbDropTreeDownViewController () <UITableViewDataSource, UITableViewDelegate>

// 一级分类tv
@property (nonatomic, strong) IBOutlet UITableView *tvMain;
// 二级分类tv
@property (nonatomic, strong) IBOutlet UITableView *tvSub;
// 一级tv的top约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvTop;
// 二级tv的height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvHeight;
// 二级tv的top约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvSubTop;
// 一级tv的height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcTvSubHeight;
// 遮罩层
@property (nonatomic, strong) IBOutlet UIControl *controlCover;

// 数据列表
@property (nonatomic, weak) NSArray *array;

// 分类数组
@property (nonatomic, weak) NSArray *typeArray;
// 子分类数组
@property (nonatomic, weak) NSDictionary *subTypeDict;
// 上次选中父类
@property (nonatomic, assign) NSInteger lastParentIndex;
// 上次选中子类
@property (nonatomic, assign) NSInteger lastSubIndex;

// 当前选中的一级选项
@property (nonatomic, assign) NSInteger curTypeMainIndex;

// 选中block
@property (nonatomic, copy) selectClickBlock curBlock;

@end

@implementation ZbDropTreeDownViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 注册cell
    [self.tvMain registerNib:[UINib nibWithNibName:@"ZbTypeParentTableViewCell" bundle:nil] forCellReuseIdentifier:TypeParentCellIdentifier];
    [self.tvMain registerNib:[UINib nibWithNibName:@"ZbTypeSubTableViewCell" bundle:nil] forCellReuseIdentifier:TypeSubCellIdentifier];
    self.tvMain.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tvSub registerNib:[UINib nibWithNibName:@"ZbTypeSubTableViewCell" bundle:nil] forCellReuseIdentifier:TypeSubCellIdentifier];
    self.tvSub.separatorStyle = UITableViewCellSeparatorStyleNone;
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//  类方法
+ (ZbDropTreeDownViewController *)showInViewController:(UIViewController *)controller startY:(CGFloat)startY typeArray:(NSArray *)typeArray subTypeDict:(NSDictionary *)subTypeDict lastMainIndex:(NSInteger)lastMainIndex lastSubIndex:(NSInteger)lastSubIndex selectBlock:(selectClickBlock)block {
    UIStoryboard *pickSb = [UIStoryboard storyboardWithName:@"DropDown" bundle:nil];
    ZbDropTreeDownViewController *dropDown = [pickSb instantiateViewControllerWithIdentifier:@"ZbDropTreeDownViewController"];
    
    // 设置数据
    dropDown.typeArray = typeArray;
    dropDown.subTypeDict = subTypeDict;
    dropDown.curBlock = block;
    dropDown.curTypeMainIndex = lastMainIndex + 1;
    dropDown.lastParentIndex = lastMainIndex;
    dropDown.lastSubIndex = lastSubIndex;
    
    [controller addChildViewController:dropDown];
    [controller.view addSubview:dropDown.view];
    
    [dropDown viewWillAppear:YES];
    
    // 下拉框界面大小
    CGRect viewFrame = CGRectMake(0, startY, CGRectGetWidth(controller.view.bounds), CGRectGetHeight(controller.view.bounds) - startY);
    dropDown.view.frame = viewFrame;
    CGFloat cellHeight = [ZbTypeParentTableViewCell getRowHeight];
    // cell总高度
    CGFloat height = cellHeight * (typeArray.count + 1);
    // 可显示高度
    CGFloat showHeight = MIN(height, CGRectGetHeight(viewFrame) * MAX_SHOW_HEIGHT_RATE);
    dropDown.alcTvTop.constant = -showHeight;
    dropDown.alcTvHeight.constant = showHeight;
    dropDown.alcTvSubTop.constant = -showHeight;
    dropDown.alcTvSubHeight.constant = showHeight;
    dropDown.alcTvTop.constant = 0;
    dropDown.alcTvSubTop.constant = 0;
    dropDown.controlCover.alpha = 1;
//    [dropDown.view layoutIfNeeded];
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
//    [UIView animateWithDuration:0.3 animations:^(void) {
//        dropDown.alcTvTop.constant = 0;
//        dropDown.alcTvSubTop.constant = 0;
//        dropDown.controlCover.alpha = 1;
//        [dropDown.view layoutIfNeeded];
//    } completion:^(BOOL finished) {
//        
//    }];
    
//    // 上次选中项y坐标
//    CGFloat tagY = dropDown.curTypeMainIndex * [ZbTypeParentTableViewCell getRowHeight];
//    [dropDown.tvMain scrollRectToVisible:CGRectMake(0, tagY, 1, 1) animated:YES];
    
    return dropDown;
}

// 关闭界面
- (void)dismiss:(BOOL)animated {
    animated = NO;
    if (!animated) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        return;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.tvMain.frame = CGRectMake(0, -CGRectGetHeight(self.tvMain.frame), CGRectGetWidth(self.tvMain.frame), CGRectGetHeight(self.tvMain.frame));
        self.tvSub.frame = CGRectMake(CGRectGetMinX(self.tvSub.frame), -CGRectGetHeight(self.tvSub.frame), CGRectGetWidth(self.tvSub.frame), CGRectGetHeight(self.tvSub.frame));
        self.controlCover.alpha = 0;
    } completion:^(BOOL finished){
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    return;
}

#pragma mark - 点击事件
- (IBAction)btnCoverOnClick:(id)sender {
    BOOL isDismiss = YES;
    self.curBlock(nil, nil, 0, 0, isDismiss);
    [self dismiss:YES];
    return;
}

#pragma mark - UITableViewDataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:self.tvMain]) {
        // 默认有‘全部’选项
        return self.typeArray.count + 1;
    } else {
        if (self.curTypeMainIndex == 0) {
            return 0;
        }
        NSDictionary *typeInfo = [self.typeArray objectAtIndex:self.curTypeMainIndex - 1];
        NSString *classId = typeInfo[@"class_id"];
        NSArray *subArray = self.subTypeDict[classId];
        return subArray.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.tvMain]) {
        return [ZbTypeParentTableViewCell getRowHeight];
    } else {
        return [ZbTypeSubTableViewCell getRowHeight];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.tvMain]) {
        if (indexPath.row == 0) {
            ZbTypeSubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TypeSubCellIdentifier forIndexPath:indexPath];
            cell.labName.text = @"全部";
            cell.alcLabNameLeading.constant = 40;
            cell.ivIcon.hidden = NO;
            BOOL isSelect = self.curTypeMainIndex == 0;
            if (isSelect) {
                cell.contentView.backgroundColor = UIColorFromHexString(@"FFFFFF");
            }else{
                cell.contentView.backgroundColor = UIColorFromHexString(@"F8F8F8");
            }
            return cell;
        } else {
            ZbTypeParentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TypeParentCellIdentifier forIndexPath:indexPath];
            // 一级类名
            NSDictionary *typeDict = [self.typeArray objectAtIndex:indexPath.row - 1];
            NSString *class_name = typeDict[@"class_name"];
            cell.labName.text = class_name;
            // 一级图标
            cell.ivIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@min", class_name]];
            // 二级类名
            NSString *classId = typeDict[@"class_id"];
            NSArray *subArray = self.subTypeDict[classId];
            NSMutableString *subClassname = [NSMutableString string];
            for (NSDictionary *subInfo in subArray) {
                [subClassname appendString:subInfo[@"class_name"]];
            }
            cell.labSubName.text = subClassname;
            
            BOOL isSelect = indexPath.row == self.curTypeMainIndex;
            [cell setIsCurSelect:isSelect];
            return cell;
        }
    } else {
        ZbTypeSubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TypeSubCellIdentifier forIndexPath:indexPath];
        cell.alcLabNameLeading.constant = 20;
        cell.ivIcon.hidden = YES;
        NSDictionary *typeInfo = [self.typeArray objectAtIndex:self.curTypeMainIndex - 1];
        NSString *classId = typeInfo[@"class_id"];
        NSArray *subArray = self.subTypeDict[classId];
        NSDictionary *typeDict = [subArray objectAtIndex:indexPath.row];
        cell.labName.text = typeDict[@"class_name"];
        
        BOOL isLastSelect = ((self.curTypeMainIndex - 1) == self.lastParentIndex && indexPath.row == self.lastSubIndex);
        [cell setIsCurSelect:isLastSelect];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([tableView isEqual:self.tvMain]) {
        if (indexPath.row == 0) {
            // 选中全部
            self.curBlock(nil, nil, -1, -1, NO);
            [self dismiss:YES];
            return;
        }
        self.curTypeMainIndex = indexPath.row;
        [self.tvMain reloadData];
        [self.tvSub reloadData];
    } else {
        if (self.curBlock) {
            if (self.curTypeMainIndex == 0) {
                self.curBlock(nil, nil, -1, -1, NO);
            } else {
                // 当前父类别信息
                NSDictionary *typeParentDict = [self.typeArray objectAtIndex:self.curTypeMainIndex - 1];
                NSString *typeParent = typeParentDict[@"class_name"];
                
                // 当前子类别信息
                NSDictionary *typeInfo = [self.typeArray objectAtIndex:self.curTypeMainIndex - 1];
                NSString *classId = typeInfo[@"class_id"];
                NSArray *subArray = self.subTypeDict[classId];
                NSDictionary *typeSubDict = [subArray objectAtIndex:indexPath.row];
                NSString *typeSub = typeSubDict[@"class_name"];
                self.curBlock(typeParent, typeSub, self.curTypeMainIndex - 1, indexPath.row, NO);
            }
            
            [self dismiss:YES];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
