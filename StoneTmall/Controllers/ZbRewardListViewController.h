//
//  ZbRewardListViewController.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/29.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

// 悬赏列表界面类型
typedef enum {
    Zb_RewardList_Type_Ing,                // 进行中
    Zb_RewardList_Type_Done,               // 已完成
} Zb_RewardList_Type;

@interface ZbRewardListViewController : BaseViewController

// 当前类型
@property (nonatomic, assign) Zb_RewardList_Type curRewardListType;

- (void)onPageShowChange:(BOOL)isShow;

@end
