//
//  ZbOrderDetailViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/11/24.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbOrderDetailViewController.h"
#import "UITableView+FDTemplateLayoutCell.h"

@interface ZbOrderDetailViewController () <UITableViewDataSource, UITableViewDelegate>

// tableview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// 订单状态描述label
@property (nonatomic, strong) IBOutlet UILabel *labOrderStatus;
// 订单状态图片
@property (nonatomic, strong) IBOutlet UIImageView *ivOrderStatus;

@end

@implementation ZbOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self drawOrderStatus];
}

#pragma mark - 界面绘制
// 绘制订单状态
- (void)drawOrderStatus {
    int statusOrder = [self.curOrderDict[@"order_status"] intValue];
    switch (statusOrder) {
        case Zb_User_Order_Status_Status_Wzf:
            self.labOrderStatus.text = @"订单未支付";
            self.ivOrderStatus.image = [UIImage imageNamed:@"order_detail_wait"];
            break;
            
        case Zb_User_Order_Status_Status_Yzf:
            self.labOrderStatus.text = @"订单待发货";
            self.ivOrderStatus.image = [UIImage imageNamed:@"order_detail_wait"];
            break;
            
        case Zb_User_Order_Status_Status_Yfh:
            self.labOrderStatus.text = @"订单已发货";
            self.ivOrderStatus.image = [UIImage imageNamed:@"order_detail_sent"];
            break;
            
        case Zb_User_Order_Status_Status_Ywc:
            self.labOrderStatus.text = @"订单已完成";
            self.ivOrderStatus.image = [UIImage imageNamed:@"success_check"];
            break;
            
        case Zb_User_Order_Status_Status_Ygq:
            self.labOrderStatus.text = @"订单已过期";
            self.ivOrderStatus.image = [UIImage imageNamed:@"success_check"];
            break;
            
        case Zb_User_Order_Status_Status_Ytk:
            self.labOrderStatus.text = @"订单已退款";
            self.ivOrderStatus.image = [UIImage imageNamed:@"success_check"];
            break;
            
            
        default:
            break;
    }
    return;
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *goodsArray = self.curOrderDict[@"order_items"];
    return 6 + goodsArray.count;
}

// 设置cell的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        case 2:
        case 3:
        case 4:
            // 下单时间、运送方式、支付方式、总金额
            return 45;
        case 1:
        {
            // 收货地址
            static UITableViewCell *sizingCell = nil;
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                sizingCell = [tableView dequeueReusableCellWithIdentifier:@"expandCell"];
                sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tv.frame), CGRectGetHeight(sizingCell.bounds));
                UILabel *labelDetail = (UILabel *)[sizingCell viewWithTag:2];
                labelDetail.preferredMaxLayoutWidth = DeviceWidth - 15 - 60 - 30 - 15;
            });
            UILabel *labelDetail = (UILabel *)[sizingCell viewWithTag:2];
            NSString *buyerName = self.curOrderDict[@"order_buyer_name"];
            NSString *buyerPhone = self.curOrderDict[@"order_buyer_phone"];
            NSString *buyerProv = self.curOrderDict[@"order_buyer_prov"];
            NSString *buyerCity = self.curOrderDict[@"order_buyer_city"];
            NSString *buyerArea = self.curOrderDict[@"order_buyer_area"];
            NSString *des = [NSString stringWithFormat:@"%@，%@，%@%@%@", buyerName, buyerPhone, buyerProv, buyerCity, buyerArea];
            labelDetail.text = des;
            CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            return size.height;
        }
        case 5:
            // 订单x件
            return 35;
            
        default:
            // 商品
            return 85;
    }
    // 商品
    return 85;
}

// 设置cell的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    switch (indexPath.row) {
        case 0:
        case 2:
        case 3:
        case 4:
        {
            // 下单时间、运送方式、支付方式、总金额
            cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell" forIndexPath:indexPath];
            UILabel *labName = (UILabel *)[cell viewWithTag:1];
            UILabel *labDetail = (UILabel *)[cell viewWithTag:2];
            
            switch (indexPath.row) {
                case 0:
                {
                    labName.text = @"下单时间";
                    labDetail.text = self.curOrderDict[@"order_inserted"];
                    break;
                }
                case 2:
                {
                    labName.text = @"运送方式";
                    labDetail.text = @"快递包邮";
                    break;
                }
                case 3:
                {
                    labName.text = @"支付方式";
                    NSString *payType = [NSString stringWithFormat:@"%@", self.curOrderDict[@"order_pay"]];
                    labDetail.text = payType.intValue == Zb_Pay_Way_Wechat ? @"微信" : @"支付宝";
                    break;
                }
                case 4:
                {
                    labName.text = @"总金额";
                    float sumPrice = 0.00;
                    for (NSDictionary *dict in self.curOrderDict[@"order_items"]) {
                        float price = [dict[@"item_price"] floatValue] / 100;
                        float amount = [dict[@"item_amount"] floatValue];
                        float tPrice = price * amount;
                        sumPrice += tPrice;
                    }
                    
                    NSString *price = [NSString stringWithFormat:@"%.2f", sumPrice];
                    NSString *title = @"元";
                    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", price, title]];
                    // 价格字属性
                    [attriString addAttribute:NSForegroundColorAttributeName
                                        value:UIColorFromHexString(@"FF7F00")
                                        range:NSMakeRange(0, price.length)];
                    [attriString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:NSMakeRange(0, price.length)];
                    // 标题字属性
                    [attriString addAttribute:NSForegroundColorAttributeName
                                        value:UIColorFromHexString(@"888888")
                                        range:NSMakeRange(price.length, title.length)];
                    [attriString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(price.length, title.length)];
                    labDetail.attributedText = attriString;
                    break;
                }
            }
            break;
        }
        case 1:
        {
            // 收货地址
            cell = [tableView dequeueReusableCellWithIdentifier:@"expandCell" forIndexPath:indexPath];
            UILabel *labelDetail = (UILabel *)[cell viewWithTag:2];
            NSString *buyerName = self.curOrderDict[@"order_buyer_name"];
            NSString *buyerPhone = self.curOrderDict[@"order_buyer_phone"];
            NSString *buyerProv = self.curOrderDict[@"order_buyer_prov"];
            NSString *buyerCity = self.curOrderDict[@"order_buyer_city"];
            NSString *buyerArea = self.curOrderDict[@"order_buyer_area"];
            NSString *des = [NSString stringWithFormat:@"%@，%@，%@%@%@", buyerName, buyerPhone, buyerProv, buyerCity, buyerArea];
            labelDetail.text = des;
            break;
        }
        case 5:
        {
            // 订单x件
            cell = [tableView dequeueReusableCellWithIdentifier:@"orderCell" forIndexPath:indexPath];
            UILabel *labelDetail = (UILabel *)[cell viewWithTag:1];
            NSArray *goodsArray = self.curOrderDict[@"order_items"];
            labelDetail.text = [NSString stringWithFormat:@"订单商品%lu件", (unsigned long)goodsArray.count];
            break;
        }
        default:
        {
            // 商品
            cell = [tableView dequeueReusableCellWithIdentifier:@"itemsCell" forIndexPath:indexPath];
            UIImageView *img = (UIImageView *)[cell viewWithTag:1];
            UILabel *labGoodsName = (UILabel *)[cell viewWithTag:2];
            UILabel *labSpecs = (UILabel *)[cell viewWithTag:3];
            UILabel *labprice = (UILabel *)[cell viewWithTag:4];
            UILabel *labnum = (UILabel *)[cell viewWithTag:5];
            
            NSArray *goodsArray = self.curOrderDict[@"order_items"];
            NSDictionary *dict = [goodsArray objectAtIndex:indexPath.row - 6];
            [img sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:dict[@"item_stone_image"]]]];
            labGoodsName.text = dict[@"item_product_tag"];
            labSpecs.text = dict[@"item_spec_name"];
            float price = ([dict[@"item_price"] floatValue] / 100);
            labprice.text = [NSString stringWithFormat:@"%.2f", price];
            labnum.text = [NSString stringWithFormat:@"元 * %@", dict[@"item_amount"]];
            break;
        }
    }
    
    return cell;
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
