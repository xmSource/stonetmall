//
//  ZbLoginViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbLoginViewController.h"
#import "ZbTelValidateViewController.h"
#import "WXApi.h"
#import "ZbLoginMainViewController.h"
#import "EaseMob.h"
#import "AppDelegate.h"

#define HTTP_REQUEST_TAG_LOGIN 0          // 登录
#define HTTP_REQUEST_TAG_WXLOGIN 1        // 微信登录

@interface ZbLoginViewController () <UITextFieldDelegate, UIAlertViewDelegate, UIActionSheetDelegate>

// 账号输入框
@property (nonatomic, strong) IBOutlet UITextField *tfAcc;
// 密码输入框
@property (nonatomic, strong) IBOutlet UITextField *tfPsw;
// 登录按钮
@property (nonatomic, strong) IBOutlet UIButton *btnLogin;
// 微信登录按钮
@property (nonatomic, strong) IBOutlet UIButton *btnWxLogin;
// '或者'label
@property (nonatomic, strong) IBOutlet UILabel *labOr;

@end

@implementation ZbLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 注册收到微信登录请求回应的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotificationGetWxCodeResult:) name:NOTIFICATION_GET_WX_CODE_SUCCESS object:nil];
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 登录按钮初始不激活
    [self setBtnLoginEnable:NO];
    
    if (IS_IPAD && ![WXApi isWXAppInstalled]) {
        // ipad收不到短信，无法用微信sdk的免微信授权功能，隐藏微信登录按钮
        self.btnWxLogin.hidden = YES;
        self.labOr.hidden = YES;
        return;
    }
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
//    [self.tfAcc becomeFirstResponder];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 设置登录按钮激活状态
- (void)setBtnLoginEnable:(BOOL)isEnable {
    self.btnLogin.enabled = isEnable;
    self.btnLogin.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

// 界面不显示
- (void)onNoShow {
    [self.tfAcc resignFirstResponder];
    [self.tfPsw resignFirstResponder];
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

// 登录按钮点击
- (IBAction)btnLoginOnClick:(id)sender {
    [self.view endEditing:YES];
    
    [self requestLogin];
    return;
}

// 微信登录点击
- (IBAction)btnWxLoginOnClick:(id)sender {
    [self.view endEditing:YES];
    
    self.hud.labelText = @"请求中..";
    [self.hud show:NO];
    
    [self sendWechatAuthRequest];
    return;
}

// 忘记密码点击
- (IBAction)btnForgetOnClick:(id)sender {
    [self.view endEditing:YES];
    
    UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"通过用户名找回", @"通过手机号找回", nil];
    [actSheet showInView:self.parentViewController.navigationController.view];
    return;
}

#pragma mark - 接口调用
// 登录接口
- (void)requestLogin {
    self.hud.labelText = @"登录中..";
    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService loginParams:self.tfAcc.text password:self.tfPsw.text] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_LOGIN;
    [conn start];
    return;
}

// 微信登录接口
- (void)requestWxLogin:(NSString *)wxCode {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService loginWxParams:wxCode] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_WXLOGIN;
    [conn start];
    return;
}

#pragma mark - 微信
// 发起微信授权
- (void)sendWechatAuthRequest {
    //构造SendAuthReq结构体
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"login" ;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendAuthReq:req viewController:self delegate:nil];
    return;
}

// 微信授权结果通知
- (void)onNotificationGetWxCodeResult:(NSNotification *)notify {
    SendAuthResp *resp = notify.object;
    if (resp.code == nil) {
        [self.hud hide:NO];
        return;
    }
    if (![resp.state isEqualToString:@"login"]) {
        return;
    }
    [self requestWxLogin:resp.code];
    return;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.tfAcc]) {
        [self.tfPsw becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (IBAction)textFieldChanged:(id)sender {
    if (self.tfAcc.text.length > 0 && self.tfPsw.text.length > 0) {
        [self setBtnLoginEnable:YES];
    } else {
        [self setBtnLoginEnable:NO];
    }
    return;
}

#pragma mark - UIAlertView delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    if (buttonIndex == 0) {
        // 通过用户名找回
        NSString *telUrl = [NSString stringWithFormat:@"telprompt:%@", APP_CONTACT_PHONE];
        NSURL *url = [[NSURL alloc] initWithString:telUrl];
        [[UIApplication sharedApplication] openURL:url];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"请拨打客服电话\n%@找回密码", APP_CONTACT_PHONE] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"立即拨打", nil];
//        [alert show];
        
    } else {
        // 通过手机号找回
        ZbTelValidateViewController *validateController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbTelValidateViewController"];
        validateController.curValidateType = Zb_TelValidate_Type_FindPsw;
        [self.parentViewController.navigationController pushViewController:validateController animated:YES];
    }
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    NSString *telUrl = [NSString stringWithFormat:@"telprompt:%@", APP_CONTACT_PHONE];
    NSURL *url = [[NSURL alloc] initWithString:telUrl];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark - 登录成功逻辑
- (void)loginSuccessLogic:(NSDictionary *)resultDic {
    
    // 设置用户信息
    NSMutableDictionary *user = resultDic[@"data"];
    NSString *account = user[@"user_name"];
    NSString *psw = user[@"easemob_passwd"];
    // 继续登录环信
    EMError *error = nil;
    [[EaseMob sharedInstance].chatManager loginWithUsername:account password:psw error:&error];
    if (error) {
        self.hud.labelText = @"登录失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    DLog(@"环信登录成功成功!");
//    //设置是否自动登录
//    [[EaseMob sharedInstance].chatManager setIsAutoLoginEnabled:YES];
//    
//    // 旧数据转换 (如果您的sdk是由2.1.2版本升级过来的，需要家这句话)
//    [[EaseMob sharedInstance].chatManager importDataToNewDatabase];
    //获取数据库中数据
    [[EaseMob sharedInstance].chatManager loadDataFromDatabase];
    
    //获取群组列表
    [[EaseMob sharedInstance].chatManager asyncFetchMyGroupsList];
    
    // 设置用户信息
    [ZbWebService setUser:user];
    // 保存cookie到本地
    [ZbWebService saveCookie];
    
    //跳转到主界面
    self.hud.labelText = @"登录成功";
    [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
        [self.parentViewController.navigationController dismissViewControllerAnimated:YES completion:^(void) {
            ZbLoginMainViewController *loginMain = (ZbLoginMainViewController *)self.parentViewController;
            dispatch_async(dispatch_get_main_queue(), ^{
                if (loginMain && loginMain.loginSucessBlock) {
                    loginMain.loginSucessBlock();
                }
            });
        }];
    }];
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"登录失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    if (connection.tag == HTTP_REQUEST_TAG_LOGIN) {
        // 登录接口
        if (result.integerValue != 0) {
            if (result.integerValue == -9) {
                self.hud.labelText = @"账号未注册";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            if (result.integerValue == -10) {
                self.hud.labelText = @"账号或密码错误";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self loginSuccessLogic:resultDic];
        
    } else if (connection.tag == HTTP_REQUEST_TAG_WXLOGIN) {
        // 登录接口
        if (result.integerValue != 0) {
            if (result.integerValue == -62) {
                self.hud.labelText = @"微信账号未注册";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            self.hud.labelText = @"登录失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        [self loginSuccessLogic:resultDic];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
