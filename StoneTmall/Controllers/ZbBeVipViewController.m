//
//  ZbBeVipViewController.m
//  StoneTmall
//
//  Created by Apple on 15/9/7.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbBeVipViewController.h"

@interface ZbBeVipViewController()

// VIP用户label
@property (nonatomic, strong) IBOutlet UILabel *labQy;

// 权限数组
@property (nonatomic, strong) NSMutableArray *rightsArray;

@property (nonatomic, strong) IBOutlet UITableView *tv;

@end

@implementation ZbBeVipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.rightsArray = Mutable_ArrayInit;
    
    // 企业用户label圆角
    self.labQy.layer.cornerRadius = 3;
    self.labQy.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

#pragma mark - 读取数据
- (void)loadData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getVipRightsInfo] delegate:self];
    [conn start];
}

#pragma mark - 点击事件
// 确定成为VIP用户
- (void)btnConfirmOnClick:(UIButton *)btn {
    NSString *phone = [btn.titleLabel.text substringFromIndex:4];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", phone]]];
}

// 关闭按钮点击
- (IBAction)btnClockOnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    return;
}

#pragma mark - Table view data source
/**
 *  设置tableView的区数
 *
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/**
 *  设置tableView每个区的行数
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.rightsArray.count + 1;
}

/**
 *  设置cell的内容
 *
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if (indexPath.row < self.rightsArray.count) {
        // 权限
        cell = [tableView dequeueReusableCellWithIdentifier:@"rightsCell" forIndexPath:indexPath];
        
        // 权限label
        UILabel *labRights = (UILabel *)[cell viewWithTag:1];
        NSString *rightsName = [NSString stringWithFormat:@"%@", [self.rightsArray objectAtIndex:indexPath.row]];
        labRights.adjustsFontSizeToFitWidth = YES;
        labRights.text = rightsName;
    } else {
        // 确定按钮
        cell = [tableView dequeueReusableCellWithIdentifier:@"footerCell" forIndexPath:indexPath];
        
        UIButton *btnConfirm = (UIButton *)[cell viewWithTag:1];;
        btnConfirm.layer.cornerRadius = 3;
        [btnConfirm addTarget:self action:@selector(btnConfirmOnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

/**
 *  设置cell的高度
 *
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.rightsArray.count) {
        // 权限
        return 40;
    } else {
        // 确定按钮
        return 80;
    }
}

/**
 *  点击事件调用
 *
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    return;
}


#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.hud.labelText = @"加载失败,请稍后再试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0f complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSString *result = dict[@"errno"];
    if (result.integerValue != 0) {
        return;
    }
    [self.rightsArray removeAllObjects];
    [self.rightsArray addObjectsFromArray:(NSArray *)dict[@"data"]];
    [self.tv reloadData];
}

@end
