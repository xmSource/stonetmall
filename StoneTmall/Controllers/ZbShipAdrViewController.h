//
//  ZbShipAdrViewController.h
//  StoneTmall
//
//  Created by chyo on 15/9/4.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

typedef enum {
    Zb_Adr_PerSon,
    Zb_Adr_QiYe,
}Zb_Adr_Type ;

@interface ZbShipAdrViewController : BaseViewController

- (void)setControllerType:(Zb_Adr_Type)zbAdrType;
// 选择模式
@property (nonatomic, assign) BOOL isSelectMode;
// 选择回调block
@property (nonatomic, copy) void (^selectBlock)(NSDictionary *addrDict);

@end
