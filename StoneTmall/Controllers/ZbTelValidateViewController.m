//
//  ZbTelValidateViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/21.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTelValidateViewController.h"
#import "ZbSetPswViewController.h"
#import "ZbBindQyViewController.h"

#define HTTP_REQUEST_TAG_CODE 0          // 获取验证码
#define HTTP_REQUEST_TAG_EDIT 1          // 绑定手机号
#define HTTP_REQUEST_TAG_FINDPSW 2       // 找回密码

@interface ZbTelValidateViewController () <UITextFieldDelegate, MZTimerLabelDelegate, UIAlertViewDelegate>

// 手机号输入框
@property (nonatomic, strong) IBOutlet UITextField *tfTel;
// 验证码输入框
@property (nonatomic, strong) IBOutlet UITextField *tfCode;
// 获取验证码按钮
@property (nonatomic, strong) IBOutlet UIButton *btnGet;
// 倒计时label
@property (nonatomic, strong) IBOutlet UILabel *labCountDown;
// 下一步按钮
@property (nonatomic, strong) IBOutlet UIButton *btnNext;
// 倒计时处理类
@property (nonatomic, strong) MZTimerLabel *timerLabel;
// 绑定描述label
@property (nonatomic, strong) IBOutlet UILabel *labDes;
// 绑定描述height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcLabDesHeight;
// 绑定描述top约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcLabDesTop;
// 绑定描述bottom约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcLabDesBottom;

// 临时id
@property (nonatomic, strong) NSString *tempId;

@end

@implementation ZbTelValidateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 按钮圆角
    self.btnNext.layer.cornerRadius = 3;
    self.btnGet.layer.cornerRadius = 3;
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 下一步按钮初始不激活
    [self setBtnNextEnable:NO];
    
    if (self.curValidateType == Zb_TelValidate_Type_FindPsw) {
        // 忘记密码验证
        self.title = @"通过手机号找回密码";
        [self.btnNext setTitle:@"下一步" forState:UIControlStateNormal];
        // 隐藏绑定描述
        self.alcLabDesHeight.constant = 0;
        self.alcLabDesTop.constant = 0;
        self.alcLabDesBottom.constant = 0;
        self.labDes.hidden = YES;
    } else if (self.curValidateType == Zb_TelValidate_Type_BindTel) {
        // 修改手机号
        self.title = @"绑定手机号";
        [self.btnNext setTitle:@"提交" forState:UIControlStateNormal];
        // 隐藏绑定描述
        self.alcLabDesHeight.constant = 0;
        self.alcLabDesTop.constant = 0;
        self.alcLabDesBottom.constant = 0;
        self.labDes.hidden = YES;
    } else {
        // 绑定手机号
        self.title = @"绑定手机号";
        [self.btnNext setTitle:@"绑定" forState:UIControlStateNormal];
    }
    
    // 倒计时label
    self.timerLabel = [[MZTimerLabel alloc] initWithLabel:self.labCountDown andTimerType:MZTimerLabelTypeTimer];
    self.timerLabel.delegate = self;
    self.timerLabel.timerType = MZTimerLabelTypeTimer;
    self.timerLabel.timeFormat = @"mm分ss秒";
    [self.timerLabel setCountDownTime:100];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 设置下一步按钮激活状态
- (void)setBtnNextEnable:(BOOL)isEnable {
    self.btnNext.enabled = isEnable;
    self.btnNext.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

// 下一步按钮点击
- (IBAction)btnNextOnClick:(id)sender {
    [self.view endEditing:YES];
    
    if (self.curValidateType == Zb_TelValidate_Type_BindTel) {
        // 绑定手机号
        [self requestEditPhone];
    } else if (self.curValidateType == Zb_TelValidate_Type_BindTel_BecommonQy) {
        // 注册企业前绑定手机号
        [self requestEditPhone];
    } else {
        // 找回密码
        [self requestFindPsw];
        return;
    }
    return;
}

// 获取验证码按钮点击
- (IBAction)btnGetCodeOnClick:(id)sender {
    NSString *phone = self.tfTel.text;
    if (![CommonCode isPhoneNumber:phone]) {
        self.hud.labelText = @"手机格式不正确";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    // 显示倒计时
    self.btnGet.hidden = YES;
    self.labCountDown.hidden = NO;
    [self.timerLabel start];
    
    [self loadValidateCodeData];
    return;
}

#pragma mark - 接口调用
// 获取验证码接口
- (void)loadValidateCodeData {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *phone = self.tfTel.text;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getVerifyCode:phone] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_CODE;
    [conn start];
    return;
}

// 修改手机号
- (void)requestEditPhone {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    NSString *phone = self.tfTel.text;
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService editMyPhone:phone tempId:self.tempId code:self.tfCode.text] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_EDIT;
    [conn start];
    return;
}

// 找回密码
- (void)requestFindPsw {
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService resetPassword:self.tfTel.text tempId:self.tempId code:self.tfCode.text] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_FINDPSW;
    [conn start];
    return;
}

#pragma mark - UITextField delegate
// 文本内容变化
- (IBAction)textFieldChanged:(id)sender {
    if ([CommonCode isPhoneNumber:self.tfTel.text] && self.tfCode.text.length > 0) {
        [self setBtnNextEnable:YES];
    } else {
        [self setBtnNextEnable:NO];
    }
    return;
}

#pragma mark - 倒计时delegate
- (void)timerLabel:(MZTimerLabel *)timerLabel finshedCountDownTimerWithTime:(NSTimeInterval)countTime {
    // 显示获取倒计时按钮
    self.btnGet.hidden = NO;
    self.labCountDown.hidden = YES;
    return;
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.navigationController popViewControllerAnimated:YES];
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    if (connection.tag == HTTP_REQUEST_TAG_CODE) {
        self.hud.labelText = @"获取验证码失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    } else {
        self.hud.labelText = @"修改失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    if (connection.tag == HTTP_REQUEST_TAG_CODE) {
        // 验证码
        if (result.integerValue != 0) {
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        self.tempId = [NSString stringWithFormat:@"%@", resultDic[@"data"]];
        self.hud.labelText = @"验证码发送成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:nil];
    } else if (connection.tag == HTTP_REQUEST_TAG_EDIT) {
        // 修改手机号
        if (result.integerValue != 0) {
            if (result.integerValue == -81) {
                self.hud.labelText = @"验证码错误";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        
        // 修改用户手机号信息
        NSMutableDictionary *user = [ZbWebService sharedUser];
        [user setValue:self.tfTel.text forKey:@"user_phone"];
        if (self.curValidateType == Zb_TelValidate_Type_BindTel) {
            // 单独修改手机号
            self.hud.labelText = @"修改手机号成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
        } else if (self.curValidateType == Zb_TelValidate_Type_BindTel_BecommonQy) {
            // 注册企业前绑定手机
            self.hud.labelText = @"绑定成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
                NSMutableArray *viewArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
                [viewArray removeLastObject];
                ZbBindQyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbBindQyViewController"];
                [viewArray addObject:controller];
                [self.navigationController setViewControllers:viewArray animated:YES];
            }];
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_FINDPSW) {
        if (result.integerValue != 0) {
            if (result.integerValue == -81) {
                self.hud.labelText = @"验证码错误";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            self.hud.labelText = resultDic[@"friendmsg"];
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
            return;
        }
        [self.hud hide:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"新密码已通过短信发送，请注意查收！" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
    }
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
