//
//  ZbTabFindQyViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTabFindQyViewController.h"
#import "YKuImageInLoopScrollView.h"
#import "ZbQiyeTableViewCell.h"
#import "ZbQySearchViewController.h"
#import "ZbDropDownViewController.h"
#import "ZbQyDetailViewController.h"

#define HTTP_REQUEST_TAG_TYPE 1         // 企业服务类型列表
#define HTTP_REQUEST_TAG_AREA 2         // 企业省份列表
#define HTTP_REQUEST_TAG_AD 3           // 广告数据
#define HTTP_REQUEST_TAG_DATA 4         // 重载石材数据
#define HTTP_REQUEST_TAG_MORE 5         // 加载更多石材数据

@interface ZbTabFindQyViewController () <UITableViewDataSource, UITableViewDelegate, YKuImageInLoopScrollViewDelegate>

// taleview
@property (nonatomic, strong) IBOutlet UITableView *tv;
// taleview头部
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 广告滚动控件
@property (nonatomic, strong) YKuImageInLoopScrollView *imageLoopView;
// 导航栏titleView
@property (nonatomic, strong) IBOutlet UIView *navTitleView;
// 导航栏titleView圆角容器
@property (nonatomic, strong) IBOutlet UIView *navRoundContainer;
// 热门分类容器
@property (nonatomic, strong) IBOutlet UIView *hotTypeView;
// 热门分类容器top约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcHotTypeViewTop;
// 热门分类control列表
@property (nonatomic, strong) IBOutletCollection(UIControl) NSArray *controlHotArray;

// 广告数组
@property (nonatomic, strong) NSMutableArray *adArray;
// 所有数据
@property (nonatomic, strong) NSMutableArray *dataArray;

// 当前页数
@property (nonatomic, assign) NSInteger curPageIndex;

// 当前服务类型选中项
@property (nonatomic, assign) NSInteger curTypeIndex;
// 企业分类文字
@property (nonatomic, strong) NSString *curTypeText;
// 当前区域选中项
@property (nonatomic, assign) NSInteger curAreaIndex;
@property (nonatomic, strong) NSString *curAreaText;
// 当前默认排序选中项
@property (nonatomic, assign) NSInteger curSortNameIndex;
@property (nonatomic, strong) NSString *curSortNameText;

// 当前下拉框
@property (nonatomic, strong) ZbDropDownViewController *curDropDownController;

@end

@implementation ZbTabFindQyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.adArray = Mutable_ArrayInit;
    self.dataArray = [NSMutableArray array];
    self.curTypeIndex = -1;
    self.curAreaIndex = -1;
    self.curSortNameIndex = 0;
    
    // 圆角
    self.navRoundContainer.layer.cornerRadius = CGRectGetHeight(self.navTitleView.frame) / 2;
    self.navRoundContainer.layer.masksToBounds = YES;
    
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbQiyeTableViewCell" bundle:nil] forCellReuseIdentifier:QiyeCellIdentifier];
    
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
    [self.tv addLegendFooterWithRefreshingBlock:^{
        [weakSelf footerRefresh];
    }];
    self.tv.footer.hidden = YES;
    
    // 设置tableview头部高度
    CGRect tvHeaderFrame = self.tvHeader.frame;
    tvHeaderFrame.size.height = DeviceWidth / 3 + HOT_TYPES_HEIGHT;
    self.alcHotTypeViewTop.constant = DeviceWidth / 3;
    self.tvHeader.frame = tvHeaderFrame;
    [self.tv setTableHeaderView:self.tvHeader];
    
    // 初始化热门分类
    NSArray *localQiyeTypeArray = [[ZbSaveManager shareManager] getLocalQyTypeArray];
    if (localQiyeTypeArray != nil) {
        // 有本地用本地存储的，否则就是默认的
        [self drawHotType:localQiyeTypeArray];
    }
    
    // 添加滚动广告
    CGFloat adHeight = DeviceWidth / 3;
    self.imageLoopView = [[YKuImageInLoopScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, adHeight)];
    self.imageLoopView.delegate = self;
    // 设置yKuImageInLoopScrollView显示类型
    self.imageLoopView.scrollViewType = ScrollViewDefault;
    // 设置styledPageControl位置
    [self.imageLoopView.styledPageControl setPageControlSite:PageControlSiteMiddle];
    [self.imageLoopView.styledPageControl setBottomDistance:12];
    // 设置styledPageControl已选中下的内心圆颜色
    [self.imageLoopView.styledPageControl setCoreSelectedColor:MAIN_COLOR];
    [self.imageLoopView.styledPageControl setCoreNormalColor:[UIColor whiteColor]];
    [self.tvHeader addSubview:self.imageLoopView];
}

// 初始化UI
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.hud.labelText = @"努力加载中..";
    [self.hud show:NO];
    
    [self requestTypeData];
    [self requestAreaData];
    [self requestAdData];
    [self requestData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    self.navTitleView.frame = CGRectMake(0, 7, 600, 30);
    self.tabBarController.navigationItem.titleView = self.navTitleView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 界面绘制
// 绘制热门分类
- (void)drawHotType:(NSArray *)typeArray {
    for (NSInteger i= 0; i < self.controlHotArray.count - 1; i++) {
        UIControl *curControl = self.controlHotArray[i];
        UIImageView *ivIcon = [curControl viewWithTag:2];
        UILabel *labName = [curControl viewWithTag:1];
        
        if (i >= typeArray.count) {
            curControl.hidden = YES;
            continue;
        }
        curControl.hidden = NO;
        NSDictionary *curTypeDict = typeArray[i];
        labName.text = curTypeDict[@"class_name"];
        [ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:curTypeDict[@"class_logo"]]]];
    }
}

#pragma mark - 接口调用
// 获取企业服务类型列表
- (void)requestTypeData {
    NSArray *typeArray = [[ZbSaveManager shareManager] getQyTypeArray];
    if (typeArray != nil) {
        // 数据已加载过
        return;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyTypeList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_TYPE;
    [conn start];
}

// 获取企业省份列表
- (void)requestAreaData {
    NSArray *areaArray = [[ZbSaveManager shareManager] getAreaArray];
    if (areaArray != nil) {
        // 数据已加载过
        return;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getProvList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_AREA;
    [conn start];
}

// 获取滚动广告列表
- (void)requestAdData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanySearchAdList] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_AD;
    [conn start];
}

// 重载石材数据
- (void)requestData {
    // 服务类型
    NSString *type = @"";
    if (self.curTypeText.length > 0) {
        type = self.curTypeText;
        if ([type isEqualToString:@"全部"]) {
            type = @"";
        }
    }
    // 省份
    NSString *area = @"";
    if (self.curAreaIndex != -1) {
        NSArray *areaArray = [[ZbSaveManager shareManager] getAreaArray];
        if (self.curAreaIndex > 0) {
            if (self.curAreaIndex - 1 < areaArray.count) {
                area = areaArray[self.curAreaIndex - 1][@"class_name"];
            }
        }
    }
    // 排序
    NSArray *sortKeyArray = [[ZbSaveManager shareManager] getQySortKeyArray];
    NSString *sortKey = sortKeyArray[self.curSortNameIndex];
    NSString *sortName = [[sortKey componentsSeparatedByString:@","] firstObject];
    NSString *sortOrder = [[sortKey componentsSeparatedByString:@","] lastObject];
    
    self.curPageIndex = 1;
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:self.curPageIndex]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyList:@"" type:type area:area sortName:sortName sortOrder:sortOrder pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_DATA;
    [conn start];
    return;
}

// 加载更多石材数据
- (void)requestMoreData {
    // 服务类型
    NSString *type = @"";
    if (self.curTypeText.length > 0) {
        type = self.curTypeText;
        if ([type isEqualToString:@"全部"]) {
            type = @"";
        }
    }
    // 省份
    NSString *area = @"";
    if (self.curAreaIndex != -1) {
        NSArray *areaArray = [[ZbSaveManager shareManager] getAreaArray];
        if (self.curAreaIndex > 0) {
            if (self.curAreaIndex - 1 < areaArray.count) {
                area = areaArray[self.curAreaIndex - 1][@"class_name"];
            }
        }
    }
    // 排序
    NSArray *sortKeyArray = [[ZbSaveManager shareManager] getQySortKeyArray];
    NSString *sortKey = sortKeyArray[self.curSortNameIndex];
    NSString *sortName = [[sortKey componentsSeparatedByString:@","] firstObject];
    NSString *sortOrder = [[sortKey componentsSeparatedByString:@","] lastObject];
    
    NSString *page = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:(self.curPageIndex + 1)]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getCompanyList:@"" type:type area:area sortName:sortName sortOrder:sortOrder pageIndex:page] delegate:self];
    conn.tag = HTTP_REQUEST_TAG_MORE;
    [conn start];
    return;
}

#pragma mark - 点击事件
// 搜索按钮点击
- (IBAction)btnSearchOnClick:(id)sender {
    ZbQySearchViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQySearchViewController"];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

// 热门分类点击
- (IBAction)btnHotOnClick:(UIControl *)sender {
    UILabel *labName = [sender viewWithTag:1];
    if ([labName.text isEqualToString:@"展开"]) {
        // 最后一个默认为分类展开
        UIControl *firstControl = [[UIControl alloc] init];
        [self btnFilterOnClick:firstControl];
        return;
    }
    self.curTypeText = labName.text;
    UILabel *labFilterName = [self labelForFilterType:sender.tag];
    labFilterName.text = self.curTypeText;
    [self requestData];
    return;
}

// 筛选按钮点击
- (IBAction)btnFilterOnClick:(UIControl *)sender {
    switch (sender.tag) {
        case 0:
        {
            // 分类
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Pic) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
        case 1:
        {
            // 区域
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Text) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
        case 2:
        {
            // 排序
            if (self.curDropDownController != nil && self.curDropDownController.curType == Zb_DropDown_Type_Sort) {
                [self.curDropDownController dismiss:NO];
                self.curDropDownController = nil;
                return;
            }
            break;
        }
    }
    
    // 关闭旧菜单
    if (self.curDropDownController != nil) {
        [self.curDropDownController dismiss:NO];
        self.curDropDownController = nil;
    }
    
    // 筛选菜单未置顶则置顶
    CGFloat tvHeaderHeight = self.tvHeader.frame.size.height;
    if (self.tv.contentOffset.y <= tvHeaderHeight) {
        [self.tv setContentOffset:CGPointMake(0, tvHeaderHeight) animated:NO];
    }
    switch (sender.tag) {
        case 0:
        {
            NSArray *typeArray = [[ZbSaveManager shareManager] getQyTypeArray];
            if (typeArray == nil) {
                self.hud.labelText = @"获取类型数据失败，请下拉重刷";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:FILTER_CONTAINER_HEIGHT dataArray:typeArray type:Zb_DropDown_Type_Pic selectIndex:self.curTypeIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self labelForFilterType:sender.tag];
                labFilterName.text = name;
                weakSelf.curTypeIndex = index;
                weakSelf.curTypeText = name;
                [weakSelf requestData];
                
                self.curDropDownController = nil;
            } isNeedAll:YES];
            break;
        }
        case 1:
        {
            NSArray *areaArray = [[ZbSaveManager shareManager] getAreaArray];
            if (areaArray == nil) {
                self.hud.labelText = @"获取区域数据失败，请下拉重刷";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:FILTER_CONTAINER_HEIGHT dataArray:areaArray type:Zb_DropDown_Type_Text selectIndex:self.curAreaIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self labelForFilterType:sender.tag];
                labFilterName.text = name;
                weakSelf.curAreaIndex = index;
                weakSelf.curAreaText = name;
                [weakSelf requestData];
                
                self.curDropDownController = nil;
            } isNeedAll:YES];
            break;
        }
        case 2:
        {
            NSArray *sortNameArray = [[ZbSaveManager shareManager] getQySortTextArray];
            NSMutableArray *tagSortDictArray = Mutable_ArrayInit;
            for (NSString *name in sortNameArray) {
                NSDictionary *sortDict = @{@"class_name":name};
                [tagSortDictArray addObject:sortDict];
            }
            
            __weak typeof(self) weakSelf = self;
            self.curDropDownController = [ZbDropDownViewController showInViewController:self startY:FILTER_CONTAINER_HEIGHT dataArray:tagSortDictArray type:Zb_DropDown_Type_Sort selectIndex:self.curSortNameIndex selectBlock:^(NSInteger index, NSString *name, BOOL isDismiss) {
                if (isDismiss) {
                    self.curDropDownController = nil;
                    return;
                }
                UILabel *labFilterName = [self labelForFilterType:sender.tag];
                labFilterName.text = name;
                weakSelf.curSortNameIndex = index;
                weakSelf.curSortNameText = name;
                [weakSelf requestData];
                
                self.curDropDownController = nil;
            } isNeedAll:NO];
            break;
        }
        default:
            break;
    }
    return;
}

// 获取筛选名label
- (UILabel *)labelForFilterType:(NSInteger)tag {
    UITableViewHeaderFooterView *sectionHeader = [self.tv headerViewForSection:0];
    UILabel *labFilterName = [sectionHeader viewWithTag:(tag + 1)];
    return labFilterName;
}

#pragma mark - UITableViewDataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbQiyeTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZbQiyeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:QiyeCellIdentifier];
    NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
    [cell setInfoDict:curDict];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *curDict = [self.dataArray objectAtIndex:indexPath.row];
    ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
    controller.companyId = curDict[@"company_id"];
    [self.navigationController pushViewController:controller animated:YES];
}

// section头部高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FILTER_CONTAINER_HEIGHT;
}

// section头部view
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewHeaderFooterView *myHeader = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"filterHeaderCell"];
    if(!myHeader) {
        myHeader = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:@"filterHeaderCell"];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"filterHeaderCell"];
        cell.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), FILTER_CONTAINER_HEIGHT);
        [myHeader addSubview:cell];
        
        UIView *v = cell.contentView.subviews.firstObject;
        for (UIControl *curControl in v.subviews) {
            if (curControl.tag == 0) {
                // 分类
                if (self.curTypeText != nil) {
                    UILabel *labTypeName = [curControl viewWithTag:(curControl.tag + 1)];
                    labTypeName.text = self.curTypeText;
                }
            } else if (curControl.tag == 1) {
                if (self.curAreaText != nil) {
                    UILabel *labAreaName = [curControl viewWithTag:(curControl.tag + 1)];
                    labAreaName.text = self.curAreaText;
                }
            } else if (curControl.tag == 2) {
                if (self.curSortNameText != nil) {
                    UILabel *labSortName = [curControl viewWithTag:(curControl.tag + 1)];
                    labSortName.text = self.curSortNameText;
                }
            }
        }
    }
    return myHeader;
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 重置选项
    self.curTypeIndex = -1;
    self.curAreaIndex = -1;
    self.curSortNameIndex = 0;
    self.curSortNameText = nil;
    self.curAreaText = nil;
    self.curTypeText = nil;
    
    // 重置筛选标签文字
    for (NSInteger i = 0; i < COMPANY_FILETER_DEFAULT_NAME_ARRAY.count; i++) {
        UILabel *labFilterName = [self labelForFilterType:i];
        labFilterName.text = [COMPANY_FILETER_DEFAULT_NAME_ARRAY objectAtIndex:i];
    }
    [self requestTypeData];
    [self requestAreaData];
    [self requestAdData];
    [self requestData];
}

- (void)footerRefresh {
    [self requestMoreData];
}

#pragma mark - YKuImageInLoopScrollViewDelegate
- (int)numOfPageForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    return (int)self.adArray.count;
}

- (int)widthForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    int width = DeviceWidth;
    return width;
}

- (UIView *)scrollView:(YKuImageInLoopScrollView *)ascrollView viewAtPageIndex:(int)apageIndex
{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    UIImageView *curImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth / 3)];
    NSDictionary *adDict = [self.adArray objectAtIndex:apageIndex];
    NSString *imgUrl = adDict[@"ad_image"];
    [curImageView sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]]];
    curImageView.contentMode = UIViewContentModeScaleToFill;
    [bgView addSubview:curImageView];
    return bgView;
}

- (void)scrollView:(YKuImageInLoopScrollView*) ascrollView didTapIndex:(int)apageIndex{
//    DLog(@"Clicked page%d",apageIndex);
    NSDictionary *adDict = [self.adArray objectAtIndex:apageIndex];
    ZbQyDetailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ZbQyDetailViewController"];
    controller.companyId = adDict[@"company_id"];
    [self.navigationController pushViewController:controller animated:YES];
    
}

/*
 选中第几页
 @param didSelectedPageIndex 选中的第几项，[0-numOfPageForScrollView];
 */
-(void) scrollView:(YKuImageInLoopScrollView*) ascrollView didSelectedPageIndex:(int) apageIndex
{
    //    NSLog(@"didSelectedPageIndex:%d",apageIndex);
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    [self.tv.footer endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    if (connection.tag != HTTP_REQUEST_TAG_MORE) {
        [self.tv.header endRefreshing];
        [self.tv.footer endRefreshing];
    }
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    
    // 加载失败
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    NSMutableArray *dataArray = resultDic[@"data"];
    if (connection.tag == HTTP_REQUEST_TAG_TYPE) {
        
        [[ZbSaveManager shareManager] setQyTypeArray:dataArray];
        // 绘制热门分类
        [self drawHotType:dataArray];
    } else if (connection.tag == HTTP_REQUEST_TAG_AREA) {
        [[ZbSaveManager shareManager] setAreaArray:dataArray];
    } else if (connection.tag == HTTP_REQUEST_TAG_AD) {
        [self.adArray removeAllObjects];
        [self.adArray addObjectsFromArray:dataArray];
        
        [self.imageLoopView reloadData];
    } else if (connection.tag == HTTP_REQUEST_TAG_DATA) {
        NSMutableArray *qyArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:qyArray];
        if (qyArray.count > 0) {
            [self.tv.footer resetNoMoreData];
            self.tv.footer.hidden = NO;
        } else {
            self.tv.footer.hidden = YES;
            [self.tv.footer noticeNoMoreData];
        }
        
        self.tvHeader.hidden = NO;
        [self.hud hide:NO];
        [self.tv reloadData];
        if (self.dataArray.count > 0) {
            // 筛选菜单置顶位置
            CGFloat tvHeaderHeight = self.tvHeader.frame.size.height;
            if (self.tv.contentOffset.y > tvHeaderHeight) {
                [self.tv setContentOffset:CGPointMake(0, tvHeaderHeight) animated:NO];
            }
        }
    } else if (connection.tag == HTTP_REQUEST_TAG_MORE) {
        // 上拉加载更多
        [self.tv.footer endRefreshing];
        NSMutableArray *qyArray = ((NSMutableDictionary *)dataArray)[@"rows"];
        [self.dataArray addObjectsFromArray:qyArray];
        if (qyArray.count > 0) {
            self.curPageIndex += 1;
            [self.tv.footer resetNoMoreData];
        } else {
            [self.tv.footer noticeNoMoreData];
        }
        [self.tv reloadData];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
