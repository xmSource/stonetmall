//
//  BaseViewController.h
//  carport
//
//  Created by 张斌 on 15/6/8.
//  Copyright (c) 2015年 zbs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonHeader.h"

@interface BaseViewController : UIViewController <CTURLConnectionDelegate>

@property (nonatomic, assign) BOOL isInit;                   // 是否已初始化UI
@property (nonatomic, strong) MBProgressHUD *hud;            // 提示器
@property (nonatomic, strong) NSString *myName;              // 友盟统计用

// 初始化UI时机，只执行一次
- (void)viewWillAppearOnce;

// 返回点击
- (IBAction)leftBarItemOnClick:(id)sender;

// 请求结束执行的方法
- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data;
// 请求失败时执行的方法
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error;

@end
