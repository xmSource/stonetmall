//
//  BaseViewController.m
//  carport
//
//  Created by 张斌 on 15/6/8.
//  Copyright (c) 2015年 zbs. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSString *)getUmentClassName {
    if (self.myName) {
        return self.myName;
    }
    NSString *className = NSStringFromClass(self.class);
    return className;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 友盟统计
//    [MobClick beginLogPageView:[self getUmentClassName]];
    
    if (!self.isInit) {
        self.isInit = YES;
        [self viewWillAppearOnce];
    }
}

// 初始化UI时机，只执行一次
- (void)viewWillAppearOnce {
    
    // 初始化统一的文字提示器
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.removeFromSuperViewOnHide = NO;
    [self.view addSubview:self.hud];
    return;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [MobClick endLogPageView:[self getUmentClassName]];
}

#pragma mark - 点击事件
// 返回点击
- (IBAction)leftBarItemOnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    return;
}

#pragma mark - CTURLConnection delegate
// 请求结束执行的方法
- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    return;
}

// 请求失败时执行的方法
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
