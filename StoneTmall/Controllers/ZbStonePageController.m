//
//  ZbStonePageController.m
//  StoneTmall
//
//  Created by chyo on 15/9/7.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbStonePageController.h"
#import "ZbStoneTableViewCell.h"
#import "ZbStoneDetailViewController.h"

@interface ZbStonePageController ()

@property (nonatomic, strong) IBOutlet UITableView *tv;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic) ZbCxl_Type zbcxlType;

@end

@implementation ZbStonePageController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArray = [[NSMutableArray alloc] init];
    // 注册cell
    [self.tv registerNib:[UINib nibWithNibName:@"ZbStoneTableViewCell" bundle:nil] forCellReuseIdentifier:StoneCellIdentifier];
    __weak typeof(self) weakSelf = self;
    //添加下拉刷新
    [self.tv addLegendHeaderWithRefreshingBlock:^{
        [weakSelf headerRefresh];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)setControllerType:(ZbCxl_Type)ZbCxlType {
    _zbcxlType = ZbCxlType;
    if (ZbCxlType == ZbCxl_Type_comme) {
        //推荐
    } else if (ZbCxlType == ZbCXl_Type_colle) {
        //收藏
        [self loadData];
    } else if (ZbCxlType == ZbCxl_Type_invol) {
        //参与
    }
}

#pragma mark - 读取数据
// 读取收藏数据
- (void)loadData {
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPostMethodUrl:ZB_WEBSERVICE_HOST body:[ZbWebService getMyMarkStoneList] delegate:self];
    [conn start];
}

#pragma mark - TableViewDelegate &Datasource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ZbStoneTableViewCell getRowCount:self.dataArray];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZbStoneTableViewCell getRowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZbStoneTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:StoneCellIdentifier];
    // 绘制
    [cell setStoneRow:indexPath.row dataArray:self.dataArray];
    // 石材点击
    cell.stoneClickBlock = ^(NSInteger stoneIndex, NSDictionary *stoneDict) {
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ZbStoneDetailViewController *controller = [main instantiateViewControllerWithIdentifier:@"ZbStoneDetailViewController"];
        NSString *stoneName = stoneDict[@"stone_name"];
        controller.stoneName = stoneName;
        [self.navigationController pushViewController:controller animated:YES];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 上拉或下拉刷新
- (void)headerRefresh {
    // 重置选项
    if (_zbcxlType == ZbCxl_Type_comme) {
        //推荐
    } else if (_zbcxlType == ZbCXl_Type_colle) {
        //收藏
        [self loadData];
    } else if (_zbcxlType == ZbCxl_Type_invol) {
        //参与
    }
}


#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    [self.tv.header endRefreshing];
    self.hud.labelText = @"加载失败，请稍后重试";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    [self.tv.header endRefreshing];
    NSError *error;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *result = resultDic[@"errno"];
    if (result.integerValue != 0) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (result.integerValue == 0) {
        [self.dataArray removeAllObjects];
        [self.dataArray addObjectsFromArray:(NSArray *)resultDic[@"data"]];
        [self.tv reloadData];
    }
}

@end
