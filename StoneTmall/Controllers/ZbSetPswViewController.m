//
//  ZbSetPswViewController.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/21.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSetPswViewController.h"

@interface ZbSetPswViewController () <UITextFieldDelegate>

// 新密码输入框
@property (nonatomic, strong) IBOutlet UITextField *tfPsw;
// 下一步按钮
@property (nonatomic, strong) IBOutlet UIButton *btnNext;

@end

@implementation ZbSetPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 按钮圆角
    self.btnNext.layer.cornerRadius = 3;
    
    // 界面点击关闭键盘手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap)];
    [self.view addGestureRecognizer:tap];
    
    // 下一步按钮初始不激活
    [self setBtnNextEnable:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 设置下一步按钮激活状态
- (void)setBtnNextEnable:(BOOL)isEnable {
    self.btnNext.enabled = isEnable;
    self.btnNext.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

#pragma mark - 点击事件
// 界面点击
- (void)viewTap {
    [self.view endEditing:YES];
}

// 下一步按钮点击
- (IBAction)btnNextOnClick:(id)sender {
    self.hud.labelText = @"敬请期待";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    return;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

// 文本内容变化
- (IBAction)textFieldChanged:(id)sender {
    if (self.tfPsw.text.length > 0) {
        [self setBtnNextEnable:YES];
    } else {
        [self setBtnNextEnable:NO];
    }
    return;
}

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
