//
//  ZbNewAdrViewController.h
//  StoneTmall
//
//  Created by chyo on 15/9/11.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, POST_TYPE) {
    POST_ADD = 0,
    POST_EDIT,
};

@interface ZbNewAdrViewController : BaseViewController

@property (nonatomic, strong) NSDictionary *detailDict;

- (void)setContollerType:(POST_TYPE)postType;

@end
