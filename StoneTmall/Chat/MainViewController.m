/************************************************************
  *  * EaseMob CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import "MainViewController.h"

#import "UIViewController+HUD.h"
#import "ChatListViewController.h"
#import "ContactsViewController.h"
#import "SettingsViewController.h"
#import "ApplyViewController.h"
//#import "CallViewController.h"
#import "ChatViewController.h"
#import "ZbSaveManager.h"
#import "CTURLConnection.h"
#import "ZbWebService.h"

@interface MainViewController () <UIAlertViewDelegate, IChatManagerDelegate, CTURLConnectionDelegate>
{
    ChatListViewController *_chatListVC;
    ContactsViewController *_contactsVC;
    SettingsViewController *_settingsVC;
//    __weak CallViewController *_callController;
    
    UIBarButtonItem *_addFriendItem;
}

@property (strong, nonatomic) NSDate *lastPlaySoundDate;

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //if 使tabBarController中管理的viewControllers都符合 UIRectEdgeNone
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.title = NSLocalizedString(@"title.conversation", @"Conversations");
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_btn_arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarItemOnClick:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    //获取未读消息数，此时并没有把self注册为SDK的delegate，读取出的未读数是上次退出程序时的
//    [self didUnreadMessagesCountChanged];
#warning 把self注册为SDK的delegate
    [self registerNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUntreatedApplyCount) name:@"setupUntreatedApplyCount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callOutWithChatter:) name:@"callOutWithChatter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callControllerClose:) name:@"callControllerClose" object:nil];
    
    [self setupSubviews];
    self.selectedIndex = 0;
    
    _addFriendItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_btn_add"] style:UIBarButtonItemStylePlain target:_contactsVC action:@selector(addFriendAction)];
    _addFriendItem.tintColor = [UIColor whiteColor];
    
    [self setupUnreadMessageCount];
    [self setupUntreatedApplyCount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [self unregisterNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 点击事件
// 返回点击
- (void)leftBarItemOnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    return;
}

#pragma mark - 接口调用

#pragma mark - 接口返回
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    return;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    return;
}

#pragma mark - UITabBarDelegate

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if (item.tag == 0) {
        self.title = NSLocalizedString(@"title.conversation", @"Conversations");
        self.navigationItem.rightBarButtonItem = nil;
    }else if (item.tag == 1){
        self.title = NSLocalizedString(@"title.addressbook", @"AddressBook");
        self.navigationItem.rightBarButtonItem = _addFriendItem;
    }else if (item.tag == 2){
        self.title = NSLocalizedString(@"title.setting", @"Setting");
        self.navigationItem.rightBarButtonItem = nil;
        [_settingsVC refreshConfig];
    }
}

#pragma mark - private

-(void)registerNotifications
{
    [self unregisterNotifications];
    
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
//    [[EaseMob sharedInstance].callManager addDelegate:self delegateQueue:nil];
}

-(void)unregisterNotifications
{
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
//    [[EaseMob sharedInstance].callManager removeDelegate:self];
}

- (void)setupSubviews
{
    self.tabBar.backgroundColor = [UIColor clearColor];
    self.tabBar.tintColor = MAIN_COLOR;
    self.tabBar.translucent = NO;
    
    _chatListVC = [[ChatListViewController alloc] init];
    [_chatListVC networkChanged:_connectionState];
    if (VERSION_LESS_THAN_IOS7) {
        _chatListVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title.conversation", @"Conversations")
                                                               image:nil
                                                                 tag:0];
        [_chatListVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_chatsHL"]
                             withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_chats"]];
    } else {
        UIImage *image = [[UIImage imageNamed:@"tabbar_chats"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImage *selectedImage = [[UIImage imageNamed:@"tabbar_chatsHL"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        _chatListVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title.conversation", @"Conversations") image:image selectedImage:selectedImage];
        _chatListVC.tabBarItem.tag = 0;
    }
    
    _contactsVC = [[ContactsViewController alloc] initWithNibName:nil bundle:nil];
    if (VERSION_LESS_THAN_IOS7) {
        _contactsVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title.addressbook", @"AddressBook")
                                                               image:nil
                                                                 tag:1];
        [_contactsVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_contactsHL"]
                             withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_contacts"]];
    } else {
        UIImage *image = [[UIImage imageNamed:@"tabbar_contacts"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImage *selectedImage = [[UIImage imageNamed:@"tabbar_contactsHL"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        _contactsVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title.addressbook", @"AddressBook") image:image selectedImage:selectedImage];
        _contactsVC.tabBarItem.tag = 1;
    }
    
    _settingsVC = [[SettingsViewController alloc] init];
    if (VERSION_LESS_THAN_IOS7) {
        _settingsVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title.setting", @"Setting")
                                                               image:nil
                                                                 tag:2];
        [_settingsVC.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"tabbar_settingHL"]
                             withFinishedUnselectedImage:[UIImage imageNamed:@"tabbar_setting"]];
    } else {
        UIImage *image = [[UIImage imageNamed:@"tabbar_setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImage *selectedImage = [[UIImage imageNamed:@"tabbar_settingHL"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        _settingsVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"title.setting", @"Setting") image:image selectedImage:selectedImage];
        _settingsVC.tabBarItem.tag = 2;
    }
    _settingsVC.view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    self.viewControllers = @[_chatListVC, _contactsVC, _settingsVC];
}

// 统计未读消息数
-(void)setupUnreadMessageCount
{
    NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    if (_chatListVC) {
        if (unreadCount > 0) {
            _chatListVC.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",(int)unreadCount];
        }else{
            _chatListVC.tabBarItem.badgeValue = nil;
        }
    }
}

- (void)setupUntreatedApplyCount
{
    NSInteger unreadCount = [[[ApplyViewController shareController] dataSource] count];
    if (_contactsVC) {
        if (unreadCount > 0) {
            _contactsVC.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",(int)unreadCount];
        }else{
            _contactsVC.tabBarItem.badgeValue = nil;
        }
    }
}

- (void)networkChanged:(EMConnectionState)connectionState
{
    _connectionState = connectionState;
    [_chatListVC networkChanged:connectionState];
}

// 用户昵称、头像信息变化
- (void)onUserExtraInfoChange {
    if (_chatListVC) {
        [_chatListVC refreshDataSource];
    }
    if (_contactsVC) {
        [_contactsVC reloadDataSource];
    }
}

#pragma mark - call

- (BOOL)canRecord
{
    __block BOOL bCanRecord = YES;
//    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending)
//    {
//        if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
//            [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
//                bCanRecord = granted;
//            }];
//        }
//    }
//    
//    if (!bCanRecord) {
//        UIAlertView * alt = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"setting.microphoneNoAuthority", @"No microphone permissions") message:NSLocalizedString(@"setting.microphoneAuthority", @"Please open in \"Setting\"-\"Privacy\"-\"Microphone\".") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", @"OK"), nil];
//        [alt show];
//    }
    
    return bCanRecord;
}

- (void)callOutWithChatter:(NSNotification *)notification
{
//    id object = notification.object;
//    if ([object isKindOfClass:[NSDictionary class]]) {
//        if (![self canRecord]) {
//            return;
//        }
//        
//        EMError *error = nil;
//        NSString *chatter = [object objectForKey:@"chatter"];
//        EMCallSessionType type = [[object objectForKey:@"type"] intValue];
//        EMCallSession *callSession = nil;
//        if (type == eCallSessionTypeAudio) {
//            callSession = [[EaseMob sharedInstance].callManager asyncMakeVoiceCall:chatter timeout:50 error:&error];
//        }
//        else if (type == eCallSessionTypeVideo){
//            if (![CallViewController canVideo]) {
//                return;
//            }
//            callSession = [[EaseMob sharedInstance].callManager asyncMakeVideoCall:chatter timeout:50 error:&error];
//        }
//        
//        if (callSession && !error) {
//            [[EaseMob sharedInstance].callManager removeDelegate:self];
//            
//            CallViewController *callController = [[CallViewController alloc] initWithSession:callSession isIncoming:NO];
//            callController.modalPresentationStyle = UIModalPresentationOverFullScreen;
//            [self presentViewController:callController animated:NO completion:nil];
//        }
//        
//        if (error) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", @"error") message:error.description delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
//            [alertView show];
//        }
//    }
}

- (void)callControllerClose:(NSNotification *)notification
{
//    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
//    [audioSession setActive:YES error:nil];
 
//    [[EaseMob sharedInstance].callManager addDelegate:self delegateQueue:nil];
}

#pragma mark - IChatManagerDelegate 消息变化

- (void)didUpdateConversationList:(NSArray *)conversationList
{
    [self setupUnreadMessageCount];
    [_chatListVC refreshDataSource];
}

// 未读消息数量变化回调
-(void)didUnreadMessagesCountChanged
{
    [self setupUnreadMessageCount];
}

- (void)didFinishedReceiveOfflineMessages
{
    [self setupUnreadMessageCount];
}

- (void)didFinishedReceiveOfflineCmdMessages
{
    
}

// 收到消息回调
-(void)didReceiveMessage:(EMMessage *)message
{
}

-(void)didReceiveCmdMessage:(EMMessage *)message
{
//    [self showHint:NSLocalizedString(@"receiveCmd", @"receive cmd message")];
}

#pragma mark - IChatManagerDelegate 登陆回调（主要用于监听自动登录是否成功）

- (void)didLoginWithInfo:(NSDictionary *)loginInfo error:(EMError *)error
{
//    if (error) {
//        NSString *hintText = NSLocalizedString(@"reconnection.retry", @"Fail to log in your account, is try again... \nclick 'logout' button to jump to the login page \nclick 'continue to wait for' button for reconnection successful");
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt")
//                                                            message:hintText
//                                                           delegate:self
//                                                  cancelButtonTitle:NSLocalizedString(@"reconnection.wait", @"continue to wait")
//                                                  otherButtonTitles:NSLocalizedString(@"logout", @"Logout"),
//                                  nil];
//        alertView.tag = 99;
//        [alertView show];
//        [_chatListVC isConnect:NO];
//    }
}

#pragma mark - IChatManagerDelegate 好友变化

- (void)didReceiveBuddyRequest:(NSString *)username
                       message:(NSString *)message
{
    [_contactsVC reloadApplyView];
}

- (void)_removeBuddies:(NSArray *)userNames
{
    [_chatListVC refreshDataSource];
}

- (void)didUpdateBuddyList:(NSArray *)buddyList
            changedBuddies:(NSArray *)changedBuddies
                     isAdd:(BOOL)isAdd
{
    [_contactsVC reloadDataSource];
}

- (void)didRemovedByBuddy:(NSString *)username
{
    [_contactsVC reloadDataSource];
}

- (void)didAcceptedByBuddy:(NSString *)username
{
    [_contactsVC reloadDataSource];
}

- (void)didRejectedByBuddy:(NSString *)username
{
}

- (void)didAcceptBuddySucceed:(NSString *)username
{
    [_contactsVC reloadDataSource];
}

#pragma mark - IChatManagerDelegate 群组变化

- (void)didReceiveGroupInvitationFrom:(NSString *)groupId
                              inviter:(NSString *)username
                              message:(NSString *)message
{
    [_contactsVC reloadGroupView];
}

//接收到入群申请
- (void)didReceiveApplyToJoinGroup:(NSString *)groupId
                         groupname:(NSString *)groupname
                     applyUsername:(NSString *)username
                            reason:(NSString *)reason
                             error:(EMError *)error
{
    if (!error) {
        [_contactsVC reloadGroupView];
    }
}

- (void)didReceiveGroupRejectFrom:(NSString *)groupId
                          invitee:(NSString *)username
                           reason:(NSString *)reason
{
}


- (void)didReceiveAcceptApplyToJoinGroup:(NSString *)groupId
                               groupname:(NSString *)groupname
{
}

#pragma mark - IChatManagerDelegate 收到聊天室邀请

- (void)didReceiveChatroomInvitationFrom:(NSString *)chatroomId
                              inviter:(NSString *)username
                              message:(NSString *)message
{
}

#pragma mark - IChatManagerDelegate 登录状态变化

- (void)didLoginFromOtherDevice
{
}

- (void)didRemovedFromServer
{
}

- (void)didServersChanged
{
}

- (void)didAppkeyChanged
{
}

#pragma mark - 自动登录回调

- (void)willAutoReconnect{
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    NSNumber *showreconnect = [ud objectForKey:@"identifier_showreconnect_enable"];
//    if (showreconnect && [showreconnect boolValue]) {
//        [self hideHud];
//        [self showHint:NSLocalizedString(@"reconnection.ongoing", @"reconnecting...")];
//    }
}

- (void)didAutoReconnectFinishedWithError:(NSError *)error{
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    NSNumber *showreconnect = [ud objectForKey:@"identifier_showreconnect_enable"];
//    if (showreconnect && [showreconnect boolValue]) {
//        [self hideHud];
//        if (error) {
//            [self showHint:NSLocalizedString(@"reconnection.fail", @"reconnection failure, later will continue to reconnection")];
//        }else{
//            [self showHint:NSLocalizedString(@"reconnection.success", @"reconnection successful！")];
//        }
//    }
}

#pragma mark - ICallManagerDelegate

//- (void)callSessionStatusChanged:(EMCallSession *)callSession changeReason:(EMCallStatusChangedReason)reason error:(EMError *)error
//{
//    if (callSession.status == eCallSessionStatusConnected)
//    {
//        EMError *error = nil;
//        do {
//            BOOL isShowPicker = [[[NSUserDefaults standardUserDefaults] objectForKey:@"isShowPicker"] boolValue];
//            if (isShowPicker) {
//                error = [EMError errorWithCode:EMErrorInitFailure andDescription:NSLocalizedString(@"call.initFailed", @"Establish call failure")];
//                break;
//            }
//            
//            if (![self canRecord]) {
//                error = [EMError errorWithCode:EMErrorInitFailure andDescription:NSLocalizedString(@"call.initFailed", @"Establish call failure")];
//                break;
//            }
//            
//#warning 在后台不能进行视频通话
//            if(callSession.type == eCallSessionTypeVideo && ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive || ![CallViewController canVideo])){
//                error = [EMError errorWithCode:EMErrorInitFailure andDescription:NSLocalizedString(@"call.initFailed", @"Establish call failure")];
//                break;
//            }
//            
//            if (!isShowPicker){
//                [[EaseMob sharedInstance].callManager removeDelegate:self];
//                CallViewController *callController = [[CallViewController alloc] initWithSession:callSession isIncoming:YES];
//                callController.modalPresentationStyle = UIModalPresentationOverFullScreen;
//                [self presentViewController:callController animated:NO completion:nil];
//                if ([self.navigationController.topViewController isKindOfClass:[ChatViewController class]])
//                {
//                    ChatViewController *chatVc = (ChatViewController *)self.navigationController.topViewController;
//                    chatVc.isInvisible = YES;
//                }
//            }
//        } while (0);
//        
//        if (error) {
//            [[EaseMob sharedInstance].callManager asyncEndCall:callSession.sessionId reason:eCallReasonHangup];
//            return;
//        }
//    }
//}

#pragma mark - public

- (void)jumpToChatList
{
    if ([self.navigationController.topViewController isKindOfClass:[ChatViewController class]]) {
        ChatViewController *chatController = (ChatViewController *)self.navigationController.topViewController;
        [chatController hideImagePicker];
    }
    else if(_chatListVC)
    {
        [self.navigationController popToViewController:self animated:NO];
        [self setSelectedViewController:_chatListVC];
    }
}

- (EMConversationType)conversationTypeFromMessageType:(EMMessageType)type
{
    EMConversationType conversatinType = eConversationTypeChat;
    switch (type) {
        case eMessageTypeChat:
            conversatinType = eConversationTypeChat;
            break;
        case eMessageTypeGroupChat:
            conversatinType = eConversationTypeGroupChat;
            break;
        case eMessageTypeChatRoom:
            conversatinType = eConversationTypeChatRoom;
            break;
        default:
            break;
    }
    return conversatinType;
}

- (void)didReceiveLocalNotification:(UILocalNotification *)notification
{
//    NSDictionary *userInfo = notification.userInfo;
//    if (userInfo)
//    {
//        if ([self.navigationController.topViewController isKindOfClass:[ChatViewController class]]) {
//            ChatViewController *chatController = (ChatViewController *)self.navigationController.topViewController;
//            [chatController hideImagePicker];
//        }
//        
//        NSArray *viewControllers = self.navigationController.viewControllers;
//        [viewControllers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop){
//            if (obj != self)
//            {
//                if (![obj isKindOfClass:[ChatViewController class]])
//                {
//                    [self.navigationController popViewControllerAnimated:NO];
//                }
//                else
//                {
//                    NSString *conversationChatter = userInfo[kConversationChatter];
//                    ChatViewController *chatViewController = (ChatViewController *)obj;
//                    if (![chatViewController.chatter isEqualToString:conversationChatter])
//                    {
//                        [self.navigationController popViewControllerAnimated:NO];
//                        EMMessageType messageType = [userInfo[kMessageType] intValue];
//                        chatViewController = [[ChatViewController alloc] initWithChatter:conversationChatter conversationType:[self conversationTypeFromMessageType:messageType]];
//                        switch (messageType) {
//                            case eMessageTypeGroupChat:
//                                {
//                                    NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
//                                    for (EMGroup *group in groupArray) {
//                                        if ([group.groupId isEqualToString:conversationChatter]) {
//                                            chatViewController.title = group.groupSubject;
//                                            break;
//                                        }
//                                    }
//                                }
//                                break;
//                            default:
//                                chatViewController.title = conversationChatter;
//                                break;
//                        }
//                        [self.navigationController pushViewController:chatViewController animated:NO];
//                    }
//                    *stop= YES;
//                }
//            }
//            else
//            {
//                ChatViewController *chatViewController = (ChatViewController *)obj;
//                NSString *conversationChatter = userInfo[kConversationChatter];
//                EMMessageType messageType = [userInfo[kMessageType] intValue];
//                chatViewController = [[ChatViewController alloc] initWithChatter:conversationChatter conversationType:[self conversationTypeFromMessageType:messageType]];
//                switch (messageType) {
//                    case eMessageTypeGroupChat:
//                    {
//                        NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
//                        for (EMGroup *group in groupArray) {
//                            if ([group.groupId isEqualToString:conversationChatter]) {
//                                chatViewController.title = group.groupSubject;
//                                break;
//                            }
//                        }
//                    }
//                        break;
//                    default:
//                        chatViewController.title = conversationChatter;
//                        break;
//                }
//                [self.navigationController pushViewController:chatViewController animated:NO];
//            }
//        }];
//    }
//    else if (_chatListVC)
//    {
//        [self.navigationController popToViewController:self animated:NO];
//        [self setSelectedViewController:_chatListVC];
//    }
}

@end
