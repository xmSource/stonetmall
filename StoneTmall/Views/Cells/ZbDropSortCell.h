//
//  ZbDropSortCell.h
//  StoneTmall
//
//  Created by 张斌 on 16/2/25.
//  Copyright © 2016年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbDropSortCell : UITableViewCell

// 点击block
@property (nonatomic, copy) void (^dropPicClickBlock)(NSInteger index, NSDictionary *curDict);

// 按钮列表
@property (nonatomic, strong) IBOutletCollection(UIControl) NSArray *btnArray;
// 数据列表
@property (nonatomic, weak) NSArray *dataArray;

// 设置数据
- (void)drawDataArray:(NSArray *)array;

// 获取行高
+ (NSInteger)getRowHeight;

@end
