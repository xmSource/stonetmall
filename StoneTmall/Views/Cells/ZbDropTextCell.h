//
//  ZbDropTextCell.h
//  StoneTmall
//
//  Created by 张斌 on 16/2/25.
//  Copyright © 2016年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbDropTextCell : UITableViewCell

// 按钮列表
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *btnArray;
// 数据列表
@property (nonatomic, weak) NSArray *dataArray;
// 行数
@property (nonatomic, assign) NSInteger curRow;

// 点击block
@property (nonatomic, copy) void (^dropTextClickBlock)(NSInteger index, NSDictionary *curDict);

// 设置数据
- (void)setRow:(NSInteger)row dataArray:(NSArray *)array;

// 根据数据计算行数
+ (NSInteger)getRowCount:(NSInteger)totalCount;
// 获取行高
+ (NSInteger)getRowHeight;

@end
