//
//  ZbQiyeTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQiyeTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbQiyeTableViewCell

- (void)awakeFromNib {
    
    // 图片边框
    self.ivIcon.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
    self.ivIcon.layer.borderWidth = 1;
    self.ivIcon.clipsToBounds = YES;
    
    // 圆角
    self.vipLvBg.layer.cornerRadius = 3;
    self.vipLvBg.layer.masksToBounds = YES;
    self.growValueBg.layer.cornerRadius = 3;
    self.growValueBg.layer.masksToBounds = YES;
    
    // 距离默认隐藏
    self.labDistance.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict {
    // 企业icon
    NSString *image = infoDict[@"company_image"];
    [self.ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:image    ]]];
    // 企业名
    self.labName.text = infoDict[@"company_name"];
    // vip等级
    NSString *level = infoDict[@"company_level"];
    self.labVipLv.text = [NSString stringWithFormat:@"V%@", level];
    self.vipLvBg.backgroundColor = level.integerValue > 0 ? MAIN_ORANGE_COLOR : UIColorFromHexString(@"CCCCCC");
    // 成长值
    NSString *grow = infoDict[@"company_grow"];
    self.labGrowValue.text = [NSString stringWithFormat:@"成长值%@", grow];
    self.growValueBg.backgroundColor = grow.integerValue > 0 ? UIColorFromHexString(@"04A875") : UIColorFromHexString(@"CCCCCC");
    // 是否验证
    NSString *licenceOk = infoDict[@"company_licence_ok"];
    self.ivLicenceOk.highlighted = licenceOk.integerValue == 1;
    // 地址
    NSString *area = infoDict[@"company_area"] == nil ? @"" : infoDict[@"company_area"];
    NSString *addr = [NSString stringWithFormat:@"%@ %@ %@", infoDict[@"company_prov"], infoDict[@"company_city"], area];
    self.labAddr.text = addr;
    return;
}

// 显示距离
- (void)showDistance:(NSDictionary *)infoDict {
    self.labDistance.hidden = NO;
    NSString *distance = infoDict[@"distance"];
    self.labDistance.text = [NSString stringWithFormat:@"%@", distance];
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 94;
}

@end
