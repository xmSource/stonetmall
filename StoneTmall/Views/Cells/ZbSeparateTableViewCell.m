//
//  ZbSeparateFooterTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/3.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSeparateTableViewCell.h"

@implementation ZbSeparateTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 10;
}

@end
