//
//  ZbStoneColorTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/30.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbStoneColorTableViewCell.h"

@implementation ZbStoneColorTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 50;
}

@end
