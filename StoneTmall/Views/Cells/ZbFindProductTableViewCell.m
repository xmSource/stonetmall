//
//  ZbFindProductTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbFindProductTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbFindProductTableViewCell

- (void)awakeFromNib {
    
    // 图片边框
    self.ivIcon.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
    self.ivIcon.layer.borderWidth = 1;
    self.ivIcon.clipsToBounds = YES;
    
    // 圆角
    self.vipLvBg.layer.cornerRadius = 3;
    self.vipLvBg.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict {
    // 图片
    NSString *image = infoDict[@"product_image"];
    [self.ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:image]]];
    // 标题
    self.labTitle.text = infoDict[@"product_title"];
    // 企业名
    self.labQyName.text = infoDict[@"company_name"];
    // vip等级
    NSString *level = infoDict[@"company_level"];
    self.labVipLv.text = [NSString stringWithFormat:@"V%@", level];
    self.vipLvBg.backgroundColor = level.integerValue > 0 ? MAIN_ORANGE_COLOR : UIColorFromHexString(@"CCCCCC");
    // 浏览、赞、评论
    NSNumber *clicks = infoDict[@"product_clicks"];
    NSNumber *marks = infoDict[@"product_marks"];
    NSNumber *likes = infoDict[@"product_likes"];
    NSString *communicate = [NSString stringWithFormat:@"%@浏览 %@收藏 %@赞", clicks, marks, likes];
    self.labCommunicate.text = communicate;
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 94;
}

@end
