//
//  ZbQyIntroductionTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/4.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbQyIntroductionTableViewCell : UITableViewCell

// 展开点击block
@property (nonatomic, copy) void (^expandClickBlock)();

// 设置介绍名和内容
- (void)setIntroductionName:(NSString *)name content:(NSString *)content expand:(BOOL)expand;

// 获取行高
- (CGFloat)calcCellHeight:(NSString *)content expand:(BOOL)expand;

@end
