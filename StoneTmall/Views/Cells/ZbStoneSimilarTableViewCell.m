//
//  ZbStoneSimilarTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/3.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbStoneSimilarTableViewCell.h"
#import "CommonHeader.h"

#define STONE_IMAGE_WIDTH 85
#define STONE_IMAGE_HEIGHT 64
#define STONE_IMAGE_SPACE 4

@implementation ZbStoneSimilarTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置数据
- (void)setStoneArray:(NSArray *)array {
    // 移除旧数据
    [self.sv.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    // 内容宽度
    CGFloat tagWidth = STONE_IMAGE_SPACE + (STONE_IMAGE_WIDTH + STONE_IMAGE_SPACE) * array.count;
    [self.sv setContentSize:CGSizeMake(tagWidth, CGRectGetHeight(self.sv.frame))];
    
    self.dataArray = array;
    // 添加图片
    for (NSInteger i = 0; i < array.count; i++) {
        NSDictionary *curDict = [array objectAtIndex:i];
        CGFloat x = STONE_IMAGE_SPACE + (STONE_IMAGE_WIDTH + STONE_IMAGE_SPACE) * i;
        UIImageView *ivStone = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, STONE_IMAGE_WIDTH, STONE_IMAGE_HEIGHT)];
        ivStone.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
        ivStone.layer.borderWidth = 1;
        ivStone.clipsToBounds = YES;
        ivStone.contentMode = UIViewContentModeScaleAspectFill;
        ivStone.userInteractionEnabled = YES;
        
        // 图片
        NSString *imgUrl = curDict[@"stone_image"];
        [ivStone sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]]];
        
        // 点击函数
        ivStone.tag = i;
        [ivStone addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stoneOnClick:)]];
        [self.sv addSubview:ivStone];
    }
    
    return;
}

// 设置产品数据
- (void)setProductArray:(NSArray *)array {
    // 移除旧数据
    [self.sv.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    // 内容宽度
    CGFloat tagWidth = STONE_IMAGE_SPACE + (STONE_IMAGE_WIDTH + STONE_IMAGE_SPACE) * array.count;
    [self.sv setContentSize:CGSizeMake(tagWidth, CGRectGetHeight(self.sv.frame))];
    
    self.dataArray = array;
    // 添加图片
    for (NSInteger i = 0; i < array.count; i++) {
        NSDictionary *curDict = [array objectAtIndex:i];
        CGFloat x = STONE_IMAGE_SPACE + (STONE_IMAGE_WIDTH + STONE_IMAGE_SPACE) * i;
        UIImageView *ivStone = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, STONE_IMAGE_WIDTH, STONE_IMAGE_HEIGHT)];
        ivStone.clipsToBounds = YES;
        ivStone.contentMode = UIViewContentModeScaleAspectFill;
        
        // 图片
        NSString *imgUrl = curDict[@"product_image"];
        [ivStone sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imgUrl]]];
        
        // 点击函数
        ivStone.tag = i;
        [ivStone addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stoneOnClick:)]];
    }
    
    return;
}

#pragma mark - 点击事件
// 石材点击
- (void)stoneOnClick:(UIGestureRecognizer *)ges {
    UIImageView *ivStone = (UIImageView *)ges.view;
    NSInteger index = ivStone.tag;
    NSDictionary *stoneDict = [self.dataArray objectAtIndex:index];
    if (self.stoneClickBlock) {
        self.stoneClickBlock(ivStone.tag, stoneDict);
    }
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 84;
}

@end
