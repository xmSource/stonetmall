//
//  ZbQyIntroductionTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/4.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQyIntroductionTableViewCell.h"
#import "CommonHeader.h"

@interface ZbQyIntroductionTableViewCell ()

// 介绍名
@property (nonatomic, strong) IBOutlet UILabel *labIntroductionName;
// 介绍内容
@property (nonatomic, strong) IBOutlet UILabel *labIntroductionContent;
// 展开按钮
@property (nonatomic, strong) IBOutlet UIButton *btnExpand;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcBtnExpandHeight;
// 展开图片
@property (nonatomic, strong) IBOutlet UIImageView *ivExpand;

@end

@implementation ZbQyIntroductionTableViewCell

- (void)awakeFromNib {
    self.labIntroductionContent.preferredMaxLayoutWidth = DeviceWidth - 15*2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置介绍名和内容
- (void)setIntroductionName:(NSString *)name content:(NSString *)content expand:(BOOL)expand {
    self.labIntroductionName.text = name;
    self.labIntroductionContent.text = content;
    
    if (expand) {
        // 已展开
        self.btnExpand.hidden = YES;
        self.alcBtnExpandHeight.constant = 0;
        self.ivExpand.hidden = YES;
        self.labIntroductionContent.numberOfLines = 0;
        return;
    }
    CGSize size = [self.labIntroductionContent.text sizeWithFont:self.labIntroductionContent.font constrainedToSize:CGSizeMake(self.labIntroductionContent.preferredMaxLayoutWidth, CGFLOAT_MAX) lineBreakMode:self.labIntroductionContent.lineBreakMode];
    CGSize oneLineSize = [@"test" sizeWithFont:self.labIntroductionContent.font constrainedToSize:CGSizeMake(self.labIntroductionContent.preferredMaxLayoutWidth, CGFLOAT_MAX) lineBreakMode:self.labIntroductionContent.lineBreakMode];
    if (size.height <= oneLineSize.height * 3) {
        self.btnExpand.hidden = YES;
        self.alcBtnExpandHeight.constant = 0;
        self.ivExpand.hidden = YES;
        self.labIntroductionContent.numberOfLines = 0;
    } else {
        self.btnExpand.hidden = NO;
        self.alcBtnExpandHeight.constant = 27;
        self.ivExpand.hidden = NO;
        self.labIntroductionContent.numberOfLines = 3;
    }
}

// 获取行高
- (CGFloat)calcCellHeight:(NSString *)content expand:(BOOL)expand {
    self.labIntroductionContent.text = content;
    
    if (expand) {
        self.alcBtnExpandHeight.constant = 0;
        self.labIntroductionContent.numberOfLines = 0;
    } else {
        CGSize size = [self.labIntroductionContent.text sizeWithFont:self.labIntroductionContent.font constrainedToSize:CGSizeMake(self.labIntroductionContent.preferredMaxLayoutWidth, CGFLOAT_MAX) lineBreakMode:self.labIntroductionContent.lineBreakMode];
        CGSize oneLineSize = [@"test" sizeWithFont:self.labIntroductionContent.font constrainedToSize:CGSizeMake(self.labIntroductionContent.preferredMaxLayoutWidth, CGFLOAT_MAX) lineBreakMode:self.labIntroductionContent.lineBreakMode];
        if (size.height <= oneLineSize.height * 3) {
            self.alcBtnExpandHeight.constant = 0;
            self.labIntroductionContent.numberOfLines = 0;
        } else {
            self.alcBtnExpandHeight.constant = 27;
            self.labIntroductionContent.numberOfLines = 3;
        }
    }
    CGSize size = [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

#pragma mark - 点击事件
// 展开按钮点击
- (IBAction)btnExpandOnClick:(id)sender {
    if (self.expandClickBlock) {
        self.expandClickBlock();
    }
    return;
}

@end
