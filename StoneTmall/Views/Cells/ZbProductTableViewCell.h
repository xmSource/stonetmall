//
//  ZbProductTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbProductTableViewCell : UITableViewCell

// 图片
@property (nonatomic, strong) IBOutlet UIImageView *ivIcon;
// 标题
@property (nonatomic, strong) IBOutlet UILabel *labTitle;
// 企业名
@property (nonatomic, strong) IBOutlet UILabel *labQyName;

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict;

// 获取行高
+ (NSInteger)getRowHeight;

@end
