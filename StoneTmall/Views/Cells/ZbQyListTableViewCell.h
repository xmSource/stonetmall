//
//  ZbQyListTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/5.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbQyListTableViewCell : UITableViewCell

// 左图片
@property (nonatomic, strong) IBOutlet UIImageView *ivLeft;
// 中图片
@property (nonatomic, strong) IBOutlet UIImageView *ivMid;
// 右图片
@property (nonatomic, strong) IBOutlet UIImageView *ivRight;

// 左数据索引
@property (nonatomic, assign) NSInteger leftIndex;
// 左数据
@property (nonatomic, weak) NSDictionary *leftDict;
// 中数据索引
@property (nonatomic, assign) NSInteger midIndex;
// 中数据
@property (nonatomic, weak) NSDictionary *midDict;
// 右数据索引
@property (nonatomic, assign) NSInteger rightIndex;
// 右数据
@property (nonatomic, weak) NSDictionary *rightDict;

// 点击block
@property (nonatomic, copy) void (^iconClickBlock)(NSInteger index, NSDictionary *infoDict);

// 设置数据
- (void)setInfoRow:(NSInteger)row dataArray:(NSArray *)array;

// 根据数据计算行数
+ (NSInteger)getRowCount:(NSArray *)dataArray;
// 获取行高
+ (NSInteger)getRowHeight;

@end
