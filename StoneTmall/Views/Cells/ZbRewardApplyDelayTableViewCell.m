//
//  ZbRewardApplyDelayTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/23.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardApplyDelayTableViewCell.h"
#import "CommonHeader.h"
#import "ZbRewardManager.h"

@implementation ZbRewardApplyDelayTableViewCell

- (void)awakeFromNib {
    // 参与按钮圆角
    self.btnDelay.layer.cornerRadius = 3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置延迟按钮激活状态
- (void)setBtnDelayEnable:(BOOL)isEnable {
    self.btnDelay.enabled = isEnable;
    self.btnDelay.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

// 设置信息数据
- (void)setPostDict:(NSDictionary *)postDict {
    [self setCounDownDate:postDict];
    NSString *post_status = postDict[@"post_status"];
    
    // 已延迟过，则按钮不激活
    [self setBtnDelayEnable:(post_status.integerValue != Zb_Post_Status_Type_Delay)];
    self.labDes.text = post_status.integerValue == Zb_Post_Status_Type_Delay ? @"您已延长过悬赏时间" : @"您可以通过增加悬赏额来延长悬赏时间";
    return;
}

// 设置参与倒计时
- (void)setCounDownDate:(NSDictionary *)postDict {
    [ZbRewardManager SetCountDownDate:self.labDate postDict:postDict];
}

// 设置选标截止时间
- (void)setSelectDate:(NSDictionary *)postDict {
    // 过期时间
    NSString *post_expired = postDict[@"post_expired"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *expiredDate = [dateFormatter dateFromString:post_expired];
    
    NSString *first = @"请在";
    [dateFormatter setDateFormat:@"M"];
    NSString *month = [dateFormatter stringFromDate:expiredDate];
    NSString *monthText = @"月";
    [dateFormatter setDateFormat:@"d"];
    NSString *day = [dateFormatter stringFromDate:expiredDate];
    NSString *dayText = @"日";
    [dateFormatter setDateFormat:@"H"];
    NSString *hour = [dateFormatter stringFromDate:expiredDate];
    NSString *hourText = @"点";
    [dateFormatter setDateFormat:@"m"];
    NSString *minute = [dateFormatter stringFromDate:expiredDate];
    NSString *minuteText = @"分";
    NSString *last = @"之前选标";
    // 设置文字
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@", first, month, monthText, day, dayText, hour, hourText, minute, minuteText, last]];
    // 月属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length, month.length)];
    // 日属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length + month.length + monthText.length, day.length)];
    // 时属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length + month.length + monthText.length + day.length + dayText.length, hour.length)];
    // 分属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length + month.length + monthText.length + day.length + dayText.length + hour.length + hourText.length, minute.length)];
    self.labDate.attributedText = attriString;
}

#pragma mark - 点击事件
// 参与点击
- (IBAction)btnJoinOnClick:(id)sender {
    if (self.delayClickBlock) {
        self.delayClickBlock();
    }
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 110;
}

@end
