//
//  ZbTypeSubTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/8.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTypeSubTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbTypeSubTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 设置选中状态
- (void)setIsCurSelect:(BOOL)isSelect {
    //当选中
    if (isSelect) {
        self.labName.textColor = MAIN_ORANGE_COLOR;
    }else{
        self.labName.textColor = UIColorFromHexString(@"333333");
    }
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 55;
}

@end
