//
//  ZbAddrChooseTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/13.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbAddrChooseTableViewCell.h"

@implementation ZbAddrChooseTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - 点击事件
// 选择地址按钮点击
- (IBAction)btnAddrChooseOnClick:(id)sender {
    if (self.addrChooseClickBlock) {
        self.addrChooseClickBlock();
    }
    return;
}

@end
