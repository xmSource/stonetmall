//
//  ZbFindProductTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbFindProductTableViewCell : UITableViewCell

// 图片
@property (nonatomic, strong) IBOutlet UIImageView *ivIcon;
// 标题
@property (nonatomic, strong) IBOutlet UILabel *labTitle;
// 企业名
@property (nonatomic, strong) IBOutlet UILabel *labQyName;
// vip等级
@property (nonatomic, strong) IBOutlet UILabel *labVipLv;
// vip等级背景
@property (nonatomic, strong) IBOutlet UIView *vipLvBg;
// 浏览、赞、评论
@property (nonatomic, strong) IBOutlet UILabel *labCommunicate;

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict;

// 获取行高
+ (NSInteger)getRowHeight;

@end
