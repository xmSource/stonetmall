//
//  ZbSeeMoreTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSeeMoreTableViewCell.h"

@implementation ZbSeeMoreTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 点击函数
- (IBAction)btnSeeMoreOnClick:(id)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight:(BOOL)isNeedBottomArea {
    if (isNeedBottomArea) {
        return 55;
    }
    return 45;
}

@end
