//
//  ZbQiyeTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbQiyeTableViewCell : UITableViewCell

// 图片
@property (nonatomic, strong) IBOutlet UIImageView *ivIcon;
// 名称
@property (nonatomic, strong) IBOutlet UILabel *labName;
// vip等级
@property (nonatomic, strong) IBOutlet UILabel *labVipLv;
// vip等级背景
@property (nonatomic, strong) IBOutlet UIView *vipLvBg;
// 成长值
@property (nonatomic, strong) IBOutlet UILabel *labGrowValue;
// 成长值背景
@property (nonatomic, strong) IBOutlet UIView *growValueBg;
// 身份是否验证
@property (nonatomic, strong) IBOutlet UIImageView *ivLicenceOk;
// 地址
@property (nonatomic, strong) IBOutlet UILabel *labAddr;
// 距离
@property (nonatomic, strong) IBOutlet UILabel *labDistance;

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict;
// 显示距离
- (void)showDistance:(NSDictionary *)infoDict;

// 获取行高
+ (NSInteger)getRowHeight;

@end
