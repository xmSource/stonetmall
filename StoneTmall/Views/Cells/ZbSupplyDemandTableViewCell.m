//
//  ZbSupplyDemandTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSupplyDemandTableViewCell.h"
#import "CommonHeader.h"
#import "ZbRewardManager.h"

@implementation ZbSupplyDemandTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置供需信息数据
- (void)setPostDict:(NSDictionary *)postDict {
    // 消息类型背景view
    UIView *postTypeBgView = (UIView *)[self viewWithTag:1];
    // 消息类型名label
    UILabel *labPostType = (UILabel *)[self viewWithTag:2];
    // 标题label
    UILabel *labPostTitle = (UILabel *)[self viewWithTag:3];
    // 地点label
    UILabel *labPostLocation = (UILabel *)[self viewWithTag:4];
    // 时间label
    UILabel *labPostDate = (UILabel *)[self viewWithTag:5];
    // 浏览量label
    UILabel *labPostLooks = (UILabel *)[self viewWithTag:6];
    // 价格label
    UILabel *labPostPrice = (UILabel *)[self viewWithTag:7];
    // 悬赏消息的标题label
    UILabel *labPostRewardTitle = (UILabel *)[self viewWithTag:8];
    
    // 消息数据
    NSDictionary *curPost = postDict;
    
    // 类型名、背景
    postTypeBgView.layer.cornerRadius = 2;
    NSString *postType = curPost[@"post_type"];
    if (postType.intValue == Zb_Home_Message_Type_Supply) {
        labPostType.text = @"供应";
        postTypeBgView.backgroundColor = UIColorFromHexString(@"4A90E2");
    } else if (postType.intValue == Zb_Home_Message_Type_Buy) {
        labPostType.text = @"求购";
        postTypeBgView.backgroundColor = UIColorFromHexString(@"00CFA0");
    } else {
        [ZbRewardManager setTypeText:labPostType typeBg:postTypeBgView postDict:curPost];
;
    }
    // 标题
    labPostTitle.text = curPost[@"post_title"];
    labPostTitle.hidden = postType.intValue == Zb_Home_Message_Type_Reward;
    labPostRewardTitle.text = curPost[@"post_title"];
    labPostRewardTitle.hidden = postType.intValue != Zb_Home_Message_Type_Reward;
    // 价格
    NSNumber *cent = curPost[@"post_price"];
    NSNumber *yuan = [NSNumber numberWithInteger:(cent.integerValue / 100)];
    labPostPrice.text = [NSString stringWithFormat:@"¥%@", yuan];
    labPostPrice.hidden = postType.intValue != Zb_Home_Message_Type_Reward;
    
    if (curPost[@"post_location"] == nil) {
        // 收藏有些字段缺失
        labPostLocation.hidden = YES;
        labPostDate.hidden = YES;
        labPostLooks.hidden = YES;
        return;
    } else {
        labPostLocation.hidden = NO;
        labPostDate.hidden = NO;
        labPostLooks.hidden = NO;
    }
    
    // 隐藏供需的浏览量
    labPostLooks.hidden = postType.intValue != Zb_Home_Message_Type_Reward;
    
    // 地点
    if (postType.intValue == Zb_Home_Message_Type_Supply) {
        labPostLocation.text = curPost[@"post_location"];
    } else if (postType.intValue == Zb_Home_Message_Type_Buy) {
        labPostLocation.text = curPost[@"post_location"];
    } else {
        // 悬赏时间
        labPostLocation.text = [CommonCode getTimeShowText:curPost[@"post_modified"]];
    }
    // 时间
    if (postType.intValue == Zb_Home_Message_Type_Supply) {
        labPostDate.text = [CommonCode getTimeShowText:curPost[@"post_modified"]];
    } else if (postType.intValue == Zb_Home_Message_Type_Buy) {
        labPostDate.text = [CommonCode getTimeShowText:curPost[@"post_modified"]];
    } else {
        // 悬赏状态
        [ZbRewardManager setStatusText:labPostDate postDict:curPost];
    }
    // 浏览量
    if (postType.intValue == Zb_Home_Message_Type_Supply) {
        labPostLooks.text = [NSString stringWithFormat:@"%@浏览", curPost[@"post_clicks"]];
    } else if (postType.intValue == Zb_Home_Message_Type_Buy) {
        labPostLooks.text = [NSString stringWithFormat:@"%@浏览", curPost[@"post_clicks"]];
    } else {
        labPostLooks.text = [NSString stringWithFormat:@"%@人参与", curPost[@"post_joins"]];
    }
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 71;
}

@end
