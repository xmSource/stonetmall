//
//  ZbNoticeTableViewCell.h
//  StoneTmall
//
//  Created by chyo on 15/10/28.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbNoticeTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *imgLeft;

@property (nonatomic, strong) IBOutlet UILabel *dateLab;

@property (nonatomic, strong) IBOutlet UILabel *contentLab;

// 设置数据
- (void)setNoticeDict:(NSDictionary *)noticeDict;
// 设置数据为计算高度
- (void)setNoticeDictForCalc:(NSDictionary *)noticeDict;

@end
