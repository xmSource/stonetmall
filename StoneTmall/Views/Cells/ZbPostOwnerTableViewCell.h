//
//  ZbPostOwnerTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbPostOwnerTableViewCell : UITableViewCell

// 人物类型名
@property (nonatomic, strong) IBOutlet UILabel *labOwnerType;
// 头像
@property (nonatomic, strong) IBOutlet UIImageView *ivImage;
// 名字
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 电话图片
@property (nonatomic, strong) IBOutlet UIImageView *ivTel;
// 微信图片
@property (nonatomic, strong) IBOutlet UIImageView *ivWechat;
// 身份证图片
@property (nonatomic, strong) IBOutlet UIImageView *ivPassport;
// 地址
@property (nonatomic, strong) IBOutlet UILabel *labAddr;
// 微聊按钮
@property (nonatomic, strong) IBOutlet UIButton *btnChat;

// 设置数据
- (void)setPostDict:(NSDictionary *)postDict;

// 获取行高
+ (NSInteger)getRowHeight;

// 微聊按钮block
@property (nonatomic, copy) void (^chatClickBlock)(void);

@end
