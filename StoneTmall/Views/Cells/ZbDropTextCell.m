//
//  ZbDropTextCell.m
//  StoneTmall
//
//  Created by 张斌 on 16/2/25.
//  Copyright © 2016年 stonetmall. All rights reserved.
//

#import "ZbDropTextCell.h"

@implementation ZbDropTextCell

- (void)awakeFromNib {
    for (UIButton *btn in self.btnArray) {
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 设置数据
- (void)setRow:(NSInteger)row dataArray:(NSArray *)array {
    
    NSArray *dataArray = array;
    for (NSInteger i = 0; i < self.btnArray.count; i++) {
        NSInteger index = row * 4 + i;
        UIButton *btn = self.btnArray[i];
        btn.hidden = index >= dataArray.count;
        if (index < dataArray.count) {
            [btn setTitle:dataArray[index][@"class_name"] forState:UIControlStateNormal];
        }
    }
    self.dataArray = dataArray;
    self.curRow = row;
    return;
}

#pragma mark - 点击事件
// 按钮点击
- (IBAction)btnOnClick:(UIButton *)sender {
    NSInteger index = self.curRow * 4 + sender.tag;
    NSDictionary *curDict = self.dataArray[index];
    if (self.dropTextClickBlock) {
        self.dropTextClickBlock(index, curDict);
    }
    return;
}

#pragma mark - 类方法
// 根据数据计算行数
+ (NSInteger)getRowCount:(NSInteger)totalCount {
    // 每行4个
    NSInteger row = ceilf(totalCount / 4.0);
    return row;
}

// 获取行高
+ (NSInteger)getRowHeight {
    return 45;
}

@end
