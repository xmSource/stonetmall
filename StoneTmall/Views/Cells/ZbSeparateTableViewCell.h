//
//  ZbSeparateFooterTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/3.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbSeparateTableViewCell : UITableViewCell

// 获取行高
+ (NSInteger)getRowHeight;

@end
