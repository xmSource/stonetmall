//
//  ZbStoneTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/24.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbStoneTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbStoneTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置石材数据
- (void)setStoneRow:(NSInteger)row dataArray:(NSArray *)array {
    self.leftStoneDict = nil;
    self.rightStoneDict = nil;
    
    // 获取控件
    UIImageView *ivStoneLeft = (UIImageView *)[self viewWithTag:1];
    ivStoneLeft.clipsToBounds = YES;
    [ivStoneLeft addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftStoneOnClick)]];
    UILabel *labStoneNameLeft = (UILabel *)[self viewWithTag:11];
    UIImageView *ivStoneRight = (UIImageView *)[self viewWithTag:2];
    ivStoneRight.clipsToBounds = YES;
    [ivStoneRight addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightStoneOnClick)]];
    UILabel *labStoneNameRight = (UILabel *)[self viewWithTag:12];
    
    NSArray *dataArray = array;
    // 左数据
    NSInteger left = row * 2;
    ivStoneLeft.hidden = !(left < dataArray.count);
    labStoneNameLeft.hidden = ivStoneLeft.hidden;
    labStoneNameLeft.superview.hidden = ivStoneLeft.hidden;
    if (left < dataArray.count) {
        // 左石材图片
        NSString *leftImgUrl = dataArray[left][@"stone_image"];
        [ivStoneLeft sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:leftImgUrl]]];
        // 左石材名
        NSString *leftStoneName = dataArray[left][@"stone_name"];
        labStoneNameLeft.text = leftStoneName;
        // 保持当前数据信息
        self.leftStoneDict = dataArray[left];
    }
    // 右数据
    NSInteger right = row * 2 + 1;
    ivStoneRight.hidden = !(right < dataArray.count);
    labStoneNameRight.hidden = ivStoneRight.hidden;
    labStoneNameRight.superview.hidden = ivStoneRight.hidden;
    if (right < dataArray.count) {
        // 右石材图片
        NSString *rightImgUrl = dataArray[right][@"stone_image"];
        [ivStoneRight sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:rightImgUrl]]];
        // 右石材名
        NSString *rightStoneName = dataArray[right][@"stone_name"];
        labStoneNameRight.text = rightStoneName;
        // 保持当前数据信息
        self.rightStoneDict = dataArray[right];
    }
    
    // 保持当前数据信息
    self.leftIndex = left;
    self.rightIndex = right;
    return;
}

#pragma mark - 点击事件
// 左石材点击
- (void)leftStoneOnClick {
    if (self.stoneClickBlock) {
        self.stoneClickBlock(self.leftIndex, self.leftStoneDict);
    }
    return;
}

// 右石材点击
- (void)rightStoneOnClick {
    if (self.stoneClickBlock) {
        self.stoneClickBlock(self.rightIndex, self.rightStoneDict);
    }
    return;
}

#pragma mark - 类方法
// 根据数据计算行数
+ (NSInteger)getRowCount:(NSArray *)dataArray {
    // 每行2个
    NSInteger stonetRow = ceilf(dataArray.count / 2.0);
    return stonetRow;
}

// 获取行高
+ (NSInteger)getRowHeight {
    // 图片宽度
    CGFloat imageWidth = (DeviceWidth - 4*3) / 2;
    // 宽高比1.4:1
    CGFloat imageHeight = imageWidth / 1.4;
    return imageHeight + 4;
}

@end
