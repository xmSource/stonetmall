//
//  ZbRewardJoinTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/23.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardJoinTableViewCell.h"
#import "CommonHeader.h"
#import "ZbRewardManager.h"

@implementation ZbRewardJoinTableViewCell

- (void)awakeFromNib {
    // 参与按钮圆角
    self.btnJoin.layer.cornerRadius = 3;
    
    [self.btnJoin setTitle:@"立即参与" forState:UIControlStateNormal];
    [self.btnJoin setTitle:@"您已参加" forState:UIControlStateDisabled];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置信息数据
- (void)setPostDict:(NSDictionary *)postDict {
    [self setCounDownDate:postDict];
    
    // 已参与，则按钮不激活
    NSString *post_role = postDict[@"post_role"];
    [self setBtnDelayEnable:(post_role.integerValue != Zb_Post_Role_Type_Join)];
    return;
}

// 设置参与按钮激活状态
- (void)setBtnDelayEnable:(BOOL)isEnable {
    self.btnJoin.enabled = isEnable;
    self.btnJoin.backgroundColor = isEnable ? MAIN_ORANGE_COLOR : MAIN_GRAY_COLOR;
    return;
}

// 设置参与倒计时
- (void)setCounDownDate:(NSDictionary *)postDict {
    [ZbRewardManager SetCountDownDate:self.labDate postDict:postDict];
}

#pragma mark - 点击事件
// 参与点击
- (IBAction)btnJoinOnClick:(id)sender {
    if (self.joinClickBlock) {
        self.joinClickBlock();
    }
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 87;
}

@end
