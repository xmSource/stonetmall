//
//  ZbPostCommentTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbPostCommentTableViewCell.h"
#import "CommonHeader.h"

@interface ZbPostCommentTableViewCell ()

@property (nonatomic, weak) NSDictionary *curCommentDict;

@end

@implementation ZbPostCommentTableViewCell

- (void)awakeFromNib {
    // 圆形头像
    self.ivImage.layer.cornerRadius = CGRectGetHeight(self.ivImage.frame) / 2;
    self.ivImage.layer.masksToBounds = YES;
    self.ivImage.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    self.ivImage.layer.borderWidth = 1;
    self.ivReply.layer.masksToBounds = YES;
    self.ivReply.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    self.ivReply.layer.borderWidth = 1;
    // 圆角按钮
    self.btnUse.layer.cornerRadius = 2;
    self.btnUse.layer.masksToBounds = YES;
    
    self.labComment.preferredMaxLayoutWidth = DeviceWidth - 15*2 - 45 - 15;
    self.ivWin.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 图片绘制
- (void)drawImageInfo:(NSDictionary *)commentDict {
    // 图片
    NSString *postImage = commentDict[@"post_image"];
    if (postImage == nil || [postImage isKindOfClass:[NSNull class]] || postImage.length <= 0) {
        // 无图片
        self.alcImageHeight.constant = 0;
        self.alcImageTop.constant = 0;
        self.ivReply.hidden = YES;
        return;
    }
    // 有图片
    self.alcImageHeight.constant = 68;
    self.alcImageTop.constant = 8;
    self.ivReply.hidden = NO;
    NSString *imageUrl = [ZbWebService getImageFullUrl:postImage];
    [self.ivReply sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
}

// 设置数据
- (void)setCommentDict:(NSDictionary *)commentDict {
    self.curCommentDict = commentDict;
    // 头像
    NSURL *imageUrl = [NSURL URLWithString:[ZbWebService getImageFullUrl:commentDict[@"user_image"]]];
    [self.ivImage sd_setImageWithURL:imageUrl];
    // 名字
    NSString *nickName = commentDict[@"user_nickname"];
    if (nickName != nil && ![nickName isKindOfClass:[NSNull class]] && nickName.length > 0) {
        self.labName.text = nickName;
    } else {
        self.labName.text = commentDict[@"user_name"];
    }
    // 电话图片
    NSNumber *tel = commentDict[@"user_phone"];
    self.ivTel.highlighted = !([tel isKindOfClass:[NSNull class]]) && tel.integerValue > 0;
    // 微信图片
    self.ivWechat.highlighted = YES;
    // 身份证图片
    NSNumber *card = commentDict[@"user_card_ok"];
    self.ivPassport.highlighted = ![card isKindOfClass:[NSNull class]] && card.integerValue == Zb_User_Verify_Status_Status_Success;
    // 内容
    self.labComment.text = commentDict[@"post_content"];
    // 时间
    NSString *date = commentDict[@"post_modified"];
    self.labDate.text = [CommonCode getTimeShowText:date];
    // 图片
    [self drawImageInfo:commentDict];
    return;
}

// 设置数据为计算高度
- (void)setCommentDictForCalc:(NSDictionary *)commentDict {
    // 内容
    self.labComment.text = commentDict[@"post_content"];
    // 图片
    [self drawImageInfo:commentDict];
}

#pragma mark - 点击事件
// 回复点击
- (IBAction)btnReplyOnClick:(id)sender {
    if (self.replyClickBlock) {
        self.replyClickBlock(self.curCommentDict);
    }
    return;
}

// 采用点击
- (IBAction)btnUseOnClick:(id)sender {
    if (self.useClickBlock) {
        self.useClickBlock(self.curCommentDict);
    }
    return;
}

@end
