//
//  ZbPostOwnerTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbPostOwnerTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbPostOwnerTableViewCell

- (void)awakeFromNib {
    // 圆形头像
    self.ivImage.layer.cornerRadius = CGRectGetHeight(self.ivImage.frame) / 2;
    self.ivImage.layer.masksToBounds = YES;
    self.ivImage.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    self.ivImage.layer.borderWidth = 1;
    // 圆角按钮
    self.btnChat.layer.cornerRadius = 3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置数据
- (void)setPostDict:(NSDictionary *)postDict {
    // 头像
    NSString *imageUrl = postDict[@"user_image"];
    [self.ivImage sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:imageUrl]]];
    // 名字
    NSString *nickName = postDict[@"user_nickname"];
    if (nickName != nil && ![nickName isKindOfClass:[NSNull class]] && nickName.length > 0) {
        self.labName.text = nickName;
    } else {
        self.labName.text = postDict[@"user_name"];
    }
    // 电话图片
    NSNumber *tel = postDict[@"user_phone"];
    self.ivTel.highlighted = !([tel isKindOfClass:[NSNull class]]) && tel.integerValue > 0;
    // 微信图片
    self.ivWechat.highlighted = YES;
    // 身份证图片
    NSNumber *card = postDict[@"user_card_ok"];
    self.ivPassport.highlighted = card.integerValue == Zb_User_Verify_Status_Status_Success;
    // 地址
    self.labAddr.text = postDict[@"post_location"];
    return;
}

#pragma mark - 点击事件
// 微聊点击
- (IBAction)btnChatOnClick:(id)sender {
    if (self.chatClickBlock) {
        self.chatClickBlock();
    }
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 110;
}

@end
