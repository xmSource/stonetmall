//
//  ZbSampleTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/11.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSampleTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbSampleTableViewCell

- (void)awakeFromNib {
    // 数量容器圆角边框
    self.viewCount.layer.borderColor = UIColorFromHexString(@"CCCCCC").CGColor;
    self.viewCount.layer.borderWidth = 1;
    self.viewCount.layer.cornerRadius = 3;
    self.viewCount.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - 点击事件
// 增加点击
- (IBAction)btnAddOnClick:(id)sender {
    if (self.addBlock) {
        self.addBlock();
    }
}

// 减少点击
- (IBAction)btnCutOnClick:(id)sender {
    if (self.cutBlock) {
        self.cutBlock();
    }
    return;
}

@end
