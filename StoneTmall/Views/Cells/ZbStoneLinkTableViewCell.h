//
//  ZbStoneLinkTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/9.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbStoneLinkTableViewCell : UITableViewCell

// 图片
@property (nonatomic, strong) IBOutlet UIImageView *ivIcon;
// 石材名称
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 石材适用范围
@property (nonatomic, strong) IBOutlet UILabel *labUsage;

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict;

// 获取行高
+ (NSInteger)getRowHeight;

@end
