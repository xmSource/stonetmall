//
//  ZbUserUnLoginHeader.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbUserUnLoginHeader.h"
#import "ZbMyorderViewController.h"
#import "ZbShopCartViewController.h"

@interface ZbUserUnLoginHeader ()

@end

@implementation ZbUserUnLoginHeader

- (void)awakeFromNib {
    self.btnLogin.layer.cornerRadius = 2.0f;
    self.btnLogin.layer.masksToBounds = YES;
}

// 登陆点击
- (IBAction)loginClick:(UIControl *)control {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_LOGIN object:nil];
}

// 我的订单
- (IBAction)orderClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbMyorderViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbMyorderViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 我的购物车
- (IBAction)shopCartClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbShopCartViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbShopCartViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}
@end
