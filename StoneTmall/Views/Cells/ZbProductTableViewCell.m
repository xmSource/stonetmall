//
//  ZbProductTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbProductTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbProductTableViewCell

- (void)awakeFromNib {
    
    // 图片边框
    self.ivIcon.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
    self.ivIcon.layer.borderWidth = 1;
    self.ivIcon.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict {
    // 图片
    NSString *image = infoDict[@"product_image"];
    [self.ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:image]]];
    // 标题
    self.labTitle.text = infoDict[@"product_title"];
    // 企业名
    self.labQyName.text = infoDict[@"company_name"];
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 70;
}

@end
