//
//  ZbUserNormalHeader.h
//  StoneTmall
//
//  Created by chyo on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZbUserHeaderDelegate.h"

@interface ZbUserNormalHeader : UIView

@property (nonatomic, strong) IBOutlet UIButton *btnQiye;
// 用户头像
@property (strong, nonatomic) IBOutlet UIImageView *imgHead;
// 用户昵称
@property (strong, nonatomic) IBOutlet UILabel *labName;
// 用户地区
@property (strong, nonatomic) IBOutlet UILabel *labProvince;
@property (strong, nonatomic) IBOutlet UIImageView *userImgFlag;
@property (strong, nonatomic) IBOutlet UIImageView *userImgFlag2;
@property (strong, nonatomic) IBOutlet UIImageView *userImgFlag3;

// 订单区域
@property (strong, nonatomic) IBOutlet UIView *orderView;

@property (nonatomic, weak) id<ZbUserHeaderDelegate> delegate;

@end
