//
//  ZbSupplyDemandTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbSupplyDemandTableViewCell : UITableViewCell

// 设置供需信息数据
- (void)setPostDict:(NSDictionary *)postDict;

// 获取行高
+ (NSInteger)getRowHeight;

@end
