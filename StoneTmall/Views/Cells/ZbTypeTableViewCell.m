//
//  ZbTypeTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTypeTableViewCell.h"

@implementation ZbTypeTableViewCell

- (void)awakeFromNib {
    // 默认不显示选择按钮
    self.btnSelect.hidden = YES;
    self.btnClick.hidden = YES;
    self.ivArrow.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - 点击事件
- (IBAction)btnSelectOnClick:(UIButton *)btn {
    BOOL selected = btn.selected;
    if (self.selectClickBlock) {
        self.selectClickBlock(selected);
    }
    return;
}

- (IBAction)btnClickOnClick:(UIButton *)btn {
    if (self.clickBlock) {
        self.clickBlock();
    }
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 36;
}

@end
