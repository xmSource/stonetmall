//
//  ZbNoRecordTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbNoRecordTableViewCell : UITableViewCell

// 获取行高
+ (NSInteger)getRowHeight;

@end
