//
//  ZbSearchStoneTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbSearchStoneTableViewCell : UITableViewCell

// 图片
@property (nonatomic, strong) IBOutlet UIImageView *ivIcon;
// 石材名称
@property (nonatomic, strong) IBOutlet UILabel *labName;

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict;

// 获取行高
+ (NSInteger)getRowHeight;

@end
