//
//  ZbTypeSubTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/8.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbTypeSubTableViewCell : UITableViewCell

// 类名
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 类名leading约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcLabNameLeading;
// 类别icon
@property (nonatomic, strong) IBOutlet UIImageView *ivIcon;

// 获取行高
+ (NSInteger)getRowHeight;
// 设置选中状态
- (void)setIsCurSelect:(BOOL)isSelect;

@end
