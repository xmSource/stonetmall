//
//  ZbUserNormalHeader.m
//  StoneTmall
//
//  Created by chyo on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbUserNormalHeader.h"
#import "ZbUserInfoEtViewController.h"
#import "ZbMyorderViewController.h"
#import "ZbShopCartViewController.h"
#import "ZbBecomeQyViewController.h"

@implementation ZbUserNormalHeader

- (void)awakeFromNib {
    self.imgHead.layer.cornerRadius = 30.0f;
    self.imgHead.layer.masksToBounds = YES;
    self.btnQiye.layer.cornerRadius = 2.0f;
    self.btnQiye.layer.masksToBounds = YES;
}
// 编辑资料
- (IBAction)edInfoClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbUserInfoEtViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbUserInfoEtViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}
// 成为企业用户
- (IBAction)beQiYeClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    ZbBecomeQyViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbBecomeQyViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 我的订单
- (IBAction)orderClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbMyorderViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbMyorderViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 我的购物车
- (IBAction)shopCartClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbShopCartViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbShopCartViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

@end
