//
//  ZbStoneSimilarTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/3.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbStoneSimilarTableViewCell : UITableViewCell

// 相似石材滚动view
@property (nonatomic, strong) IBOutlet UIScrollView *sv;
// 相似石材数据
@property (nonatomic, weak) NSArray *dataArray;

// 石材点击block
@property (nonatomic, copy) void (^stoneClickBlock)(NSInteger stoneIndex, NSDictionary *stoneDict);

// 设置石材数据
- (void)setStoneArray:(NSArray *)array;
// 设置产品数据
- (void)setProductArray:(NSArray *)array;

// 获取行高
+ (NSInteger)getRowHeight;

@end
