//
//  ZbRewardApplyDelayTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/23.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbRewardApplyDelayTableViewCell : UITableViewCell

// 结束时间label
@property (nonatomic, strong) IBOutlet UILabel *labDate;
// 参与按钮
@property (nonatomic, strong) IBOutlet UIButton *btnDelay;
// 描述文字
@property (nonatomic, strong) IBOutlet UILabel *labDes;

// 设置信息数据
- (void)setPostDict:(NSDictionary *)postDict;
// 设置参与倒计时
- (void)setCounDownDate:(NSDictionary *)postDict;
// 设置选标截止时间
- (void)setSelectDate:(NSDictionary *)postDict;

// 获取行高
+ (NSInteger)getRowHeight;

// 延长按钮block
@property (nonatomic, copy) void (^delayClickBlock)(void);

@end
