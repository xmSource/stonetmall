//
//  ZbUserQiYeHeader.h
//  StoneTmall
//
//  Created by chyo on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZbUserHeaderDelegate.h"

@interface ZbUserQiYeHeader : UIView

@property (nonatomic, strong) IBOutlet UIButton *btnVip;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *userProvince;
@property (strong, nonatomic) IBOutlet UIImageView *userImgFlag;
@property (strong, nonatomic) IBOutlet UIImageView *userImgFlag2;
@property (strong, nonatomic) IBOutlet UIImageView *userImgFlag3;
@property (strong, nonatomic) IBOutlet UIImageView *qiyeImage;
@property (strong, nonatomic) IBOutlet UILabel *qiyeName;
@property (strong, nonatomic) IBOutlet UILabel *qiyeLevel;
@property (strong, nonatomic) IBOutlet UIImageView *qiyeImgFlag1;
@property (strong, nonatomic) IBOutlet UILabel *qiyeImgFlag2;
@property (strong, nonatomic) IBOutlet UILabel *qiyeAdr;

// 企业信息容器高度
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcCompanyViewHeight;

@property (nonatomic, weak) id<ZbUserHeaderDelegate> delegate;

@end
