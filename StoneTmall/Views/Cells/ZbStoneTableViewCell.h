//
//  ZbStoneTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/24.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbStoneTableViewCell : UITableViewCell

// 左石材数据索引
@property (nonatomic, assign) NSInteger leftIndex;
// 左市场数据
@property (nonatomic, weak) NSDictionary *leftStoneDict;
// 右石材数据索引
@property (nonatomic, assign) NSInteger rightIndex;
// 右市场数据
@property (nonatomic, weak) NSDictionary *rightStoneDict;

// 石材点击block
@property (nonatomic, copy) void (^stoneClickBlock)(NSInteger stoneIndex, NSDictionary *stoneDict);

// 设置石材数据
- (void)setStoneRow:(NSInteger)row dataArray:(NSArray *)array;

// 根据数据计算行数
+ (NSInteger)getRowCount:(NSArray *)dataArray;
// 获取行高
+ (NSInteger)getRowHeight;

@end
