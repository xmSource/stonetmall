//
//  ZbNoticeTableViewCell.m
//  StoneTmall
//
//  Created by chyo on 15/10/28.
//  Copyright © 2015年 stonetmall. All rights reserved.
//

#import "ZbNoticeTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbNoticeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 设置数据
- (void)setNoticeDict:(NSDictionary *)noticeDict {
    // 图片 zb test
    self.imgLeft.image = [UIImage imageNamed:@"icon_noting"];
    // 文字
    NSString *content = noticeDict[@"message_body"];
    self.contentLab.text = content;
    // 时间
    self.dateLab.text = [CommonCode getTimeShowText:noticeDict[@"message_inserted"]];
}
// 设置数据为计算高度
- (void)setNoticeDictForCalc:(NSDictionary *)noticeDict {
    // 文字
    NSString *content = noticeDict[@"message_body"];
    self.contentLab.text = content;
}

@end
