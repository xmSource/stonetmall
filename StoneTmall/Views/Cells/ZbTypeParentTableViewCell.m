//
//  ZbTypeParentTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/8.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbTypeParentTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbTypeParentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 设置选中状态
- (void)setIsCurSelect:(BOOL)isSelect {
    //当选中
    if (isSelect) {
        self.bg.backgroundColor = UIColorFromHexString(@"FFFFFF");
    }else{
        self.bg.backgroundColor = UIColorFromHexString(@"F8F8F8");
    }
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 55;
}

@end
