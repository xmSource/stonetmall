//
//  ZbStoneLinkTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/9.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbStoneLinkTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbStoneLinkTableViewCell

- (void)awakeFromNib {
    
    // 图片边框
    self.ivIcon.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
    self.ivIcon.layer.borderWidth = 1;
    self.ivIcon.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict {
    // 图片
    NSString *image = infoDict[@"stone_image"];
    [self.ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:image]]];
    // 名称
    self.labName.text = infoDict[@"stone_name"];
    // 描述
    NSMutableArray *desArray = [NSMutableArray array];
    NSString *type = infoDict[@"stone_type"];
    if (type.length > 0) {
        [desArray addObject:type];
    }
    NSString *color = infoDict[@"stone_color"];
    if (color.length > 0) {
        [desArray addObject:color];
    }
    NSString *texture = infoDict[@"stone_texture"];
    if (![texture isKindOfClass:[NSNull class]] && texture.length > 0) {
        [desArray addObject:texture];
    }
    self.labUsage.text = [desArray componentsJoinedByString:@" "];
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 70;
}

@end
