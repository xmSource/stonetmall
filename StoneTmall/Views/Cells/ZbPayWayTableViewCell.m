//
//  ZbPayWayTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/13.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbPayWayTableViewCell.h"

@implementation ZbPayWayTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - 点击事件
// 支付方式选择
- (IBAction)btnPayWayOnClick:(id)sender {
    if (self.payWayClickBlock) {
        self.payWayClickBlock(self.curPayWay);
    }
    return;
}

@end
