//
//  ZbQySingleInfoTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/5.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQySingleInfoTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbQySingleInfoTableViewCell

- (void)awakeFromNib {
    self.labText.preferredMaxLayoutWidth = DeviceWidth - 50 - 30;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 获取行高
- (CGFloat)calcCellHeight:(NSString *)content {
    self.labText.text = content;
    
    CGSize size = [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

@end
