//
//  ZbNoRecordTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbNoRecordTableViewCell.h"

@implementation ZbNoRecordTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 203;
}

@end
