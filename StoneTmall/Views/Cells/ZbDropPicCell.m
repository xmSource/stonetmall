//
//  ZbDropPicCell.m
//  StoneTmall
//
//  Created by 张斌 on 16/2/25.
//  Copyright © 2016年 stonetmall. All rights reserved.
//

#import "ZbDropPicCell.h"
#import "CommonHeader.h"

@implementation ZbDropPicCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

// 设置数据
- (void)setRow:(NSInteger)row dataArray:(NSArray *)array {
    
    NSArray *dataArray = array;
    for (NSInteger i = 0; i < self.btnArray.count; i++) {
        NSInteger index = row * 4 + i;
        UIControl *btn = self.btnArray[i];
        btn.hidden = index >= dataArray.count;
        if (index < dataArray.count) {
            UIImageView *ivIcon = [btn viewWithTag:11];
            UILabel *labName = [btn viewWithTag:12];
            NSString *class_name = dataArray[index][@"class_name"];
            
            if ([class_name isEqualToString:@"全部"]) {
                ivIcon.image = [UIImage imageNamed:class_name];
            } else {
                [ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:dataArray[index][@"class_logo"]]]];
            }
            labName.text = class_name;
        }
    }
    self.dataArray = dataArray;
    self.curRow = row;
    return;
}

#pragma mark - 点击事件
// 按钮点击
- (IBAction)btnOnClick:(UIControl *)sender {
    NSInteger index = self.curRow * 4 + sender.tag;
    NSDictionary *curDict = self.dataArray[index];
    if (self.dropPicClickBlock) {
        self.dropPicClickBlock(index, curDict);
    }
    return;
}

#pragma mark - 类方法
// 根据数据计算行数
+ (NSInteger)getRowCount:(NSInteger)totalCount {
    // 每行4个
    NSInteger row = ceilf(totalCount / 4.0);
    return row;
}

// 获取行高
+ (NSInteger)getRowHeight {
    return 87;
}

@end
