//
//  ZbQyListTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/5.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbQyListTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbQyListTableViewCell

- (void)awakeFromNib {
    // 图片边框
    self.ivLeft.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
    self.ivLeft.layer.borderWidth = 1;
    self.ivLeft.layer.masksToBounds = YES;
    [self.ivLeft addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftIconOnClick)]];
    self.ivMid.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
    self.ivMid.layer.borderWidth = 1;
    self.ivMid.clipsToBounds = YES;
    [self.ivMid addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(midIconOnClick)]];
    self.ivRight.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
    self.ivRight.layer.borderWidth = 1;
    self.ivRight.clipsToBounds = YES;
    [self.ivRight addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightIconOnClick)]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置数据
- (void)setInfoRow:(NSInteger)row dataArray:(NSArray *)array {
    self.leftDict = nil;
    self.midDict = nil;
    self.rightDict = nil;
    NSArray *dataArray = array;
    
    // 左数据
    NSInteger left = row * 3;
    self.ivLeft.hidden = !(left < dataArray.count);
    if (left < dataArray.count) {
        NSDictionary *companyDict = dataArray[left];
        NSString *leftImgUrl = [ZbWebService getImageFullUrl:companyDict[@"ad_image"]];
        [self.ivLeft sd_setImageWithURL:[NSURL URLWithString:leftImgUrl]];
        self.leftDict = companyDict;
    }
    // 中数据
    NSInteger mid = row * 3 + 1;
    self.ivMid.hidden = !(mid < dataArray.count);
    if (mid < dataArray.count) {
        NSDictionary *companyDict = dataArray[mid];
        NSString *midImgUrl = [ZbWebService getImageFullUrl:companyDict[@"ad_image"]];
        [self.ivMid sd_setImageWithURL:[NSURL URLWithString:midImgUrl]];
        self.midDict = companyDict;
    }
    // 右数据
    NSInteger right = row * 3 + 2;
    self.ivRight.hidden = !(right < dataArray.count);
    if (right < dataArray.count) {
        NSDictionary *companyDict = dataArray[right];
        NSString *rightImgUrl = [ZbWebService getImageFullUrl:companyDict[@"ad_image"]];
        [self.ivRight sd_setImageWithURL:[NSURL URLWithString:rightImgUrl]];
        self.rightDict = companyDict;
    }
    
    // 保持当前数据信息
    self.leftIndex = left;
    self.midIndex = mid;
    self.rightIndex = right;
    return;
}

#pragma mark - 点击事件
// 左icon点击
- (void)leftIconOnClick {
    if (self.iconClickBlock) {
        self.iconClickBlock(self.leftIndex, self.leftDict);
    }
    return;
}

// 中icon点击
- (void)midIconOnClick {
    if (self.iconClickBlock) {
        self.iconClickBlock(self.midIndex, self.midDict);
    }
    return;
}

// 右icon点击
- (void)rightIconOnClick {
    if (self.iconClickBlock) {
        self.iconClickBlock(self.rightIndex, self.rightDict);
    }
    return;
}

#pragma mark - 类方法
// 根据数据计算行数
+ (NSInteger)getRowCount:(NSArray *)dataArray {
    // 每行3个
    NSInteger stonetRow = ceilf(dataArray.count / 3.0);
    return stonetRow;
}

// 获取行高
+ (NSInteger)getRowHeight {
    // 图片宽度
    CGFloat imageWidth = (DeviceWidth - 4*4) / 3.0;
    // 宽高比1.4:1
    CGFloat imageHeight = imageWidth / 1.4;
    return imageHeight + 4;
}

@end
