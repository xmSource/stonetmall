//
//  ZbIndexCharTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbIndexCharTableViewCell : UITableViewCell

// 标题
@property (nonatomic, strong) IBOutlet UILabel *labChar;

// 获取行高
+ (NSInteger)getRowHeight;

@end
