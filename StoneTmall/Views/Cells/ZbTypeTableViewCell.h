//
//  ZbTypeTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbTypeTableViewCell : UITableViewCell

// 类型名
@property (nonatomic, strong) IBOutlet UILabel *labTypeName;
// 勾选按钮
@property (nonatomic, strong) IBOutlet UIButton *btnSelect;
// 点击按钮
@property (nonatomic, strong) IBOutlet UIButton *btnClick;
// 箭头图片
@property (nonatomic, strong) IBOutlet UIImageView *ivArrow;

// 选择按钮block
@property (nonatomic, copy) void (^selectClickBlock)(BOOL selected);
// 点击按钮block
@property (nonatomic, copy) void (^clickBlock)();

// 获取行高
+ (NSInteger)getRowHeight;

@end
