//
//  ZbUserUnLoginHeader.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZbUserHeaderDelegate.h"

@interface ZbUserUnLoginHeader : UIView

@property (nonatomic, strong) IBOutlet UIButton *btnLogin;

@property (nonatomic, weak) id<ZbUserHeaderDelegate> delegate;

@end
