//
//  ZbUserQiYeHeader.m
//  StoneTmall
//
//  Created by chyo on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbUserQiYeHeader.h"
#import "ZbUserInfoEtViewController.h"
#import "ZbQiYeInfoEtViewController.h"
#import "ZbPubProductViewController.h"
#import "ZbPubSupViewController.h"
#import "ZbBeVipViewController.h"

@implementation ZbUserQiYeHeader

- (void)awakeFromNib {
    self.userImage.layer.cornerRadius = 22.5f;
    self.userImage.layer.masksToBounds = YES;
    self.btnVip.layer.cornerRadius = 2.0f;
    self.btnVip.layer.masksToBounds = YES;
    self.qiyeImgFlag2.layer.cornerRadius = 2.0f;
    self.qiyeImgFlag2.layer.masksToBounds = YES;
}

// 编辑资料
- (IBAction)edInfoClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbUserInfoEtViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbUserInfoEtViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 完善资料
- (IBAction)completeInfoClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbQiYeInfoEtViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbQiYeInfoEtViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 成为vip
-(IBAction)beVipClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbBeVipViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbBeVipViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 发布供需
- (IBAction)pubInfoClcik:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbPubSupViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbPubSupViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 发布产品
- (IBAction)pubProductClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbPubProductViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbPubProductViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 个人资料区域点击
- (IBAction)controlUserClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbUserInfoEtViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbUserInfoEtViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

// 企业资料区域点击
- (IBAction)controlQyClick:(id)sender {
    UIStoryboard *mineSb = [UIStoryboard storyboardWithName:@"Mine" bundle:nil];
    ZbQiYeInfoEtViewController *controller = [mineSb instantiateViewControllerWithIdentifier:@"ZbQiYeInfoEtViewController"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pushToController:)]) {
        [self.delegate pushToController:controller];
    }
}

@end
