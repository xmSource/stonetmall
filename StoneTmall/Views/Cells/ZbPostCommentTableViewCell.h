//
//  ZbPostCommentTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbPostCommentTableViewCell : UITableViewCell

// 头像
@property (nonatomic, strong) IBOutlet UIImageView *ivImage;
// 名字
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 电话图片
@property (nonatomic, strong) IBOutlet UIImageView *ivTel;
// 微信图片
@property (nonatomic, strong) IBOutlet UIImageView *ivWechat;
// 身份证图片
@property (nonatomic, strong) IBOutlet UIImageView *ivPassport;
// 内容
@property (nonatomic, strong) IBOutlet UILabel *labComment;
// 图片数组
@property (strong, nonatomic) IBOutlet UIImageView *ivReply;
// 图片height约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcImageHeight;
// 图片top约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcImageTop;
// 时间
@property (nonatomic, strong) IBOutlet UILabel *labDate;
// 采用按钮
@property (nonatomic, strong) IBOutlet UIButton *btnUse;
// 中标图片
@property (nonatomic, strong) IBOutlet UIImageView *ivWin;

// 设置数据
- (void)setCommentDict:(NSDictionary *)commentDict;
// 设置数据为计算高度
- (void)setCommentDictForCalc:(NSDictionary *)commentDict;

// 回复按钮block
@property (nonatomic, copy) void (^replyClickBlock)(NSDictionary *commentDict);
// 采用按钮block
@property (nonatomic, copy) void (^useClickBlock)(NSDictionary *commentDict);

@end
