//
//  ZbSampleTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/11.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbSampleTableViewCell : UITableViewCell

// 规格
@property (nonatomic, strong) IBOutlet UILabel *labSize;
// 库存
@property (nonatomic, strong) IBOutlet UILabel *labStockCount;
// 价格
@property (nonatomic, strong) IBOutlet UILabel *labPrice;
// 数量容器
@property (nonatomic, strong) IBOutlet UIView *viewCount;
// 数量
@property (nonatomic, strong) IBOutlet UILabel *labCount;

// 增加block
@property (nonatomic, copy) void (^addBlock)(void);
// 减少block
@property (nonatomic, copy) void (^cutBlock)(void);

@end
