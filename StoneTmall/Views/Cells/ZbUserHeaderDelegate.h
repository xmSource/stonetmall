//
//  ZbUserHeaderDelegate.h
//  StoneTmall
//
//  Created by Apple on 15/8/28.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//


@protocol ZbUserHeaderDelegate <NSObject>

- (void)presentToController:(UINavigationController *)controller;
- (void)pushToController:(UIViewController *)controller;

@end
