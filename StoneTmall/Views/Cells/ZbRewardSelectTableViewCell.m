//
//  ZbRewardSelectTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/23.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardSelectTableViewCell.h"

@implementation ZbRewardSelectTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置信息数据
- (void)setPostDict:(NSDictionary *)postDict {
    return;
}

// 设置选标截止时间
- (void)setSelectDate:(NSDictionary *)postDict isOwner:(BOOL)isOwner {
    // 过期时间
    NSString *post_expired = postDict[@"post_expired"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *expiredDate = [dateFormatter dateFromString:post_expired];
    
    NSString *first = isOwner ? @"请在" : @"悬赏者将在";
    [dateFormatter setDateFormat:@"M"];
    NSString *month = [dateFormatter stringFromDate:expiredDate];
    NSString *monthText = @"月";
    [dateFormatter setDateFormat:@"d"];
    NSString *day = [dateFormatter stringFromDate:expiredDate];
    NSString *dayText = @"日";
    [dateFormatter setDateFormat:@"H"];
    NSString *hour = [dateFormatter stringFromDate:expiredDate];
    NSString *hourText = @"点";
    [dateFormatter setDateFormat:@"m"];
    NSString *minute = [dateFormatter stringFromDate:expiredDate];
    NSString *minuteText = @"分";
    NSString *last = @"之前选标";
    // 设置文字
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@", first, month, monthText, day, dayText, hour, hourText, minute, minuteText, last]];
    // 月属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length, month.length)];
    // 日属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length + month.length + monthText.length, day.length)];
    // 时属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length + month.length + monthText.length + day.length + dayText.length, hour.length)];
    // 分属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length + month.length + monthText.length + day.length + dayText.length + hour.length + hourText.length, minute.length)];
    self.labDate.attributedText = attriString;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 46;
}

@end
