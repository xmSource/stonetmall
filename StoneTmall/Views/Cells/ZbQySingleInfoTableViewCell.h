//
//  ZbQySingleInfoTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/5.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbQySingleInfoTableViewCell : UITableViewCell

// 图标
@property (nonatomic, strong) IBOutlet UIImageView *ivIcon;
// 文字
@property (nonatomic, strong) IBOutlet UILabel *labText;
// 箭头
@property (nonatomic, strong) IBOutlet UIImageView *ivArrow;
// 底线leading约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcBottomLineLeading;

// 获取行高
- (CGFloat)calcCellHeight:(NSString *)content;

@end
