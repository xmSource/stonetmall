//
//  ZbRewardJoinTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/23.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbRewardJoinTableViewCell : UITableViewCell

// 结束时间label
@property (nonatomic, strong) IBOutlet UILabel *labDate;
// 参与按钮
@property (nonatomic, strong) IBOutlet UIButton *btnJoin;

// 设置信息数据
- (void)setPostDict:(NSDictionary *)postDict;
// 设置参与倒计时
- (void)setCounDownDate:(NSDictionary *)postDict;

// 获取行高
+ (NSInteger)getRowHeight;

// 参与按钮block
@property (nonatomic, copy) void (^joinClickBlock)(void);

@end
