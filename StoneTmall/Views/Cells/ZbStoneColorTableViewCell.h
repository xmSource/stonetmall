//
//  ZbStoneColorTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/30.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbStoneColorTableViewCell : UITableViewCell

// 色块
@property (nonatomic, strong) IBOutlet UIView *colorView;
// 颜色名
@property (nonatomic, strong) IBOutlet UILabel *labColorName;

// 获取行高
+ (NSInteger)getRowHeight;

@end
