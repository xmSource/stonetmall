//
//  ZbTypeParentTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/8.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbTypeParentTableViewCell : UITableViewCell

// 背景
@property (nonatomic, strong) IBOutlet UIView *bg;
// 类别icon
@property (nonatomic, strong) IBOutlet UIImageView *ivIcon;
// 类名
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 描述
@property (nonatomic, strong) IBOutlet UILabel *labSubName;

// 获取行高
+ (NSInteger)getRowHeight;
// 设置选中状态
- (void)setIsCurSelect:(BOOL)isSelect;

@end
