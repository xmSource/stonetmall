//
//  ZbSeeMoreTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbSeeMoreTableViewCell : UITableViewCell

// 查看更多的说明
@property (nonatomic, strong) IBOutlet UILabel *labDes;

// 点击block
@property (nonatomic, copy) void (^clickBlock)(void);

// 获取行高，isNeedBottomArea是否需要底部灰色区域
+ (NSInteger)getRowHeight:(BOOL)isNeedBottomArea;

@end
