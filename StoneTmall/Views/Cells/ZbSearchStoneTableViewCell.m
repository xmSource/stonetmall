//
//  ZbSearchStoneTableViewCell.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/25.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSearchStoneTableViewCell.h"
#import "CommonHeader.h"

@implementation ZbSearchStoneTableViewCell

- (void)awakeFromNib {
    
    // 图片边框
    self.ivIcon.layer.borderColor = UIColorFromHexString(@"#E5E5E5").CGColor;
    self.ivIcon.layer.borderWidth = 1;
    self.ivIcon.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

// 设置数据
- (void)setInfoDict:(NSDictionary *)infoDict {
    // 图片
    NSString *image = infoDict[@"stone_image"];
    [self.ivIcon sd_setImageWithURL:[NSURL URLWithString:[ZbWebService getImageFullUrl:image]]];
    // 名称
    self.labName.text = infoDict[@"stone_name"];
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 70;
}

@end
