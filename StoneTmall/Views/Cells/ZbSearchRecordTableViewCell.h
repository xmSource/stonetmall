//
//  ZbSearchRecordTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/27.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbSearchRecordTableViewCell : UITableViewCell

// 标题
@property (nonatomic, strong) IBOutlet UILabel *labRecordName;

// 获取行高
+ (NSInteger)getRowHeight;

@end
