//
//  ZbPayWayTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/13.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonHeader.h"

@interface ZbPayWayTableViewCell : UITableViewCell

// 支付方式图标
@property (nonatomic, strong) IBOutlet UIImageView *ivPayWay;
// 支付方式文字
@property (nonatomic, strong) IBOutlet UILabel *labPayWay;
// 支付方式按钮
@property (nonatomic, strong) IBOutlet UIButton *btnPayWay;
// 当前支付方式
@property (nonatomic, assign) Zb_Pay_Way_Type curPayWay;

// 勾选按钮点击block
@property (nonatomic, copy) void (^payWayClickBlock)(Zb_Pay_Way_Type curPayWay);

@end
