//
//  ZbRewardSelectTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/23.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbRewardSelectTableViewCell : UITableViewCell

// 结束时间label
@property (nonatomic, strong) IBOutlet UILabel *labDate;

// 设置信息数据
- (void)setPostDict:(NSDictionary *)postDict;

// 获取行高
+ (NSInteger)getRowHeight;
// 设置选标截止时间
- (void)setSelectDate:(NSDictionary *)postDict isOwner:(BOOL)isOwner;

@end
