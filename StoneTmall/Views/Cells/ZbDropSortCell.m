//
//  ZbDropSortCell.m
//  StoneTmall
//
//  Created by 张斌 on 16/2/25.
//  Copyright © 2016年 stonetmall. All rights reserved.
//

#import "ZbDropSortCell.h"

@implementation ZbDropSortCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 设置数据
- (void)drawDataArray:(NSArray *)array {
    
    NSArray *dataArray = array;
    for (NSInteger i = 0; i < self.btnArray.count; i++) {
        NSInteger index = i;
        UIControl *btn = self.btnArray[i];
        
        UIImageView *ivIcon = [btn viewWithTag:11];
        UILabel *labName = [btn viewWithTag:12];
        NSString *class_name = dataArray[index][@"class_name"];
        
        ivIcon.image = [UIImage imageNamed:class_name];
        labName.text = class_name;
    }
    self.dataArray = dataArray;
    return;
}

#pragma mark - 点击事件
// 按钮点击
- (IBAction)btnOnClick:(UIControl *)sender {
    NSInteger index = sender.tag;
    NSDictionary *curDict = self.dataArray[index];
    if (self.dropPicClickBlock) {
        self.dropPicClickBlock(index, curDict);
    }
    return;
}

#pragma mark - 类方法
// 获取行高
+ (NSInteger)getRowHeight {
    return 100;
}

@end
