//
//  ZbAddrChooseTableViewCell.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/13.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbAddrChooseTableViewCell : UITableViewCell

// 选择地址按钮点击block
@property (nonatomic, copy) void (^addrChooseClickBlock)(void);

@end
