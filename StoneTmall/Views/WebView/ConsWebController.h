//
//  ConsWebController.h
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/23.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ConsWebController : BaseViewController

@property (nonatomic, strong) NSString * mainUrl;
@property (nonatomic, strong) NSString * showTitle;

@end
