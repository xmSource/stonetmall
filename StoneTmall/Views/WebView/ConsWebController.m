//
//  ConsWebController.m
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/23.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "ConsWebController.h"

@interface ConsWebController () <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView * webView;

- (void)back;

@end

@implementation ConsWebController

- (void)viewWillAppearOnce
{
    [super viewWillAppearOnce];
    
    if (self.showTitle) {
        self.title = self.showTitle;
    }
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_btn_arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarItemOnClick:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.delegate = self;
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.scalesPageToFit = YES;
    self.webView.dataDetectorTypes = UIDataDetectorTypeNone;
    self.webView.mediaPlaybackRequiresUserAction = YES;
    self.webView.allowsInlineMediaPlayback = YES;
    
    for (UIView * subView in self.webView.subviews) {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            ((UIScrollView *)subView).bounces = NO;
        }
    }
    [self.view addSubview:self.webView];
    self.hud.labelText = @"加载中..";
    [self.hud show:NO];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.mainUrl]]];
}

- (void)back
{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

#pragma mark -
#pragma mark UIWebView Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    self.hud.labelText = @"加载失败";
    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    DLog(@"%@", error);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (!self.showTitle) {
        self.title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    }
    [self.hud hide:NO];
//    if ([self.webView canGoBack]) {
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"后退" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
//    } else {
//        self.navigationItem.rightBarButtonItem = nil;
//    }
}

@end
