//
//  RoundRectView.h
//  AgreementRide
//
//  Created by qian on 15/5/6.
//  Copyright (c) 2015年 黄旺鑫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZbIndexButton.h"

IB_DESIGNABLE
@interface RoundRectView : UIView
///边框颜色
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

///边框宽度
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;

///设置圆角
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;

@end

IB_DESIGNABLE
@interface RoundRectButton : ZbIndexButton

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) id userInfo;

///边框颜色
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

///边框宽度
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;

///设置圆角
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;

@end

IB_DESIGNABLE
@interface RoundRectControl : UIControl
///边框颜色
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

///边框宽度
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;

///设置圆角
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;

@end

IB_DESIGNABLE
@interface RoundRectImageView : UIImageView
///边框颜色
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

///边框宽度
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;

///设置圆角
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;

@end

IB_DESIGNABLE
@interface RoundRectTextView : UITextView
///边框颜色
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

///边框宽度
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;

///设置圆角
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;

@end

IB_DESIGNABLE
@interface RoundRectTextField : UITextField
///边框颜色
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

///边框宽度
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;

///设置圆角
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;

@end

IB_DESIGNABLE
@interface RoundRectLabel : UILabel
///边框颜色
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

///边框宽度
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;

///设置圆角
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;

@end
