//
//  RoundRectView.m
//  AgreementRide
//
//  Created by qian on 15/5/6.
//  Copyright (c) 2015年 黄旺鑫. All rights reserved.
//

#import "RoundRectView.h"

@implementation RoundRectView

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    //设置边框颜色
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    //设置边框宽度
    self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    //设置圆角
    self.layer.cornerRadius = cornerRadius;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

@implementation RoundRectButton

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    //设置边框颜色
    self.layer.borderColor = borderColor.CGColor;
}


- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    //设置边框宽度
    self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    //设置圆角
    self.layer.cornerRadius = cornerRadius;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

@implementation RoundRectControl


- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    //设置边框颜色
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    //设置边框宽度
    self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    //设置圆角
    self.layer.cornerRadius = cornerRadius;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

@implementation RoundRectImageView


- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    //设置边框颜色
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    //设置边框宽度
    self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    //设置圆角
    self.layer.cornerRadius = cornerRadius;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

@implementation RoundRectTextView


- (void)setBorderColor:(UIColor *)borderColor {
    //设置边框颜色
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    //设置边框宽度
    self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    //设置圆角
    self.layer.cornerRadius = cornerRadius;
}

@end

@implementation RoundRectTextField


- (void)setBorderColor:(UIColor *)borderColor {
    //设置边框颜色
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    //设置边框宽度
    self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    //设置圆角
    self.layer.cornerRadius = cornerRadius;
}

@end

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@implementation RoundRectLabel


- (void)setBorderColor:(UIColor *)borderColor {
    //设置边框颜色
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    //设置边框宽度
    self.layer.borderWidth = borderWidth;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    //设置圆角
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = YES;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
