//
//  ZbIndexButton.h
//  ddLogisticsGuest
//
//  Created by 张斌 on 15/5/31.
//  Copyright (c) 2015年 qianxx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbIndexButton : UIButton

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) id userInfo;

@end
