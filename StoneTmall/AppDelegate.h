//
//  AppDelegate.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplyViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, IChatManagerDelegate>
{
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BMKMapManager *mapManager;
@property (nonatomic, assign) EMConnectionState connectionState;

// 单独清空登录信息
- (void)clearLoginInfo;
// 退出登录
- (void)logOut;
// 广告显示结束
- (void)onAdShowOk;

@end

