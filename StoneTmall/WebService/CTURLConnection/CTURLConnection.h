//
//  CMURLConnection.h
//  CM_APP
//
//  Created by zb 13-4-1.
//  Copyright (c) 2013年 Xiamen Icss-Haisheng Information Technology Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@class CTURLConnection;

@protocol CTURLConnectionDelegate <NSObject>

@optional

// 请求结束执行的方法
- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data;
// 请求失败时执行的方法
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error;

@end

@interface CTURLConnection : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, assign) id<CTURLConnectionDelegate> delegate;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * params;
// 超时时间
@property (nonatomic, assign) NSInteger timeOut;
@property (nonatomic) int tag;
@property (nonatomic) id userInfo;

// 标识是否需要输出log信息
+ (void)setNeedsLog:(BOOL)needsLog;

// 使用 GET 方式来请求数据
- (id)initWithGetMethodUrl:(NSString *)url delegate:(id<CTURLConnectionDelegate>)delegate;
// 使用 POST 方式来请求或提交数据
- (id)initWithPostMethodUrl:(NSString *)url body:(NSString *)params delegate:(id<CTURLConnectionDelegate>)delegate;

// 获取公共头信息
+ (NSDictionary *)getCommonHeaderDict;
// 设置AFNet库请求的头信息
+ (void)setAFNetHeaderInfo:(AFHTTPRequestOperationManager *)afManager;

- (void)start;
- (void)cancel;

@end
