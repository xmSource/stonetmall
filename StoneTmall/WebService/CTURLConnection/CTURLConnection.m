//
//  CTURLConnection.m
//  CM_APP
//
//  Created by 陈 宏超 on 13-4-1.
//  Copyright (c) 2013年 Xiamen Icss-Haisheng Information Technology Co., Ltd. All rights reserved.
//

#import "CTURLConnection.h"
#import <UIKit/UIKit.h>

#define CTURL_TIME_OUT_TERMINAL 25

Class object_getClass(id object);

static BOOL _needsLog = false;

@interface CTURLConnection()

@property (nonatomic, strong) NSMutableData * data;
@property (nonatomic, strong) NSURLConnection * conn;
@property (nonatomic, weak) Class originalClass;

@end
@implementation CTURLConnection

+ (void)setNeedsLog:(BOOL)needsLog
{
    _needsLog = needsLog;
}

- (void)dealloc
{
    self.delegate = nil;
    [self.conn cancel];
    self.conn = nil;
}

- (id)initWithGetMethodUrl:(NSString *)url delegate:(id<CTURLConnectionDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.url = url;
        self.delegate = delegate;
        _originalClass = object_getClass(delegate);
    }
    return self;
}

- (id)initWithPostMethodUrl:(NSString *)url body:(NSString *)params delegate:(id<CTURLConnectionDelegate>)delegate
{
    if (_needsLog) {
        NSLog(@"%@", params);
    }
    self = [super init];
    if (self) {
        self.url = url;
        self.params = params;
        self.delegate = delegate;
        _originalClass = object_getClass(delegate);
    }
    return self;
}

- (void)start
{
    if (_needsLog) {
        NSLog(@"%@", self.url);
    }
    if (self.conn == nil && self.params == nil) {
        NSURL * mUrl = [[NSURL alloc] initWithString:self.url];
        NSInteger timeOut = self.timeOut > 0 ? self.timeOut : CTURL_TIME_OUT_TERMINAL;
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:mUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:timeOut];
        NSURLConnection * conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        // header
        [self setCommonHeader:request];
        self.conn = conn;
    } else if (self.conn == nil) {
        NSURL * mUrl = [[NSURL alloc] initWithString:self.url];
        NSInteger timeOut = self.timeOut > 0 ? self.timeOut : CTURL_TIME_OUT_TERMINAL;
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:mUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:timeOut];
        [request setHTTPMethod:@"POST"];
        // header
        [self setCommonHeader:request];
        // body
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        NSData *postData = [self.params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:[self.params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
        NSURLConnection * conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        self.conn = conn;
    }
    
    if (self.data == nil) {
        NSMutableData * data = [[NSMutableData alloc] init];
        self.data = data;
    }
    [self.conn start];
}

// 获取公共头信息
+ (NSDictionary *)getCommonHeaderDict {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:@"fb21f850e00296289c94bf240faed97c" forKey:@"SG-Api-Token"];
    [dict setValue:@"1.0" forKey:@"SG-Api-Version"];
    // info.plist信息
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
    NSString *agent = [NSString stringWithFormat:@"%@/%@(%@/%@;%@)", [infoDict objectForKey:@"CFBundleIdentifier"], [infoDict objectForKey:@"CFBundleShortVersionString"], [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion], identifierForVendor];
    [dict setValue:agent forKey:@"X-User-Agent"];
    return dict;
}

// 设置AFNet库请求的头信息
+ (void)setAFNetHeaderInfo:(AFHTTPRequestOperationManager *)afManager {
    NSDictionary *headerDict = [CTURLConnection getCommonHeaderDict];
    for (NSString *key in headerDict) {
        NSString *value = [headerDict objectForKey:key];
        [afManager.requestSerializer setValue:value forHTTPHeaderField:key];
    }
}

// 设置公共头信息
- (void)setCommonHeader:(NSMutableURLRequest *)request {
    NSDictionary *dict = [CTURLConnection getCommonHeaderDict];
    for (NSString *key in dict) {
        NSString *value = [dict objectForKey:key];
        [request setValue:value forHTTPHeaderField:key];
    }
//    [request setValue:@"aed6f98f2e75ad8dd7b427fd42b604fa" forHTTPHeaderField:@"SG-Api-Token"];
//    [request setValue:@"1.0" forHTTPHeaderField:@"SG-Api-Version"];
//    // info.plist信息
//    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
//    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
//    NSString *agent = [NSString stringWithFormat:@"%@/%@(%@/%@;%@)", [infoDict objectForKey:@"CFBundleIdentifier"], [infoDict objectForKey:@"CFBundleShortVersionString"], [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion], identifierForVendor];
//    [request setValue:agent forHTTPHeaderField:@"X-User-Agent"];
}

- (void)cancel
{
    self.delegate = nil;
    [self.conn cancel];
    self.conn = nil;
}

- (void)setConn:(NSURLConnection *)conn
{
    if (_conn != nil) {
        [self setDelegate:nil];
    }
    if (conn == nil) {
        [self setDelegate:nil];
    }
    _conn = conn;
}

#pragma mark -
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (_needsLog) {
        NSLog(@"%@", [error description]);
    }
    Class currentClass = object_getClass(_delegate);
    if (currentClass == _originalClass) {
        if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFailWithError:)]) {
            [_delegate connection:self didFailWithError:error];
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
        if (_needsLog) {
            NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
        }
        Class currentClass = object_getClass(_delegate);
        if (currentClass == _originalClass) {
            if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFailWithError:)]) {
                [_delegate connection:self didFailWithError:nil];
            }
        }
        
        [self.conn cancel];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (_needsLog) {
        NSString *result = [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding];
        NSLog(@"result=[%@] len=%lu", result, (unsigned long)result.length);
    }
    Class currentClass = object_getClass(_delegate);
    if (currentClass == _originalClass) {
        if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFinishLoading:)]) {
            [_delegate connection:self didFinishLoading:self.data];
        }
    }
     
}

- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection {
    return NO;
}

//下面两段是重点，要服务器端单项HTTPS 验证，iOS 客户端忽略证书验证。
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
//    NSLog(@"didReceiveAuthenticationChallenge %@ %zd", [[challenge protectionSpace] authenticationMethod], (ssize_t) [challenge previousFailureCount]);
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        [[challenge sender]  useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        [[challenge sender]  continueWithoutCredentialForAuthenticationChallenge: challenge];
    }
    
}

@end
