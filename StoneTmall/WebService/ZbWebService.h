//
//  ZbWebService.h
//  ddLogisticsGuest
//
//  Created by 张斌 on 15/6/8.
//  Copyright (c) 2015年 qianxx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZbQyInfo.h"

// 接口服务器地址
//#define ZB_WEBSERVICE_HOST @"https://api.stonetmall.com/"
#define ZB_WEBSERVICE_HOST @"https://api.test.stonetmall.com/"
// 图片服务器地址
#define ZB_IMAGE_HOST @"http://test.stonetmall.com/"

// 微信url scheme，支付宝拉回本app也用它
#define  WX_URL_SCHEME @"wx8e05a412ab33ffc4"

// 检查更新地址
#define APP_CHECKVERSION_URL @"http://itunes.apple.com/lookup?id=780515139"
#define APP_APPSTORE_URL @"http://itunes.apple.com/us/app/id780515139"

// 软件许可及服务协议地址
#define APP_ITEMS_URL @"http://www.stonetmall.com/data/doc/register-agreement.html"
// 参与悬赏免责协议地址
#define JOIN_REWARD_ITEMS_URL @"http://www.stonetmall.com/data/doc/paiddemand-agreement.html"
// 关于软件URL
#define ABOUT_US_URL @"http://www.stonetmall.com/data/doc/about_us.html"
// 使用帮助URL
#define HOW_TO_USE_URL @"http://www.stonetmall.com/data/doc/how_to_use.html"

// 客服电话
#define APP_CONTACT_PHONE @"400-000-2268"

static NSString * APP_TYPE = @"0";  // 1 安卓 0 ios

// 石材筛选标签默认文字数组
#define STONE_FILETER_DEFAULT_NAME_ARRAY @[@"分类", @"颜色", @"纹理", ]
// 产品筛选标签默认文字数组
#define PRODUCT_FILETER_DEFAULT_NAME_ARRAY @[@"分类", @"区域", @"排序", ]
// 企业筛选标签默认文字数组
#define COMPANY_FILETER_DEFAULT_NAME_ARRAY @[@"分类", @"区域", @"排序", ]
// 默认国家
#define DEFAULT_COUNTRY_NAME @"中国"
// 默认注册企业描述
#define DEFAULT_COMPANY_DESCRIPT @"该企业尚未填写简介，请补充！"

@interface ZbWebService : NSObject

#pragma mark - 当前用户
// 当前用户
+ (void)setUser:(NSMutableDictionary *)info;
+ (NSMutableDictionary *)sharedUser;
+ (void)setQy:(ZbQyInfo *)info;
+ (ZbQyInfo *)sharedQy;
+ (BOOL)isLogin;
+ (BOOL)isVipQy;

#pragma mark - cookie存本地
// 保存cookie
+ (void)saveCookie;
// 恢复cookie
+ (BOOL)recoverCookie;
// 清空cookie
+ (void)clearCookie;

#pragma mark - 公用方法
// 获取图片完整地址
+ (NSString *)getImageFullUrl:(NSString *)image;
// 数据列表转成首字母数组
+ (NSMutableArray *)sortByFirstCharact:(NSArray *)dataArray;
// 首字母数组拼接
+ (void)firstCharactArrayJoin:(NSMutableArray *)curArray addArray:(NSMutableArray *)addArray;
// 二级分类数据解析
+ (NSArray *)treeTypeParser:(NSArray *)typeInfoArray;
// 二级分类数据解析
+ (NSArray *)treeSTypeParser:(NSArray *)typeInfoArray;
// 过滤null
+ (void)filterNullData:(NSMutableDictionary *)dict;

#pragma mark - 登录接口
// 登录参数
+ (NSString *)loginParams:(NSString *)acc password:(NSString *)psw;
// 微信登录参数
+ (NSString *)loginWxParams:(NSString *)wxCode;
// 创建企业
+ (NSString *)newCompany:(NSString *)name description:(NSString *)description contact:(NSString *)contact phone:(NSString *)phone country:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area;
// 申请绑定企业
+ (NSString *)applyForBindCompany:(NSString *)companyId;

#pragma mark - 自动登录接口
// 登录参数
+ (NSString *)autoLogin;

#pragma mark - 注册接口
// 注册参数
+ (NSString *)registerParams:(NSString *)acc password:(NSString *)psw wxCode:(NSString *)wxCode;
// 判断用户名是否存在参数
+ (NSString *)checkUserNameExistParams:(NSString *)name;

#pragma mark - 重置密码接口
// 重置密码参数
+ (NSString *)resetPassword:(NSString *)phone tempId:(NSString *)tempId code:(NSString *)code;

// 退出登录参数
+ (NSString *)logoutParams;

#pragma mark - 首页信息
// 闪屏广告
+ (NSString *)getSplashScreenAdList;
// 首页品牌列表
+ (NSString *)getCompanyShowAdList;
// 首页广告列表
+ (NSString *)getCoverRollAdList;
// 首页行业头条
+ (NSString *)getLatestNewsList;
// 首页供需列表
+ (NSString *)getLatestSupplyDemandList;
// 首页推荐品种列表
+ (NSString *)getNewProductAdList;

#pragma mark - 全局搜索
// 全局企业搜索
+ (NSString *)globalQySearchParams:(NSString *)key pageIndex:(NSString *)index pageSize:(NSString *)size;
// 全局产品搜索
+ (NSString *)globalProductSearchParams:(NSString *)key pageIndex:(NSString *)index pageSize:(NSString *)size;
// 全局石材搜索
+ (NSString *)globalStoneSearchParams:(NSString *)key pageIndex:(NSString *)index pageSize:(NSString *)size;
// 全局供需搜索
+ (NSString *)globalSAndDSearchParams:(NSString *)key pageIndex:(NSString *)index pageSize:(NSString *)size;

#pragma mark - 石材数据
// 石材类别列表
+ (NSString *)getStoneTypeList;
// 石材颜色列表
+ (NSString *)getStoneColorList;
// 石材纹理列表
+ (NSString *)getStoneTextureList;
// 查询石材数据
+ (NSString *)getStoneList:(NSString *)key type:(NSString *)type color:(NSString *)color texture:(NSString *)texture pageIndex:(NSString *)index;

#pragma mark - 石材详情数据
// 获取石材相关企业
+ (NSString *)getCompanyListByStone:(NSString *)stoneName pageIndex:(NSString *)pageIndex lng:(NSString *)lng lat:(NSString *)lat;
// 获取石材推荐企业
+ (NSString *)getRecommendCompanyListByStone:(NSString *)stoneName lng:(NSString *)lng lat:(NSString *)lat;
// 相似石材
+ (NSString *)getSimilarStoneListByStone:(NSString *)stoneName;
// 获取石材详情
+ (NSString *)getStoneInfo:(NSString *)stoneId;
// 获取石材详情by名字
+ (NSString *)getStoneInfoByName:(NSString *)stoneName;
// 获取样品规格
+ (NSString *)getSampleProductSpecList:(NSString *)stoneName;
// 加入供应商
+ (NSString *)joinStoneToCompany:(NSString *)stoneName;
// 收藏石材
+ (NSString *)markStone:(NSString *)stoneId;
// 取消收藏石材
+ (NSString *)unmarkStone:(NSString *)stoneId;
// 赞石材
+ (NSString *)likeStone:(NSString *)stoneId;
// 取消赞石材
+ (NSString *)unlikeStone:(NSString *)stoneId;

#pragma mark - 企业数据
// 企业服务类型列表
+ (NSString *)getCompanyTypeList;
// 省份列表
+ (NSString *)getProvList;
// 企业滚动广告
+ (NSString *)getCompanySearchAdList;
// 查询企业数据
+ (NSString *)getCompanyList:(NSString *)key type:(NSString *)type area:(NSString *)area sortName:(NSString *)sortName sortOrder:(NSString *)sortOrder pageIndex:(NSString *)index;

#pragma mark - 企业详情数据
// 企业主营产品
+ (NSString *)getCompanyProductList:(NSString *)companyId;
// 企业主营石材
+ (NSString *)getCompanyStoneList:(NSString *)companyId;
// 企业详细信息
+ (NSString *)getCompanyInfo:(NSString *)companyId;
// 获取企业更多地址
+ (NSString *)getCompanyAddressListByComId:(NSString *)companyId;
// 获取企业更多联系人
+ (NSString *)getCompanyContactListByComId:(NSString *)companyId;
// 收藏企业
+ (NSString *)markCompany:(NSString *)companyId;
// 取消收藏企业
+ (NSString *)unmarkCompany:(NSString *)companyId;
// 赞企业
+ (NSString *)likeCompany:(NSString *)companyId;
// 取消赞企业
+ (NSString *)unlikeCompany:(NSString *)companyId;

#pragma mark - 产品数据
// 产品分类列表
+ (NSString *)getProductTypeList;
// 产品滚动广告
+ (NSString *)getProductSearchAdList;
// 查询产品数据
+ (NSString *)getProductList:(NSString *)key type:(NSString *)type area:(NSString *)area sortName:(NSString *)sortName sortOrder:(NSString *)sortOrder pageIndex:(NSString *)index;

#pragma mark - 产品详情数据
// 删除产品
+ (NSString *)delProduct:(NSString *)productId;
// 获取产品价格单位
+ (NSString *)getPriceUnitList;
// 修改产品标题
+ (NSDictionary *)editProductTitle:(NSString *)id withTitle:(NSString *)title;
// 修改产品推荐
+ (NSString *)editProductRecommend:(NSString *)id withRecomand:(NSString *)recommend;
// 修改产品标签
+ (NSString *)editProductTag:(NSString *)id withTag:(NSString *)tag;
// 修改产品参考价
+ (NSString *)editProductPrice:(NSString *)id withPrice:(NSString *)price priceUnit:(NSString *)priceUnit;
// 修改产品分类
+ (NSDictionary *)editProductType:(NSString *)id withType:(NSString *)type withSubType:(NSString *)subtype;
// 修改产品描述
+ (NSDictionary *)editProductDescription:(NSString *)id withDesc:(NSString *)description;
// 公司简短信息
+ (NSString *)getCompanyShortInfo:(NSString *)companyId;
// 获取产品图片列表
+ (NSString *)getProductImageList:(NSString *)productId;
// 获取相似产品
+ (NSString *)getSimilarProductListByStone:(NSString *)stoneName pageIndex:(NSString *)pageIndex;
// 获取产品详情
+ (NSString *)getProductInfo:(NSString *)productId;
// 石材简短信息
+ (NSString *)getStoneShortInfo:(NSString *)stoneName;
// 收藏石材
+ (NSString *)markProduct:(NSString *)productId;
// 取消收藏石材
+ (NSString *)unmarkProduct:(NSString *)productId;
// 赞石材
+ (NSString *)likeProduct:(NSString *)productId;
// 取消赞石材
+ (NSString *)unlikeProduct:(NSString *)productId;

#pragma mark - 供需、悬赏列表
// 需求列表广告
+ (NSString *)getDemandSearchAdList;
// 需求搜索
+ (NSString *)getDemandListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex;
// 供应列表广告
+ (NSString *)getSupplySearchAdList;
// 供应搜索
+ (NSString *)getSupplyListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex;
// 头几条悬赏
+ (NSString *)getPaidDemandListTopN;
// 悬赏搜索
+ (NSString *)getPaidDemandListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex;
// 已完成悬赏搜索
+ (NSString *)getFinishPaidDemandListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex;

#pragma mark - 供需详情
// 帖子图片
+ (NSString *)getSupplyDemandImageList:(NSString *)postId;
// 帖子评论列表
+ (NSString *)getCommentList:(NSString *)postId;
// 供需详情
+ (NSString *)getSupplyDemandInfo:(NSString *)postId;
// 收藏帖子
+ (NSString *)markPost:(NSString *)postId;
// 取消收藏帖子
+ (NSString *)unmarkPost:(NSString *)postId;
// 评论帖子
+ (NSString *)addComment:(NSString *)postId content:(NSString *)content;
// 新闻详情
+ (NSString *)getNewsInfo:(NSString *)postId;

#pragma mark - 悬赏详情
// 获取悬赏的回复列表
+ (NSString *)getPaidDemandCommentList:(NSString *)postId;
// 获取悬赏详情
+ (NSString *)getPaidDemandInfo:(NSString *)postId;
// 获取中标回答
+ (NSString *)getWinBidComment:(NSString *)postId;
// 投标回答
+ (NSDictionary *)addPaidDemandComment:(NSString *)postId content:(NSString *)content;
// 增加悬赏金额，延长悬赏时间
+ (NSString *)applyAddDaysForPaidDemand:(NSString *)postId addDays:(NSString *)addDays addMoney:(NSString *)addMoney;
// 采用回答
+ (NSString *)usePaidDemandComment:(NSString *)postId commentId:(NSString *)commentId;

#pragma mark - 石材、企业、产品分享地址
// 石材分享url
+ (NSString *)shareStoneUrl:(NSString *)stoneId;
// 产品分享url
+ (NSString *)shareProductUrl:(NSString *)productId;
// 企业分享url
+ (NSString *)shareCompanyUrl:(NSString *)companyId;
// 供需分享url
+ (NSString *)shareSAndDUrl:(NSString *)posttId;

#pragma mark - 个人信息及修改
// 获取用户信息
+ (NSString *)getUserInfoByName:(NSString *)userName;
// 获取用户个人名片
+ (NSString *)getUserInfoParams:(NSString *)userName;
// 获取绑定的企业信息
+ (NSString *)getBindCompanyInfo;
// 修改用户头像
+ (NSDictionary *)editMyLogoParams;
// 修改昵称
+ (NSString *)editMyNickname:(NSString *)nickName;
// 修改性别
+ (NSString *)editMySex:(NSString *)sex;
// 绑定身份证
+ (NSDictionary *)bindMyCard;
// 获取验证码
+ (NSString *)getVerifyCode:(NSString *)phone;
// 修改手机号
+ (NSString *)editMyPhone:(NSString *)phone tempId:(NSString *)tempId code:(NSString *)code;
// 修改密码
+ (NSString *)editMyPasswd:(NSString *)oldPwd newPwd:(NSString *)newPwd;
// 获取邮寄列表
+ (NSString *)getPostAddressList;
// 获取区域列表
+ (NSString *)getRegionList;
// 增加邮寄列表
+ (NSString *)addPostAddress:(NSString *)name phone:(NSString *)phone country:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area;
// 编辑邮寄列表
+ (NSString *)editMyAddress:(NSString *)id name:(NSString *)name phone:(NSString *)phone country:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area;
// 删除邮寄列表
+ (NSString *)delPostAddress:(NSString *)id;
// 我的订单
+ (NSString *)getMyOrderList:(NSString *)pageIndex;
// 我的订单确认收货
+ (NSString *)confirmMyOrder:(NSString *)orderNumber;

#pragma mark  - 编辑企业信息
// 编辑企业信息 - 增加企业展示图片
+ (NSDictionary *)addBindCompanyImage;
// 编辑企业信息 - 关联石材
+ (NSString *)addCompanyStone:(NSString *)stoneName;
// 编辑企业信息 - 关联企业类型
+ (NSString *)addCompanyType:(NSString *)companyType;
// 编辑企业信息 - 绑定营业执照认证, 提交文件供后台审核
+ (NSDictionary *)bindCompanyLicense;
// 编辑企业信息 - 删除企业展示图片
+ (NSString *)delBindCompanyImage:(NSString *)id;
// 编辑企业信息 - 增加企业地址
+ (NSString *)addCompanyAddress:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area;
// 编辑企业信息 - 删除企业地址
+ (NSString *)delCompanyAddress:(NSString *)id;
// 编辑企业信息 - 取消关联石材
+ (NSString*)delCompanyStone:(NSString *)stoneName;
// 编辑企业信息 - 取消关联企业类型
+ (NSString *)delCompanyType:(NSString *)companyType;
// 编辑企业信息 - 修改企业地址
+ (NSString *)editCompanyAddress:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area;
// 编辑企业信息 - 修改企业其他地址
+ (NSString *)editCompanyOtherAddress:(NSString *)id country:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area;
// 编辑企业信息 - 修改企业简介
+ (NSString *)editCompanyDescription:(NSString *)description;
// 编辑企业信息 - 定位经纬度
+ (NSString *)editCompanyLocation:(NSString *)longitude latitude:(NSString *)latitude;
// 编辑企业信息 - 修改公司Logo
+ (NSDictionary *)editCompanyLogo;
// 编辑企业信息 - 修改企业网站
+ (NSString *)editCompanySite:(NSString *)site;
// 编辑企业信息 - 修改企业电话
+ (NSString *)editCompanyTel:(NSString *)tel;
// 编辑企业信息 - 获取企业的展示图片列表
+ (NSString *)getBindCompanyImageList;
// 编辑企业信息 - 获取企业主营石材列表
+ (NSString *)getBindCompanyStoneList;
// 编辑企业信息 - 获取企业地址列表
+ (NSString *)getCompanyAddressList:(NSString *)companyId;
// 编辑企业信息 - 获取企业联系人列表
+ (NSString *)getCompanyContactList:(NSString *)companyId;
// 编辑企业信息 - 新增企业联系人列表
+ (NSString *)addCompanyContact:(NSString *)name phone:(NSString *)phone;
// 编辑企业信息 - 编辑企业主联系人
+ (NSString *)editCompanyContact:(NSString *)name phone:(NSString *)phone;
// 编辑企业信息 - 编辑企业其他联系人
+ (NSString *)editCompanyOtherContact:(NSString *)id name:(NSString *)name phone:(NSString *)phone;
// 编辑企业信息 - 删除企业联系人
+ (NSString *)delCompanyContact:(NSString *)id;

#pragma mark - VIP权限
// 获取企业用户特权说明
+ (NSString *)getComUserRightsInfo;
// 编辑企业信息 - 获取VIP特权说明
+ (NSString *)getVipRightsInfo;
// 允许发布产品的数量
+ (NSString *)getAddProductLimit;
// 单个产品允许上传图片的数量
+ (NSString *)getAddProductImageLimit:(NSString *)productId;
// 允许关联石材的个数
+ (NSString *)getAddStoneLimit;
// 允许关联企业经营类别的数量
+ (NSString *)getAddCompanyTypeLimit;
// 允许上传公司展示图片的数量
+ (NSString *)getAddCompanyImageLimit;
// 是否允许启用企业营运执照认证
+ (NSString *)getAddCompanyLicenceLimit;
// 允许添加企业其他联系方式的数量
+ (NSString *)getAddCompanyContactLimit;
// 允许添加企业其他地址的数量
+ (NSString *)getAddCompanyAddressLimit;
// 获取所有权限的上限
+ (NSString *)getVipLimits;

#pragma mark - 我的收藏
// 我的收藏 - 石材
+ (NSString *)getMyMarkStoneList;
// 我的收藏 - 产品
+ (NSString *)getMyMarkProductList;
// 我的收藏 - 企业
+ (NSString *)getMyMarkCompanyList;
// 我的收藏 - 供求
+ (NSString *)getMyMarkSupplyDemandList;

// 我的发布 - 产品
+ (NSString *)getMyProductList:(NSString *)pageIndex;
// 我的发布 - 供需列表
+ (NSString *)getMySupplyDemandList:(NSString *)pageIndex;

#pragma mark - 发布供求
// 发布供求 - 发布求购
+ (NSDictionary *)addDemand:(NSString *)title content:(NSString *)content location:(NSString *)location;
// 发布供求 - 发布悬赏
+ (NSDictionary *)addPaidDemand:(NSString *)title content:(NSString *)content location:(NSString *)location price:(NSString *)price expired:(NSString *)expired;
// 发布供求 - 发布供应
+ (NSDictionary *)addSupply:(NSString *)title content:(NSString *)content location:(NSString *)location;
// 发布悬赏限制
+ (NSString *)getLimitRangeForPaidDemand;

#pragma mark - 发布产品
// 发布产品
+ (NSDictionary *)addProduct:(NSString *)title description:(NSString *)description price:(NSString *)price amount:(NSString *)amount type:(NSString *)type subtype:(NSString *)subtype tag:(NSString *)tag recommend:(NSString *)recommend priceUnit:(NSString *)priceUnit;
// 样品下单
+ (NSString *)addSampleProductOrder:(NSString *)postAddressId payType:(NSString *)payType buyerRemark:(NSString *)buyerRemark products:(NSString *)products;
// 支付订单
+ (NSString *)payOrder:(NSString *)orderNumber payType:(NSString *)payType;

#pragma mark - 微聊
// 搜索用户，添加好友用
+ (NSString *)getUserListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex;
// 批量获取用户信息，如群组详情中成员的头像、昵称获取
+ (NSString *)getUsersByUserNames:(NSString *)userNames;
// 客服信息
+ (NSString *)getKefuInfo;

#pragma mark - 通知
// 通知列表
+ (NSString *)getMyMessageList:(NSString *)pageIndex;
// 通知详情
+ (NSString *)getMyMessageInfo:(NSString *)messageId;

#pragma mark - VIP控制开关
+ (NSString *)getCtrlInfo;

@end
