//
//  ZbWebService.m
//  ddLogisticsGuest
//
//  Created by 张斌 on 15/6/8.
//  Copyright (c) 2015年 qianxx. All rights reserved.
//

#import "ZbWebService.h"
#import "NSString+MD5.h"
#import <UIKit/UIKit.h>

// cookie保存的key
#define COOKIE_SAVE_KEY @"COOKIE_SAVE_KEY"

static NSMutableDictionary * USER;    // 当前用户
static ZbQyInfo * QY;          // 当前企业

@implementation ZbWebService

#pragma mark - 当前用户
+ (void)setUser:(NSMutableDictionary *)info
{
    [ZbWebService filterNullData:info];
    USER = info;
}

+ (NSMutableDictionary *)sharedUser
{
    return USER;
}

+ (void)setQy:(ZbQyInfo *)info {
    QY = info;
}

+ (ZbQyInfo *)sharedQy {
    return QY;
}

+ (BOOL)isLogin {
    NSDictionary *curUser = [ZbWebService sharedUser];
    if (curUser == nil) {
        return NO;
    }
    return YES;
}

+ (BOOL)isVipQy {
    if (QY == nil) {
        return NO;
    }
    if (QY.company_level.integerValue <= 0) {
        return NO;
    }
    return YES;
}

#pragma mark - cookie存本地
// 保存cookie
+ (void)saveCookie {
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookie *cookie;
    for (NSHTTPCookie *curCookie in [cookieJar cookies]) {
        NSString *a = curCookie.domain;
        if ([a hasSuffix:@"stonetmall.com"]) {
            // 石猫的cookie
            cookie = curCookie;
        }
    }
    // 找出石猫的cookie
//    NSLog(@"保存cookie=%@", cookie);
    if (!cookie) {
//        NSLog(@"保存cookie失败");
        return;
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *tagData = [NSKeyedArchiver archivedDataWithRootObject:cookie];
    [userDefaults setValue:tagData forKey:COOKIE_SAVE_KEY];
    [userDefaults synchronize];
//    NSLog(@"保存cookie成功");
    return;
}
// 恢复cookie
+ (BOOL)recoverCookie {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *tagData = [userDefaults valueForKey:COOKIE_SAVE_KEY];
    if (!tagData) {
        return NO;
    }
    NSHTTPCookie *cookie = [NSKeyedUnarchiver unarchiveObjectWithData:tagData];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    return YES;
}
// 清空cookie
+ (void)clearCookie {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:nil forKey:COOKIE_SAVE_KEY];
    [userDefaults synchronize];
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    for (NSHTTPCookie *cookie in cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
//    NSLog(@"清空cookie成功");
}

#pragma mark - 公用方法
// 获取图片完整地址
+ (NSString *)getImageFullUrl:(NSString *)image {
    if (image == nil) {
        return @"";
    }
    return [ZB_IMAGE_HOST stringByAppendingString:image];
}

// 数据列表转成索引选择器所需数组
+ (NSMutableArray *)sortByFirstCharact:(NSArray *)dataArray {
    // 首字母－[数据、数据]
    NSMutableDictionary *keyDict = [NSMutableDictionary dictionary];
    // 遍历数据
    for (NSDictionary *dataDict in dataArray) {
        // 获取首字母
        NSString *key = dataDict[@"description"];
        key = [key uppercaseString];
        
        // 无此首字母则创建
        if (![[keyDict allKeys] containsObject:key]) {
            [keyDict setValue:[NSMutableArray array] forKey:key];
        }
        // 将数据添加到其首字母对应的数组
        NSMutableArray *keyInfoArray = [keyDict objectForKey:key];
        [keyInfoArray addObject:dataDict];
    }
    
    NSMutableArray *resultArray = [NSMutableArray array];
    for (NSString *key in @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", ]) {
        if ([[keyDict allKeys] containsObject:key]) {
            [resultArray addObject:@{@"indexTitle":key, @"data":[keyDict objectForKey:key]}];
        }
    }
    return resultArray;
}

// 首字母数组拼接
+ (void)firstCharactArrayJoin:(NSMutableArray *)curArray addArray:(NSMutableArray *)addArray {
    for (NSDictionary *addDict in addArray) {
        NSString *addKey = addDict[@"indexTitle"];
        NSMutableArray *addDataArray = addDict[@"data"];
        
        BOOL isFind = NO;
        for (NSDictionary *curDict in curArray) {
            NSString *curKey = curDict[@"indexTitle"];
            if ([addKey isEqualToString:curKey]) {
                NSMutableArray *curDataArray = curDict[@"data"];
                [curDataArray addObjectsFromArray:addDataArray];
                isFind = YES;
                break;
            }
        }
        
        if (!isFind) {
            [curArray addObject:addDict];
        }
    }
}

// 二级分类数据解析
+ (NSArray *)treeTypeParser:(NSArray *)typeInfoArray {
    NSMutableArray *tagTypeArray = [NSMutableArray array];
    NSMutableDictionary *subTypeDict = [NSMutableDictionary dictionary];
    
    for (NSDictionary *typeInfo in typeInfoArray) {
        NSString *parentId = typeInfo[@"class_parent_id"];
        if (parentId.integerValue == 0) {
            // 一级分类
            [tagTypeArray addObject:typeInfo];
        } else {
            // 二级分类
            if (![subTypeDict.allKeys containsObject:parentId]) {
                [subTypeDict setValue:[NSMutableArray array] forKey:parentId];
            }
            NSMutableArray *subTypeArray = subTypeDict[parentId];
            [subTypeArray addObject:typeInfo];
        }
    }
    
    return @[tagTypeArray, subTypeDict];
}

// 城市二级分类数据解析
+ (NSArray *)treeSTypeParser:(NSArray *)typeInfoArray {
    NSMutableArray *tagTypeArray = [NSMutableArray array];
    NSMutableDictionary *subTypeDict = [NSMutableDictionary dictionary];
    
    for (NSDictionary *typeInfo in typeInfoArray) {
        NSString *parentId = typeInfo[@"class_parent_id"];
        if (parentId.integerValue == 0) {
            continue;
        } else if (parentId.integerValue == 38) {
            // 一级分类
            [tagTypeArray addObject:typeInfo];
        } else {
            // 二级分类
            if (![subTypeDict.allKeys containsObject:parentId]) {
                [subTypeDict setValue:[NSMutableArray array] forKey:parentId];
            }
            NSMutableArray *subTypeArray = subTypeDict[parentId];
            [subTypeArray addObject:typeInfo];
        }
    }
    
    return @[tagTypeArray, subTypeDict];
}

// 获取参数string
+ (NSString *)getParamsString:(NSMutableDictionary *)rawParamsDic
{
    NSMutableArray *paramsArray = [[NSMutableArray alloc] init];
    for (NSString *keyStr in rawParamsDic) {
        NSString *valueStr = [rawParamsDic objectForKey:keyStr];
        NSString *bStr = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                               (CFStringRef)valueStr,
                                                               NULL,
                                                               CFSTR(":/?#[]@!$&’()*+,;="),
                                                               kCFStringEncodingUTF8));
        [paramsArray addObject:[NSString stringWithFormat:@"%@=%@", keyStr, bStr]];
    }
    return [paramsArray componentsJoinedByString:@"&"];
}

// 获取设备号
+ (NSString *)getDeviceId {
    NSString *idfv =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    return idfv;
}

// 获取登录ip
+ (NSString *)getSessionId {
    return @"192.168.1.33";
}

// 过滤null
+ (void)filterNullData:(NSMutableDictionary *)info {
    // 过滤null信息
    NSArray *keys = [info allKeys];
    for (int i = 0; i < keys.count; i ++)
    {
        id obj = [info objectForKey:keys[i]];
        if (![obj isKindOfClass:[NSNull class]]) {
            continue;
        }
        [info setObject:@"" forKey:keys[i]];
    }
}

#pragma mark - 登录接口
// 登录参数
+ (NSString *)loginParams:(NSString *)acc password:(NSString *)psw {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"login" forKey:@"action"];
    [paramsDic setValue:acc forKey:@"userName"];
    NSString *pswMd5 = [psw md5Encrypt];
    [paramsDic setValue:pswMd5 forKey:@"userPasswd"];
    [paramsDic setValue:APP_TYPE forKey:@"sessionFrom"];
    [paramsDic setValue:[ZbWebService getSessionId] forKey:@"sessionIp"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 微信登录参数
+ (NSString *)loginWxParams:(NSString *)wxCode {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"loginByWeixin" forKey:@"action"];
    [paramsDic setValue:wxCode forKey:@"wxcode"];
    [paramsDic setValue:APP_TYPE forKey:@"sessionFrom"];
    [paramsDic setValue:[ZbWebService getSessionId] forKey:@"sessionIp"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 创建企业
+ (NSString *)newCompany:(NSString *)name description:(NSString *)description contact:(NSString *)contact phone:(NSString *)phone country:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"newCompany" forKey:@"action"];
    [paramsDic setValue:name forKey:@"name"];
    [paramsDic setValue:description forKey:@"description"];
    [paramsDic setValue:contact forKey:@"contact"];
    [paramsDic setValue:phone forKey:@"phone"];
    [paramsDic setValue:country forKey:@"country"];
    [paramsDic setValue:prov forKey:@"prov"];
    [paramsDic setValue:city forKey:@"city"];
    [paramsDic setValue:area forKey:@"area"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 申请绑定企业
+ (NSString *)applyForBindCompany:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"applyForBindCompany" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 自动登录接口
// 登录参数
+ (NSString *)autoLogin {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"autoLogin" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 注册接口
// 注册参数
+ (NSString *)registerParams:(NSString *)acc password:(NSString *)psw wxCode:(NSString *)wxCode {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"register" forKey:@"action"];
    [paramsDic setValue:acc forKey:@"userName"];
    NSString *pswMd5 = [psw md5Encrypt];
    [paramsDic setValue:pswMd5 forKey:@"userPasswd"];
    [paramsDic setValue:@"1" forKey:@"autoLogin"];
    [paramsDic setValue:APP_TYPE forKey:@"sessionFrom"];
    [paramsDic setValue:[ZbWebService getSessionId] forKey:@"sessionIp"];
    [paramsDic setValue:wxCode forKey:@"wxcode"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 判断用户名是否存在参数
+ (NSString *)checkUserNameExistParams:(NSString *)name {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"checkUserNameExist" forKey:@"action"];
    [paramsDic setValue:name forKey:@"userName"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 重置密码接口
// 重置密码参数
+ (NSString *)resetPassword:(NSString *)phone tempId:(NSString *)tempId code:(NSString *)code {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"resetPassword" forKey:@"action"];
    [paramsDic setValue:phone forKey:@"phone"];
    [paramsDic setValue:tempId forKey:@"tempId"];
    [paramsDic setValue:code forKey:@"code"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 退出登录接口
// 退出登录参数
+ (NSString *)logoutParams {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:[ZbWebService getDeviceId] forKey:@"deviceId"];
    [paramsDic setValue:APP_TYPE forKey:@"type"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 首页信息
// 闪屏广告
+ (NSString *)getSplashScreenAdList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getSplashScreenAdList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 首页品牌列表
+ (NSString *)getCompanyShowAdList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyShowAdList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 首页广告列表
+ (NSString *)getCoverRollAdList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCoverRollAdList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 首页行业头条
+ (NSString *)getLatestNewsList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getLatestNewsList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 首页供需列表
+ (NSString *)getLatestSupplyDemandList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getLatestSupplyDemandList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 首页推荐品种列表
+ (NSString *)getNewProductAdList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getNewProductAdList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 全局搜索
// 全局整体企业搜索
+ (NSString *)globalQySearchParams:(NSString *)key pageIndex:(NSString *)index pageSize:(NSString *)size {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyListByQuery" forKey:@"action"];
    [paramsDic setValue:key forKey:@"query"];
    [paramsDic setValue:index forKey:@"pageIndex"];
    [paramsDic setValue:size forKey:@"pageSize"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 全局整体产品搜索
+ (NSString *)globalProductSearchParams:(NSString *)key pageIndex:(NSString *)index pageSize:(NSString *)size {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getProductListByQuery" forKey:@"action"];
    [paramsDic setValue:key forKey:@"query"];
    [paramsDic setValue:index forKey:@"pageIndex"];
    [paramsDic setValue:size forKey:@"pageSize"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 全局整体石材搜索
+ (NSString *)globalStoneSearchParams:(NSString *)key pageIndex:(NSString *)index pageSize:(NSString *)size {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getStoneListByQuery" forKey:@"action"];
    [paramsDic setValue:key forKey:@"query"];
    [paramsDic setValue:index forKey:@"pageIndex"];
    [paramsDic setValue:size forKey:@"pageSize"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 全局整体供需搜索
+ (NSString *)globalSAndDSearchParams:(NSString *)key pageIndex:(NSString *)index pageSize:(NSString *)size {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getSupplyDemandListByQuery" forKey:@"action"];
    [paramsDic setValue:key forKey:@"query"];
    [paramsDic setValue:index forKey:@"pageIndex"];
    [paramsDic setValue:size forKey:@"pageSize"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 石材数据
// 石材类别列表
+ (NSString *)getStoneTypeList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getStoneTypeList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 石材颜色列表
+ (NSString *)getStoneColorList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getStoneColorList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 石材纹理列表
+ (NSString *)getStoneTextureList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getStoneTextureList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 查询石材数据
+ (NSString *)getStoneList:(NSString *)key type:(NSString *)type color:(NSString *)color texture:(NSString *)texture pageIndex:(NSString *)index {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getStoneList" forKey:@"action"];
    [paramsDic setValue:key forKey:@"query"];
    [paramsDic setValue:type forKey:@"type"];
    [paramsDic setValue:color forKey:@"color"];
    [paramsDic setValue:texture forKey:@"texture"];
    [paramsDic setValue:index forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 石材详情数据
// 获取石材相关企业
+ (NSString *)getCompanyListByStone:(NSString *)stoneName pageIndex:(NSString *)pageIndex lng:(NSString *)lng lat:(NSString *)lat {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyListByStone" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    [paramsDic setValue:lng forKey:@"myLongitude"];
    [paramsDic setValue:lat forKey:@"myLatitude"];
    // 百度坐标系
    [paramsDic setValue:@"2" forKey:@"mapType"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取石材推荐企业
+ (NSString *)getRecommendCompanyListByStone:(NSString *)stoneName lng:(NSString *)lng lat:(NSString *)lat {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getRecommendCompanyListByStone" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    [paramsDic setValue:@"1" forKey:@"pageIndex"];
    [paramsDic setValue:lng forKey:@"myLongitude"];
    [paramsDic setValue:lat forKey:@"myLatitude"];
    // 百度坐标系
    [paramsDic setValue:@"2" forKey:@"mapType"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 相似石材
+ (NSString *)getSimilarStoneListByStone:(NSString *)stoneName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getSimilarStoneListByStone" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取石材详情
+ (NSString *)getStoneInfo:(NSString *)stoneId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getStoneInfo" forKey:@"action"];
    [paramsDic setValue:stoneId forKey:@"id"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取石材详情by名字
+ (NSString *)getStoneInfoByName:(NSString *)stoneName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getStoneInfoByName" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取样品规格
+ (NSString *)getSampleProductSpecList:(NSString *)stoneName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getSampleProductSpecList" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 加入供应商
+ (NSString *)joinStoneToCompany:(NSString *)stoneName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"joinStoneToCompany" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 收藏石材
+ (NSString *)markStone:(NSString *)stoneId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"markStone" forKey:@"action"];
    [paramsDic setValue:stoneId forKey:@"stoneId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 取消收藏石材
+ (NSString *)unmarkStone:(NSString *)stoneId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"unmarkStone" forKey:@"action"];
    [paramsDic setValue:stoneId forKey:@"stoneId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 赞石材
+ (NSString *)likeStone:(NSString *)stoneId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"likeStone" forKey:@"action"];
    [paramsDic setValue:stoneId forKey:@"stoneId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 取消赞石材
+ (NSString *)unlikeStone:(NSString *)stoneId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"unlikeStone" forKey:@"action"];
    [paramsDic setValue:stoneId forKey:@"stoneId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 企业数据
// 企业服务类型列表
+ (NSString *)getCompanyTypeList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyTypeList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 省份列表
+ (NSString *)getProvList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getProvList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 企业滚动广告
+ (NSString *)getCompanySearchAdList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanySearchAdList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 查询企业数据
+ (NSString *)getCompanyList:(NSString *)key type:(NSString *)type area:(NSString *)area sortName:(NSString *)sortName sortOrder:(NSString *)sortOrder pageIndex:(NSString *)index {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyList" forKey:@"action"];
    [paramsDic setValue:key forKey:@"query"];
    [paramsDic setValue:type forKey:@"type"];
    [paramsDic setValue:area forKey:@"area"];
    if (sortName != nil) {
        [paramsDic setValue:sortName forKey:@"sortName"];
    }
    if (sortOrder != nil) {
        [paramsDic setValue:sortOrder forKey:@"sortOrder"];
    }
    [paramsDic setValue:index forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 企业详情数据
// 企业主营产品
+ (NSString *)getCompanyProductList:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyProductList" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 企业主营石材
+ (NSString *)getCompanyStoneList:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyStoneList" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 企业详细信息
+ (NSString *)getCompanyInfo:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyInfo" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"id"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取企业更多地址
+ (NSString *)getCompanyAddressListByComId:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyAddressListByComId" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取企业更多联系人
+ (NSString *)getCompanyContactListByComId:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyContactListByComId" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 收藏企业
+ (NSString *)markCompany:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"markCompany" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 取消收藏企业
+ (NSString *)unmarkCompany:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"unmarkCompany" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 赞企业
+ (NSString *)likeCompany:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"likeCompany" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 取消赞企业
+ (NSString *)unlikeCompany:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"unlikeCompany" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 产品数据
// 产品分类列表
+ (NSString *)getProductTypeList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getProductTypeList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 产品滚动广告
+ (NSString *)getProductSearchAdList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getProductSearchAdList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 查询产品数据
+ (NSString *)getProductList:(NSString *)key type:(NSString *)type area:(NSString *)area sortName:(NSString *)sortName sortOrder:(NSString *)sortOrder pageIndex:(NSString *)index {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getProductList" forKey:@"action"];
    [paramsDic setValue:key forKey:@"query"];
    [paramsDic setValue:type forKey:@"type"];
    [paramsDic setValue:area forKey:@"area"];
    [paramsDic setValue:sortName forKey:@"sortName"];
    [paramsDic setValue:sortOrder forKey:@"sortOrder"];
    [paramsDic setValue:index forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 产品详情数据
// 删除产品
+ (NSString *)delProduct:(NSString *)productId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"delProduct" forKey:@"action"];
    [paramsDic setValue:productId forKey:@"id"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取产品价格单位
+ (NSString *)getPriceUnitList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getPriceUnitList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 修改产品标题
+ (NSDictionary *)editProductTitle:(NSString *)id withTitle:(NSString *)title {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editProductTitle" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    [paramsDic setValue:title forKey:@"title"];
    
    return paramsDic;
}
// 修改产品推荐
+ (NSString *)editProductRecommend:(NSString *)id withRecomand:(NSString *)recommend{
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editProductRecommend" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    [paramsDic setValue:recommend forKey:@"recommend"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 修改产品标签
+ (NSString *)editProductTag:(NSString *)id withTag:(NSString *)tag {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editProductTag" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    [paramsDic setValue:tag forKey:@"tag"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 修改产品参考价
+ (NSString *)editProductPrice:(NSString *)id withPrice:(NSString *)price priceUnit:(NSString *)priceUnit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editProductPrice" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    // 要传分
    NSNumber *cent = [NSNumber numberWithInteger:(price.integerValue * 100)];
    [paramsDic setValue:[NSString stringWithFormat:@"%@", cent] forKey:@"price"];
    [paramsDic setValue:priceUnit forKey:@"priceUnit"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 修改产品分类
+ (NSDictionary *)editProductType:(NSString *)id withType:(NSString *)type withSubType:(NSString *)subtype{
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editProductType" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    [paramsDic setValue:type forKey:@"type"];
    [paramsDic setValue:subtype forKey:@"subtype"];
    
    // 转化成string
    return paramsDic;
}
// 修改产品描述
+ (NSDictionary *)editProductDescription:(NSString *)id withDesc:(NSString *)description {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editProductDescription" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    [paramsDic setValue:description forKey:@"description"];
    
    // 转化成string
    return paramsDic;
}

// 公司简短信息
+ (NSString *)getCompanyShortInfo:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyShortInfo" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取产品图片列表
+ (NSString *)getProductImageList:(NSString *)productId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getProductImageList" forKey:@"action"];
    [paramsDic setValue:productId forKey:@"productId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取相似产品
+ (NSString *)getSimilarProductListByStone:(NSString *)stoneName pageIndex:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getProductListByStone" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取产品详情
+ (NSString *)getProductInfo:(NSString *)productId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getProductInfo" forKey:@"action"];
    [paramsDic setValue:productId forKey:@"id"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 石材简短信息
+ (NSString *)getStoneShortInfo:(NSString *)stoneName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getStoneShortInfo" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 收藏石材
+ (NSString *)markProduct:(NSString *)productId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"markProduct" forKey:@"action"];
    [paramsDic setValue:productId forKey:@"productId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 取消收藏石材
+ (NSString *)unmarkProduct:(NSString *)productId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"unmarkProduct" forKey:@"action"];
    [paramsDic setValue:productId forKey:@"productId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 赞石材
+ (NSString *)likeProduct:(NSString *)productId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"likeProduct" forKey:@"action"];
    [paramsDic setValue:productId forKey:@"productId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 取消赞石材
+ (NSString *)unlikeProduct:(NSString *)productId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"unlikeProduct" forKey:@"action"];
    [paramsDic setValue:productId forKey:@"productId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 供需、悬赏列表
// 需求列表广告
+ (NSString *)getDemandSearchAdList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getDemandSearchAdList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 需求搜索
+ (NSString *)getDemandListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getDemandListByQuery" forKey:@"action"];
    [paramsDic setValue:query forKey:@"query"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 供应列表广告
+ (NSString *)getSupplySearchAdList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getSupplySearchAdList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 供应搜索
+ (NSString *)getSupplyListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getSupplyListByQuery" forKey:@"action"];
    [paramsDic setValue:query forKey:@"query"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 头几条悬赏
+ (NSString *)getPaidDemandListTopN {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getPaidDemandListTopN" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 悬赏搜索
+ (NSString *)getPaidDemandListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getPaidDemandListByQuery" forKey:@"action"];
    [paramsDic setValue:query forKey:@"query"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 已完成悬赏搜索
+ (NSString *)getFinishPaidDemandListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getFinishPaidDemandListByQuery" forKey:@"action"];
    [paramsDic setValue:query forKey:@"query"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 供需详情
// 帖子图片
+ (NSString *)getSupplyDemandImageList:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getSupplyDemandImageList" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 帖子评论列表
+ (NSString *)getCommentList:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCommentList" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 供需详情
+ (NSString *)getSupplyDemandInfo:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getSupplyDemandInfo" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 收藏帖子
+ (NSString *)markPost:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"markPost" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 取消收藏帖子
+ (NSString *)unmarkPost:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"unmarkPost" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 评论帖子
+ (NSString *)addComment:(NSString *)postId content:(NSString *)content {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addComment" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    [paramsDic setValue:content forKey:@"content"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 行业头条
// 新闻详情
+ (NSString *)getNewsInfo:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getNewsInfo" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"id"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 悬赏详情
// 获取悬赏的回复列表
+ (NSString *)getPaidDemandCommentList:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getPaidDemandCommentList" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取悬赏详情
+ (NSString *)getPaidDemandInfo:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getPaidDemandInfo" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 获取中标回答
+ (NSString *)getWinBidComment:(NSString *)postId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getWinBidComment" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 投标回答
+ (NSDictionary *)addPaidDemandComment:(NSString *)postId content:(NSString *)content {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addPaidDemandComment" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    [paramsDic setValue:content forKey:@"content"];
    
    return paramsDic;
}
// 增加悬赏金额，延长悬赏时间
+ (NSString *)applyAddDaysForPaidDemand:(NSString *)postId addDays:(NSString *)addDays addMoney:(NSString *)addMoney {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"applyAddDaysForPaidDemand" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    [paramsDic setValue:addDays forKey:@"addDays"];
    // 要传分
    NSNumber *cent = [NSNumber numberWithInteger:(addMoney.integerValue * 100)];
    [paramsDic setValue:[NSString stringWithFormat:@"%@", cent] forKey:@"addMoney"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 采用回答
+ (NSString *)usePaidDemandComment:(NSString *)postId commentId:(NSString *)commentId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"usePaidDemandComment" forKey:@"action"];
    [paramsDic setValue:postId forKey:@"postId"];
    [paramsDic setValue:commentId forKey:@"commentId"];
    [paramsDic setValue:@"4" forKey:@"status"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 石材、企业、产品分享地址
// 石材分享url
+ (NSString *)shareStoneUrl:(NSString *)stoneId {
    return [NSString stringWithFormat:@"http://www.stonetmall.com/pg/wechatapp/stonedetail.php?id=%@", stoneId];
}
// 产品分享url
+ (NSString *)shareProductUrl:(NSString *)productId {
    return [NSString stringWithFormat:@"http://www.stonetmall.com/pg/wechatapp/productdetail.php?id=%@", productId];
}
// 企业分享url
+ (NSString *)shareCompanyUrl:(NSString *)companyId {
    return [NSString stringWithFormat:@"http://www.stonetmall.com/pg/wechatapp/comdetail.php?id=%@", companyId];
}
// 供需分享url
+ (NSString *)shareSAndDUrl:(NSString *)posttId {
    return [NSString stringWithFormat:@"http://www.stonetmall.com/pg/wechatapp/supplydemand.php?id=%@", posttId];
}

#pragma mark - 个人信息及修改
// 获取用户信息
+ (NSString *)getUserInfoByName:(NSString *)userName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getUserInfo" forKey:@"action"];
        [paramsDic setValue:userName forKey:@"userName"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 获取用户个人名片
+ (NSString *)getUserInfoParams:(NSString *)userName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyInfo" forKey:@"action"];
//    [paramsDic setValue:userName forKey:@"userName"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 获取绑定的企业信息
+ (NSString *)getBindCompanyInfo {
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getBindCompanyInfo" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}


// 修改用户头像
+ (NSDictionary *)editMyLogoParams {
    //构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editMyLogo" forKey:@"action"];
    
    // 转化成string
    return paramsDic;
}

// 修改昵称
+ (NSString *)editMyNickname:(NSString *)nickName {
    //构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editMyNickname" forKey:@"action"];
    [paramsDic setValue:nickName forKey:@"nickname"];
    
    //转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 修改性别
+ (NSString *)editMySex:(NSString *)sex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editMySex" forKey:@"action"];
    [paramsDic setValue:sex forKey:@"sex"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 绑定身份证
+ (NSDictionary *)bindMyCard {
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"bindMyCard" forKey:@"action"];
    return paramsDic;
}

// 获取验证码
+ (NSString *)getVerifyCode:(NSString *)phone {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getVerifyCode" forKey:@"action"];
    [paramsDic setValue:phone forKey:@"phone"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 修改手机号
+ (NSString *)editMyPhone:(NSString *)phone tempId:(NSString *)tempId code:(NSString *)code {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editMyPhone" forKey:@"action"];
    [paramsDic setValue:phone forKey:@"phone"];
    [paramsDic setValue:tempId forKey:@"tempId"];
    [paramsDic setValue:code forKey:@"code"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 修改密码
+ (NSString *)editMyPasswd:(NSString *)oldPwd newPwd:(NSString *)newPwd {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editMyPasswd" forKey:@"action"];
    NSString *pswMd5 = [oldPwd md5Encrypt];
    [paramsDic setValue:pswMd5 forKey:@"oldPasswd"];
    NSString *newMd5 = [newPwd md5Encrypt];
    [paramsDic setValue:newMd5 forKey:@"newPasswd"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 获取邮寄列表
+ (NSString *)getPostAddressList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getPostAddressList" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 获取区域列表
+ (NSString *)getRegionList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getRegionList" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 增加邮寄列表
+ (NSString *)addPostAddress:(NSString *)name phone:(NSString *)phone country:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addPostAddress" forKey:@"action"];
    [paramsDic setValue:name forKey:@"name"];
    [paramsDic setValue:phone forKey:@"phone"];
    [paramsDic setValue:country forKey:@"country"];
    [paramsDic setValue:prov forKey:@"prov"];
    [paramsDic setValue:city forKey:@"city"];
    [paramsDic setValue:area forKey:@"area"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 编辑邮寄列表
+ (NSString *)editMyAddress:(NSString *)id name:(NSString *)name phone:(NSString *)phone country:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editMyAddress" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    [paramsDic setValue:name forKey:@"name"];
    [paramsDic setValue:phone forKey:@"phone"];
    [paramsDic setValue:country forKey:@"country"];
    [paramsDic setValue:prov forKey:@"prov"];
    [paramsDic setValue:city forKey:@"city"];
    [paramsDic setValue:area forKey:@"area"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 删除邮寄列表
+ (NSString *)delPostAddress:(NSString *)id {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"delPostAddress" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}


// 我的订单
+ (NSString *)getMyOrderList:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyOrderList" forKey:@"action"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 我的订单确认收货
+ (NSString *)confirmMyOrder:(NSString *)orderNumber {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"confirmMyOrder" forKey:@"action"];
    [paramsDic setValue:orderNumber forKey:@"orderNumber"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark  - 编辑企业信息
// 编辑企业信息 - 增加企业展示图片
+ (NSDictionary *)addBindCompanyImage {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addBindCompanyImage" forKey:@"action"];
    return paramsDic;
}
// 编辑企业信息 - 关联石材
+ (NSString *)addCompanyStone:(NSString *)stoneName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addCompanyStone" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 关联企业类型
+ (NSString *)addCompanyType:(NSString *)companyType {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addCompanyType" forKey:@"action"];
    [paramsDic setValue:companyType forKey:@"companyType"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 绑定营业执照认证, 提交文件供后台审核
+ (NSDictionary *)bindCompanyLicense {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"bindCompanyLicense" forKey:@"action"];
    
    return paramsDic;
}
// 编辑企业信息 - 删除企业展示图片
+ (NSString *)delBindCompanyImage:(NSString *)id {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"delBindCompanyImage" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 增加企业地址
+ (NSString *)addCompanyAddress:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addCompanyAddress" forKey:@"action"];
    [paramsDic setValue:country forKey:@"country"];
    [paramsDic setValue:prov forKey:@"prov"];
    [paramsDic setValue:city forKey:@"city"];
    [paramsDic setValue:area forKey:@"area"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 删除企业地址
+ (NSString *)delCompanyAddress:(NSString *)id {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"delCompanyAddress" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 取消关联石材
+ (NSString*)delCompanyStone:(NSString *)stoneName {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"delCompanyStone" forKey:@"action"];
    [paramsDic setValue:stoneName forKey:@"stoneName"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 取消关联企业类型
+ (NSString *)delCompanyType:(NSString *)companyType {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"delCompanyType" forKey:@"action"];
    [paramsDic setValue:companyType forKey:@"companyType"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 修改企业地址
+ (NSString *)editCompanyAddress:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area{
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyAddress" forKey:@"action"];
    [paramsDic setValue:country forKey:@"country"];
    [paramsDic setValue:prov forKey:@"prov"];
    [paramsDic setValue:city forKey:@"city"];
    [paramsDic setValue:area forKey:@"area"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 修改企业其他地址
+ (NSString *)editCompanyOtherAddress:(NSString *)id country:(NSString *)country prov:(NSString *)prov city:(NSString *)city area:(NSString *)area {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyOtherAddress" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    [paramsDic setValue:country forKey:@"country"];
    [paramsDic setValue:prov forKey:@"prov"];
    [paramsDic setValue:city forKey:@"city"];
    [paramsDic setValue:area forKey:@"area"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 编辑企业信息 - 修改企业简介
+ (NSString *)editCompanyDescription:(NSString *)description {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyDescription" forKey:@"action"];
    [paramsDic setValue:description forKey:@"description"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 定位经纬度
+ (NSString *)editCompanyLocation:(NSString *)longitude latitude:(NSString *)latitude {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyLocation" forKey:@"action"];
    [paramsDic setValue:longitude forKey:@"longitude"];
    [paramsDic setValue:latitude forKey:@"latitude"];
    [paramsDic setValue:@"2" forKey:@"mapType"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 修改公司Logo
+ (NSDictionary *)editCompanyLogo {
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyLogo" forKey:@"action"];
    return paramsDic;
}
// 编辑企业信息 - 修改企业网站
+ (NSString *)editCompanySite:(NSString *)site {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanySite" forKey:@"action"];
    [paramsDic setValue:site forKey:@"site"];
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 修改企业电话
+ (NSString *)editCompanyTel:(NSString *)tel {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyTel" forKey:@"action"];
    [paramsDic setValue:tel forKey:@"tel"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 获取企业的展示图片列表
+ (NSString *)getBindCompanyImageList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getBindCompanyImageList" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 获取企业主营石材列表
+ (NSString *)getBindCompanyStoneList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getBindCompanyStoneList" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 获取企业地址列表
+ (NSString *)getCompanyAddressList:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyAddressList" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];

}
// 编辑企业信息 - 获取企业联系人列表
+ (NSString *)getCompanyContactList:(NSString *)companyId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCompanyContactList" forKey:@"action"];
    [paramsDic setValue:companyId forKey:@"companyId"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 新增企业联系人列表
+ (NSString *)addCompanyContact:(NSString *)name phone:(NSString *)phone {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addCompanyContact" forKey:@"action"];
    [paramsDic setValue:name forKey:@"name"];
    [paramsDic setValue:phone forKey:@"phone"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 编辑企业主联系人
+ (NSString *)editCompanyContact:(NSString *)name phone:(NSString *)phone {
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyContact" forKey:@"action"];
    [paramsDic setValue:name forKey:@"name"];
    [paramsDic setValue:phone forKey:@"phone"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 编辑企业副联系人
+ (NSString *)editCompanyOtherContact:(NSString *)id name:(NSString *)name phone:(NSString *)phone {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyOtherContact" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    [paramsDic setValue:name forKey:@"name"];
    [paramsDic setValue:phone forKey:@"phone"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 编辑企业其他联系人
+ (NSString *)editCompanyOtherContact:(NSString *)name phone:(NSString *)phone {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"editCompanyOtherContact" forKey:@"action"];
    [paramsDic setValue:name forKey:@"name"];
    [paramsDic setValue:phone forKey:@"phone"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 删除企业联系人
+ (NSString *)delCompanyContact:(NSString *)id {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"delCompanyContact" forKey:@"action"];
    [paramsDic setValue:id forKey:@"id"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - VIP权限
// 获取企业用户特权说明
+ (NSString *)getComUserRightsInfo {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getComUserRightsInfo" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 编辑企业信息 - 获取VIP特权说明
+ (NSString *)getVipRightsInfo {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getVipRightsInfo" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 允许发布产品的数量
+ (NSString *)getAddProductLimit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getAddProductLimit" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 单个产品允许上传图片的数量
+ (NSString *)getAddProductImageLimit:(NSString *)productId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getAddProductImageLimit" forKey:@"action"];
    [paramsDic setValue:productId forKey:@"productId"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 允许关联石材的个数
+ (NSString *)getAddStoneLimit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getAddStoneLimit" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 允许关联企业经营类别的数量
+ (NSString *)getAddCompanyTypeLimit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getAddCompanyTypeLimit" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
// 允许上传公司展示图片的数量
+ (NSString *)getAddCompanyImageLimit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getAddCompanyImageLimit" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}
//是否允许启用企业营运执照认证
+ (NSString *)getAddCompanyLicenceLimit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getAddCompanyLicenceLimit" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 允许添加企业其他联系方式的数量
+ (NSString *)getAddCompanyContactLimit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getAddCompanyContactLimit" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 允许添加企业其他地址的数量
+ (NSString *)getAddCompanyAddressLimit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getAddCompanyAddressLimit" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}

// 获取所有权限的上限
+ (NSString *)getVipLimits {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getVipLimits" forKey:@"action"];
    
    // 转化为string
    return [ZbWebService getParamsString:paramsDic];
}


#pragma mark - 我的收藏
// 我的收藏 - 石材

+ (NSString *)getMyMarkStoneList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyMarkStoneList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 我的收藏 - 产品

+ (NSString *)getMyMarkProductList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyMarkProductList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 我的收藏 - 企业
+ (NSString *)getMyMarkCompanyList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyMarkCompanyList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 我的收藏 - 供求
+ (NSString *)getMyMarkSupplyDemandList {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyMarkSupplyDemandList" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

// 我的发布 - 产品
+ (NSString *)getMyProductList:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyProductList" forKey:@"action"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 我的发布 - 供需列表
+ (NSString *)getMySupplyDemandList:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMySupplyDemandList" forKey:@"action"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}


#pragma mark - 发布供求
// 发布供求 - 发布求购
+ (NSDictionary *)addDemand:(NSString *)title content:(NSString *)content location:(NSString *)location {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addDemand" forKey:@"action"];
    [paramsDic setValue:title forKey:@"title"];
    [paramsDic setValue:content forKey:@"content"];
    [paramsDic setValue:location forKey:@"location"];

    return paramsDic;
}

// 发布供求 - 发布悬赏
+ (NSDictionary *)addPaidDemand:(NSString *)title content:(NSString *)content location:(NSString *)location price:(NSString *)price expired:(NSString *)expired {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addPaidDemand" forKey:@"action"];
    [paramsDic setValue:title forKey:@"title"];
    [paramsDic setValue:content forKey:@"content"];
    [paramsDic setValue:location forKey:@"location"];
    // 要传分
    NSNumber *cent = [NSNumber numberWithInteger:(price.integerValue * 100)];
    [paramsDic setValue:[NSString stringWithFormat:@"%@", cent] forKey:@"price"];
    [paramsDic setValue:expired forKey:@"expired"];
    
    return paramsDic;
}
// 发布供求 - 发布供应
+ (NSDictionary *)addSupply:(NSString *)title content:(NSString *)content location:(NSString *)location {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addSupply" forKey:@"action"];
    [paramsDic setValue:title forKey:@"title"];
    [paramsDic setValue:content forKey:@"content"];
    [paramsDic setValue:location forKey:@"location"];
    
    return paramsDic;
}
// 发布悬赏限制
+ (NSString *)getLimitRangeForPaidDemand {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getLimitRangeForPaidDemand" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 发布产品
// 发布产品
+ (NSDictionary *)addProduct:(NSString *)title description:(NSString *)description price:(NSString *)price amount:(NSString *)amount type:(NSString *)type subtype:(NSString *)subtype tag:(NSString *)tag recommend:(NSString *)recommend priceUnit:(NSString *)priceUnit {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addProduct" forKey:@"action"];
    [paramsDic setValue:title forKey:@"title"];
    [paramsDic setValue:description forKey:@"description"];
    // 要传分
    NSNumber *cent = [NSNumber numberWithInteger:(price.integerValue * 100)];
    [paramsDic setValue:cent forKey:@"price"];
    [paramsDic setValue:amount forKey:@"amount"];
    [paramsDic setValue:type forKey:@"type"];
    [paramsDic setValue:subtype forKey:@"subtype"];
    [paramsDic setValue:tag forKey:@"tag"];
    [paramsDic setValue:recommend forKey:@"recommend"];
    [paramsDic setValue:priceUnit forKey:@"priceUnit"];
    
    return paramsDic;
}

#pragma mark - 订单
// 样品下单
+ (NSString *)addSampleProductOrder:(NSString *)postAddressId payType:(NSString *)payType buyerRemark:(NSString *)buyerRemark products:(NSString *)products {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"addSampleProductOrder" forKey:@"action"];
    [paramsDic setValue:postAddressId forKey:@"postAddressId"];
    [paramsDic setValue:payType forKey:@"payType"];
    [paramsDic setValue:buyerRemark forKey:@"buyerRemark"];
    [paramsDic setValue:products forKey:@"products"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 支付订单
+ (NSString *)payOrder:(NSString *)orderNumber payType:(NSString *)payType {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"payOrder" forKey:@"action"];
    [paramsDic setValue:orderNumber forKey:@"orderNumber"];
    [paramsDic setValue:payType forKey:@"payType"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 微聊
// 搜索用户，添加好友用
+ (NSString *)getUserListByQuery:(NSString *)query pageIndex:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getUserListByQuery" forKey:@"action"];
    [paramsDic setValue:query forKey:@"query"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 批量获取用户信息，如群组详情中成员的头像、昵称获取
+ (NSString *)getUsersByUserNames:(NSString *)userNames {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getUsersByUserNames" forKey:@"action"];
    [paramsDic setValue:userNames forKey:@"userNames"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 客服信息
+ (NSString *)getKefuInfo {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getKefuInfo" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - 通知
// 通知列表
+ (NSString *)getMyMessageList:(NSString *)pageIndex {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyMessageList" forKey:@"action"];
    [paramsDic setValue:pageIndex forKey:@"pageIndex"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}
// 通知详情
+ (NSString *)getMyMessageInfo:(NSString *)messageId {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getMyMessageInfo" forKey:@"action"];
    [paramsDic setValue:messageId forKey:@"messageId"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

#pragma mark - VIP控制开关
+ (NSString *)getCtrlInfo {
    // 构造参数
    NSMutableDictionary *paramsDic = [[NSMutableDictionary alloc] init];
    [paramsDic setValue:@"getCtrlInfo" forKey:@"action"];
    
    // 转化成string
    return [ZbWebService getParamsString:paramsDic];
}

@end
