//
//  CommonCode.h
//  CommonCode
//
//  Created by Pop Web on 15/1/8.
//  Copyright (c) 2015年 黄旺鑫. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//------------------------系统版本判断-----------------------------

// 系统版本是否大于ios6
#define Greater_Than_IOS6 [CommonCode greaterThanIOS6]

// 判断是否是ios8
#define IS_IOS_8 [CommonCode isIOS8]

//判断是否是IPad
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

//判断是否是iPhone
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

// 当前系统版本
#define CurrentSystemVersion [CommonCode getCurrentSystemVersion]


//------------------------获取设备大小-----------------------------
//设备的Bounds
#define DeviceBounds [CommonCode getDeviceBounds]

///设备的宽度
#define DeviceWidth CGRectGetWidth(DeviceBounds)

//设备的高度
#define DeviceHeight CGRectGetHeight(DeviceBounds)

//判断屏幕是否大于3.5英寸
#define Greater_Than_480_Height (DeviceHeight > 480)

//判断屏幕是否大于3.5英寸 大于则返回a
#define Greater_Than_480H(a, b) DeviceHeight > 480 ? a : b
//------------------------可变对象初始化-----------------------------

//初始化可变字典
#define Mutable_DictionaryInit [NSMutableDictionary dictionaryWithCapacity:5]

//初始化可变集合
#define Mutable_SetInit [NSMutableSet setWithCapacity:5]

//初始化可变数组
#define Mutable_ArrayInit [NSMutableArray arrayWithCapacity:5]

/**
 *  @brief 将NSData转换成字符串
 *
 *  @param data NSData对象
 *
 *  @return 返回字符串
 */
#define NSStringFromData(data) [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]

///NSUserDefaults实例
#define User_Defaults [NSUserDefaults standardUserDefaults]

//NSFileManager实例
#define FileManager [NSFileManager defaultManager]

//------------------------打印日志-----------------------------
//DEBUG  模式下打印日志,当前行
#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif


//重写NSLog,Debug模式下打印日志和当前行数
#define DEBUG_NSLog(FORMAT, ...) fprintf(stderr,"\n----------------------------------\n\nfunction:%s line:%d content:%s\n\n----------------------------------\n\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


//DEBUG  模式下打印日志,当前行 并弹出一个警告
#ifdef DEBUG
#   define ULog(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }
#else
#   define ULog(...)
#endif

//------------------------颜色-----------------------------
/**
 *  @brief Hex转换成UIColor对象
 *
 *  @param string Hex如@"#00B9FF"
 *
 *  @return UIColor实例
 */
#define UIColorFromHexString(string) [CommonCode colorWithHexString:string]

/**
 *  RGB值
 *
 *  @param R 值0-255
 *  @param G 值0-255
 *  @param B 值0-255
 *  @param A 值0-1
 *
 *  @return UIColor对象
 */
#define ColorWithRGBA(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]

#define UIColorWithRGB(r, g, b)  ColorWithRGBA(r, g, b, 1.0f)

//------------------------图片-----------------------------
//图片对象
#define ImageWithName(imageName) [UIImage imageNamed:imageName]

//------------------------本地文件地址-----------------------------
/**
 *  获取NSBundle文件地址
 *
 *  @param fileName 文件名称
 *  @param type     文件类型
 *
 *  @return 返回文件地址字符串
 */
#define BundleFilePath(fileName, type) [[NSBundle mainBundle] pathForResource:fileName ofType:type]
#define BundleFileURL(fileName, type)  [[NSBundle mainBundle] URLForResource:fileName withExtension:type]

//------------------------线程-----------------------------
//定义代码块类型
#define Async(...) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ __VA_ARGS__ })
#define Async_Main(...) dispatch_async(dispatch_get_main_queue(), ^{ __VA_ARGS__ })

//------------------------网络请求-----------------------------
//将字符串转化成NSURL
#define URLWithString(urlStr) [NSURL URLWithString:urlStr]

//将URL转化为NSURLRequest
#define RequestWithURL(url) [NSURLRequest requestWithURL:url]

//字符串生成NSURLRequest
#define RequestWithString(urlStr) RequestWithURL(URLWithString(urlStr))

//------------------------其他-----------------------------
//从字典获取对象
#define ObjectWithKey(dictionary, key) [dictionary valueForKey:key]
#define ObjectWithKeyPath(dictionary, key) [dictionary valueForKeyPath:key]
//创建按钮
#define ButtonInit [UIButton buttonWithType:UIButtonTypeCustom]
//判断设备是否横屏
#define UIInterfaceOrientationIsPortrait(orientation)  ((orientation) == UIInterfaceOrientationPortrait || (orientation) == UIInterfaceOrientationPortraitUpsideDown)
#define UIInterfaceOrientationIsLandscape(orientation) ((orientation) == UIInterfaceOrientationLandscapeLeft || (orientation) == UIInterfaceOrientationLandscapeRight)
///判空 
#define isEmptyString(string) string == nil ? @"" : string

#define RectLog(rect)  NSLog(@"orige = {%f,%f},size={%f,%f}",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height)

//--------------------------------------------------------

@interface CommonCode : NSObject

/**
 *  @brief 获取当前系统版本
 *
 */
+ (NSString *)getCurrentSystemVersion;

/**
 *  @brief 区分IOS6以上的版本
 *
 *  @return 大于IOS6返回真
 */
+ (BOOL)greaterThanIOS6;

/**
 *  @brief 判断是IOS8
 *
 */
+ (BOOL)isIOS8;

/**
 *  @brief 获取设备屏幕的大小
 *
 */
+ (CGRect)getDeviceBounds;

/**
 *  @brief 计算文本所需的高度或宽度
 *
 *  @param string  文本内容
 *  @param font    显示的字体
 *  @param maxSize 最大宽度或者最大高度， 需要准确值  比如计算高度，CGSizeMake(280, 9999), 宽度准确值， 高度要比较大
 *
 */
+ (CGSize)calculateSizeWithString:(NSString *)string andFont:(UIFont *)font andMaxSize:(CGSize)maxSize;

/**
 *  @brief 获取设备型号
 *
 *  @return 返回如 iPhone 5s (A1453/A1533)
 */
+ (NSString *)deviceModelString;

/**
 *  Hex生成Color对象
 *
 *  @param stringToConvert 传入如26BFFC或者#FFB25C
 *
 *  @return Color对象
 */
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;

/**
 *  绘制纯色图片
 *
 *  @param color 颜色对象
 *  @param size  图片大小
 *
 *  @return 纯色图片
 */
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

/**
 *  MD5加密
 *
 *  @param string 要加密的字符串
 *  @param len    加密位数  注意， 需要32位的字符串，可能是传入16
 *
 */
+ (NSString *)md5WithStr:(NSString *)string andDigestLength:(NSInteger)len;

/**
 *  清理tmp目录下超过两天的缓存
 */
+ (void)clearTempCache;

/**
 *  获取tmp目录的大小
 */
+ (NSUInteger)getTmpCacheSize;

/**
 *  清理tmp目录下所有文件
 */
+ (void)clearTempAllFile;

/**
 *  字数统计
 *
 *  @param string 要统计的字符串
 *
 *  @return 返回字数
 */
+ (int)countWord:(NSString *)string;

/**
 *  去掉一些字符，防止json解析不正确
 *
 *  @param jsonString json字符串
 *
 */
+ (NSString *)removeIllegalCharactersWithString:(NSString *)jsonString;

// 获取日期显示
+ (NSString *)getTimeShowText:(NSString *)timeStr;

// 是否手机号
+ (BOOL)isPhoneNumber:(NSString *)tel;

// 是否由字母和数字混合组成
+ (BOOL)isMakeByCharAndNumber:(NSString *)str;

// 是否由字母组成
+ (BOOL)isMakeByChar:(NSString *)str;

// 是否由数字组成
+ (BOOL)isMakeByNumber:(NSString *)str;

// 是否由字母或数字组成
+ (BOOL)isMakeByCharOrNumber:(NSString *)str;

// 数据列表转成索引选择器所需数组
+ (NSMutableArray *)sortByFirstCharact:(NSArray *)dataArray;

// 判断字符串为空，剔除左右空格
+ (BOOL)isStringEmpty:(NSString *)str;

@end

/*
//实现工厂方法，返回单一对象
+ (instancetype)shareInstance {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        detailsViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"detailsView"];
    });
    
    return detailsViewController;
}

//防止通过alloc或new创建新的实例
+ (id)allocWithZone:(struct _NSZone *)zone {
    @synchronized(self) {
        if (detailsViewController == nil) {
            detailsViewController = [[super allocWithZone:zone] init];
        }
    }
    
    return detailsViewController;
}

+ (instancetype)alloc {
    return [self allocWithZone:nil];
}
*/

