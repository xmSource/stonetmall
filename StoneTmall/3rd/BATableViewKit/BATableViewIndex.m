//
//  ABELTableViewIndex.m
//  ABELTableViewDemo
//
//  Created by abel on 14-4-28.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import "BATableViewIndex.h"
#import "CommonHeader.h"

#if !__has_feature(objc_arc)
#error AIMTableViewIndexBar must be built with ARC.
// You can turn on ARC for only AIMTableViewIndexBar files by adding -fobjc-arc to the build phase for each of its files.
#endif

#define RGB(r,g,b,a)  [UIColor colorWithRed:(double)r/255.0f green:(double)g/255.0f blue:(double)b/255.0f alpha:a]

@interface BATableViewIndex (){
    BOOL isLayedOut;
    CAShapeLayer *shapeLayer;
    CGFloat letterHeight;
    CGFloat letterWidth;
}

@property (nonatomic, strong) NSArray *letters;
@property (nonatomic, strong) NSMutableArray *lettersLayers;
@property (nonatomic, weak) CATextLayer *lastSelectLayer;

@end


@implementation BATableViewIndex

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.lettersLayers = [NSMutableArray array];
        letterWidth = CGRectGetWidth(frame);
    }
    return self;
}

- (void)setup{
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.lineWidth = 1.0f;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.lineJoin = kCALineCapSquare;
    shapeLayer.strokeColor = [[UIColor clearColor] CGColor];
    shapeLayer.strokeEnd = 1.0f;
    self.layer.masksToBounds = NO;
}

- (void)setTableViewIndexDelegate:(id<BATableViewIndexDelegate>)tableViewIndexDelegate
{
    _tableViewIndexDelegate = tableViewIndexDelegate;
    self.letters = [self.tableViewIndexDelegate tableViewIndexTitle:self];
    isLayedOut = NO;
    [self layoutSubviews];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self setup];
    
    if (!isLayedOut){
        [self.lettersLayers removeAllObjects];
        self.lastSelectLayer = nil;
        [self.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
        
        shapeLayer.frame = (CGRect) {.origin = CGPointZero, .size = self.layer.frame.size};
//        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
//        [bezierPath moveToPoint:CGPointZero];
//        [bezierPath addLineToPoint:CGPointMake(0, self.frame.size.height)];
        letterHeight = 16; //self.frame.size.height / [letters count];
        CGFloat fontSize = 12;
        if (letterHeight < 14){
            fontSize = 11;
        }
        
        [self.letters enumerateObjectsUsingBlock:^(NSString *letter, NSUInteger idx, BOOL *stop) {
            CGFloat originY = idx * letterHeight;
            CATextLayer *ctl = [self textLayerWithSize:fontSize
                                                string:letter
                                              andFrame:CGRectMake(0, originY, letterWidth, letterHeight)];
            if (self.lastSelectLayer == nil) {
                self.lastSelectLayer = ctl;
//                self.lastSelectLayer.cornerRadius = self.lastSelectLayer.bounds.size.height / 2;
//                self.lastSelectLayer.backgroundColor = MAIN_COLOR.CGColor;
            }
            [self.layer addSublayer:ctl];
            [self.lettersLayers addObject:ctl];
//            [bezierPath moveToPoint:CGPointMake(0, originY)];
//            [bezierPath addLineToPoint:CGPointMake(ctl.frame.size.width, originY)];
        }];
        
//        shapeLayer.path = bezierPath.CGPath;
        [self.layer addSublayer:shapeLayer];
        
        isLayedOut = YES;
    }
}

- (CATextLayer*)textLayerWithSize:(CGFloat)size string:(NSString*)string andFrame:(CGRect)frame{
    CATextLayer *tl = [CATextLayer layer];
    [tl setFont:@"ArialMT"];
    [tl setFontSize:size];
    [tl setFrame:frame];
    [tl setAlignmentMode:kCAAlignmentCenter];
    [tl setContentsScale:[[UIScreen mainScreen] scale]];
    [tl setForegroundColor:MAIN_COLOR.CGColor];
    [tl setString:string];
    return tl;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
	[self sendEventToDelegate:event];
    [self.tableViewIndexDelegate tableViewIndexTouchesBegan:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesMoved:touches withEvent:event];
	[self sendEventToDelegate:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.tableViewIndexDelegate tableViewIndexTouchesEnd:self];
}

- (void)sendEventToDelegate:(UIEvent*)event{
    UITouch *touch = [[event allTouches] anyObject];
	CGPoint point = [touch locationInView:self];
    
    NSInteger indx = ((NSInteger) floorf(point.y) / letterHeight);
    
    if (indx< 0 || indx > self.letters.count - 1) {
        return;
    }
    
    // 清除上次选中
    if (self.lastSelectLayer != nil) {
//        self.lastSelectLayer.cornerRadius = 0;
//        self.lastSelectLayer.backgroundColor = [UIColor whiteColor].CGColor;
    }
    
    // 设置本次选中
    self.lastSelectLayer = [self.lettersLayers objectAtIndex:indx];
//    self.lastSelectLayer.cornerRadius = self.lastSelectLayer.bounds.size.height / 2;
//    self.lastSelectLayer.backgroundColor = MAIN_COLOR.CGColor;
    
    [self.tableViewIndexDelegate tableViewIndex:self didSelectSectionAtIndex:indx withTitle:self.letters[indx]];
}

@end
