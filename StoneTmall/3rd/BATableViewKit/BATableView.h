//
//  ABELTableView.h
//  ABELTableViewDemo
//
//  Created by abel on 14-4-28.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BATableViewDelegate;

@interface BATableView : NSObject

@property (nonatomic, weak) UITableView * tableView;
@property (nonatomic, strong) id<BATableViewDelegate> delegate;

- (id)initWithTableView:(UITableView *)tv frame:(CGRect)tvFrame;
- (void)reloadData;

@end

@protocol BATableViewDelegate <UITableViewDataSource,UITableViewDelegate>

- (NSArray *)sectionIndexTitlesForABELTableView:(BATableView *)tableView;


@end
