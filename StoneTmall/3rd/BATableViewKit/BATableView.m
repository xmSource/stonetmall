//
//  ABELTableView.m
//  ABELTableViewDemo
//
//  Created by abel on 14-4-28.
//  Copyright (c) 2014年 abel. All rights reserved.
//

#import "BATableView.h"
#import "BATableViewIndex.h"
#import "CommonHeader.h"

@interface BATableView() <BATableViewIndexDelegate>

@property (nonatomic, strong) UILabel * flotageLabel;
@property (nonatomic, strong) BATableViewIndex * tableViewIndex;
// 索引显示区域
@property (nonatomic, assign) CGRect showFrame;

@end

@implementation BATableView

- (id)initWithTableView:(UITableView *)tv frame:(CGRect)showFrame {
    self = [super init];
    if (self) {
        // Initialization code
        
//        self.tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
//        self.tableView.showsVerticalScrollIndicator = NO;
//        self.tableView.sectionIndexBackgroundColor = [UIColor colorWithRed:64/255.0 green:170/255.0 blue:83/255.0 alpha:1];
//        self.tableView.sectionIndexColor = [UIColor colorWithRed:64/255.0 green:170/255.0 blue:83/255.0 alpha:1];
//
//        [self addSubview:self.tableView];
        
//        self.backgroundColor = [UIColor clearColor];
//        self.userInteractionEnabled = NO;
        self.tableView = tv;
        self.showFrame = showFrame;
        
        // 背景栏
        UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(showFrame.size.width - 25, showFrame.origin.y, 25, showFrame.size.height)];
        bg.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
        [self.tableView.superview addSubview:bg];
        
        self.tableViewIndex = [[BATableViewIndex alloc] initWithFrame:(CGRect){showFrame.size.width - 25, showFrame.origin.y, 25, showFrame.size.height}];
        [self.tableView.superview addSubview:self.tableViewIndex];
        
        self.flotageLabel = [[UILabel alloc] initWithFrame:(CGRect){(showFrame.size.width - 64 ) / 2,(showFrame.size.height - 64) / 2, 64, 64}];
        self.flotageLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        self.flotageLabel.layer.cornerRadius = 5;
        self.flotageLabel.layer.masksToBounds = YES;
        self.flotageLabel.hidden = YES;
        self.flotageLabel.textAlignment = NSTextAlignmentCenter;
        self.flotageLabel.textColor = MAIN_COLOR;
        [self.tableView.superview addSubview:self.flotageLabel];
    }
    return self;
}

- (void)setDelegate:(id<BATableViewDelegate>)delegate
{
    _delegate = delegate;
//    self.tableView.delegate = delegate;
//    self.tableView.dataSource = delegate;
    self.tableViewIndex.indexes = [self.delegate sectionIndexTitlesForABELTableView:self];
    CGRect rect = self.tableViewIndex.frame;
    rect.size.height = self.tableViewIndex.indexes.count * 16;
    rect.origin.y = self.showFrame.origin.y + (self.showFrame.size.height - rect.size.height) / 2;
    self.tableViewIndex.frame = rect;
    
    self.tableViewIndex.tableViewIndexDelegate = self;
}

- (void)reloadData
{
    [self.tableView reloadData];
    
    UIEdgeInsets edgeInsets = self.tableView.contentInset;
    
    self.tableViewIndex.indexes = [self.delegate sectionIndexTitlesForABELTableView:self];
    CGRect rect = self.tableViewIndex.frame;
    rect.size.height = self.tableViewIndex.indexes.count * 16;
    rect.origin.y = self.showFrame.origin.y + (self.showFrame.size.height - rect.size.height - edgeInsets.top - edgeInsets.bottom) / 2 + edgeInsets.top;
    self.tableViewIndex.frame = rect;
    self.tableViewIndex.tableViewIndexDelegate = self;
}


#pragma mark - BATableViewIndex delegate
- (void)tableViewIndex:(BATableViewIndex *)tableViewIndex didSelectSectionAtIndex:(NSInteger)index withTitle:(NSString *)title
{
    if ([self.tableView numberOfSections] > index && index > -1){   // for safety, should always be YES
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:NO];
        self.flotageLabel.text = title;
    }
}

- (void)tableViewIndexTouchesBegan:(BATableViewIndex *)tableViewIndex
{
    self.flotageLabel.hidden = NO;
}

- (void)tableViewIndexTouchesEnd:(BATableViewIndex *)tableViewIndex
{
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [self.flotageLabel.layer addAnimation:animation forKey:nil];
    
    self.flotageLabel.hidden = YES;
}

- (NSArray *)tableViewIndexTitle:(BATableViewIndex *)tableViewIndex
{
    return [self.delegate sectionIndexTitlesForABELTableView:self];
}
@end
