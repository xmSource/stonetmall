//
//  AppDelegate.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/15.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "AppDelegate.h"
#import "CommonHeader.h"
#import <ShareSDK/ShareSDK.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WXApi.h"
#import "LocationServiceManager.h"
#import <AlipaySDK/AlipaySDK.h>
#import "EaseMob.h"
#import "AppDelegate+EaseMob.h"
#import "XGPush.h"
#import "ZbTabViewController.h"
#import "BNCoreServices.h"

@interface AppDelegate () <WXApiDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // 开启接口日志
//    [CTURLConnection setNeedsLog:YES];
    
    // 电池栏白色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // 去除导航栏底部线条
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    // barButtonItem字号
    NSDictionary* textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont systemFontOfSize:14], NSFontAttributeName,
                                    nil];
    [[UIBarButtonItem appearance] setTitleTextAttributes:textAttributes forState:0];
    // 去除tabbar顶部线条
    [[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    //初始化导航SDK
    [BNCoreServices_Instance initServices: @"41FKxiXejSyQT24k9thP1U7z"];
    [BNCoreServices_Instance startServicesAsyn:^(void) {
//        NSLog(@"导航启动success");
    } fail:^(void) {
//        NSLog(@"导航启动fail");
    }];
    // 启动百度地图
    _mapManager = [[BMKMapManager alloc]init];
    BOOL ret = [_mapManager start:@"41FKxiXejSyQT24k9thP1U7z"  generalDelegate:nil];
    if (!ret) {
        DLog(@"manager start failed!");
    } else {
        DLog(@"manager start success!");
    }
    // 启动定位服务
    [[LocationServiceManager shareManager] startLocationService];
    
    // ShareSDK注册
    [ShareSDK registerApp:@"b1a61dcaf704"];//字符串api20为您的ShareSDK的AppKey
    // 添加QQ应用  注册网址
    [ShareSDK connectQQWithAppId:@"1101130082" qqApiCls:[QQApiInterface class]];
    // 微信登陆的时候需要初始化
    [ShareSDK connectWeChatWithAppId:WX_URL_SCHEME
                           appSecret:@"3bf65fb084a732b9edd8a7dfaeee2fdc"
                           wechatCls:[WXApi class]];
    //连接短信分享
    [ShareSDK connectSMS];
    
    // 微信注册，微信登录走原生
    [WXApi registerApp:WX_URL_SCHEME];
    
    // 初始化环信SDK，详细内容在AppDelegate+EaseMob.m 文件中
    [self easemobApplication:application didFinishLaunchingWithOptions:launchOptions];
    
    // 信鸽推送
    [XGPush startApp:2200157171 appKey:@"IVYP53S351SP"];
    [XGPush handleLaunching: launchOptions];
    
    // 注册请求登录通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationRequestLogin:) name:NOTIFICATION_REQUEST_LOGIN object:nil];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
//    [BMKMapView willBackGround];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[EaseMob sharedInstance] applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[EaseMob sharedInstance] applicationWillEnterForeground:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //[BMKMapView didForeGround];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[EaseMob sharedInstance] applicationWillTerminate:application];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    // shareSDK先处理
    BOOL result = [ShareSDK handleOpenURL:url
                               wxDelegate:self];
    if (result) {
        return result;
    }
    
    // 微信先处理
    result = [WXApi handleOpenURL:url delegate:self];
    if (result) {
        return YES;
    }
    
    // 否则支付宝处理
    //跳转支付宝钱包进行支付，处理支付结果
    [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
        DLog(@"appDelegate alipay result = %@",resultDic);
    }];
    return YES;
}

#pragma mark - 推送
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [XGPush handleReceiveNotification:userInfo];
    if (application.applicationState == UIApplicationStateActive) {
        // 正在运行不管
    } else {
        // 后台运行点击推送打开
        NSString *pushType = userInfo[@"push_type"];
        if (pushType != nil) {
            // 信鸽约定的推送
            UIViewController *root = self.window.rootViewController;
            if ([root isKindOfClass:[UINavigationController class]]) {
                UINavigationController *rootNav = (UINavigationController *)root;
                if (rootNav) {
                    ZbTabViewController *mainTap = (ZbTabViewController *)rootNav.viewControllers.firstObject;
                    if (mainTap) {
                        [mainTap handleAppNotification:userInfo];
                    }
                }
            }
        }
    }
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
}


#pragma mark - WXApi delegate
- (void)onReq:(BaseReq*)req {
    if ([req isKindOfClass:[ShowMessageFromWXReq class]]) {
        // 微信让显示信息
        ShowMessageFromWXReq *wxReq = (ShowMessageFromWXReq *)req;
        WXAppExtendObject *extObject = wxReq.message.mediaObject;
        NSString *extInfo = extObject.extInfo;
        
        // 解析json
        NSError *error;
        NSDictionary *infoDic = [NSJSONSerialization JSONObjectWithData:[extInfo dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
        if (error != nil) {
            DLog(@"ShowMessageFromWXReq extInfo Parser error:%@", error);
            return;
        }
        
        // 通知
        [ZbSaveManager setClickShareDict:infoDic];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_WX_SHOW_MESSAGE object:infoDic];
    }
    return;
}

- (void)onResp:(BaseResp*)resp {
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *authResq = (SendAuthResp *)resp;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GET_WX_CODE_SUCCESS object:authResq];
    }
    if ([resp isKindOfClass:[PayResp class]]){
        PayResp *response = (PayResp*)resp;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_WX_PAY_RESULT object:response];
    }
    return;
}

#pragma mark - 通知
// 请求登录通知
- (void)notificationRequestLogin:(NSNotification *)nof {    UIStoryboard *loginSb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UINavigationController *nav = [loginSb instantiateViewControllerWithIdentifier:@"LoginNav"];
    [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
    return;
}

#pragma mark - public
// 清空登录信息
- (void)clearLoginInfo {
    [ZbWebService clearCookie];
    [ZbWebService setUser:nil];
    [ZbWebService setQy:nil];;
    [ZbSaveManager clearShoppingCart];
    [ZbSaveManager clearUserInfo];
}

// 退出登录
- (void)logOut {
    [self clearLoginInfo];
    UIStoryboard *loginSb = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
    UINavigationController *nav = [loginSb instantiateViewControllerWithIdentifier:@"LoginNav"];
    [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
    return;
}

// 广告显示结束
- (void)onAdShowOk {
    UIStoryboard *mainSb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nav = [mainSb instantiateViewControllerWithIdentifier:@"MainNav"];
    self.window.rootViewController = nav;
}

@end
