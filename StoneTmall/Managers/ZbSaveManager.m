//
//  ZbSaveManager.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/29.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSaveManager.h"

// 全局记录上限
#define MAX_GLOBAL_SEARCH_RECORD_COUNT 50
// 单项记录上限
#define MAX_SINGLE_SEARCH_RECORD_COUNT 50
// 超过上限后删除条数
#define RECORD_DELETE_COUNT 5

// 全局整体搜索历史记录保存key
static NSString * const GLOBAL_SEARCH_HISTORY_SAVE_KEY = @"GLOBAL_SEARCH_HISTORY_SAVE_KEY";
// 全局石材搜索历史记录保存key
static NSString * const GLOBAL_STONE_SEARCH_HISTORY_SAVE_KEY = @"GLOBAL_STONE_SEARCH_HISTORY_SAVE_KEY";
// 全局产品搜索历史记录保存key
static NSString * const GLOBAL_PRODUCT_SEARCH_HISTORY_SAVE_KEY = @"GLOBAL_PRODUCT_SEARCH_HISTORY_SAVE_KEY";
// 全局企业搜索历史记录保存key
static NSString * const GLOBAL_QY_SEARCH_HISTORY_SAVE_KEY = @"GLOBAL_QY_SEARCH_HISTORY_SAVE_KEY";
// 全局供需搜索历史记录保存key
static NSString * const GLOBAL_SANDD_SEARCH_HISTORY_SAVE_KEY = @"GLOBAL_SANDD_SEARCH_HISTORY_SAVE_KEY";

// 热门产品分类保存key
static NSString * const HOT_PRODUCT_TYPE_SAVE_KEY = @"HOT_PRODUCT_TYPE_SAVE_KEY";
// 热门企业分类保存key
static NSString * const HOT_COMPANY_TYPE_SAVE_KEY = @"HOT_COMPANY_TYPE_SAVE_KEY";
// 热门石材分类保存key
static NSString * const HOT_STONE_TYPE_SAVE_KEY = @"HOT_STONE_TYPE_SAVE_KEY";

// 管理器实例
static ZbSaveManager *manager = nil;

// 石材类别数组
static NSArray *STONE_TYPE_ARRAY = nil;
// 石材颜色数组
static NSArray *STONE_COLOR_ARRAY = nil;
// 石材纹理数组
static NSArray *STONE_TEXTURE_ARRAY = nil;
// 企业服务类型数组
static NSArray *QY_TYPE_ARRAY = nil;
// 区域数组
static NSArray *COMMON_AREA_ARRAY = nil;
// 产品类别数组
static NSArray *PRODUCT_TYPE_ARRAY = nil;
// 产品子类别数组
static NSDictionary *PRODUCT_SUB_TYPE_DICT = nil;
// 省份类别数组
static NSArray *PROV_TYPE_ARRAY = nil;
// 城市子类别数组
static NSDictionary *CITY_SUB_TYPE_DICT = nil;

// 购物车数组
static NSMutableArray *SHOPPING_CART_ARRAY = nil;

// 微聊用户信息字典
static NSMutableDictionary *USER_INFO_DICT = nil;

// 客服信息字典
static NSDictionary *KEFU_INFO_DICT = nil;

// 价格单位数组
static NSArray *PRICE_UNIT_ARRAY = nil;

// 点击的闪屏广告
static NSDictionary *CLICK_AD_DICT = nil;

// 点击的推送
static NSDictionary *CLICK_NOTIFICATION = nil;

// 点击的分享
static NSDictionary *CLICK_SHARE_DICT = nil;

// vip开关控制数据
static NSDictionary *CTRL_DICT = nil;

@implementation ZbSaveManager

+ (instancetype)shareManager {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ZbSaveManager alloc] init];
        [manager onInit];
    });
    
    return manager;
}

// 初始化信息
- (void)onInit {
    return;
}

#pragma mark - 全局整体搜索记录
// 添加全局整体搜索记录
- (void)addGlobalSearchHistory:(NSString *)key {
    NSArray *array = [self getGlobalSearchHistory];
    NSMutableArray *tagArray = [NSMutableArray arrayWithArray:array];
    
    //  删除重复记录
    __block NSInteger deleteAddrIndex = -1;
    [tagArray enumerateObjectsUsingBlock:^(NSString *searchName, NSUInteger index, BOOL *stop) {
        if ([searchName isEqualToString:key]) {
            deleteAddrIndex = index;
            *stop = YES;
        }
    }];
    if (deleteAddrIndex != -1) {
        [tagArray removeObjectAtIndex:deleteAddrIndex];
    }
    
    // 超过保存上限
    if (tagArray.count > MAX_GLOBAL_SEARCH_RECORD_COUNT) {
        // 删除若干条
        [tagArray removeObjectsInRange:NSMakeRange(tagArray.count - RECORD_DELETE_COUNT, RECORD_DELETE_COUNT)];
    }
    
    // 添加新记录
    [tagArray insertObject:key atIndex:0];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:tagArray] forKey:GLOBAL_SEARCH_HISTORY_SAVE_KEY];
    [userDefault synchronize];
}
// 获取全局整体搜索记录
- (NSArray *)getGlobalSearchHistory {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:GLOBAL_SEARCH_HISTORY_SAVE_KEY];
}

#pragma mark - 全局石材搜索记录
// 添加全局石材搜索记录
- (void)addGlobalStoneSearchHistory:(NSString *)key {
    NSArray *array = [self getGlobalStoneSearchHistory];
    NSMutableArray *tagArray = [NSMutableArray arrayWithArray:array];
    
    //  删除重复记录
    __block NSInteger deleteAddrIndex = -1;
    [tagArray enumerateObjectsUsingBlock:^(NSString *searchName, NSUInteger index, BOOL *stop) {
        if ([searchName isEqualToString:key]) {
            deleteAddrIndex = index;
            *stop = YES;
        }
    }];
    if (deleteAddrIndex != -1) {
        [tagArray removeObjectAtIndex:deleteAddrIndex];
    }
    
    // 超过保存上限
    if (tagArray.count > MAX_SINGLE_SEARCH_RECORD_COUNT) {
        // 删除若干条
        [tagArray removeObjectsInRange:NSMakeRange(tagArray.count - RECORD_DELETE_COUNT, RECORD_DELETE_COUNT)];
    }
    
    // 添加新记录
    [tagArray insertObject:key atIndex:0];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:tagArray] forKey:GLOBAL_STONE_SEARCH_HISTORY_SAVE_KEY];
    [userDefault synchronize];
}
// 获取全局石材搜索记录
- (NSArray *)getGlobalStoneSearchHistory {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:GLOBAL_STONE_SEARCH_HISTORY_SAVE_KEY];
}

#pragma mark - 全局产品搜索记录
// 添加全局产品搜索记录
- (void)addGlobalProductSearchHistory:(NSString *)key {
    NSArray *array = [self getGlobalProductSearchHistory];
    NSMutableArray *tagArray = [NSMutableArray arrayWithArray:array];
    
    //  删除重复记录
    __block NSInteger deleteAddrIndex = -1;
    [tagArray enumerateObjectsUsingBlock:^(NSString *searchName, NSUInteger index, BOOL *stop) {
        if ([searchName isEqualToString:key]) {
            deleteAddrIndex = index;
            *stop = YES;
        }
    }];
    if (deleteAddrIndex != -1) {
        [tagArray removeObjectAtIndex:deleteAddrIndex];
    }
    
    // 超过保存上限
    if (tagArray.count > MAX_SINGLE_SEARCH_RECORD_COUNT) {
        // 删除若干条
        [tagArray removeObjectsInRange:NSMakeRange(tagArray.count - RECORD_DELETE_COUNT, RECORD_DELETE_COUNT)];
    }
    
    // 添加新记录
    [tagArray insertObject:key atIndex:0];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:tagArray] forKey:GLOBAL_PRODUCT_SEARCH_HISTORY_SAVE_KEY];
    [userDefault synchronize];
}
// 获取全局产品搜索记录
- (NSArray *)getGlobalProductSearchHistory {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:GLOBAL_PRODUCT_SEARCH_HISTORY_SAVE_KEY];
}

#pragma mark - 全局企业搜索记录
// 添加全局企业搜索记录
- (void)addGlobalQySearchHistory:(NSString *)key {
    NSArray *array = [self getGlobalQySearchHistory];
    NSMutableArray *tagArray = [NSMutableArray arrayWithArray:array];
    
    //  删除重复记录
    __block NSInteger deleteAddrIndex = -1;
    [tagArray enumerateObjectsUsingBlock:^(NSString *searchName, NSUInteger index, BOOL *stop) {
        if ([searchName isEqualToString:key]) {
            deleteAddrIndex = index;
            *stop = YES;
        }
    }];
    if (deleteAddrIndex != -1) {
        [tagArray removeObjectAtIndex:deleteAddrIndex];
    }
    
    // 超过保存上限
    if (tagArray.count > MAX_SINGLE_SEARCH_RECORD_COUNT) {
        // 删除若干条
        [tagArray removeObjectsInRange:NSMakeRange(tagArray.count - RECORD_DELETE_COUNT, RECORD_DELETE_COUNT)];
    }
    
    // 添加新记录
    [tagArray insertObject:key atIndex:0];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:tagArray] forKey:GLOBAL_QY_SEARCH_HISTORY_SAVE_KEY];
    [userDefault synchronize];
}
// 获取全局企业搜索记录
- (NSArray *)getGlobalQySearchHistory {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:GLOBAL_QY_SEARCH_HISTORY_SAVE_KEY];
}

#pragma mark - 全局供需搜索记录
// 添加全局供需搜索记录
- (void)addGlobalSAndDSearchHistory:(NSString *)key {
    NSArray *array = [self getGlobalSAndDSearchHistory];
    NSMutableArray *tagArray = [NSMutableArray arrayWithArray:array];
    
    //  删除重复记录
    __block NSInteger deleteAddrIndex = -1;
    [tagArray enumerateObjectsUsingBlock:^(NSString *searchName, NSUInteger index, BOOL *stop) {
        if ([searchName isEqualToString:key]) {
            deleteAddrIndex = index;
            *stop = YES;
        }
    }];
    if (deleteAddrIndex != -1) {
        [tagArray removeObjectAtIndex:deleteAddrIndex];
    }
    
    // 超过保存上限
    if (tagArray.count > MAX_SINGLE_SEARCH_RECORD_COUNT) {
        // 删除若干条
        [tagArray removeObjectsInRange:NSMakeRange(tagArray.count - RECORD_DELETE_COUNT, RECORD_DELETE_COUNT)];
    }
    
    // 添加新记录
    [tagArray insertObject:key atIndex:0];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:tagArray] forKey:GLOBAL_SANDD_SEARCH_HISTORY_SAVE_KEY];
    [userDefault synchronize];
}
// 获取全局供需搜索记录
- (NSArray *)getGlobalSAndDSearchHistory {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:GLOBAL_SANDD_SEARCH_HISTORY_SAVE_KEY];
}

#pragma mark - 石材筛选类型
// 设置石材类别数组
- (void)setStoneTypeArray:(NSArray *)array {
    STONE_TYPE_ARRAY = array;
    // 存本地
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:array] forKey:HOT_STONE_TYPE_SAVE_KEY];
    [userDefault synchronize];
}
- (NSArray *)getStoneTypeArray {
    return STONE_TYPE_ARRAY;
}
- (NSArray *)getLocalStoneTypeArray {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSArray *productTypeArray = [userDefault valueForKey:HOT_STONE_TYPE_SAVE_KEY];
    return productTypeArray;
}

// 设置石材颜色数组
- (void)setStoneColorArray:(NSArray *)array {
    STONE_COLOR_ARRAY = array;
}
- (NSArray *)getStoneColorArray {
    return STONE_COLOR_ARRAY;
}

// 设置石材纹理数组
- (void)setStoneTextureArray:(NSArray *)array {
    STONE_TEXTURE_ARRAY = array;
}
- (NSArray *)getStoneTextureArray {
    return STONE_TEXTURE_ARRAY;
}

#pragma mark - 企业筛选类型
// 设置企业服务类型数组
- (void)setQyTypeArray:(NSArray *)array {
    QY_TYPE_ARRAY = array;
    // 存本地
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:array] forKey:HOT_COMPANY_TYPE_SAVE_KEY];
    [userDefault synchronize];
}
- (NSArray *)getQyTypeArray {
    return QY_TYPE_ARRAY;
}
- (NSArray *)getLocalQyTypeArray {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSArray *productTypeArray = [userDefault valueForKey:HOT_COMPANY_TYPE_SAVE_KEY];
    return productTypeArray;
}

// 获取企业默认排序名称数组
- (NSArray *)getQySortTextArray {
    return @[@"默认排序", @"拼音排序"];
}
// 获取企业默认排序字段值数组
- (NSArray *)getQySortKeyArray {
    return @[@"company_level|company_grow,DESC|DESC", @"company_name_pinyin,ASC"];
}

#pragma mark - 区域筛选数据
// 区域数组
- (void)setAreaArray:(NSArray *)array {
    COMMON_AREA_ARRAY = array;
}
- (NSArray *)getAreaArray {
    return COMMON_AREA_ARRAY;
}

#pragma mark - 产品筛选类型
// 产品类别数组
- (void)setProductTypeArray:(NSArray *)array {
    PRODUCT_TYPE_ARRAY = array;
    // 存本地
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:array] forKey:HOT_PRODUCT_TYPE_SAVE_KEY];
    [userDefault synchronize];
}
- (NSArray *)getProductTypeArray {
    return PRODUCT_TYPE_ARRAY;
}
- (NSArray *)getLocalProductTypeArray {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSArray *productTypeArray = [userDefault valueForKey:HOT_PRODUCT_TYPE_SAVE_KEY];
    return productTypeArray;
}
// 子类别数组
- (void)setProductSubTypeDict:(NSDictionary *)array {
    PRODUCT_SUB_TYPE_DICT = array;
}
- (NSDictionary *)getProductSubTypeDict {
    return PRODUCT_SUB_TYPE_DICT;
}
// 产品默认排序名称数组
- (NSArray *)getProductSortTextArray {
    return @[@"默认排序", @"发布时间排序"];
}
// 产品默认排序字段值数组
- (NSArray *)getProductSortKeyArray {
    return @[@"company_level|product_clicks,DESC|DESC", @"product_modified,DESC"];
}

#pragma mark - 省份城市
// 省份类别数组
- (void)setProvTypeArray:(NSArray *)array {
    PROV_TYPE_ARRAY = array;
}
- (NSArray *)getProvTypeArray {
    return PROV_TYPE_ARRAY;
}
// 城市子类别数组
- (void)setCitySubTypeDict:(NSDictionary *)array {
    CITY_SUB_TYPE_DICT = array;
}
- (NSDictionary *)getCitySubTypeDict {
    return CITY_SUB_TYPE_DICT;
}

#pragma mark - 购物车
// 获取购物车
+ (NSMutableArray *)getShoppingCart {
    if (SHOPPING_CART_ARRAY == nil) {
        SHOPPING_CART_ARRAY = [NSMutableArray array];
    }
    return SHOPPING_CART_ARRAY;
}
// 设置购物车指定样品数据
+ (void)addSamepleToCart:(NSMutableArray *)sampleArray {
    if (SHOPPING_CART_ARRAY == nil) {
        SHOPPING_CART_ARRAY = [NSMutableArray array];
    }
    
    // 删除旧数据
    NSMutableArray *removeArray = [NSMutableArray array];
    for (NSMutableDictionary *addDict in sampleArray) {
        NSString *addMainKey = addDict[@"mainKey"];
        for (NSInteger i = 0; i < SHOPPING_CART_ARRAY.count; i++) {
            NSMutableDictionary *curDict = [SHOPPING_CART_ARRAY objectAtIndex:i];
            NSString *curMainKey = curDict[@"mainKey"];
            if ([curMainKey isEqualToString:addMainKey]) {
                [removeArray addObject:curDict];
            }
        }
    }
    while (removeArray.count > 0) {
        id first = [removeArray firstObject];
        [SHOPPING_CART_ARRAY removeObject:first];
        [removeArray removeObject:first];
    }
    
    // 添加新数据
    for (NSMutableDictionary *addDict in [sampleArray reverseObjectEnumerator].allObjects) {
        NSNumber *count = addDict[@"count"];
        if (count == nil || count.integerValue <= 0) {
            continue;
        }
        [SHOPPING_CART_ARRAY insertObject:addDict atIndex:0];
    }
    
}
// 获取购物车指定样品数据
+ (NSMutableDictionary *)getSampleFromCart:(NSString *)mainKey {
    NSMutableArray *sampleArray = [ZbSaveManager getShoppingCart];
    for (NSMutableDictionary *sampleDict in sampleArray) {
        NSString *curMainKey = sampleDict[@"mainKey"];
        if ([curMainKey isEqualToString:mainKey]) {
            return sampleDict;
        }
    }
    return nil;
}
// 清空购物车
+ (void)clearShoppingCart {
    SHOPPING_CART_ARRAY = [NSMutableArray array];
}
// 过滤数量为0数据
+ (void)fileterEmptyCountSample {
    return;
}

#pragma mark - 聊天昵称、👦管理
// 添加一组用户
+ (void)addUserInfoArray:(NSArray *)array {
    if (USER_INFO_DICT == nil) {
        USER_INFO_DICT = [NSMutableDictionary dictionary];
    }
    
    for (NSDictionary *userInfo in array) {
        [USER_INFO_DICT setValue:userInfo forKey:userInfo[@"user_name"]];
    }
}
// 添加一个用户
+ (void)addUserInfo:(NSDictionary *)dict {
    if (USER_INFO_DICT == nil) {
        USER_INFO_DICT = [NSMutableDictionary dictionary];
    }
    [USER_INFO_DICT setValue:dict forKey:dict[@"user_name"]];
}
// 获取指定用户信息
+ (NSDictionary *)getUserInfo:(NSString *)userName {
    if (USER_INFO_DICT == nil) {
        return nil;
    }
    return [USER_INFO_DICT valueForKey:userName];
}
// 是否已加载聊天用户信息
+ (BOOL)isLoadUserInfo {
    return USER_INFO_DICT != nil;
}
// 清空聊天用户信息
+ (void)clearUserInfo {
    USER_INFO_DICT = nil;
}

#pragma mark - 客服信息
+ (void)setKefuDict:(NSDictionary *)infoDict {
    KEFU_INFO_DICT = infoDict;
}
+ (NSDictionary *)getKefuDict {
    if (KEFU_INFO_DICT == nil) {
        KEFU_INFO_DICT = [NSDictionary dictionary];
    }
    return KEFU_INFO_DICT;
}

#pragma mark - 价格单位
+ (void)setPriceUnitArray:(NSArray *)array {
    PRICE_UNIT_ARRAY = array;
}
+ (NSArray *)getPriceUnitArray {
    return PRICE_UNIT_ARRAY;
}

#pragma mark - 点击的闪屏广告
+ (void)setClickAdDict:(NSDictionary *)adDict {
    CLICK_AD_DICT = adDict;
}
+ (NSDictionary *)getClickAdDict {
    return CLICK_AD_DICT;
}

#pragma mark - 点击的推送
+ (void)setClickNotification:(NSDictionary *)notify {
    CLICK_NOTIFICATION = notify;
}
+ (NSDictionary *)getClickNotification {
    return CLICK_NOTIFICATION;
}

#pragma mark - 点击的分享
+ (void)setClickShareDict:(NSDictionary *)dict {
    CLICK_SHARE_DICT = dict;
}
+ (NSDictionary *)getClickShareDict {
    return CLICK_SHARE_DICT;
}

#pragma mark - 功能开关控制
+ (void)setCtrlInfo:(NSDictionary *)ctrlInfo {
    CTRL_DICT = ctrlInfo;
}
+ (NSDictionary *)getCtrlInfo {
    return CTRL_DICT;
}
// 是否隐藏VIP
+ (BOOL)isCtrlHide {
    if (CTRL_DICT == nil) {
        return YES;
    }
    NSNumber *ctrl_hide = CTRL_DICT[@"ctrl_hide"];
    return ctrl_hide.boolValue;
}
// 是否显示样品订单
+ (BOOL)isSampleShow {
    if (CTRL_DICT == nil) {
        return NO;
    }
    NSNumber *ctrl_hide = CTRL_DICT[@"ctrl_show_sample"];
    return ctrl_hide.boolValue;
}
// 是否显示悬赏
+ (BOOL)isPaiddemandShow {
    if (CTRL_DICT == nil) {
        return NO;
    }
    NSNumber *ctrl_hide = CTRL_DICT[@"ctrl_show_paiddemand"];
    return ctrl_hide.boolValue;
}

@end
