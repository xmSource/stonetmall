//
//  LocationServiceSingleton.m
//  AgreementRide
//
//  Created by qian on 15/5/9.
//  Copyright (c) 2015年 黄旺鑫. All rights reserved.
//

#import "LocationServiceManager.h"


static LocationServiceManager *manager = nil;

@interface LocationServiceManager ()<BMKLocationServiceDelegate, BMKGeoCodeSearchDelegate>

///位置更新后执行的代码
@property (strong, nonatomic) LocationBlock locationBlock;
// 反地址编码检索
@property (strong, nonatomic) BMKGeoCodeSearch *geoSearch;
// 当前位置
@property (assign, nonatomic) BMKUserLocation *curLoc;
// 当前poi信息
@property (strong, nonatomic) BMKPoiInfo *curLocPoiInfo;

@end

@implementation LocationServiceManager

+ (instancetype)shareManager {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[LocationServiceManager alloc] init];
        [manager mapconfigure];
    });
    
    return manager;
}

- (void)mapconfigure {
    
    //设置定位精确度，默认：kCLLocationAccuracyBest
    [BMKLocationService setLocationDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
    //指定最小距离更新(米)，默认：kCLDistanceFilterNone
    [BMKLocationService setLocationDistanceFilter:100.f];
    
    //初始化定位服务
    self.locationService = [[BMKLocationService alloc] init];
    self.locationService.delegate = self;
    // 反地址检索
    self.geoSearch =[[BMKGeoCodeSearch alloc] init];
    self.geoSearch.delegate = self;
}

- (void)dealloc {
    self.locationService.delegate = nil;
}

/**
 *  打开定位服务
 */
- (void)startLocationService {
    
    [self.locationService startUserLocationService];
}

/**
 *  停止定位服务
 */
- (void)stopLocationService {
    [self.locationService stopUserLocationService];
}

/**
 *  用户更新调用此方法
 *
 *  @param locationBlock 位置更新后要执行的代码块
 */
- (void)updateLocationBlock:(LocationBlock)locationBlock {
    self.locationBlock = locationBlock;
}

// 获取当前位置经纬度
- (BMKUserLocation *)getCurLocation {
    return self.curLoc;
}

// 获取当前位置poi信息
- (BMKPoiInfo *)getCurLocationPoiInfo {
    return self.curLocPoiInfo;
}

/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation {
    
    // 不重复更新位置
    if (self.curLoc && self.curLoc.location.coordinate.longitude == userLocation.location.coordinate.longitude && self.curLoc.location.coordinate.latitude == userLocation.location.coordinate.latitude) {
        return;
    }
    self.curLoc = userLocation;
    //发起反向地理编码检索
    BMKReverseGeoCodeOption *reverseGeoCodeSearchOption = [[
                                                            BMKReverseGeoCodeOption alloc]init];
    reverseGeoCodeSearchOption.reverseGeoPoint = userLocation.location.coordinate;
    BOOL flag = [_geoSearch reverseGeoCode:reverseGeoCodeSearchOption];
    if(flag)
    {
        NSLog(@"反向地理编码检索发送成功");
    }
    else
    {
        NSLog(@"反向地理编码检索发送失败");
    }
}

#pragma mark - 反向地理编码检索结果
//接收反向地理编码结果
-(void) onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:
(BMKReverseGeoCodeResult *)result
                        errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        // 保存周围地址
        self.curLocPoiInfo = [result.poiList firstObject];
        
        // 已获取到定位信息，关闭定位
        [self stopLocationService];
        if (self.locationBlock) {
            self.locationBlock();
        }
    }
    else {
        NSLog(@"抱歉，未找到反向地理编码检索结果");
    }
}

@end
