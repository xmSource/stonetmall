//
//  ZbSaveManager.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/29.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZbSaveManager : NSObject

// 获取单例
+ (instancetype)shareManager;

// 添加全局搜索记录
- (void)addGlobalSearchHistory:(NSString *)key;
// 获取全局搜索记录
- (NSArray *)getGlobalSearchHistory;

// 添加全局石材搜索记录
- (void)addGlobalStoneSearchHistory:(NSString *)key;
// 获取全局石材搜索记录
- (NSArray *)getGlobalStoneSearchHistory;

// 添加全局产品搜索记录
- (void)addGlobalProductSearchHistory:(NSString *)key;
// 获取全局产品搜索记录
- (NSArray *)getGlobalProductSearchHistory;

// 添加全局企业搜索记录
- (void)addGlobalQySearchHistory:(NSString *)key;
// 获取全局企业搜索记录
- (NSArray *)getGlobalQySearchHistory;

// 添加全局供需搜索记录
- (void)addGlobalSAndDSearchHistory:(NSString *)key;
// 获取全局供需搜索记录
- (NSArray *)getGlobalSAndDSearchHistory;

// 设置石材类别数组
- (void)setStoneTypeArray:(NSArray *)array;
- (NSArray *)getStoneTypeArray;
- (NSArray *)getLocalStoneTypeArray;

// 设置石材颜色数组
- (void)setStoneColorArray:(NSArray *)array;
- (NSArray *)getStoneColorArray;

// 设置石材纹理数组
- (void)setStoneTextureArray:(NSArray *)array;
- (NSArray *)getStoneTextureArray;

// 设置企业服务类型数组
- (void)setQyTypeArray:(NSArray *)array;
- (NSArray *)getQyTypeArray;
- (NSArray *)getLocalQyTypeArray;

// 获取企业默认排序名称数组
- (NSArray *)getQySortTextArray;
// 获取企业默认排序字段值数组
- (NSArray *)getQySortKeyArray;

// 区域数组
- (void)setAreaArray:(NSArray *)array;
- (NSArray *)getAreaArray;

// 产品类别数组
- (void)setProductTypeArray:(NSArray *)array;
- (NSArray *)getProductTypeArray;
- (NSArray *)getLocalProductTypeArray;
// 产品子类别数组
- (void)setProductSubTypeDict:(NSDictionary *)array;
- (NSDictionary *)getProductSubTypeDict;
// 产品默认排序名称数组
- (NSArray *)getProductSortTextArray;
// 产品默认排序字段值数组
- (NSArray *)getProductSortKeyArray;

// 省份类别数组
- (void)setProvTypeArray:(NSArray *)array;
- (NSArray *)getProvTypeArray;
// 城市子类别数组
- (void)setCitySubTypeDict:(NSDictionary *)array;
- (NSDictionary *)getCitySubTypeDict;

// 获取购物车
+ (NSMutableArray *)getShoppingCart;
// 设置购物车指定样品数据
+ (void)addSamepleToCart:(NSMutableArray *)sampleArray;
// 获取购物车指定样品数据
+ (NSMutableDictionary *)getSampleFromCart:(NSString *)mainKey;
// 清空购物车
+ (void)clearShoppingCart;
// 过滤数量为0数据
+ (void)fileterEmptyCountSample;

#pragma mark - 聊天昵称、👦管理
// 添加一组用户
+ (void)addUserInfoArray:(NSArray *)array;
// 添加一个用户
+ (void)addUserInfo:(NSDictionary *)dict;
// 获取指定用户信息
+ (NSDictionary *)getUserInfo:(NSString *)userName;
// 是否已加载聊天用户信息
+ (BOOL)isLoadUserInfo;
// 清空聊天用户信息
+ (void)clearUserInfo;

#pragma mark - 客服信息
+ (void)setKefuDict:(NSDictionary *)infoDict;
+ (NSDictionary *)getKefuDict;

#pragma mark - 价格单位
+ (void)setPriceUnitArray:(NSArray *)array;
+ (NSArray *)getPriceUnitArray;

#pragma mark - 点击的闪屏广告
+ (void)setClickAdDict:(NSDictionary *)adDict;
+ (NSDictionary *)getClickAdDict;

#pragma mark - 点击的推送
+ (void)setClickNotification:(NSDictionary *)notify;
+ (NSDictionary *)getClickNotification;

#pragma mark - 点击的分享
+ (void)setClickShareDict:(NSDictionary *)dict;
+ (NSDictionary *)getClickShareDict;

#pragma mark - 功能开关控制
+ (void)setCtrlInfo:(NSDictionary *)ctrlInfo;
+ (NSDictionary *)getCtrlInfo;
// 是否隐藏VIP
+ (BOOL)isCtrlHide;
// 是否显示样品订单
+ (BOOL)isSampleShow;
// 是否显示悬赏
+ (BOOL)isPaiddemandShow;

@end
