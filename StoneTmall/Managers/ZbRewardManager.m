//
//  ZbRewardManager.m
//  StoneTmall
//
//  Created by 张斌 on 15/9/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbRewardManager.h"
#import "CommonHeader.h"
#import "ZbRewardJoinTableViewCell.h"
#import "ZbRewardApplyDelayTableViewCell.h"
#import "ZbRewardSelectTableViewCell.h"

@implementation ZbRewardManager

// 悬赏状态文字
+ (void)setStatusText:(UILabel *)lab postDict:(NSDictionary *)postDict {
    NSString *post_status = postDict[@"post_status"];
    NSString *showText = @"";
    switch (post_status.integerValue) {
        case Zb_Post_Status_Type_Pay:
        {
            // 待支付
            showText = @"待支付";
            break;
        }
        case Zb_Post_Status_Type_Delay:
        case Zb_Post_Status_Type_Join:
        {
            // 进行中
            showText = @"正在进行";
            break;
        }
        case Zb_Post_Status_Type_Select:
        {
            // 选标中
            showText = @"正在选标";
            break;
        }
        case Zb_Post_Status_Type_AnswerOk:
        case Zb_Post_Status_Type_BePublic:
        case Zb_Post_Status_Type_TimeOut:
        case Zb_Post_Status_Type_Bid:
        {
            // 选标结束
            showText = @"选标已结束";
            break;
        }
        case Zb_Post_Status_Type_Finish:
        {
            // 完成
            showText = @"悬赏已结束";
            break;
        }
        default:
            showText = @"异常状态";
            break;
    }
    lab.text = showText;
}

// 悬赏类型文字
+ (void)setTypeText:(UILabel *)lab typeBg:(UIView *)bg postDict:(NSDictionary *)postDict {
    NSString *post_status = postDict[@"post_status"];
    lab.text = post_status.intValue < Zb_Post_Status_Type_Select ? @"悬赏" : @"结束";
    bg.backgroundColor = post_status.intValue < Zb_Post_Status_Type_Select ? UIColorFromHexString(@"EF684D") : UIColorFromHexString(@"BBBBBB");
}

// 获取悬赏状态cell高度
+ (CGFloat)getStatusRowHeight:(NSDictionary *)postDict {
    NSString *post_status = postDict[@"post_status"];
    NSString *post_role = postDict[@"post_role"];
    if (post_status.intValue == Zb_Post_Status_Type_Pay) {
        // 待支付，只有悬赏人才能看到
        return [ZbRewardSelectTableViewCell getRowHeight];
    } else if (post_status.intValue == Zb_Post_Status_Type_Join || post_status.intValue == Zb_Post_Status_Type_Delay) {
        // 进行中
        
        if (post_role.intValue == Zb_Post_Role_Type_Owner) {
            // 悬赏人看到延长cell
            return [ZbRewardApplyDelayTableViewCell getRowHeight];
        } else {
            // 其他人看到参与cell
            return [ZbRewardJoinTableViewCell getRowHeight];
        }
    } else if (post_status.intValue == Zb_Post_Status_Type_Select) {
        // 选标中
        if (post_role.intValue == Zb_Post_Role_Type_Owner) {
            // 悬赏人看到延长cell
            return [ZbRewardApplyDelayTableViewCell getRowHeight];
        } else {
            // 其他人看到文字cell
            return [ZbRewardSelectTableViewCell getRowHeight];
        }
    } else {
        // 其它状态，看到描述文字
        return [ZbRewardSelectTableViewCell getRowHeight];
    }
}

// 获取悬赏状态cell
+ (UITableViewCell *)getStatusRowCell:(NSDictionary *)postDict {
    return nil;
}

// 判断悬赏是否进行中倒计时结束
+ (BOOL)isCountDownEnd:(NSDictionary *)postDict {
    // 过期时间
    NSString *post_expired = postDict[@"post_expired"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *expiredDate = [dateFormatter dateFromString:post_expired];
    // 剩余时间
    NSTimeInterval left = [expiredDate timeIntervalSinceNow];
    return left <= 0;
}

// 设置倒计时文字
+ (void)SetCountDownDate:(UILabel *)lab postDict:(NSDictionary *)postDict {
    // 过期时间
    NSString *post_expired = postDict[@"post_expired"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *expiredDate = [dateFormatter dateFromString:post_expired];
    // 剩余时间
    NSTimeInterval left = (int)MAX(0, [expiredDate timeIntervalSinceNow]);
    
    NSString *first = @"距离投标结束还有";
    [dateFormatter setDateFormat:@"d"];
    int dayCnt = left / (60 * 60 * 24);
    NSString *day = [NSString stringWithFormat:@"%d", dayCnt];
    NSString *dayText = @"天";
    int hourCnt = (fmod(left, (60 * 60 * 24))) / (60 * 60);
    NSString *hour = [NSString stringWithFormat:@"%d", hourCnt];
    NSString *hourText = @"小时";
    int minuteCnt = (fmod(left, (60 * 60))) / 60;
    NSString *minute = [NSString stringWithFormat:@"%d", minuteCnt];
    NSString *minuteText = @"分";
    // 设置文字
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@%@%@%@%@", first, day, dayText, hour, hourText, minute, minuteText]];
    // 日属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length, day.length)];
    // 时属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length + day.length + dayText.length, hour.length)];
    // 分属性
    [attriString addAttribute:NSFontAttributeName
                        value:[UIFont systemFontOfSize:18]
                        range:NSMakeRange(first.length + day.length + dayText.length + hour.length + hourText.length, minute.length)];
    lab.attributedText = attriString;
}

@end
