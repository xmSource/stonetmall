//
//  ZbRewardManager.h
//  StoneTmall
//
//  Created by 张斌 on 15/9/26.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZbRewardManager : NSObject

// 悬赏状态文字
+ (void)setStatusText:(UILabel *)lab postDict:(NSDictionary *)postDict;
// 悬赏类型文字
+ (void)setTypeText:(UILabel *)lab typeBg:(UIView *)bg postDict:(NSDictionary *)postDict;

// 获取悬赏状态cell高度
+ (CGFloat)getStatusRowHeight:(NSDictionary *)postDict;
// 获取悬赏状态cell
+ (UITableViewCell *)getStatusRowCell:(NSDictionary *)postDict;

// 判断悬赏是否进行中倒计时结束
+ (BOOL)isCountDownEnd:(NSDictionary *)postDict;
// 设置倒计时文字
+ (void)SetCountDownDate:(UILabel *)lab postDict:(NSDictionary *)postDict;

@end
